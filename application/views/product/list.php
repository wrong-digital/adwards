	    <div class="top-inner">
                <header>
				        <div>
                                <a href="<?=URL::site($lang);?>" title="">
                                        <img src="<?=URL::base();?>assets/css/img/logo-latplanta.png" alt="Latplanta logo" />
                                </a>
						</div>
						
						<nav><?=Helper_Section::public_tree(0);?></nav>
                        <script>
						$("nav ul li a").attr("style","border-bottom:none;");
						$("nav ul li:last-child a").attr("style","padding-right:0;border-bottom:none;");
						</script>
                </header>
	    </div>
		
		<div class="outter">
        		<div class="inner">
		        		<div class="left-content" style="padding: 0 0 160px 0;">
				        		<div class="title"><?=$section->parent_id > 0 ? Helper_Section::title($section->parent_id) : Helper_Section::title($section->id);?></div>
				        		<nav>
									<?=Helper_Section::public_tree($section->parent_id > 0 ? $section->parent_id : $section->id);?>
					    		</nav>

								<div class="search">
								    <form method="get" action="<?=URL::section($section->id);?>">
								    <input type="text" name="key" value="<?=l('search_product');?>" onclick="this.value='';" />
									<input type="submit" value="" />
								    </form>
								</div>
						</div>
				
						<div class="right-content">
						        <div class="head">
				                        <h1><?=isset($key) ? e($key) : e($section->title);?></h1>
								</div>
								<div class="data">

										<?
										foreach($products as $item){
											if(isset($key)){
											    if(Helper_Section::parent_id($item->section_id) == $section->parent_id){
												?>
												<div class="product">
														<div class="product-img">
																<a href="<?=URL::section($item->section_id);?>/<?=$item->link;?>.html" title="">
																		<img src="<?=URL::base();?>assets/<?=$item->image_168_148 ? 'media/images/'.$item->image_168_148 : 'css/img/empty_203.jpg';?>" alt="" />
																</a>
																<div class="product-label">
																		<img src="<?=URL::base();?>assets/css/img/label_03.png" alt="" />
																</div>
														</div>
														<div class="product-data">
																<div class="title">
																		<a href="<?=URL::section($item->section_id);?>/<?=$item->link;?>.html" title=""><span><?=$item->title;?></span></a>
																</div>
																<p><?=Text::limit_chars(strip_tags($item->description),100);?></p>
																<div class="more">
																		<a href="<?=URL::section($item->section_id);?>/<?=$item->link;?>.html" title="">Apskatīt</a>
																</div>
														</div>
												</div>
												<div class="product-line">&nbsp;</div>
												<?
												}
											}else{
												?>
												<div class="product">
														<div class="product-img">
																<a href="<?=URL::section($item->section_id);?>/<?=$item->link;?>.html" title="">
																		<img src="<?=URL::base();?>assets/<?=$item->image_168_148 ? 'media/images/'.$item->image_168_148 : 'css/img/empty_203.jpg';?>" alt="" />
																</a>
																<div class="product-label">
																		<img src="<?=URL::base();?>assets/css/img/label_03.png" alt="" />
																</div>
														</div>
														<div class="product-data">
																<div class="title">
																		<a href="<?=URL::section($item->section_id);?>/<?=$item->link;?>.html" title=""><span><?=$item->title;?></span></a>
																</div>
																<p><?=Text::limit_chars(strip_tags($item->description),100);?></p>
																<div class="more">
																		<a href="<?=URL::section($item->section_id);?>/<?=$item->link;?>.html" title="">Apskatīt</a>
																</div>
														</div>
												</div>
												<div class="product-line">&nbsp;</div>
												<?
											}
										}
										?>
										
								</div>
								<div class="footer">&nbsp;</div>
						</div>
				</div>
		</div>
		
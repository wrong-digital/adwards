	    <div class="top-inner">
                <header>
				        <div>
                                <a href="<?=URL::site($lang);?>" title="">
                                        <img src="<?=URL::base();?>assets/css/img/logo-latplanta.png" alt="Latplanta logo" />
                                </a>
						</div>
						
						<nav><?=Helper_Section::public_tree(0);?></nav>
                        <script>
						$("nav ul li a").attr("style","border-bottom:none;");
						$("nav ul li:last-child a").attr("style","padding-right:0;border-bottom:none;");
						</script>
                </header>
	    </div>
		
		<div class="outter">
        		<div class="inner">
		        		<div class="left-content" style="padding: 0 0 160px 0;">
				        		<div class="title"><?=$section->parent_id > 0 ? Helper_Section::title($section->parent_id) : Helper_Section::title($section->id);?></div>
				        		<nav>
									<?=Helper_Section::public_tree($section->parent_id > 0 ? $section->parent_id : $section->id);?>
					    		</nav>
								
								<div class="search">
								    <form method="get" action="<?=URL::section($section->id);?>">
								    <input type="text" name="key" value="<?=l('search_product');?>" onclick="this.value='';" />
									<input type="submit" value="" />
								    </form>
								</div>
						</div>

						<div class="right-content" style="background:url(<?=URL::base();?>assets/css/img/bg-product.jpg) no-repeat top;">
						        <div class="product-nav">
								    <?if($prev){?><div class="nav-left"><a href="<?=URL::section($section->id);?>/<?=$prev;?>.html" title=""><img src="<?=URL::base();?>assets/css/img/navi-left.png" alt="" /> <?=l('prev');?></a></div><?}?>
								    <?if($next){?><div class="nav-right"><a href="<?=URL::section($section->id);?>/<?=$next;?>.html" title=""><?=l('next');?> <img src="<?=URL::base();?>assets/css/img/navi-right.png" alt="" /></a></div><?}?>
								</div>
								<div class="product-item-data">
								        <div class="image">
										    <img src="<?=URL::base();?>assets/<?=$item['image_285_338'] ? 'media/images/'.$item['image_285_338'] : 'css/img/empty_285.jpg';?>" alt="" />
											<div class="product-label">
											        <img src="<?=URL::base();?>assets/css/img/label_03.png" alt="" />
											</div>
										</div>
										<div class="item-data">
										    <div class="title">
												<a href="<?=URL::section($section->id);?>/<?=$item['link'];?>.html" title=""><span><?=$item['title'];?></span></a>
											</div>
											<?=$item['description'];?>
										</div>
								</div>
								<div class="back-btn">
								    <a href="<?=URL::section($section->id);?>" title=""><?=l('to_catalogue');?></a>
								</div>
								<div class="product-types">
								    <div class="head"><?=l('types');?></div>
									
									<div class="row"<?=(count($item['types']) <= 6 ? ' style="background:none;"' : '');?>>
									    <?
										$nr = 1;
										$selected = explode(',', $item['types']);
										foreach($types as $item){
										    if(in_array($item->id, $selected)){
											?>
											<div class="item">
												<div class="ico"><img src="<?=URL::base();?>assets/media/images/<?=$item->image;?>" alt="" /></div>
												<div class="weight"><img src="<?=URL::base();?>assets/css/img/ico-weight.png" alt="" /> <?=$item->weight;?></div>
												<div class="t"><?=$item->title;?></div>
											</div>
											<?
											$nr++;
												if($nr == 7 ){
												    $nr == 1;
											?>
									</div>
									<div class="row">
											<?php
												}
											}
										}
										?>
									</div>
									<div class="bottom">&nbsp;</div>
								</div>
								<div class="footer">&nbsp;</div>
						</div>
				</div>
		</div>
		
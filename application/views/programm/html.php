				<div class="page">
				    <div class="leftside">
						<ul><?=Helper_Section::public_tree($section->parent_id == 0 ? $section->id : $section->parent_id);?></ul>
						<p>&nbsp;</p>
					</div>
					<div class="page-content-empty">
					    <div class="h">
						    <h1><?=$section->title;?></h1>
							<a style="display:none;" href="<?=l('programm_print');?>" title="<?=l('print');?>" target="_blank"><?=l('print');?></a>
						</div>
						<div class="sections">
						    <ul>
								<?$n=1;foreach($categories as $i){?>
							    <li>
							        <a href="<?=URL::section($section->id);?>/<?=$i->link;?>.html" title="<?=$i->title;?>" class="<?=!isset($item) && $n == 1 ? 'ac' : (isset($item) && $item['id'] == $i->id ? 'ac' : '');?>"><?=$i->title;?></a>
								</li>
								<?$n++;}?>
							</ul>
						</div>
						<div class="c">
														
							
											
							<p>Piedalīties "Latvijas institūta" konferencē un diskusijā ir aicināti valsts pārvaldes un lielāko eksporta uzņēmumu komunikatori. Plašāka informācija un pieteikšanās - <a href="http://www.li.lv" target="_blank" title="">www.li.lv</a></p>
							
							<div class="programm">
							
								<h1><span>Konferenču centrā "Citadele" (Republikas laukums 2A) 12.aprīlis plkst 9:00<span></h1>
								
								<h1>Meistarība Mūsdienu komunikācijā</h1>
							
								<div class="line-blue">
									<div class="col1">8:40 – 9:00</div>
								    <div class="col2">Dalībnieku reģistrācija</div>
								    <div class="col3"></div>
								</div>
								<div class="line-white">
									<div class="col1">9:00 – 9:10</div>
								    <div class="col2">Latvijas institūta direktores Karinas Pētersones ievadvārdi</div>
								    <div class="col3"></div>
								</div>
								<div class="line-blue">
									<div class="col1">9:10 – 9:15</div>
								    <div class="col2">!MOOZ radošā direktora Ērika Stendzenieka ievadvārdi</div>
								    <div class="col3"></div>
								</div>
								
								<h1><span>Pirmā daļa:</span> "Meistari: Teorija"</h1>
								
								<div class="line-blue">
									<div class="col1">9:20-9:35</div>
								    <div class="col2">Sabiedriskās attiecības</div>
								    <div class="col3"><strong>Ralfs Vīlands</strong>, valdes priekšsēdētājs, Hill and Knowlton Latvia</div>
								</div>
								<div class="line-white">
									<div class="col1">9:35-9:50</div>
								    <div class="col2">Krīzes komunikācija</div>
								    <div class="col3"><strong>Maija Celmiņa</strong>, komunikāciju eksperte, premjera ārštata padomniece</div>
								</div>
								<div class="line-blue">
									<div class="col1">9:50-10:05</div>
								    <div class="col2">Korporatīvā komunikācija</div>
								    <div class="col3"><strong>Kerli Gabriloviča</strong>, komercdirektore, Lattelecom</div>
								</div>
								<div class="line-white">
									<div class="col1">10:05-10:20</div>
								    <div class="col2">Multimediju projektu vadība</div>
								    <div class="col3"><strong>Dāvids Mitrēvics</strong>, direktors, Dd Studio</div>
								</div>
								<div class="line-blue">
									<div class="col1">10:20-10:30</div>
								    <div class="col2">Jautājumi, atbildes</div>
								    <div class="col3"></div>
								</div>
								<div class="line-white">
									<div class="col1">10:30-10:45</div>
								    <div class="col2">Kafijas pauze</div>
								    <div class="col3"></div>
								</div>
								
								<h1><span>Otrā daļa:</span> "Meistari: prakse"</h1>
								
								<div class="line-blue">
									<div class="col1">10:45-11:00</div>
								    <div class="col2">Tirgzinības/zīmolvedība</div>
								    <div class="col3"><strong>Zigurds Zaķis</strong>, komunikācijas konsultants</div>
								</div>
								<div class="line-white">
									<div class="col1">11:00-11:15</div>
								    <div class="col2">Interaktīvas spēles, aplikācijas</div>
								    <div class="col3"><strong>Mārtiņš Dambis</strong>, direktors, Cube Media</div>
								</div>
								<div class="line-blue">
									<div class="col1">11:15-11:30</div>
								    <div class="col2">Video</div>
								    <div class="col3"><strong>Reinis Traidās</strong>, ārštata operators</div>
								</div>
								<div class="line-white">
									<div class="col1">11:30-11:45</div>
								    <div class="col2">Dizains, mājas lapas</div>
								    <div class="col3"><strong>Krišjānis Jukumsons-Jukumnieks</strong>, izpilddirektors, Asketic</div>
								</div>
								<div class="line-blue">
									<div class="col1">11:45-12:00</div>
								    <div class="col2">Sociālie mediji</div>
								    <div class="col3"><strong>Ansis Egle</strong>, Leo Burnett Riga, direktors</div>
								</div>
								<div class="line-white">
									<div class="col1">12:00-12:10</div>
								    <div class="col2">Jautājumi, atbildes</div>
								    <div class="col3"></div>
								</div>
								<div class="line-blue">
									<div class="col1">12:10-12:25</div>
								    <div class="col2">Kafijas pauze</div>
								    <div class="col3"></div>
								</div>
								
								<h1><span>Trešā daļa:</span> Trešā daļa:  Paneļdiskusija "Meistarība valsts komunikācijā"</h1>
								
								<div class="line-blue">
									<div class="col1">12:25-13:25</div>
								    <div class="col2" style="width:80px;">Moderators:<br/>Dalībnieki:</div>
								    <div class="col3" style="width:375px;">
										Maija Celmiņa, komunikāciju eksperte, premjera ārštata padomniece<br/>
										<strong>Komunikācija valsts pārvaldē: Laine Kučinska</strong>, Valsts Kancelejas Komunikācijas departamenta vadītāja<br/>
										<strong>Komunikācija, veicinot Latvijas biznesa nozares atpazīstamību ārpus Latvijas: Uģis Magonis</strong>, VAS "Latvijas dzelzceļš" prezidents<br/>
										<strong>Komunikācija tūrisma jomā: Inese Šīrava</strong>, Tūrisma attīstības valsts aģentūra stratēģiskās plānošanas speciāliste<br/>
										<strong>Komunikācija, veicinot Latvijas eksportspēju: Ilga Kikjauka</strong>, Latvijas Investīciju attīstības aģentūras Klientu apkalpošanas nodaļas vadītāja<br/>
										<strong>Komunikācija, veicinot Latvijas atpazīstamību: Rihards Kalniņš</strong>, LI sabiedrisko attiecību speciālists
									</div>
								</div>
								<div class="line-white">
									<div class="col1">13:25-13:45</div>
								    <div class="col2">Diskusija</div>
								    <div class="col3"></div>
								</div>
								
							</div>
														
						</div>
					</div>
				</div>
					
				<script>
				$(function() {
					var m = $(".leftside ul").height();
					var h = $(".page-content-empty").height();
					var n = h - m - 4;
					$(".leftside p").height(1460);
				});
				</script>
				
				<?if(isset($status) && $status == 'ok'){?>
				<script>
				$(function() {
					site.popup.show('Paldies, pieteikšanās pasākumam ir veiksmīgi pabeigta! Esam nosūtījuši Jums apstiprinājumu uz e-pastu. Ja esat atzīmējis, ka vēlaties saņemt rēķinu, to nosūtīsim uz jūsu norādīto pasta adresi.');
				});
				</script>
				<?}?>
				
				<?if(isset($status) && $status == 'failed'){?>
				<script>
				$(function() {
					site.popup.show('Kļūda maksājuma veikšanā! Pārbaudiet datus un mēģiniet vēlreiz!');
				});
				</script>
				<?}?>
			</div>
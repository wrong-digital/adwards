 <?php
    if(count($lecturers)) { ?>
        <div id="section-lecturers" class="adwards-section" style="min-height:670px;background:url(<?=URL::base().'assets/media/images/2005_ADW16_2_2.jpg';?>) no-repeat center center fixed; background-size: cover;">
            <h2>Lektori</h2>
        </div>
        <div class="adwards-section-wrapper">
            <div class="adwards-section-content">
                <div class="jury section-wide">
                    <?foreach($lecturers as $item){?>
                        <div class="item" style="<?=++$i%4==0?'margin-right:0;':'';?>background:url(<?=URL::base().'assets/media/images/'.$item->image_214_299;?>) no-repeat;">
                            <a href="<?=URL::site('lv/programma/lektori').'/'.$item->link;?>" class="jury-link">
                                <div class="over" data-num="<?=$i;?>" data-id="<?=$item->id;?>">
                                    <h3><?=$item->title;?></h3>
                                    <h4><?=$item->job;?></h4>
                                </div></a>
                        </div>
                    <?}?>
                </div>
            </div>
        </div>
    <?php } ?>


    <div id="section-program" class="adwards-section" style="min-height:670px;background:url(<?=URL::base().'assets/media/images/2005_ADW16_2_3.jpg';?>) no-repeat center center fixed; background-size: cover;">
        <h2>Programma</h2>
    </div>
 <div class="page">
     <div class="pr_container">

        <?

                $curDate = null;
		foreach($plist as $itm){
			if($itm->start_date != $curDate) {
                            if($curDate == null) {
                                ?><div class="datezone"><div class="pr_line"></div><?
                            }
                            else {
                                ?></div><div class="datezone"><div class="pr_line"></div><?
                            }
                            $curDate = $itm->start_date; 
                            $dateparts = explode("-",$curDate);
                            $showDate = $dateparts[2].'/'.$dateparts[1];
                        }
			if($itm->intermedia !== ""){
			?>

			<div class='pr_prepend_container'>
				<?=$itm->intermedia;?>
			</div>
			<?
			}
			?>
		
		<div class="pr_item">
			<?
			if($itm->intermedia == ""){
			?>
			
			<?
			} else {
			?>
			<div class="pr_line_zero"></div>
			<?
			} 
			
			if($showDate != null) {?>
                            <div class="pr_time">
                                    <div class="pr_time_text">
                                            <? 
                                               echo $showDate;
                                               $showDate = null;
                                            ?>
                                    </div>
                            </div>
			<?}?>
                        
			<div class="pr_pic">
				<div style='width:138px;height:138px;background-image:url("/assets/media/images/<?=$itm->img;?>");position:absolute;background-repeat:no-repeat;background-size:cover;'>
				</div>
				<div class="pr_pic_texts">
					<h1 class="pr_item_title"><?= $itm->title;?></h1>
					<div class="pr_item_subtitle"><?=$itm->subtitle;?></div>
                                        <div class="pr_item_time"><?=$itm->laiks;?></div>
				</div>
			</div>
            <?
            
            if($itm->mailto !== ""){?>
                <div class="pr_time_text pr_time_text_offs"<?if(strlen($itm->text) < 1) echo ' style="margin-bottom: 0;"'; ?>>
                       <a class="pr_mailto" href="mailto:<?=$itm->mailto;?>?Subject=<?=$itm->subject;?>"><?=$itm->mailtotext;?></a>
                </div>
            <?}?>	                        
            <div class="pr_item_text"<?if(strlen($itm->text) > 0 && $itm->mailto == "") echo ' style="padding-bottom: 30px;"'; ?>>
                <?=$itm->text;?>
                
            </div>
		</div>
                    <?if($itm->appendix !== ""){
                        $appended = true;
                    ?>
                            </div>
                            <div class="pr_line_zero"></div>
                            <div class='pr_append_container'>
                                    <?=$itm->appendix;?>
                            </div>
                    <?
                        }
                    
                    
                    }
                    if(!isset($appended)) {?>
                </div>
                    <?}?>
		<div class="pr_line"></div>
		
	</div>
</div>
<script>
    var dateZones = [];
    var activeZone = false;
    var scrollPos = 0;

    var juryScrollPos = 0;
    var juryView = false;

    function Datezone(object)
    {
        this.height = object.height();
        this.top = object.offset().top;
        this.bottom = this.top + this.height;
        this.fixBox = object.find('.pr_time');
        this.setFixed = function() {
            this.fixBox.css({'position': 'fixed', 'top': '0px', 'padding-top': '40px'});
        };
        this.setNormal = function() {
            this.fixBox.css({'position': 'absolute', 'top': 'initial', 'padding-top': '40px'});
        };
        this.setBottom = function() {
            this.fixBox.css({'position': 'absolute', 'top': 'initial', 'padding-top': (this.height - 120)+'px'});
        };
        this.isInside = function(winTop) {
            if(winTop > this.top && winTop < (this.bottom - 160)) {
                return true;
            }
            else {
                return false;
            }
        };
        this.setActive = function(direction) {
            activeZone = this;
            activeZone.setFixed();
        };
    }
    function scrollHandle() {
            var winTop = $(window).scrollTop();
            if(winTop > scrollPos) {
                var direction = 'down';                       
            }
            else {
                var direction = 'up';
            }            
            if(activeZone && !activeZone.isInside(winTop)) {
                switch(direction) {
                    case 'down':
                        activeZone.setBottom();
                        break;
                    case 'up':
                        activeZone.setNormal();
                        break;
                }
                activeZone = false;
            }
            else {
                for(i in dateZones) {
                    if(dateZones[i].isInside(winTop)) {
                        dateZones[i].setActive(direction);
                    }
                }
            }
            scrollPos = winTop;
    }
    $(function(){
        $('.datezone').each(function() {
            dateZones.push(new Datezone($(this)));
        });
        var iOS = /(iPad|iPhone|iPod)/g.test( navigator.userAgent );
        if(iOS) {
            $(window).bind('touchstart',scrollHandle);
            $(window).bind('touchmove',scrollHandle);
            $(window).bind('touchend',scrollHandle);
            $(window).bind('touchcancel',scrollHandle);

            $('.adwards-section').each(function() {
                var x = (1600 - $(window).width())/2;
                $(this).css({
                    'height': '670px',
                    'width': '100%',
                    'background-size': '1600px 670px',
                    'background-position': -x+'px '+$(this).offset().top+'px'
                });
            });
        }
        else {
            $(window).scroll(scrollHandle);
        }



        function topVisible(elem)
        {
            var docViewTop = $(window).scrollTop();
            var elemTop = $(elem).offset().top;
            return (docViewTop > (elemTop - (2 * $(window).height())));
        }
        function bottomVisible(elem)
        {
            var docViewTop = $(window).scrollTop();
            var elemBottom = $(elem).offset().top + $(elem).height();
            return (docViewTop < (elemBottom + ($(window).height())));
        }
        function visible(elem)
        {
            var docViewTop = $(window).scrollTop();
            var docViewBottom = docViewTop + $(window).height();

            var elemTop = $(elem).offset().top;
            var elemBottom = elemTop + $(elem).height();

            return (((docViewBottom > elemTop) && (docViewTop < elemTop)) || ((docViewTop < elemBottom) && (docViewBottom > elemBottom)));

        }


        $('a.jury-link').click(function(event) {
            event.preventDefault();
        });
        // 'div#section-'+sections[i])[0]
        $(window).scroll(function(event) {

            var st = $(this).scrollTop();
            var scrolldif = st - juryScrollPos;
            var height = $('.jury-popup').height();
            var top = $('.jury-popup').offset().top;
            var bottom = top + height;
            var bdif = st - (bottom-500);
            var tdif = -(st - (top-100));
            var cur = parseInt($('.jury-left, .jury-right').css('top'));


            if(juryView === true && juryScrollPos > 0) {
                if(st > juryScrollPos) {
                    if(bdif > 0) {
                        $('.jury-left, .jury-right').css({'top': (320-bdif)+'px'});
                    }
                    else if(tdif > 0) {
                        $('.jury-left, .jury-right').css({'top': (320+tdif)+'px'});
                    }
                    else {
                        $('.jury-left, .jury-right').css({'top': '320px'});
                    }
                    if(!bottomVisible('.jury-popup')) {
                        $('.overlay, .jury-popup, .jury-left, .jury-right').fadeOut(150,function() {
                            $('.jury-left, .jury-right').css({'top': '320px'});
                        });
                        juryView = false;
                    }
                } //down
                else if(st < juryScrollPos){
                    if(tdif > 0) {
                        $('.jury-left, .jury-right').css({'top': (cur-scrolldif)+'px'});
                    }
                    else if(bdif > 0 ) {
                        $('.jury-left, .jury-right').css({'top': (cur-scrolldif)+'px'});
                    }
                    else {
                        $('.jury-left, .jury-right').css({'top': '320px'});
                    }
                    if(!topVisible('.jury-popup')) {
                        $('.overlay, .jury-popup, .jury-left, .jury-right').fadeOut(150,function() {
                            $('.jury-left, .jury-right').css({'top': '320px'});
                        });
                        juryView = false;
                    }
                } //up
            }
            juryScrollPos = st;
        });

        function juryPositioning() {
            $('.jury-popup').offset({
                top: $(window).scrollTop()
            });
        }

        $('.jury-left, .jury-right').click(function(){
            $('.jury-popup .c').fadeOut(150);
            //$('.jury-right').fadeOut(75);
            //$('.jury-left').fadeOut(75);
            var current = $('[data-id='+$('input[name=selected_jury]').val()+']');
            if($(this).attr('class') == 'jury-left'){
                var prev = current.closest('.item').prev('.item');
                if(prev.length > 0) {
                    var obj = prev.find('.over');
                }
                else {
                    var obj = $('.item').filter(':last').find('.over');
                }
                //var obj = current.closest('.item').prev('.item').find('.over');

            }else{
                var next = current.closest('.item').next('.item');
                if(next.length > 0) {
                    var obj = next.find('.over');
                }
                else {
                    var obj = $('.item').filter(':first').find('.over');
                }
                //var obj = next.find('.over');

            }

            var id = obj.data('id');
            var num = obj.data('num');

            $('input[name=selected_jury]').val(id);

            $.post('<?=URL::section(92);?>', {id:id}, function(data){
                $('.jury-popup h2').html(data.name);
                $('.jury-popup h3').html(data.job);
                $('.jury-popup .about div div').html(data.text);
                $('.jury-popup .image').html(data.image);
                $('input[name=selected_jury]').val(data.id);
                $('.jury-popup .c').fadeIn(150);

            },'json');
        });

        $('.over').click(function(){
            var id = $(this).data('id');
            var num = $(this).data('num');
            juryView = true;
            $('.jury-left').fadeIn();
            $('.jury-right').fadeIn();

            $('input[name=selected_jury]').val(id);

            $.post('<?=URL::section(92);?>', {id:id}, function(data){
                $('.jury-popup h2').html(data.name);
                $('.jury-popup h3').html(data.job);
                $('.jury-popup .about div div').html(data.text);
                $('.jury-popup .image').html(data.image);
                $('.overlay, .jury-popup').fadeIn(function(){

                });

            },'json');
            juryPositioning();
        });
        $('.jury-popup .close a, .overlay').click(function(){
            $('.overlay, .jury-popup, .jury-left, .jury-right').fadeOut(150,function() {
                $('.jury-left, .jury-right').css({'top': '320px'});
            });
            juryView = false;
        });
        if(typeof initialMember !== 'undefined')
        {
            $('div.over[data-id="'+initialMember+'"]').click();
        }


    });

    jQuery(".adwards-section h2").fitText();
</script>
			
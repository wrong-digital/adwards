<div class="page" style="padding-bottom:18px;">
    
    <?
    $n = 1;
    $cur = 0;
    foreach($categories as $cat) {
        if(count($cat['winners']) > 0) {?>
            <div class='section awards-section'>
                <h1 class='section-title'><?=$n . '. ' . $cat['name'];?></h1>
                <div class='jobs'><?
                    foreach($cat['winners'] as $winner) {
                        if($cur == 0) {?>
                            <div class='work-row'>
                        <?}
                        $cur++;?>
                                <div class='item award-itm' id='item-<?=$winner->id;?>'>
                                    <p class='image'>
                                        <a href='javascript:;' title='' class='overlaid-img' data-id='<?=$winner->id;?>'><?
                                        if($winner->thumbnail == null) {
                                            $winner->thumbnail = 'black_thumb.jpg';
                                        }?>
                                            <img src='/assets/media/images/<?=$winner->thumbnail;?>' alt="" width="308px" height="163px"/>
                                            <span class='img-overlay'>
                                                <span class='award-title ttl-centered'><?=$winner->sub_category;?></span>
                                            </span>
                                        </a>
                                    </p>
                                    <p class='title'>
                                        <a href='javascript:;' title='' data-id='<?=$winner->id;?>'><?=$winner->title;?></a>
                                    </p>
                                    <p class='involved-ppl'>
                                        <span class='role'>Klients: </span><?=$winner->client;?><br>
                                        <span class='role'>Iesniedzējs: </span><?=$winner->author;?>
                                    </p>
                                </div>
                                
                    <?if($cur == 3) {
                        ?></div><?
                        $cur = 0;
                        }
                    }
                    if($cur > 0 && $cur < 3) {
                        ?></div><?
                        $cur = 0;
                    }?>    
                </div>
            </div>
                <?
        $n++;
        }
    }
?>
</div>
<script>
    var year = <?=$year;?>;
    var opened = <?=(isset($_GET['item']) ? $_GET['item'] : 0);?>;
    var urlbase	= '<?=URL::base();?>';
    var urlbasefull = '<?=$_SERVER['HTTP_HOST'];?>';
    var aTHUMBS = new Array();
    var fullscreenState                             = false;
    
    var gallery = {
            media: [],
            active: null,
            total: 0,
            open: function(from) {
                    this.active = from;
                    for(i in this.media) {
                        $('.gallery-popup .pic-gallery').append(this.media[i]);
                        $('.gallery-popup .dots').append('<div class="pic-dot" data-id='+i+'></div>');
                        this.total++;
                    }
                    this.setDots();
                    this.setActive(from);
                    $('.gallery-popup').fadeIn(150);
                    $('video').each(function() {
                        this.pause();
                    });
            },
            close: function() {
                $('.gallery-popup .pic-gallery').empty();
                $('.gallery-popup .dots').empty();
                this.media = [];
                this.total = 0;
                this.active = null;
                $('.gallery-popup').fadeOut(150);
                $('video').each(function() {
                    this.pause();
                });
            },
            hide: function() {
                $('.gallery-popup').fadeOut(150);
                $('video').each(function() {
                    this.pause();
                });
            },
            show: function(at) {
                if(typeof at != 'undefined') {
                    this.setActive(at);
                }
                $('.gallery-popup').fadeIn(150);
            },
            setActive: function(id) {
                $('.fulldot').removeClass('fulldot');
                $('.pic-dot[data-id='+id+']').addClass('fulldot');
                $('.gallery-pic').css('display','none');
                $('.gallery-pic[data-id='+id+']').css('display','block');
            },
            goRight: function() {
                if(this.active == (this.total - 1)) {
                    this.active = 0;
                }
                else {
                    this.active++;
                }
                this.setActive(this.active);
                $('video').each(function() {
                    this.pause();
                });
            },
            goLeft: function() {
                if(this.active == 0) {
                    this.active = (this.total - 1);
                }
                else {
                    this.active--;
                }
                this.setActive(this.active);
                $('video').each(function() {
                    this.pause();
                });
            },
            setDots: function() {
                $(window).on('click','.pic-dot',function() {
                    var id = $(this).data('id');
                    if(gallery.active != id) {
                        gallery.active = id;
                        gallery.setActive(id);
                    }
                });
            }
            
        };
    
    function popupPosition() {
        $('.voting-popup').css({
            top: $(window).scrollTop(),
            'position': 'absolute'
        });
    }
    function topVisible(elem)
    {
        var docViewTop = $(window).scrollTop();
        var elemTop = $(elem).offset().top;
        return (docViewTop > (elemTop - (2 * $(window).height())));
    }
    function bottomVisible(elem)
    {
        var docViewTop = $(window).scrollTop();
        var elemBottom = $(elem).offset().top + $(elem).height();
        return (docViewTop < (elemBottom + ($(window).height())));
    }
    var scrollPos = 0;
    function scrollHandle() {
            var winTop = $(window).scrollTop();
            if(winTop > scrollPos) {
                var direction = 'down';                       
            }
            else {
                var direction = 'up';
            }            
            var scrolldif = winTop - scrollPos;
            scrollPos = winTop;
            if($('.voting-popup').is(':visible')) {
                        var height = $('.voting-popup').height();
                        var top = $('.voting-popup').offset().top;
                        var bottom = top + height;
                        var bdif = scrollPos - (bottom-500);
                        var tdif = -(scrollPos - (top-100));
                        var cur = parseInt($('.navi-left, .navi-right').css('top'));
                        //console.log(tdif);
                        switch(direction) {
                            case 'down':
                                if(bdif > 0) {
                                    $('.navi-left, .navi-right').css({'top': (320-bdif)+'px'});
                                }
                                else if(tdif > 0) {
                                    $('.navi-left, .navi-right').css({'top': (320+tdif)+'px'});
                                }
                                else {
                                    $('.navi-left, .navi-right').css({'top': '320px'});                                    
                                }
                                if(!bottomVisible('.voting-popup')  && !fullscreenState) {
                                    /*
                                $('.overlay, .voting-popup').fadeOut(150);
                                $('.media').empty();
                                gallery.close();
                                */
                                    reset_path();
                                }
                                break;
                            case 'up':
                                if(tdif > 0) {
                                    $('.navi-left, .navi-right').css({'top': (cur-scrolldif)+'px'});
                                }
                                else if(bdif > 0 ) {
                                    $('.navi-left, .navi-right').css({'top': (cur-scrolldif)+'px'});
                                }
                                else {
                                    $('.navi-left, .navi-right').css({'top': '320px'});  
                                }
                                if(!topVisible('.voting-popup')  && !fullscreenState) {
                                    /*
                                $('.overlay, .voting-popup').fadeOut(150);
                                $('.media').empty();
                                gallery.close();
                                    */
                                   reset_path();
                                }
                                break;
                        }
                    }          
    }
            function m_pushState(url){
		var stateObj				= { name: "gipsugabrika" };
                if (history.replaceState) {
                        window.history.replaceState(stateObj, "gipsugabrika", url);
                } else{
			var szurl			= url.replace(urlbase,"");
			szurl.replace('#',"");
                        window.location.hash		= szurl;
                }
        }
        
        function open(id) {
            aTHUMBS = new Array();
            m_pushState('/lv/shortlist/?item='+id);
            $('.pop01 .media').empty();
            $('.pop01 .title').empty();
            $('.pop01 .involved-ppl').empty();
            $('.pop01 .bottom').empty();
            $('.pop01 .media').prepend('<img src="/assets/css/img/ajax-loader.gif" alt="Loading..." class="ajax-loader"></img>');
            $('.overlay, .container').fadeIn(350,function() { $('.pop01').fadeIn(); });
            $.ajax({ url: 'uzvaretaji/?id='+id+'&all=true' }).done(function ( data ) {
                if(data.type == 'failed') {
                    reset_path();
                }
                else {
                    popupPosition();
                    var base = '<?=URL::base();?>';
                    switch(data.type) {
                        case 'video':
                            //var media = '<video width="818" height="428" controls><source src="'+base+data.url+'" type="video/'+data.ext+'"></video>';
                            var media = '<video width="818" height="428" src="'+base+data.url+'" controls><p>Jūsu pārlūks neatbalsta HTML5 video</p></video>';
                            break;
                        case 'audio':
                            //var media = '<audio controls><source src="'+base+data.url+'" type="audio/'+data.ext+'"></audio>';
                            var media = '<object id="video" width="818px" height="428px" type="application/x-shockwave-flash" data="/assets/js/mediaplayer-5.8/player.swf" bgcolor="#000000" name="video" tabindex="0"><param name="allowfullscreen" value="true"><param name="allowscriptaccess" value="always"><param name="seamlesstabbing" value="true"><param name="wmode" value="opaque"><param name="flashvars" value="file='+base+data.url+'&image=&autostart=false&controlbar.position=over"></object>';
                            break;
                        case 'picture':
                            var media = '<img src="'+base+'assets/media/images/'+data.url+'" alt="workpic" height=428>';
                            break;
                    }
                    $('.pop01 .media').append(media);
                    $('.pop01 .title').text(data.title);
                    $('.pop01 .involved-ppl').append("<span class='role'>Klients: </span>"+data.client+"<br><span class='role'>Iesniedzējs: </span>"+data.author);
                    
                    var all = data.allmedia;
                    for(var i = 0; i < 6; i++){
				if(all.pictures[i] != ""){
					aTHUMBS.push({
						_type:"image",
						_spec_a:all.pictures[i]
					});
				}
                    }
                    if(all.video != ""){
				aTHUMBS.push({
					_type:"video",
					_spec_a:all.video
				});
			}
                        if(all.video_2 != ""){
				aTHUMBS.push({
					_type:"video",
					_spec_a:all.video_2
				});
			}
                        if(all.video_3 != ""){
				aTHUMBS.push({
					_type:"video",
					_spec_a:all.video_3
				});
			}
                        if(all.video_4 != ""){
				aTHUMBS.push({
					_type:"video",
					_spec_a:all.video_4
				});
			}
                        if(all.audio != ""){
				aTHUMBS.push({
					_type:"audio",
					_spec_a:all.audio
				});
			}
                                                var socpic = false;
                        var base = '<?=URL::base();?>assets/upload/jobs/';
			for(var i = 0; i < aTHUMBS.length; i++){
                            switch(aTHUMBS[i]._type) {
                                case 'image':
                                    var media = '<img src="'+base+'resize/img/'+aTHUMBS[i]._spec_a+'" alt="workpic">';
                                    var bigMedia = media;
                                    if(!socpic) {
                                        socpic = '<?=URL::base('http');?>'+'assets/upload/jobs/'+'resize/img/'+aTHUMBS[i]._spec_a;
                                    }
                                    break;
                                case 'audio':
                                    var media = '<object id="video" width="393px" height="206px" type="application/x-shockwave-flash" data="/assets/js/mediaplayer-5.8/player.swf" bgcolor="#000000" name="video" tabindex="0"><param name="allowfullscreen" value="true"><param name="allowscriptaccess" value="always"><param name="seamlesstabbing" value="true"><param name="wmode" value="opaque"><param name="flashvars" value="file='+base+aTHUMBS[i]._spec_a+'&image=&autostart=false&controlbar.position=over"></object>';
                                    var bigMedia = '<object id="video" width="818px" height="428px" type="application/x-shockwave-flash" data="/assets/js/mediaplayer-5.8/player.swf" bgcolor="#000000" name="video" tabindex="0"><param name="allowfullscreen" value="true"><param name="allowscriptaccess" value="always"><param name="seamlesstabbing" value="true"><param name="wmode" value="opaque"><param name="flashvars" value="file='+base+aTHUMBS[i]._spec_a+'&image=&autostart=false&controlbar.position=over"></object>';
                                    break;
                                case 'video':
                                    var media = '<video width="393" height="206" src="'+base+aTHUMBS[i]._spec_a+'" controls><p>Jūsu pārlūks neatbalsta HTML5 video</p></video>';
                                    var bigMedia = '<video width="818" height="428" src="'+base+aTHUMBS[i]._spec_a+'" controls><p>Jūsu pārlūks neatbalsta HTML5 video</p></video>';
                                    //var media = '<video id="video" width="393" height="206" controls="controls"><source src="'+base+aTHUMBS[i]._spec_a+'"></video>';
                                    //var bigMedia = '<video id="video" width="818" height="428" controls="controls"><source src="'+base+aTHUMBS[i]._spec_a+'"></video>';
                                    //var media = '<object id="video" width="393px" height="206px" type="application/x-shockwave-flash" data="/assets/js/mediaplayer-5.8/player.swf" bgcolor="#000000" name="video" tabindex="0"><param name="allowfullscreen" value="true"><param name="allowscriptaccess" value="always"><param name="seamlesstabbing" value="true"><param name="wmode" value="opaque"><param name="flashvars" value="file='+base+aTHUMBS[i]._spec_a+'&image=&autostart=false&controlbar.position=over"></object>';
                                    //var bigMedia = '<object id="video" width="818px" height="428px" type="application/x-shockwave-flash" data="/assets/js/mediaplayer-5.8/player.swf" bgcolor="#000000" name="video" tabindex="0"><param name="allowfullscreen" value="true"><param name="allowscriptaccess" value="always"><param name="seamlesstabbing" value="true"><param name="wmode" value="opaque"><param name="flashvars" value="file='+base+aTHUMBS[i]._spec_a+'&image=&autostart=false&controlbar.position=over"></object>';
                                    break;
                            }
                            $('.pop01 .bottom').append('<div class="work-media" data-id="'+i+'">'+media+'</div>');
                            gallery.media.push('<div class="gallery-pic" data-id="'+i+'">'+bigMedia+'</div>');
                            //$('.pop_dotarray').append('<li><a class="popclicker" id="pop_dot'+i+'" href="javascript:;" title=""></a></li>');
			}
                        $('.work-media').click(function() {
                            var id = $(this).data('id');
                            if(gallery.active == null) {
                                gallery.open(id);
                            }
                            else {
                                gallery.show(id);
                            }
                        });
                        $('.ajax-loader').remove();
                        //socialHandler.load(aJSON[nCurrCatID].title_lv,aJSON[nCurrCatID].description_lv,socpic);
                        
                   
                    $('.navi-right').unbind('click');
                    $('.navi-left').unbind('click');
                    var $items = $('.item');
                    var $next = $items.eq($items.index($('#item-'+id)[0]) + 1);
                    var $prev = $items.eq($items.index($('#item-'+id)[0]) - 1);
                    if($items.index($('#item-'+id)[0]) > 0) { //$('#item-'+id).prev('.item').length
                        $('.navi-left').css('display','block');
                        $('.navi-left').bind('click',function() {
                            open($($prev).find('a').data('id'));
                        });
                    }
                    else {
                        $('.navi-left').css('display','none');
                    }
                    if($next['length'] == 1) {
                        $('.navi-right').css('display','block');
                        $('.navi-right').bind('click',function() {
                            open($($next).find('a').data('id'));
                         });
                    }
                    else {
                        $('.navi-right').css('display','none');
                    }
                }
            });
        }
        var socialHandler = {
            fb_params: {
                method: 'feed',
                link: null,
                picture: null,
                name: null,
                caption: 'ADWARDS2015',
                description: null
            },
            load: function(title, desc, pic) {
                $('.social-share').empty();
                var social = '<a class="twitter-share-button" id="work-tw-share" href="https://twitter.com/share" data-text="ADWARDS2015 - '+title+'" data-count="none">Tweet</a>'+
                                            '<div id="work-fb-share"></div>'+
                                            '<div id="dr-share-button"></div>';
                $('.social-share').append(social);
                
                this.fb_params.link = window.location.href;
                this.fb_params.name = title;
                this.fb_params.description = desc;
                this.fb_params.picture = pic;
                
                $(function() {
                    window.twttr.widgets.load();
                    var dr = {
                        link: window.location.href,
                        title: title,
                        titlePrefix: 'ADWARDS2015 - ',
                        popup: true
                    };
                    new DApi.Like(dr).append('dr-share-button');
                });
            },
            init: function() {
                $(document.body).on('click','#work-fb-share',function() {
                    FB.ui(socialHandler.fb_params, function(response){});
                });
            }
        };
        function reset_path(){
		m_pushState('/lv/shortlist/');
		$('.overlay, .container').fadeOut(250,function() {
                    $('.navi-left, .navi-right').css({'top': '320px'});
                });
		$('.media').empty();
                $('.voting-popup .bottom').empty();
                gallery.close();
		site.popup.hide();
                $('video').each(function() {
                    this.pause();
                });
            }
    
    $(function() {
        $(document).on ('mozfullscreenchange webkitfullscreenchange fullscreenchange',function(e) {
            var state = document.fullScreen || document.mozFullScreen || document.webkitIsFullScreen;
            fullscreenState = state ? true : false;
        });
        
        var iOS = /(iPad|iPhone|iPod)/g.test( navigator.userAgent );
        if(iOS) {
            $('.section-title').addClass('notrans');
            $(window).bind('touchmove',scrollHandle);
        }
        else {
            $(window).scroll(scrollHandle);
        }
        if(opened > 0) {
            open(opened);
        }
        $('.award-itm a').click(function() {
            var id = $(this).data('id');
            open(id);
        });
        $('.gallery-close a').click(function() {
            gallery.hide();
            });
            $('.gallery-popup .next-arrow').click(function() {
               gallery.goRight(); 
            });
            $('.gallery-popup .prev-arrow').click(function() {
               gallery.goLeft(); 
            });
        /*
        $('.navi-left').click(function() {
                        open($('#item-'+id).prev('.item').find('a').data('id'));
                    });
                    $('.navi-right').click(function() {
                        open($('#item-'+id).next('.item').find('a').data('id'));
                    });
                    */
        $('#close_pop').click(reset_path);
        $('.overlay').click(reset_path);

    });
</script>
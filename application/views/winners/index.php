<nav class="year-nav">
        <ul>
            <? 
            foreach($years as $y) {?>
                <li><a href="/lv/uzvaretaji/<?=$y;?>.html"<?if($y == $year) echo ' class="ac"';?>><?=$y;?></a></li>
            <?}?>
        </ul>
</nav>
<div class="page" style="padding-bottom:18px;">
    
    <?
    foreach($specawards as $spec) {?>
        <div class='section awards-section'>
            <h1 class='section-title'><?=$spec->_award->title;?></h1>
            <div class='jobs'>
                <div class='work-row'>
                    <div class='item award-itm grandprix-itm' id='item-<?=$spec->id;?>'>
                                    <p class='image'>
                                        <a href='javascript:;' title='' class='overlaid-img' data-id='<?=$spec->id;?>'><?
                                        if($spec->thumbnail == null) {
                                            $spec->thumbnail = 'black_thumb.jpg';
                                        }?>
                                            <img src='/assets/media/images/<?=$spec->big_thumb;?>' alt="" width="648px" height="343px"/>
                                            <span class='img-overlay'>
                                                <?
                                                if($spec->_award->image != '') {?>
                                                    <img src='/assets/css/img/<?=$spec->_award->image;?>' alt='' class='award-lg'>
                                                <?}?>
                                                <span class='award-title<?=($spec->_award->image == '' ? ' ttl-centered' : '');?>'><?=$spec->_award->title;?></span>
                                            </span>
                                        </a>
                                    </p>
                                    <p class='title'>
                                        <a href='javascript:;' title='' data-id='<?=$spec->id;?>'><?=$spec->title;?></a>
                                    </p>
                                    <p class='involved-ppl'>
                                        <span class='role'>Klients: </span><?=$spec->client;?><br>
                                        <span class='role'>Iesniedzējs: </span><?=$spec->author;?>
                                    </p>
                                    
                    </div>
                </div>
            </div>
        </div>
    <?}
    $n = 1;
    $cur = 0;
    foreach($categories as $cat) {
        if(count($cat['winners']) > 0) {?>
            <div class='section awards-section'>
                <h1 class='section-title'><?=$n . '. ' . $cat['name'];?></h1>
                <div class='jobs'><?
                    foreach($cat['winners'] as $winner) {
                        if($cur == 0) {?>
                            <div class='work-row'>
                        <?}
                        $cur++;?>
                                <div class='item award-itm' id='item-<?=$winner->id;?>'>
                                    <p class='image'>
                                        <a href='javascript:;' title='' class='overlaid-img' data-id='<?=$winner->id;?>'><?
                                        if($winner->thumbnail == null) {
                                            $winner->thumbnail = 'black_thumb.jpg';
                                        }?>
                                            <img src='/assets/media/images/<?=$winner->thumbnail;?>' alt="" width="308px" height="163px"/>
                                            <span class='img-overlay'>
                                                <?
                                                if($winner->_award->image != '') {?>
                                                    <img src='/assets/css/img/<?=$winner->_award->image;?>' alt='' class='award-sm'>
                                                <?}?>
                                                <span class='award-title<?=($winner->_award->image == '' ? ' ttl-centered' : '');?>'><?=$winner->_award->title;?></span>
                                            </span>
                                        </a>
                                    </p>
                                    <p class='title'>
                                        <a href='javascript:;' title='' data-id='<?=$winner->id;?>'><?=$winner->title;?></a>
                                    </p>
                                    <p class='involved-ppl'>
                                        <span class='role'>Klients: </span><?=$winner->client;?><br>
                                        <span class='role'>Iesniedzējs: </span><?=$winner->author;?>
                                    </p>
                                </div>
                                
                    <?if($cur == 3) {
                        ?></div><?
                        $cur = 0;
                        }
                    }
                    if($cur > 0 && $cur < 3) {
                        ?></div><?
                        $cur = 0;
                    }?>    
                </div>
            </div>
                <?
        $n++;
        }
    }
?>
</div>
<script>
    var year = <?=$year;?>;
    var opened = <?=(isset($_GET['item']) ? $_GET['item'] : 0);?>;
    var urlbase	= '<?=URL::base();?>';
    var urlbasefull = '<?=$_SERVER['HTTP_HOST'];?>';
    
    function popupPosition() {
        $('.voting-popup').css({
            top: $(window).scrollTop(),
            'position': 'absolute'
        });
    }
    function topVisible(elem)
    {
        var docViewTop = $(window).scrollTop();
        var elemTop = $(elem).offset().top;
        return (docViewTop > (elemTop - (2 * $(window).height())));
    }
    function bottomVisible(elem)
    {
        var docViewTop = $(window).scrollTop();
        var elemBottom = $(elem).offset().top + $(elem).height();
        return (docViewTop < (elemBottom + ($(window).height())));
    }
    var scrollPos = 0;
    function scrollHandle() {
            var winTop = $(window).scrollTop();
            if(winTop > scrollPos) {
                var direction = 'down';                       
            }
            else {
                var direction = 'up';
            }            
            var scrolldif = winTop - scrollPos;
            scrollPos = winTop;
            if($('.voting-popup').is(':visible')) {
                        var height = $('.voting-popup').height();
                        var top = $('.voting-popup').offset().top;
                        var bottom = top + height;
                        var bdif = scrollPos - (bottom-500);
                        var tdif = -(scrollPos - (top-100));
                        var cur = parseInt($('.navi-left, .navi-right').css('top'));
                        //console.log(tdif);
                        switch(direction) {
                            case 'down':
                                if(bdif > 0) {
                                    $('.navi-left, .navi-right').css({'top': (320-bdif)+'px'});
                                }
                                else if(tdif > 0) {
                                    $('.navi-left, .navi-right').css({'top': (320+tdif)+'px'});
                                }
                                else {
                                    $('.navi-left, .navi-right').css({'top': '320px'});                                    
                                }
                                if(!bottomVisible('.voting-popup')) {
                                    /*
                                $('.overlay, .voting-popup').fadeOut(150);
                                $('.media').empty();
                                gallery.close();
                                */
                                    reset_path();
                                }
                                break;
                            case 'up':
                                if(tdif > 0) {
                                    $('.navi-left, .navi-right').css({'top': (cur-scrolldif)+'px'});
                                }
                                else if(bdif > 0 ) {
                                    $('.navi-left, .navi-right').css({'top': (cur-scrolldif)+'px'});
                                }
                                else {
                                    $('.navi-left, .navi-right').css({'top': '320px'});  
                                }
                                if(!topVisible('.voting-popup')) {
                                    /*
                                $('.overlay, .voting-popup').fadeOut(150);
                                $('.media').empty();
                                gallery.close();
                                    */
                                   reset_path();
                                }
                                break;
                        }
                    }          
    }
            function m_pushState(url){
		var stateObj				= { name: "gipsugabrika" };
                if (history.replaceState) {
                        window.history.replaceState(stateObj, "gipsugabrika", url);
                } else{
			var szurl			= url.replace(urlbase,"");
			szurl.replace('#',"");
                        window.location.hash		= szurl;
                }
        }
        
        function open(id) {
            m_pushState('/lv/uzvaretaji/'+year+'.html/?item='+id);
            $('.pop01 .media').empty();
            $('.pop01 .title').empty();
            $('.pop01 .involved-ppl').empty();
            $.ajax({ url: 'uzvaretaji/?id='+id }).done(function ( data ) {
                if(data.type == 'failed') {
                    reset_path();
                }
                else {
                    popupPosition();
                    var base = '<?=URL::base();?>';
                    switch(data.type) {
                        case 'video':
                            var media = '<video width="818" height="428" controls><source src="'+base+data.url+'" type="video/'+data.ext+'"></video>';
                            break;
                        case 'audio':
                            var media = '<audio controls><source src="'+base+data.url+'" type="audio/'+data.ext+'"></audio>';
                            break;
                        case 'picture':
                            var media = '<img src="'+base+'assets/media/images/'+data.url+'" alt="workpic" height=428>';
                            break;
                    }
                    $('.pop01 .media').append(media);
                    $('.pop01 .title').text(data.title);
                    $('.pop01 .involved-ppl').append("<span class='role'>Klients: </span>"+data.client+"<br><span class='role'>Iesniedzējs: </span>"+data.author);
                    $('.overlay, .container').fadeIn(350,function() { $('.pop01').fadeIn(); });
                    $('.navi-right').unbind('click');
                    $('.navi-left').unbind('click');
                    if($('#item-'+id).prev('.item').length) {
                        $('.navi-left').css('display','block');
                        $('.navi-left').bind('click',function() {
                            open($('#item-'+id).prev('.item').find('a').data('id'));
                        });
                    }
                    else {
                        $('.navi-left').css('display','none');
                    }
                    if($('#item-'+id).next('.item').length) {
                        $('.navi-right').css('display','block');
                        $('.navi-right').bind('click',function() {
                            open($('#item-'+id).next('.item').find('a').data('id'));
                         });
                    }
                    else {
                        $('.navi-right').css('display','none');
                    }
                }
            });
        }
        
        function reset_path(){
		m_pushState('/lv/uzvaretaji/'+year+'.html');
		$('.overlay, .container').fadeOut(250,function() {
                    $('.navi-left, .navi-right').css({'top': '320px'});
                });
		$('.media').empty();
                $('.voting-popup .bottom').empty();
		site.popup.hide();
            }
    
    $(function() {
        var iOS = /(iPad|iPhone|iPod)/g.test( navigator.userAgent );
        if(iOS) {
            $('.section-title').addClass('notrans');
            $(window).bind('touchmove',scrollHandle);
        }
        else {
            $(window).scroll(scrollHandle);
        }
        if(opened > 0) {
            open(opened);
        }
        $('.award-itm a').click(function() {
            var id = $(this).data('id');
            open(id);
        });
        /*
        $('.navi-left').click(function() {
                        open($('#item-'+id).prev('.item').find('a').data('id'));
                    });
                    $('.navi-right').click(function() {
                        open($('#item-'+id).next('.item').find('a').data('id'));
                    });
                    */
        $('#close_pop').click(reset_path);
        $('.overlay').click(reset_path);

    });
</script>


<div class="page" style="padding-bottom:18px;">
	<?
	$n		= 0;
	//$clamper	= 0;
        
        $cur = 0;
	foreach($categories as $cat){
            $n++;
            if(count($jobs[$cat->id]) > 0) {
	?>
		<div class="section">
			<h1 class="section-title"><?=$n.'.'.$cat->title;?></h1>
			
			<div class="jobs">
				 
				<?
				foreach($jobs[''.$cat->id] as $js){
                                    if($cur == 0) {?>
                                        <div class="work-row"><?
                                    }
                                    $cur++;
				?>
					<div class="item" data-id='<?=$js->id;?>'>
						<p class="image">
							<a id="popid<?=$js->id;?>" href="javascript:;" title="">
								<?
								if($js->image_1_284_150 != ""){
									$thumb	= $js->image_1_284_150;
								} 
                                                                else if($js->image_2_284_150 != ""){
									$thumb	= $js->image_2_284_150;
								}
                                                                else if($js->image_3_284_150 != ""){
									$thumb	= $js->image_3_284_150;
								}
                                                                else if($js->image_4_284_150 != ""){
									$thumb	= $js->image_4_284_150;
								}
                                                                else if($js->image_5_284_150 != ""){
									$thumb	= $js->image_5_284_150;
								}
                                                                else if($js->image_6_284_150 != ""){
									$thumb	= $js->image_6_284_150;
								}
                                                                else {
									$thumb	= "default_void.jpg";
								}
								?>
								<img src="<?=URL::base();?>assets/upload/jobs/resize/img/<?=$thumb;?>" alt="" width="308px" height="163px"/>
							</a>
						</p>
						<p class="title">
							<a href="javascript:;" title=""><?=$js->title_lv;?></a>
                            <?php if($js->noprice == 1) {
                                echo '<span class="limbo-notice">LIMBO darbs</span>';
                            } ?>
						</p>
						<!--
                                                class="clamper clamper<?//=$clamper;?>
                                                <p  class="clampdots clampdot<?//=$clamper;?>">
							•••
						</p>
                                                -->
						
						
					</div>
								
				<?
                                if($cur == 3) {
                                    ?></div><?
                                    $cur = 0;
                                }
				//$clamper++;
				}
                                if($cur > 0 && $cur < 3) {
                                    ?></div><?
                                    $cur = 0;
                                }
				?>
				
			</div>
		</div>
	<?
            }
	}
	?>
					
</div>
			
<script>
        var voteFeedback = false;
        <? if(isset($_GET['voted'])) { ?>
            voteFeedback = true;
        <?}?>
    	var aJSON;
	var nCurrCatID					= 0;
	var ticker					= 3;
	var ticker_re					= 3;
	var nTimerPopFBDR;
	var sz_lang					= "lv"; //lang
	var eject					= '<?=$eject?>';
	var urlbase					= '<?=URL::base();?>';
	var urlbasefull				= '<?=$_SERVER['HTTP_HOST'];?>';
    var sz_section              = '<?php echo URL::site(Request::detect_uri(), true); ?>'.match(/\/?\w{2}\/([^\/]*)/i)[1];

	var aTHUMBS					= new Array();
        var headerIndex                                 = 'none';
        var headerType                                  = 'none';
	var nCurrThumb					= 0;
	var votes					= 0;
	var ll_alloved					= true;
	var rr_alloved					= true;
	var nCurrGlobalWork				= 0;
	var nGlobalWorksCount				= 0;
        var voted                                       = false;
        var fullscreenState                             = false;


    function popupPosition() {
        $('.voting-popup').css({
            top: $(window).scrollTop(),
            'position': 'absolute'
        });
    }
    function topVisible(elem)
    {
        var docViewTop = $(window).scrollTop();
        var elemTop = $(elem).offset().top;
        return (docViewTop > (elemTop - (2 * $(window).height())));
    }
    function bottomVisible(elem)
    {
        var docViewTop = $(window).scrollTop();
        var elemBottom = $(elem).offset().top + $(elem).height();
        return (docViewTop < (elemBottom + ($(window).height())));
    }    
    
    var zones = [];
    var activeZone = false;
    var scrollPos = 0;
    function Zone(object)
    {
        this.height = object.height();
        this.top = object.offset().top;
        this.bottom = this.top + this.height;
        this.fixBox = object.find('.section-title');
        this.setFixed = function() {
            this.fixBox.css({'position': 'fixed', 'top': '0px', 'padding-top': '0px'});
        };
        this.setNormal = function() {
            this.fixBox.css({'position': 'absolute', 'top': 'initial', 'padding-top': '0px'});
        };
        this.setBottom = function() {
            this.fixBox.css({'position': 'absolute', 'top': 'initial', 'padding-top': (this.height - 43)+'px'});
        };
        this.isInside = function(winTop) {
            if(winTop > this.top && winTop < this.bottom+43) {
                return true;
            }
            else {
                return false;
            }
        };
        this.setActive = function(direction) {
            $('.active-title').removeClass('active-title');
            activeZone = this;
            activeZone.fixBox.addClass('active-title');
            activeZone.setFixed();
        };
    }

    function scrollHandle() {
            var winTop = $(window).scrollTop();
            if(winTop > scrollPos) {
                var direction = 'down';                       
            }
            else {
                var direction = 'up';
            }            
            if(activeZone && !activeZone.isInside(winTop)) {
                switch(direction) {
                    case 'down':
                        activeZone.setBottom();
                        break;
                    case 'up':
                        activeZone.setNormal();
                        break;
                }
                activeZone = false;
            }
            else {
                for(i in zones) {
                    if(zones[i].isInside(winTop)) {
                        zones[i].setActive(direction);
                    }
                }
            }
            var scrolldif = winTop - scrollPos;
            scrollPos = winTop;
            if($('.voting-popup').is(':visible')) {
                        var height = $('.voting-popup').height();
                        var top = $('.voting-popup').offset().top;
                        var bottom = top + height;
                        var bdif = scrollPos - (bottom-500);
                        var tdif = -(scrollPos - (top-100));
                        var cur = parseInt($('.navi-left, .navi-right').css('top'));
                        //console.log(tdif);
                        switch(direction) {
                            case 'down':
                                if(bdif > 0) {
                                    $('.navi-left, .navi-right').css({'top': (320-bdif)+'px'});
                                }
                                else if(tdif > 0) {
                                    $('.navi-left, .navi-right').css({'top': (320+tdif)+'px'});
                                }
                                else {
                                    $('.navi-left, .navi-right').css({'top': '320px'});                                    
                                }
                                if(!bottomVisible('.voting-popup') && !fullscreenState) {
                                    /*
                                $('.overlay, .voting-popup').fadeOut(150);
                                $('.media').empty();
                                gallery.close();
                                */
                                    reset_path();
                                }
                                break;
                            case 'up':
                                if(tdif > 0) {
                                    $('.navi-left, .navi-right').css({'top': (cur-scrolldif)+'px'});
                                }
                                else if(bdif > 0 ) {
                                    $('.navi-left, .navi-right').css({'top': (cur-scrolldif)+'px'});
                                }
                                else {
                                    $('.navi-left, .navi-right').css({'top': '320px'});  
                                }
                                if(!topVisible('.voting-popup') && !fullscreenState) {
                                    /*
                                $('.overlay, .voting-popup').fadeOut(150);
                                $('.media').empty();
                                gallery.close();
                                    */
                                   reset_path();
                                }
                                break;
                        }
                    }          
    }
    
    $(function(){
        $(document).on ('mozfullscreenchange webkitfullscreenchange fullscreenchange',function(e) {
            var state = document.fullScreen || document.mozFullScreen || document.webkitIsFullScreen;
            fullscreenState = state ? true : false;
        });
        
        $('.section').each(function() {
            zones.push(new Zone($(this)));
        });
        var iOS = /(iPad|iPhone|iPod)/g.test( navigator.userAgent );
        if(iOS) {
            $('.section-title').addClass('notrans');
            $(window).bind('touchmove',scrollHandle);
        }
        else {
            $(window).scroll(scrollHandle);
        }
        
        
            $('.gallery-close a').click(function() {
            gallery.hide();
            });
            $('.gallery-popup .next-arrow').click(function() {
               gallery.goRight(); 
            });
            $('.gallery-popup .prev-arrow').click(function() {
               gallery.goLeft(); 
            });
			$('.navi-right').click(function(){
				if(!rr_alloved){
					return;
				}
				pop_next(1);
                                popupPosition();
			});
			
			$('.navi-left').click(function(){
				if(!ll_alloved){
					return;
				}
				pop_next(-1);
                                popupPosition();
			});
			$('#close_pop').click(reset_path);
                        $('.overlay').click(reset_path);
                        $('.balsot_allow').mouseenter(function() {
                            fbdrlv_opened = true;
                            $('.hide_fbdrlv').stop().fadeIn(300);
                        });
                        $('.vote-with').mouseleave(function() {
                            fbdrlv_opened = false;
                            $('.hide_fbdrlv').stop().fadeOut(300);                               
                        });
                        $('.voting-feedback .close').click(function() {
                            $('.voting-feedback').fadeOut(150);
                        });
                            $('.overlay').click(function() {
                                $('.voting-feedback').fadeOut(150);
                            });
                        $('.voting-feedback .feedback-return').click(function() {
                            $('.voting-feedback').fadeOut(150,opener(eject));
                        });
                        socialHandler.init();
                        if(voteFeedback) {
                            $('.overlay, .voting-feedback').fadeIn(150);

                        }
                        else if(eject != ""){
                            opener(eject);
                        }
    });
    
        var gallery = {
            media: [],
            active: null,
            total: 0,
            open: function(from) {
                    this.active = from;
                    for(i in this.media) {
                        $('.gallery-popup .pic-gallery').append(this.media[i]);
                        $('.gallery-popup .dots').append('<div class="pic-dot" data-id='+i+'></div>');
                        this.total++;
                    }
                    this.setDots();
                    this.setActive(from);
                    $('.gallery-popup').fadeIn(150);
                    $('video').each(function() {
                        this.pause();
                    });
            },
            close: function() {
                $('.gallery-popup .pic-gallery').empty();
                $('.gallery-popup .dots').empty();
                this.media = [];
                this.total = 0;
                this.active = null;
                $('.gallery-popup').fadeOut(150);
                $('video').each(function() {
                    this.pause();
                });
            },
            hide: function() {
                $('.gallery-popup').fadeOut(150);
                $('video').each(function() {
                    this.pause();
                });
            },
            show: function(at) {
                if(typeof at != 'undefined') {
                    this.setActive(at);
                }
                $('.gallery-popup').fadeIn(150);
            },
            setActive: function(id) {
                $('.fulldot').removeClass('fulldot');
                $('.pic-dot[data-id='+id+']').addClass('fulldot');
                $('.gallery-pic').css('display','none');
                $('.gallery-pic[data-id='+id+']').css('display','block');
            },
            goRight: function() {
                if(this.active == (this.total - 1)) {
                    this.active = 0;
                }
                else {
                    this.active++;
                }
                this.setActive(this.active);
                $('video').each(function() {
                    this.pause();
                });
            },
            goLeft: function() {
                if(this.active == 0) {
                    this.active = (this.total - 1);
                }
                else {
                    this.active--;
                }
                this.setActive(this.active);
                $('video').each(function() {
                    this.pause();
                });
            },
            setDots: function() {
                $(window).on('click','.pic-dot',function() {
                    var id = $(this).data('id');
                    if(gallery.active != id) {
                        gallery.active = id;
                        gallery.setActive(id);
                    }
                });
            }
            
        };
       
    
	//adwards.lv/lv/tautas-balsojums/?item=132
	
	//console.log('****************');
	//console.log('< ?=$_con;?>');
	//console.log('****************');
	

	
	$('.jobs .item a').click(function(){
		var idd					= String( $(this).parent().parent().attr('data-id') );
		opener(idd);
	});
	
        function m_pushState(url){
		var stateObj				= { name: "gipsugabrika" };
                if (history.replaceState) {
                        window.history.replaceState(stateObj, "gipsugabrika", url);
                } else{
			var szurl			= url.replace(urlbase,"");
			szurl.replace('#',"");
                        window.location.hash		= szurl;
                }
        }	
	

	
	function opener(idd){
		m_pushState('/' + sz_lang + '/' + sz_section + '/?item='+idd);
                $('.pop_title').text('');
		$('.pop_desc').text('');
                $('.voting-popup .text-content > .left-side').prepend('<img src="/assets/css/img/ajax-loader.gif" alt="Loading..." class="ajax-loader"></img>');
		$('.overlay, .container').fadeIn(350, function(){
				$('.pop00').fadeIn();
			});
		$.ajax({ url: 'tautas-balsojums/?item='+idd }).done(function ( data ) {
			aJSON				= data;
			nCurrCatID			= parseInt(aJSON.shift());
			nGlobalWorksCount		= aJSON.length;
			switchGlobalWorks(0);
			popupPosition();
                        fbdrlv_opened = false;
                        $('.hide_fbdrlv').hide();
			//$('.navi-left').fadeTo(0, 0.1);
			//$('.navi-right').fadeTo(0, 0.1);
			
			

			//$('.container').unbind('mousemove');
			//$('.container').bind('mousemove', overTheVote);
			
			//$('.hide_fbdrlv').unbind('mousemove');
			//$('.hide_fbdrlv').bind('mousemove', reset_votetimeout);
			//$('.hide_fbdrlv').bind('mouseout', closefbdrlv);
		
			
			$('.navi-left').unbind('mouseover');
			$('.navi-left').bind('mouseover', function(){
				if(!ll_alloved){
					return;
				}
				//$('.navi-left').stop().fadeTo(500, 1);
			})
			$('.navi-right').unbind('mouseover');
			$('.navi-right').bind('mouseover', function(){
				if(!rr_alloved){
					return;
				}
				//$('.navi-right').stop().fadeTo(500, 1);
			})
			$('.navi-left').unbind('mouseout');
			$('.navi-left').bind('mouseout', function(){
				//$('.navi-left').stop().fadeTo(500, 0.1);
			})
			$('.navi-right').unbind('mouseout');
			$('.navi-right').bind('mouseout', function(){
				//$('.navi-right').stop().fadeTo(500, 0.1);
			})
			
		});		
	}
	
	function pop_next(dir){
		if(dir > 0){
			if(nCurrCatID < nGlobalWorksCount-1){
				nCurrCatID++;
				$('.hide_fbdrlv').css('display', 'none');
                                $('.pop00').fadeOut();
                                gallery.close();
                                switchGlobalWorks(nCurrCatID);
                                $('.pop00').stop().fadeIn(150);
				/*
                                $('.pop00').fadeOut().promise().done(function(){
					$('.pop00').fadeIn();
				});
                                */
			}
		} else {
			if(nCurrCatID > 0){
				nCurrCatID--;
				$('.hide_fbdrlv').css('display', 'none');
                                $('.pop00').fadeOut();
                                gallery.close();
                                switchGlobalWorks(nCurrCatID);
                                $('.pop00').stop().fadeIn(150);
                                /*
				$('.pop00').fadeOut().promise().done(function(){
					switchGlobalWorks(nCurrCatID);
					$('.pop00').fadeIn();
				});
				*/
			}
		}
	}
	
	function switchGallery(id){
		$('.media').empty();
                console.log(id);
		switch(aTHUMBS[id]._type){
			case 'image':{
				//$('.media').append('<a class="open_img" id=big_img"'+id+'" href="javascript:;"><img src="'+urlbase+'assets/upload/jobs/resize/img/'+aTHUMBS[id]._spec_a+'" id="pop_media_img" width="668" height="376" alt="" /></a>');
				$('.media').append('<div class="pop_media_img"><img src="'+urlbase+'assets/upload/jobs/resize/img/'+aTHUMBS[id]._spec_a+'" class="_pop_media_img" width="" height="428px" alt="" /></div>');
				setupImgPop();
			}
			break;
			case 'video':{
                                var base = '<?=URL::base();?>assets/upload/jobs/';
                                var html_data = '<video width="818" height="428" src="'+base+aTHUMBS[id]._spec_a+'" controls><p>Jūsu pārlūks neatbalsta HTML5 video</p></video>';
				/*
                                var path	= urlbase+"assets/upload/jobs/resize/flv/"+aTHUMBS[id]._spec_a;
				var html_data	= '<object id="video" width="668px" height="376px" type="application/x-shockwave-flash" data="/assets/js/mediaplayer-5.8/player.swf" bgcolor="#000000" name="video" tabindex="0">';
				html_data	+= '<param name="allowfullscreen" value="true"><param name="allowscriptaccess" value="always"><param name="seamlesstabbing" value="true"><param name="wmode" value="opaque">';
				html_data	+= '<param name="flashvars" value="file='+path+'&image=&autostart=true&controlbar.position=over"></object>';
				*/
                                $('.media').append(html_data);	
			}
			break;
			case 'filename_audio':{
                                var html_data = '<object id="video" width="818px" height="428px" type="application/x-shockwave-flash" data="/assets/js/mediaplayer-5.8/player.swf" bgcolor="#000000" name="video" tabindex="0"><param name="allowfullscreen" value="true"><param name="allowscriptaccess" value="always"><param name="seamlesstabbing" value="true"><param name="wmode" value="opaque"><param name="flashvars" value="file='+base+aTHUMBS[id]._spec_a+'&image=&autostart=false&controlbar.position=over"></object>';
                                /*				
                                var path	= urlbase+"assets/upload/jobs/"+aTHUMBS[id]._spec_a;
				var html_data	= '<object id="video" width="668px" height="376px" type="application/x-shockwave-flash" data="/assets/js/mediaplayer-5.8/player.swf" bgcolor="#000000" name="video" tabindex="0">';
				html_data	+= '<param name="allowfullscreen" value="true"><param name="allowscriptaccess" value="always"><param name="seamlesstabbing" value="true"><param name="wmode" value="opaque">';
				html_data	+= '<param name="flashvars" value="file='+path+'&image=&autostart=true&controlbar.position=over"></object>';
				*/
                                $('.media').append(html_data);	
			}
			break;
		}
		/*
		for(var i = 0; i < 7; i++){
			$('#pop_dot'+i).removeClass('ac');
		}
		nCurrThumb			= id;
		$('#pop_dot'+nCurrThumb).addClass('ac');
                */
	}
	
	function setupImgPop(){
		$('.open_img').click(function(){
			var id = parseInt($(this).attr('id').split("big_img")[1]);
			//console.log(aJSON[nCurrThumb]
		});
	}
	//setupImgPop();
	
	//switch global works
	var m_buttonsoutmode				= 0;
	function switchGlobalWorks(id){
			votes				= aJSON[nCurrCatID].votes;
                        headerIndex = 'none';
                        headerType = 'none';
			$('#voter').text(votes);
			$('.vote_fb').css('display', 'block');
			$('.vote_dr').css('display', 'block');
			$('.balsot_allow').css('display', 'block');
			$('.balsot_rest').css('display', 'none');
			$('.voting-popup .bottom').empty();
            $('.pop00 .limbo-notice').remove();
			if(aJSON[nCurrCatID].allow_fb == 0 && aJSON[nCurrCatID].allow_dr == 0){
				//$('.voter').css('display', 'none');
				$('.vote_fb').css('display', 'none');
				$('.vote_dr').css('display', 'none');
				//$('.balsot_allow').css('display', 'none');
				$('.balsot_rest').css('display', 'block');
                                voted = true;
			} else {
				
				if(aJSON[nCurrCatID].allow_fb == 0){
					$('.vote_fb').css('display', 'none');
                                        voted = true;
				}
				if(aJSON[nCurrCatID].allow_dr == 0){
					$('.vote_dr').css('display', 'none');
                                        voted = true;
				}
			}
                        
                        $('video').each(function() {
                            this.pause();
                        });
			
			//$('.hide_fbdrlv').css('display', 'none');
			if(aJSON[nCurrCatID].allow_fb != 0 && aJSON[nCurrCatID].allow_dr != 0){
				//$('.vote-with ul').css('margin-top', '-150px');
				m_buttonsoutmode	= 0;
                                voted = false;
			} else if(aJSON[nCurrCatID].allow_fb != 0){
				//$('.vote-with ul').css('margin-top', '-100px');
				m_buttonsoutmode	= 1;
			} else if(aJSON[nCurrCatID].allow_dr != 0){
				//$('.vote-with ul').css('margin-top', '-100px');
				m_buttonsoutmode	= 1;
			}
			if(voted) {
                            $('.voting-star').addClass('full-star');
                        }
                        else {
                            $('.full-star').removeClass('full-star');
                        }
                        
                        
			aTHUMBS				= new Array();
                        
			$('.pop_dotarray').empty();
			for(var i = 0; i < 6; i++){
				if(aJSON[nCurrCatID].im0a[i] != ""){
                                        if(headerType == 'none') {
                                            headerType = 'pic';
                                            headerIndex = aTHUMBS.length;
                                        }
					aTHUMBS.push({
						_type:"image",
						_spec_a:aJSON[nCurrCatID].im0b[i]
					});
				}
			}
			if(aJSON[nCurrCatID].video != ""){
                                if(headerType != 'video') {
                                            headerType = 'video';
                                            headerIndex = aTHUMBS.length;
                                        }
				aTHUMBS.push({
					_type:"video",
					_spec_a:aJSON[nCurrCatID].video
				});
			}
                        if(aJSON[nCurrCatID].video_2 != ""){
				aTHUMBS.push({
					_type:"video",
					_spec_a:aJSON[nCurrCatID].video_2
				});
			}
                        if(aJSON[nCurrCatID].video_3 != ""){
				aTHUMBS.push({
					_type:"video",
					_spec_a:aJSON[nCurrCatID].video_3
				});
			}
                        if(aJSON[nCurrCatID].video_4 != ""){
				aTHUMBS.push({
					_type:"video",
					_spec_a:aJSON[nCurrCatID].video_4
				});
			}
			if(aJSON[nCurrCatID].filename_audio != ""){
                                if(headerType == 'none') {
                                            headerType = 'pic';
                                            headerIndex = aTHUMBS.length;
                                }
				aTHUMBS.push({
					_type:"filename_audio",
					_spec_a:aJSON[nCurrCatID].filename_audio
				});
			}
                        var socpic = false;
                        var base = '<?=URL::base();?>assets/upload/jobs/';
			for(var i = 0; i < aTHUMBS.length; i++){
                            switch(aTHUMBS[i]._type) {
                                case 'image':
                                    var media = '<img src="'+base+'resize/img/'+aTHUMBS[i]._spec_a+'" alt="workpic">';
                                    var bigMedia = media;
                                    if(!socpic) {
                                        socpic = '<?=URL::base('http');?>'+'assets/upload/jobs/'+'resize/img/'+aTHUMBS[i]._spec_a;
                                    }
                                    break;
                                case 'filename_audio':
                                    var media = '<object id="video" width="393px" height="206px" type="application/x-shockwave-flash" data="/assets/js/mediaplayer-5.8/player.swf" bgcolor="#000000" name="video" tabindex="0"><param name="allowfullscreen" value="true"><param name="allowscriptaccess" value="always"><param name="seamlesstabbing" value="true"><param name="wmode" value="opaque"><param name="flashvars" value="file='+base+aTHUMBS[i]._spec_a+'&image=&autostart=false&controlbar.position=over"></object>';
                                    var bigMedia = '<object id="video" width="818px" height="428px" type="application/x-shockwave-flash" data="/assets/js/mediaplayer-5.8/player.swf" bgcolor="#000000" name="video" tabindex="0"><param name="allowfullscreen" value="true"><param name="allowscriptaccess" value="always"><param name="seamlesstabbing" value="true"><param name="wmode" value="opaque"><param name="flashvars" value="file='+base+aTHUMBS[i]._spec_a+'&image=&autostart=false&controlbar.position=over"></object>';
                                    break;
                                case 'video':
                                    var media = '<video width="393" height="206" src="'+base+aTHUMBS[i]._spec_a+'" controls><p>Jūsu pārlūks neatbalsta HTML5 video</p></video>';
                                    var bigMedia = '<video width="818" height="428" src="'+base+aTHUMBS[i]._spec_a+'" controls><p>Jūsu pārlūks neatbalsta HTML5 video</p></video>';
                                    //var media = '<video id="video" width="393" height="206" controls="controls"><source src="'+base+aTHUMBS[i]._spec_a+'"></video>';
                                    //var bigMedia = '<video id="video" width="818" height="428" controls="controls"><source src="'+base+aTHUMBS[i]._spec_a+'"></video>';
                                    //var media = '<object id="video" width="393px" height="206px" type="application/x-shockwave-flash" data="/assets/js/mediaplayer-5.8/player.swf" bgcolor="#000000" name="video" tabindex="0"><param name="allowfullscreen" value="true"><param name="allowscriptaccess" value="always"><param name="seamlesstabbing" value="true"><param name="wmode" value="opaque"><param name="flashvars" value="file='+base+aTHUMBS[i]._spec_a+'&image=&autostart=false&controlbar.position=over"></object>';
                                    //var bigMedia = '<object id="video" width="818px" height="428px" type="application/x-shockwave-flash" data="/assets/js/mediaplayer-5.8/player.swf" bgcolor="#000000" name="video" tabindex="0"><param name="allowfullscreen" value="true"><param name="allowscriptaccess" value="always"><param name="seamlesstabbing" value="true"><param name="wmode" value="opaque"><param name="flashvars" value="file='+base+aTHUMBS[i]._spec_a+'&image=&autostart=false&controlbar.position=over"></object>';
                                    break;
                            }
                            $('.voting-popup .bottom').append('<div class="work-media" data-id="'+i+'">'+media+'</div>');
                            gallery.media.push('<div class="gallery-pic" data-id="'+i+'">'+bigMedia+'</div>');
                            //$('.pop_dotarray').append('<li><a class="popclicker" id="pop_dot'+i+'" href="javascript:;" title=""></a></li>');
			}
                        /*
			$('#pop_dot'+nCurrThumb).addClass('ac');
			$('.popclicker').bind('click', function(){
				var idd			= String( $(this).attr('id') ).split("pop_dot")[1];
				switchGallery(idd);
			});		
                        */
                        $('.work-media').click(function() {
                            var id = $(this).data('id');
                            if(gallery.active == null) {
                                gallery.open(id);
                            }
                            else {
                                gallery.show(id);
                            }
                        });
                        
		for(var i = 0; i < 7; i++){
			$('#pop_dot'+i).removeClass('ac');
		}
		nCurrThumb			= 0;
		$('#pop_dot'+nCurrThumb).addClass('ac');
		$('.ajax-loader').remove();
		$('.pop_title').text( aJSON[nCurrCatID].title_lv );
		$('.pop_desc').text( aJSON[nCurrCatID].description_lv);
        if(parseInt(aJSON[nCurrCatID].limbo) == 1) {
            $('.pop_desc').after('<span class="limbo-notice">LIMBO darbs</span>');
        }
		
		switchGallery(headerIndex);
		
		m_pushState('/'+sz_lang+'/' + sz_section + '/?item='+aJSON[nCurrCatID].id);
		
		checkLRvis();
                socialHandler.load(aJSON[nCurrCatID].title_lv,aJSON[nCurrCatID].description_lv,socpic);

	}
	var socialHandler = {
            fb_params: {
                method: 'feed',
                link: null,
                picture: null,
                name: null,
                caption: 'ADWARDS2016',
                description: null
            },
            load: function(title, desc, pic) {
                $('.social-share').empty();
                var social = '<a class="twitter-share-button" id="work-tw-share" href="https://twitter.com/share" data-text="ADWARDS2016 - '+title+'" data-count="none">Tweet</a>'+
                                            '<div id="work-fb-share"></div>'+
                                            '<div id="dr-share-button"></div>';
                $('.social-share').append(social);
                
                this.fb_params.link = window.location.href;
                this.fb_params.name = title;
                this.fb_params.description = desc;
                this.fb_params.picture = pic;
                
                $(function() {
                    window.twttr.widgets.load();
                    var dr = {
                        link: window.location.href,
                        title: title,
                        titlePrefix: 'ADWARDS2016 - ',
                        popup: true
                    };
                    new DApi.Like(dr).append('dr-share-button');
                });
            },
            init: function() {
                $(document.body).on('click','#work-fb-share',function() {
                    FB.ui(socialHandler.fb_params, function(response){});
                });
            }
        };
        
	function checkLRvis(){
		rr_alloved				= true;
                $('.navi-right').show();
		if(nCurrCatID == nGlobalWorksCount-1){
			rr_alloved			= false;
			//$('.navi-right').fadeTo(500, 0.01);
                        $('.navi-right').hide();
		}
		
		ll_alloved				= true;
                $('.navi-left').show();
		if(nCurrCatID == 0){
			ll_alloved			= false;
			//$('.navi-left').fadeTo(500, 0.01);
                        $('.navi-left').hide();
		}
	}
	
	function reset_path(){
                console.log('RESET');
		m_pushState('/'+sz_lang+'/' + sz_section + '/');
		$('.overlay, .container').fadeOut(250,function() {
                    $('.navi-left, .navi-right').css({'top': '320px'});
                });/*.promise().done(function(){
			$('.media').empty();
                        $('.voting-popup .bottom').empty();
                        gallery.close();
		});*/
		$('.media').empty();
                $('.voting-popup .bottom').empty();
                gallery.close();
		site.popup.hide();
                $('video').each(function() {
                    this.pause();
                });
	};
	
        /*
	function reset_votetimeout(){
		ticker					= ticker_re;
	}
	
	var fbdrlv_opened				= false;
	function overTheVote(e){
		var parentOffset			= $('.container').offset(); 
		var mx					= e.pageX - parentOffset.left;
		var my					= e.pageY - parentOffset.top;
		
		if(fbdrlv_opened){
			
			var y_lim			= 410;
			if(m_buttonsoutmode == 1){
				y_lim			= 460;
			}
			
			if(mx < 19 || mx > 287 || my < y_lim || my > 559){
				fbdrlv_opened			= false;
				clearInterval(nTimerPopFBDR);
				$('.hide_fbdrlv').stop().fadeOut(300);
			}
		} else {
			if(mx > 19 && mx < 287 && my > 510 && my < 559){
				fbdrlv_opened			= true;
				$('.hide_fbdrlv').stop().fadeIn(300, function(){
					ticker			= ticker_re;
					//clearInterval(nTimerPopFBDR);
					//nTimerPopFBDR		= setInterval(popTimeout, 1000);
				});
			}
		}
	}
	
	function closefbdrlv(){
		//clearInterval(nTimerPopFBDR);
		$('.hide_fbdrlv').stop().fadeOut(300);
	}
	//hide_fbdrlv
	
	function popTimeout(){
		ticker--;
		if(ticker <= 0){
			//clearInterval(nTimerPopFBDR);
			$('.hide_fbdrlv').stop().fadeOut();
		}
	}
	/*
	for(var i = 0; i < <?=$clamper;?>; i++){
		if( $('.clamper'+i).height() > 95){
			$('.clampdot'+i).css('display', 'block');
		}
	}
        */
</script>
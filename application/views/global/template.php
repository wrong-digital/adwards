<!DOCTYPE html>
<html lang="lv"<?if(!empty($firstpage)) echo ' style="background:#000;color:#000;"';?>>
<head>
	<meta charset="utf-8" />
	<title><?if(!empty($title)){ echo $title; }else{ echo l('page_title'); }?></title>
	<meta http-equiv="X-UA-Compatible" content="IE=8" />
	<meta name="viewport" content="width=1024px">
	<meta name="title" content="ADWARDS 2016" />
	<meta name="description" content="It's time for the ugly truth" />

	<meta property="og:title" content="<?=empty($og_title) ? 'ADWARDS 2016' : $og_title;?>" />
	<meta property="og:image" content="<?=empty($og_image) ? Kohana::config('site.domain').'facebook-img16.jpg' : Kohana::config('site.domain').'assets/media/images/'.$og_image;?>" />
	<meta property="og:description" content="Reizi gadā pienāk patiesības mirklis, kad, ļoti iespējams, izrādās - tu neesi tik labs, kā domā. Jautājums - vai esi gana drosmīgs to pārbaudīt? Darbu iesniegšana līdz 8. augustam, patiesības mirklis - 26.augusts. It's time for the ugly truth." />
	<meta property="og:type" content="website" />
	<meta property="og:url" content="<?=empty($og_url) ? 'http://www.adwards.lv/' : $og_url;?>" />
	<meta property="og:site_name" content="ADWARDS.LV" />
	<meta property="fb:admins" content="1112544086" />

	<link rel="image_src" type="image/jpeg" href="<?=$og_image;?>" />
        <link rel="icon" href="<?=URL::base();?>favicon.png" type="image/png" />
	<link rel="stylesheet" href="<?=URL::base();?>assets/video/bigvideo/bower_components/BigVideo/css/bigvideo.css">
	<link href='http://fonts.googleapis.com/css?family=Arbutus+Slab&subset=latin,latin-ext' rel='stylesheet' type='text/css'><?
	$_css = array('reset', 'jscroll/jquery.jscrollpane', 'main');
	$_js = array('jquery', 'jquery.filestyle', 'modernizr', 'scripts', 'jquery.fittext', 'jquery.fullscreenBackground', 'jquery.placeholder', 'jscroll/jquery.jscrollpane.min', 'clamp', 'countdown');
	if(!empty($css) && is_array($css)) $_css = Arr::merge($_css, $css);
	if(!empty($js) && is_array($js)) $_js = Arr::merge($_js, $js);

        //$_css = Arr::merge($_css,['styles15']);

	foreach($_css as $file) echo "\n\t",HTML::style('assets/css/'.$file.'.css?v=9');
	?>
	<script>
	    var lang = '<?=$lang;?>';
	    var base = '<?=URL::base();?>';
	</script>
	<?
	foreach($_js as $file) echo "\n\t",HTML::script('assets/js/'.$file.'.js?v=6');
	?>
	<!--[if IE 8]><link rel="stylesheet" type="text/css" href="<?=URL::base();?>assets/css/fix.ie8.css" /><![endif]-->
    <link rel="stylesheet" href="<?=URL::base();?>assets/video/bigvideo/bower_components/BigVideo/css/bigvideo.css">
    <script type="text/javascript" src="http://maquettica.eu/assets/js/jquery.mousewheel.js?v=1392721591"></script>
	<script type="text/javascript" src="//www.draugiem.lv/api/api.js"></script>
	<script type="text/javascript">

	  var _gaq = _gaq || [];
	  _gaq.push(['_setAccount', 'UA-1849204-70']);
	  _gaq.push(['_trackPageview']);

	  (function() {
		var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
		ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
		var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
	  })();


	</script>
</head>
<body<?if(!empty($firstpage)) { echo ' style="background:#000;color:#000;"'; }  else { echo ' style="position:relative"'; }?>>
        <script>window.twttr = (function(d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0],
          t = window.twttr || {};
        if (d.getElementById(id)) return;
        js = d.createElement(s);
        js.id = id;
        js.src = "https://platform.twitter.com/widgets.js";
        fjs.parentNode.insertBefore(js, fjs);

        t._e = [];
        t.ready = function(f) {
          t._e.push(f);
        };

        return t;
      }(document, "script", "twitter-wjs"));</script>
	<div id="fb-root"></div>
	<script>(function(d, s, id) {
	  var js, fjs = d.getElementsByTagName(s)[0];
	  if (d.getElementById(id)) return;
	  js = d.createElement(s); js.id = id;
	  js.src = "//connect.facebook.net/en_GB/all.js#xfbml=1&appId=291096294291833";
	  fjs.parentNode.insertBefore(js, fjs);
	}(document, 'script', 'facebook-jssdk'));</script>
	<script type="text/javascript" src="//www.draugiem.lv/api/api.js"></script>
	<?if(!empty($firstpage)){?>
	<!-- BigVideo Dependencies -->
    <script src="<?=URL::base();?>assets/video/bigvideo/bower_components/jquery-ui/ui/jquery-ui.js"></script>
    <script src="<?=URL::base();?>assets/video/bigvideo/bower_components/jquery-ui/ui/minified/jquery-ui.min.js"></script>
    <script src="//vjs.zencdn.net/4.3/video.js"></script>

    <!-- BigVideo -->

    <script src="<?=URL::base();?>assets/video/bigvideo/bower_components/BigVideo/lib/bigvideo.js"></script>

	<script>
		var BV;
		$(function() {
			$('.absolute-firstpage').css('marginTop',$(window).height()+'px');
                        $('.vidoverlay').hide();
			BV = new $.BigVideo();
			BV.init();
                        BV.getPlayer().on('loadedmetadata', function(){
                            $('#big-video-wrap').show();
                        });


                        var isSafari = !!navigator.userAgent.match(/Version\/[\d\.]+.*Safari/);
                        if(isSafari) {
                            BV.show('/assets/video/adwards-16.mp4', {});
                        }
                        else if (Modernizr.video.webm) {
                            BV.show('/assets/video/adwards-16.webm', {});
                        }
                        else {
                            BV.show('/assets/video/adwards-16.mp4', {});
                        }

                        //BV.getPlayer().volume(0);
        //BV.show('http://www.adwards.lv/assets/video/Adwards_video_nologo_nosound_720p.m4v', {altSource:'http://www.adwards.lv/assets/video/Adwards_video_nologo_nosound_720p.ogv'});
		});
	</script>

	<audio autoplay="true" loop="true" id="background-audio">
	  <source src="/assets/media/adwards16bg.ogg" type="audio/ogg">
	  <source src="/assets/media/adwards16bg.mp3" type="audio/mpeg">
	</audio>

	<?}?>

	<header<?if(!empty($firstpage)) echo ' class="firstpage"';?>>

		<div class="links">
			<ul>
				<li>
					<a href="<?=URL::section(l('id_contacts'));?>" title="<?=l('menu_contacts');?>"><?=l('menu_contacts');?></a>
				</li>
				<?if(Session::instance()->get('user_uid') > 0){?>
				<li>
					<a href="<?=URL::section(85);?>" title="" class="profile-link"><?=Session::instance()->get('user_name');?></a>
					 /
					<a href="<?=URL::section(31);?>" title="<?=l('logout');?>"><?=l('logout');?></a>
				</li>
				<?}else{?>
				<li>
					<a href="javascript:;" title="<?=l('login');?>" id="login"><?=l('login');?></a>
					 /
					<a href="<?=URL::section(77);?>" title="<?=l('register');?>"><?=l('register');?></a>
				</li>
				<?}?>
				<li>
					<a href="http://www.ladc.lv" title="<?=l('ladc');?>" target="_blank"><?=('ladc');?></a>
				</li>
			</ul>
		</div>

		<p class="logo top-logo">
                    <a href="<?=URL::base();?>" title="" class="inline-svg<?if(!empty($firstpage)) echo ' front-logo';?>" data-fallback="<?=URL::base('http',true);?>assets/css/img/adwards-logo2015.png">
                        <?
                                Helper_Svg::adwards_logo();
                        ?>
                    </a>
		</p>

		<nav <?if(!empty($firstpage)) echo 'class="invert-nav"';?>>
			<?=Helper_Section::top_menu(0);?>
		</nav>


		<p class="logo logo-title">
					<a href="<?=URL::base();?>" title="" class="inline-svg<?if(!empty($firstpage)) echo ' front-logo';?>" data-fallback="<?=URL::base('http',true);?>assets/css/img/adwards-title-2016.png">
						<?
								Helper_Svg::adwards_title();
						?>
					</a>
		</p>


		<?if(!empty($firstpage)){?>
		<div style="clear:both"></div>
		<div class="countdowner">

	        <div id="countdown" class="fittext"></div>
	        <div id="countdown-message" class="fittext-none"><?=$countdownmessage;?></div>


	    </div>
	    <?}?>
	</header>

	<?if(!empty($firstpage)){?><div class="vidoverlay"></div><?}?>
        <div class="page-wrapper<?if(!empty($firstpage)){?> absolute-firstpage<?}?>">
	<?=$content;?>

	<section class="sponsors">
		<h2><?=l('support');?></h2>
		<div></div>
                    <div class="adwards-sponsor-links">
                        <a href="http://www.antalis.lv"style="min-width: 110px; min-height: 80px;" target="_blank"></a>
                        <a href="http://clearchannel.lv/"style="min-width: 180px; min-height: 80px;" target="_blank"></a>
						<a href="http://www.audi.lv/"style="min-width: 100px; min-height: 80px;" target="_blank"></a>
						<a href="http://www.rigasnami.lv/"style="min-width: 150px; min-height: 80px;" target="_blank"></a>
						<a href="http://www.wrong.lv/"style="min-width: 115px; min-height: 80px;" target="_blank"></a>
						<a href="http://www.kustmotion.com/" style="min-width: 90px; min-height: 80px;" target="_blank"></a>
						<a href="http://www.dardedze.lv/"style="min-width: 243px; min-height: 80px;" target="_blank"></a>

						<a href="http://www.delfi.lv/"style="min-width: 130px; min-height: 85px;" target="_blank"></a>
						<a href="http://www.digitalguru.lv/"style="min-width: 160px; min-height: 85px;" target="_blank"></a>
						<a href="http://www.alberthotel.lv/" style="min-width: 135px; min-height: 85px;" target="_blank"></a>
						<a href="http://www.rigasvilni.lv/"style="min-width: 140px; min-height: 85px;" target="_blank"></a>
						<a href="http://www.mtg.com/"style="min-width: 140px; min-height: 85px;" target="_blank"></a>
						<a href="http://www.mooz.lv/"style="min-width: 188px; min-height: 85px;" target="_blank"></a>
						<a href="http://picture-agency.lv/"style="min-width: 95px; min-height: 85px;" target="_blank"></a>

						<a href="http://f64.lv/"style="min-width: 140px; min-height: 75px;" target="_blank"></a>
                    </div>
	</section>
        <p class="copyright">
			<?=l('copyright');?>
	</p>
	<footer>
            <div class="centered">
		<ul>
			<li>
				<a href="<?=URL::section(l('id_about'));?>" class="<?=$section->id == l('id_about') ? 'ac' : '';?>" title="<?=l('menu_about');?>"><?=l('menu_about');?></a>
			</li>
			<li>
				<a href="<?=URL::section(l('id_sponsors'));?>" class="<?=$section->id == l('id_sponsors') ? 'ac' : '';?>" title="<?=l('menu_sponsors');?>"><?=l('menu_sponsors');?></a>
			</li>
			<li>
				<a href="<?=URL::section(l('id_contacts'));?>" class="<?=$section->id == l('id_contacts') ? 'ac' : '';?>" title="<?=l('menu_contacts');?>"><?=l('menu_contacts');?></a>
			</li>
		</ul>
                <div class="wrong">
                    <h5>Made by</h5><a href="http://wrong.lv/" class="svg-img wrong-logo" target="_blank" title="WRONG"></a><a href="http://digibrand.lv/" class="svg-img digibrand-logo" target="_blank" title="Digibrand"></a><a href="http://www.kustmotion.com" class="svg-img kust-logo" target="_blank" title="Kust"></a>
                </div>
                <div class="social">
                    <a href="https://www.facebook.com/pages/Latvian-Art-Directors-Club/180515075321085" class="svg-img fb-icon" target="_blank"></a>
                    <a href="https://twitter.com/LADC_Adwards" class="svg-img tw-icon" target="_blank"></a>
                </div>

            </div>

            <!--
		<p class="twitter"><a href="<//?=l('link_twitter');?>" title="Twitter" target="_blank">&nbsp;</a></p>

		<p class="copyright">
			Lapas radošana:
			<a href="http://www.pika.lv" title="" target="_blank">
				<img src="<//?=URL::base();?>assets/css/img/logo-footer-pika.png" alt="" style="margin:0 7px -6px 5px;" />
			</a>
			<a href="http://www.digibrand.lv" title="" target="_blank">
				<img src="<//?=URL::base();?>assets/css/img/logo-footer-dbr.png" alt="" style="margin:0 0 -6px 0;" />
			</a>
		</p>
                -->
	</footer>
	</div>
	<?if(!empty($firstpage)){?>

	<!-- <div class="countdowner">

        <div id="countdown"></div>
        <div id="countdown-message"><?=$countdownmessage;?></div>


    </div> -->

	<div class="down-arrow">&nbsp;</div>

	<script>
		$(function() {
                    <? if($countdown) { ?>

                    	$(".fittext").fitText();
                        var targetTime = '<?=$countdown; ?>';
                        var dateParts = targetTime.split(/[-  :]/);
                        var year, month, day, hours, mins, secs;
                            year = parseInt(dateParts[0]);
                            month = parseInt(dateParts[1]) - 1;
                            day = parseInt(dateParts[2]);
                            hours = parseInt(dateParts[3]);
                            mins = parseInt(dateParts[4]);
                            secs = parseInt(dateParts[5]);
                        var remaining = {
                            time: null,
                            update: function() {
                                remaining.time = countdown(null,new Date(year,month,day,hours,mins,secs),~(countdown.YEARS | countdown.MONTHS | countdown.WEEKS));
                            },
                            getString: function() {
                                return remaining.time.days + 'd ' + remaining.time.hours + 'h ' + remaining.time.minutes + 'm ' + remaining.time.seconds + 's' /* '.' + (remaining.time.milliseconds < 100 ? '0' : '')  + (remaining.time.milliseconds < 10 ? '0' : '') + remaining.time.milliseconds + 's'*/;
                            }
                        };
                        remaining.update();
                        $('#countdown').text(remaining.getString());
                        setInterval(function(){
                            remaining.update();
                            $('#countdown').text(remaining.getString());
                          }, 33);
                    <?}?>
			var wheel = false;
                        $('.absolute-firstpage').css('marginTop',$(window).height()+'px');


			var $bgAudio = $('#background-audio');	// background <audio>

			$(window).scroll(function() {
				if(wheel == false) {
					if($('.absolute-firstpage').length > 0) {
						var m = $('.absolute-firstpage').css('marginTop').slice(0, -2);
						if(m > 0){
							$('body,html').animate({
								scrollTop: 0
							}, 0);

							wheel = true;
							$('.down-arrow').fadeOut(50);
                                                        $('.logo a').removeClass('front-logo');
							$('.absolute-firstpage').animate({marginTop:'0px'}, 550, function(){
								wheel = false;
							});
                                                        $('nav').removeClass('invert-nav');
                                                        BV.getPlayer().pause();
														$bgAudio.animate({volume: 0}, 1000);

                                                        <? if($countdown) { ?>
                                                            $('#countdown, #countdown-message').fadeOut(550);
                                                        <?}?>
						}
					}
				}
			})

			$('body').bind('mousewheel', function(event, delta, deltaX, deltaY) {
				if(wheel == false){
					if (delta < 0) {
						//
					} else {
						var m = $('.absolute-firstpage').css('marginTop').slice(0, -2)*1;
						if($('html').offset().top == 0 && m > 0 && m != $(window).height() || $('html').offset().top == 0 && m == 0){
							/*$('body,html').animate({
								scrollTop: 0
							}, 0);*/
							wheel = true;
							$('.down-arrow').fadeIn(50);
                                                        $('.logo a').addClass('front-logo');
							$('.absolute-firstpage').animate({marginTop:$(window).height()+'px'}, 550, function(){
								wheel = false;
							});
							//$('.vidoverlay').fadeIn(550);
                                                        $('.vidoverlay').animate({marginTop: 0},550);
                                                        $('nav').addClass('invert-nav');
                                                        BV.getPlayer().play();
														$bgAudio.animate({volume: 1}, 1000);

                                                        <? if($countdown) { ?>
                                                            $('#countdown, #countdown-message').fadeIn(550);
                                                        <?}?>
							return false;
						}
					}
				}else{
					return false;
				}
			});

			$('.down-arrow').click(function(){
                                //window.location.href = 'http://www.adwards.lv/lv/par-adwards';

				$(this).fadeOut();

                                $('.logo a').removeClass('front-logo');
                                <? if($countdown) { ?>
                                                            $('#countdown, #countdown-message').fadeOut(550);
                                                        <?}?>;
				$('.absolute-firstpage').animate({marginTop:'0px'}, 550, function(){
					wheel = false;
				});
                                $('nav').removeClass('invert-nav');

			});
		});
	</script>
	<?}?>
	<script>
		$(function() {
                    function fixDif() {
                        var height_diff = $(window).height() - $('body').height();
                        if ( height_diff > 0 ) {
                            $('footer').css( 'margin-top', height_diff );
                        }

                    };
                    fixDif();
                    $(window).resize(fixDif());

                });
        </script>
	<div class="overlay"></div>

	<div class="popup-registration">
		<div class="close"><a href="javascript:;" title="">&nbsp;</a></div>
		<div class="content"></div>
	</div>

	<div class="jury-popup">
            <div class="jury-container">
		<div class="close">
			<a href="javascript:;" title="" class="svg-img">&nbsp;</a>
		</div>
		<div class="jury-left">
			<a href="javascript:;" class="svg-img"></a>
		</div>
		<div class="c">
			<div class="image"></div>
			<div class="about">
				<h2></h2>
				<h3></h3>
				<div>
					<div></div>
				</div>
			</div>
		</div>
		<div class="jury-right">
			<a href="javascript:;" class="svg-img"></a>
		</div>
            </div>
		<?=Form::hidden('selected_jury','');?>
            <span class="bottom-marker"></span>
	</div>

	<div class="login-box">
		<p class="close"><a href="javascript:;" title="" class="svg-img">&nbsp;</a></p>
                <p class="img">
                    <span class="svg-img adwards-logo"></span>
                </p>
		<form method="post" action="<?=URL::section(30);?>">
		    <fieldset>
			    <label for="email"><?=l('login_email');?></label>
				<input type="text" name="email" id="email" />
			</fieldset>
			<fieldset>
			    <label for="password"><?=l('login_password');?><a class="login-restore"><?=l('login_restore');?></a></label>
				<input type="password" name="password" id="password" />
			</fieldset>
			<input type="submit" class="submit" value="<?=l('login_login');?>" />
		</form>
                <p class="text"><span><?=l('login_oldloginworks');?></span><br/><a href="<?=URL::section(77);?>" title="">Reģistrēties</a></p>
	</div>

    <div class="restore-pass">
        <p class="close"><a href="javascript:;" title="" style="margin-right: 95px;">&nbsp;</a></p>
        <p class="img"><span class="svg-img adwards-logo"></span></p>
        <div class="fff">
            <p style="text-align:center; width: 270px;"><?=l('login_restore_text1');?></p>
        </div>
        <form method="post" action="<?=URL::base();?>lv/actions/restorepass">
            <fieldset>
                <label for="email"><?=l('login_email');?></label>
                <input type="text" name="email" id="email" />
            </fieldset>
            <input type="submit" class="submit" value="<?=l('login_restorepass');?>" />
        </form>
        <p class="text"><?=l('login_oldloginworks');?><br/><a href="<?=URL::section(77);?>" title="">Reģistrēties</a></p>
    </div>

	<div class="popup">
		<div class="close">
			<a href="javascript:;" title="">
				<img src="<?=URL::base();?>assets/css/img/ico-close.png" alt="" />
			</a>
		</div>
		<div class="c"></div>
	</div>

	<div class="container voting-popup">
            <div id="close_pop" class="close"><a class="close"></a></div>
            <div class="navi-left"><a></a>
		</div>
		<div class="navi-right"><a></a>
		</div>
		<div class="content">
			<div class="pop00">
				<div class="media">
				<!--generated-->
				</div>
                                <div class="text-content">
                                    <div class="left-side pop00">
                                            <h2 class="pop_title">TITLE</h2>
                                            <p class="pop_desc">DESC</p>
                                    </div>

									<?php /* VOTING FINISHED
                                    <div class="right-side">
                                            <?
                                            //if(!Session::instance()->get('uid')){
                                                    ///!!! separate test vote FB/DR16px
                                            ?>
                                            <h4 class="voting-text">ATDOD SAVU BALSI!</h4>
                                            <div class="buttons">
                                                    <div class="voting-star"></div>
                                                    <p><span class="weak">+</span><span id="voter"></span></p>
                                                    <!--
                                                    <div>--
                                                            <ul class="pop_dotarray">

                                                            </ul>
                                                    </div>
                                                    -->
                                            </div>
                                        <!--<div class="vote-with"></div>-->
                                            <div class="vote-with voter">
                                                    <!--h6 class="balsot_allow">Balsošana beigusies</h6-->
                                                    <h6 class="balsot_allow">Balsot</h6> <!-- Slēgts -->
                                                    <h6 class="balsot_rest">paldies, tava balss<br>jau pieskaitīta</h6>
                                                    <ul class="hide_fbdrlv">
                                                            <li class="vote_fb">
                                                                    <a href="<?=URL::base();?>socials/facebook" title="">
                                                                            FACEBOOK
                                                                            <!--<img src="<?//=URL::base();?>assets/css/img/vote-facebook.png" alt="" />-->
                                                                    </a>
                                                            </li>
                                                            <li class="vote_dr">
                                                                    <a href="<?=URL::base();?>socials/draugiem" title="">
                                                                            DRAUGIEM.LV
                                                                            <!--<img src="<?//=URL::base();?>assets/css/img/vote-draugiem.png" alt="" />-->
                                                                    </a>
                                                            </li>
                                                    </ul>
                                            </div>
                                            <?
                                            //}
                                            ?>


                                    </div>
 									*/ ?>
                                </div>
				<div class="bottom">

				</div>
                                <div class="social-share">

                                </div>
			</div>
                        <div class='pop01'>
                            <div class="media"></div>
                            <div class='text-content'>
                                <p class='title'></p>
                                <p class='involved-ppl'></p>
                            </div>
                            <div class="bottom">

                            </div>
                            <div class="social-share"></div>
                        </div>
		</div>

                <div class="gallery-popup">
                        <div class="gallery-overlay"></div>
                        <div class="gallery-content">
                            <div class="gallery-close">
                                <a href="javascript:;" title="" class="svg-img">&nbsp;</a>
                            </div>
                            <div class="prev-arrow"></div>
                            <div class="pic-gallery"></div>
                            <div class="dots"></div>
                            <div class="next-arrow"></div>
                        </div>

                </div>
	</div>
            <div class="voting-feedback">
                <div class="vf-content">
                    <div class="close">
                            <a href="javascript:;" title="" class="svg-img">&nbsp;</a>
                    </div>
                    <div class="bigstar"></div>
                    <h3>TAVS BALSOJUMS IR PIEŅEMTS!</h3>
                    <a href="javascirpt:;" class="feedback-return">ATGRIEZTIES PIE DARBA</a>
                </div>
            </div>
</body>
</html>

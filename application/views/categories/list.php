	<div class="textdoc">
		<div class="leftside">
			<ul class="submenu">
				<?if($section->id == 3 || $section->parent_id == 3){?>
                                <li>
                                    <a href="<?=URL::section(83);?>" title="">Iesniegt darbu</a>
                                </li>
                                <li>
                                    <a href="<?=URL::section(84);?>" title="">Mani darbi</a>
                                </li>
                                <li>
                                    <a href="<?=URL::section(85);?>" title="">Profils</a>
                                </li>
				<?}?>
				<?=Helper_Section::public_tree($section->parent_id == 0 ? $section->id : $section->parent_id);?>
			</ul>
		</div>
		<div class="page-content">
			<?if($section->image){?>
				<div class="page-image">
					<img src="<?=URL::base().'assets/media/images/'.$section->image;?>" alt="" />
				</div>
			<?}?>

			<div class="sections">
				<div class="selected">
				    <div class="title">Izvēlieties kategoriju</div>
                                    <div class="branding-logo"></div>
				    <div class="ico svg-img">&nbsp;</div>
				</div>
				<ul>
					<?$n=0;foreach($categories as $i){?>
					<li<?/*=($i->id == 19) ? ' class="branded'.($i->id == 19 ? ' antalis' : '').'"' : '';*/?>>
						<a href="javascript:;" title="<?=$i->title;?>" data-id="<?=$i->id;?>"><?=++$n.'. '.$i->title;?></a>
					</li>
					<?}?>
				</ul>
			</div>

			<div id="formats-wrapper" style="padding:0;">
				<?/*foreach($formats as $format){?>
				<div class="format" data-id="<?=$format->id;?>">
					<div class="title"><?=strip_tags($format->title);?></div>
					<div class="ico svg-img">&nbsp;</div>
				</div>
				<div class="format-content" data-content="<?=$format->id;?>">
					<h3><?=l('for_jury');?> <span style="font-size:16px;"><?=l('for_note');?></span></h3>
					<?=$format->jury;?>
					<h3><?=l('for_book');?></h3>
					<?=$format->book;?>
					<h3><?=l('for_web');?></h3>
					<?=$format->web;?>
				</div>
				<?}*/?>
			</div>
		</div>
	</div>

	<script>
		$(function(){
			$('.sections .selected').click(function(){
				if($('.sections ul').is(':visible')) {
					$('.sections .selected .ico').removeClass('down');
					$('.sections ul').slideUp(250);
				}else{
					$('.sections .selected .ico').addClass('down');
					$('.sections ul').slideDown(250);
				}
			})

			$('.sections ul li a').click(function(){
				if($(this).data('id') == 19){
					/* ANTALIS DISABLED
					$('.sections .selected').attr('style','background:url(<?=URL::base();?>assets/css/img/antalisbg2015.png) no-repeat;');
                                        $('.sections .selected .branding-logo').attr('style','');
                                        */
                                }

								// VIDES REKLĀMA brandings diseiblots
								/*
                                else if($(this).data('id') == 28) {
                                        $('.sections .selected').attr('style','');
                                        $('.sections .selected .branding-logo').attr('style','background:url(<?=URL::base();?>assets/css/img/vides-reklama-branding.svg) no-repeat; width: 356px; height: 27px;');
                                }
								*/
                                else{
					$('.sections .selected').attr('style','');
                                        $('.sections .selected .branding-logo').attr('style','');
				}
				$.post('<?=URL::section($section->id);?>', { id:$(this).data('id') }, function(data){
					$('#formats-wrapper').html(data);
					$('.format').click(function(){
						var content = $('[data-content='+$(this).data('id')+']');
						if(content.is(':visible')) {
							$(this).find('.ico').removeClass('down');
							content.slideUp(250);
						}else{
							$(this).find('.ico').addClass('down');
							content.slideDown(250);
						}
					})
				});
				$('.sections .selected .title').html($(this).html());
				$('.sections .selected .ico').removeClass('down');
				$('.sections ul').slideUp(250);
			});

			$('.format').click(function(){
				var content = $('[data-content='+$(this).data('id')+']');
				if(content.is(':visible')) {
					$(this).find('.ico').removeClass('down');
					content.slideUp(250);
				}else{
					$(this).find('.ico').addClass('down');
					content.slideDown(250);
				}
			})
		})
	</script>






	<!--

				<div class="page">
				    <div class="leftside">
						<ul><?=Helper_Section::public_tree($section->parent_id == 0 ? $section->id : $section->parent_id);?></ul>
						<p>&nbsp;</p>
					</div>
					<div class="page-content-empty">
					    <div class="h">
						    <h1><?=$section->title;?></h1>
							<a style="display:none;" href="<?=l('categories_print');?>" title="<?=l('print');?>" target="_blank"><?=l('print');?></a>
						</div>
						<div class="sections">
						    <ul>
								<?$n=1;foreach($categories as $i){?>
							    <li>
							        <a href="<?=URL::section($section->id);?>/<?=$i->link;?>.html" title="<?=$i->title;?>" class="<?=!isset($item) && $n == 1 ? 'ac' : (isset($item) && $item['id'] == $i->id ? 'ac' : '');?>"><?=$i->title;?></a>
								</li>
								<?$n++;}?>
							</ul>
						</div>
						<div class="c">

							<div class="formats">
							    <div class="cols">
								    <div class="col1"><?=l('subsection');?></div>
								    <div class="col2"><?=l('for_jury');?></div>
								    <div class="col3"><?=l('for_book');?></div>
								    <div class="col4"><?=l('for_web');?></div>
								</div>

								<?$i=1;foreach($formats as $format){?>
								<div class="format-<?=$i == 1 ? 'blue' : 'white';?>">
								    <div class="col1"><?=$format->title;?></div>
								    <div class="col2"><?=$format->jury;?></div>
								    <div class="col3"><?=$format->book;?></div>
								    <div class="col4"><?=$format->web;?></div>
								</div>
								<?
									if($i == 2) $i = 0;
									$i++;
								}
								?>
							</div>

						</div>
					</div>
				</div>

			</div>
-->

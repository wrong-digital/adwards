<!DOCTYPE html>
<html lang="lv">
<head>
    <meta charset="utf-8" />
	<title>Bacsktage CMS / Login</title>
	<link rel="stylesheet" href="<?=URL::base();?>assets/css/cms/login.css" />
	<script type="text/javascript" src="<?=URL::base();?>assets/js/cms/jquery.js"></script>
	<script type="text/javascript" src="<?=URL::base();?>assets/js/cms/admin.js"></script>
	<script type="text/javascript">
		admin.url = {
			base: "<?=URL::base();?>",
			site: "<?=URL::site();?>"
		};
		$(function(){
			admin.login();
		});
	</script>
</head>
<body>
	<div class="content">
        <form method="post" action="#" id="login">
			<div class="inputs">
				<div class="input"><input type="text" id="username" name="username" value="Lietotāja vārds" /></div>
				<div class="input"><input type="password" id="password" name="password" value="Parole" /></div>
				<div class="submit"><input type="submit" class="submit" value="Ienākt" /></div>
			</div>
        </form>
        <div class="copyright">Visas tiesības aizsargātas © <a href="http://www.digibrand.lv" target="_blank"><img src="<?=URL::base();?>assets/css/cms/img/login-logo-digibrand.png" alt="Digibrand" /></a></div>
    </div>
    <div class="error">Nepareizs lietotāja vārds vai parole</div>
</body>
</html>
<form method="post" id="admin" action="<?=URL::site('admin/textdoc/view/'.$section['id']);?>" enctype="multipart/form-data">
	<div class="insert-area">
		<div class="path"><?=Helper_Admin_Sections::path($section['id']);?></div>
		<p class="module">Modulis: <span>Teksta dokuments</span></p>
		<div class="block-insert p-100">
			<div class="h-exp" id="content-h">
				<div class="title">Satura administrēšana</div>
				<div class="arrow"><a href="javascript:;" onclick="admin.block.toggle('content-h', 'content-area');">&nbsp;</a></div>
			</div>
			<div class="insert-content" id="content-area">
				<div class="insert-input">
					<div class="label left p-30">Virsraksts</div>
					<div class="field left p-70"><?=Form::input('title', $section['title'], array('onkeyup' => 'admin.utils.title.rewrite(this);'));?></div>
				</div>
				<div class="insert-input">
					<div class="label left p-30">Saite</div>
					<div class="field left p-70"><?=Form::input('link', $section['link']);?></div>
				</div>
				<div class="insert-input">
					<div class="label left p-30">Teksts</div>
					<div class="field left p-70"><?=Form::textarea('content', $textdoc ? $textdoc['content'] : '', array('class' => 'pb'));?></div>
				</div>
			</div>
			<div class="h-exp" id="seo-h">
				<div class="title">SEO</div>
				<div class="arrow"><a href="javascript:;" onclick="admin.block.toggle('seo-h', 'seo-area');">&nbsp;</a></div>
			</div>
			<div class="insert-content" id="seo-area">
				<div class="insert-input">
					<div class="label left p-30">Meta apraksts</div>
					<div class="field left p-70"><?=Form::textarea('metadescription', $section['metadescription'], array('class' => 'p'));?></div>
				</div>
				<div class="insert-input">
					<div class="label left p-30">Meta atslēgas vārdi</div>
					<div class="field left p-70"><?=Form::textarea('metakeywords', $section['metakeywords'], array('class' => 'p'));?></div>
				</div>
			</div>
		</div>
	</div>
	<div class="insert-right right">
		<div class="block-insert w-259" style="margin:28px 0 0 1px;">
			<div class="h-exp" id="statuss-h">
				<div class="title">Publicēšana</div>
				<div class="arrow"><a href="javascript:;" onclick="admin.block.toggle('statuss-h', 'statuss');">&nbsp;</a></div>
			</div>
			<div class="rightblock-content" id="statuss">
				<div class="statuss" style="margin-bottom: 5px;">
					<div class="label">Statuss:</div>
					<label><?=Form::radio('status', ORM::STATUS_ACTIVE, $section ? $section['status'] == ORM::STATUS_ACTIVE : false, array('style' => 'width: auto;'));?> Publicēts</label>
					<label><?=Form::radio('status', ORM::STATUS_DRAFT, $section ? $section['status'] == ORM::STATUS_DRAFT : true, array('style' => 'width: auto;'));?> Melnraksts</label>
					<label><?=Form::radio('status', ORM::STATUS_HIDDEN, $section ? $section['status'] == ORM::STATUS_HIDDEN : false, array('style' => 'width: auto;'));?> Slēpts</label>
				</div>
				<div class="statuss-btns"><button type="submit" class="save" style="margin-left:65px;">Saglabāt</a></div>
			</div>
		</div>
	</div>
</form>
<script type="text/javascript" src="<?=URL::base();?>assets/js/cms/ckeditor/ckeditor.js"></script>
<script type="text/javascript" src="<?=URL::base();?>assets/js/cms/ckeditor/adapters/jquery.js"></script>
<script type="text/javascript">
	admin.tree.active = <?=$section['id'];?>;
	$(function(){
		admin.WYSIWYG.create("textarea.pb");
	});
</script>
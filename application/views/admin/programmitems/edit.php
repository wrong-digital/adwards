<form method="post" id="admin" action="<?=URL::site('admin/programmitems/'.($programmitems ? 'edit/'.$programmitems['id'] : 'add'));?>" enctype="multipart/form-data">
	<div class="insert-area">
		<div class="path">Programmas ieraksti</div>
		<div class="block-insert p-100" style="padding-top:6px;">
			<div class="h-exp" id="content-h">
				<div class="title">Programmas ieraksti</div>
				<div class="arrow"><a href="javascript:;" onclick="admin.block.toggle('content-h','content-area');" title="">&nbsp;</a></div>
			</div>
			<div class="insert-content" id="content-area">
				<div class="insert-input">
					<div class="label left p-30">Laiks</div>
					<div class="field left p-70"><?=Form::input('time', $programmitems ? $programmitems['time'] : '', array());?></div>
				</div>
				<div class="insert-input">
					<div class="label left p-30">Nosaukums</div>
					<div class="field left p-70"><?=Form::input('title', $programmitems ? $programmitems['title'] : '', array());?></div>
				</div>
				<div class="insert-input">
					<div class="label left p-30">Vieta</div>
					<div class="field left p-70"><?=Form::input('place', $programmitems ? $programmitems['place'] : '', array());?></div>
				</div>
				
				<div class="insert-input">
					<div class="label left p-30">Saistītā ziņa</div>
					<div class="field left p-70">
					    <select name="news_id">
						    <option value=""></option>
							<?foreach($news as $n){?>
							<option value="<?=$n->id;?>"<?if($programmitems && $programmitems['news_id'] == $n->id){?> selected="selected"<?}?>><?=$n->title;?></option>
							<?}?>
						</select>
					</div>
				</div>
				
				<?=Form::hidden('section_id', $programmitems ? $programmitems['section_id'] : $section_id);?>
			</div>
		</div>
	</div>
	<div class="insert-right right">
		<div class="block-insert w-259" style="margin:35px 0 0 1px;">
			<div class="h-exp" id="statuss-h">
				<div class="title">Publicēšana</div>
				<div class="arrow"><a href="javascript:;" onclick="admin.block.toggle('statuss-h','statuss');" title="">&nbsp;</a></div>
			</div>
			<div class="rightblock-content" id="statuss">
				<div class="statuss" style="margin-bottom: 5px;">
					<div class="label">Statuss:</div>
					<label><?=Form::radio('status', ORM::STATUS_ACTIVE, $programmitems ? $programmitems['status'] == ORM::STATUS_ACTIVE : false, array('style' => 'width: auto;'));?> Publicēts</label>
					<label><?=Form::radio('status', ORM::STATUS_DRAFT, $programmitems ? $programmitems['status'] == ORM::STATUS_DRAFT : true, array('style' => 'width: auto;'));?> Melnraksts</label>
					<label><?=Form::radio('status', ORM::STATUS_HIDDEN, $programmitems ? $programmitems['status'] == ORM::STATUS_HIDDEN : false, array('style' => 'width: auto;'));?> Slēpts</label>
				</div>
				<div class="statuss-btns"><button class="save" type="submit" style="margin-left:65px;">Saglabāt</button></div>
			</div>
		</div>
	</div>
</form>
<script type="text/javascript" src="<?=URL::base();?>assets/js/cms/ckeditor/ckeditor.js"></script>
<script type="text/javascript" src="<?=URL::base();?>assets/js/cms/ckeditor/adapters/jquery.js"></script>
<script type="text/javascript">
	admin.tree.active = 22;
	$(function(){
		admin.WYSIWYG.create("textarea.fx");
	});
</script>
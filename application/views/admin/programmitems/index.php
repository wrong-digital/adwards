<div class="content-area">
	<div class="path">Programmas ieraksti</div>
	<div class="table">
		<div class="add-btn"><a href="<?=URL::site('admin/programmitems/add/'.$section_id);?>" title="Pievienot">Pievienot</a></div>
		<div class="table-header-l">
			<div class="table-header">
				<div class="p-80 left"><div>Laiks, nosaukums</div></div>
				<div class="p-10 left centered">Aktīvs</div>
				<div class="p-10 left centered">Dzēst</div>
			</div>
		</div>
		<div class="table-data">
		<?
		$i = 0;
		foreach($programmitems as $p){
		?>
			<div class="table-item<?if(++$i%2 == 0){?> grey<?}?>" id="prod" data="<?=$p->id;?>">
				<div class="p-80 left cellbg"><div><a href="<?=URL::site('admin/programmitems/edit/'.$p->id);?>" title=""><?=$p->time.', '.$p->title;?></a></div></div>
				<div class="p-10 left centered cellbg"><?=$p->cmsStatusIcon();?></div>
				<div class="p-10 left centered"><a href="<?=URL::site('admin/programmitems/delete/'.$p->id);?>" title=""><img src="<?=URL::site();?>assets/css/cms/img/table-ico-delete.png"></a></div>
			</div>
		<?}?>
		</div>
	</div>
</div>
<script type="text/javascript">
$(".table-data").sortable({
	update: function() {
		var data = new Array();
		$(".table-data #prod").each(function(){
			data.push($(this).attr("data"));
		});
		$.post(admin.url.site+"admin/programmitems/sort", {
			data: data
		});
	}
});
admin.tree.active = 22;
</script>
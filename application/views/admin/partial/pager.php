<?
$query = array_filter($_GET);
if(isset($query['p'])) unset($query['p']);
$query['p'] = '';
$url = '?'.str_replace('%', '%%', http_build_query($query)).'%d';
$pages = ceil($count/$perpage);
if($pages < 2) return;
echo '<div class="footer-paging"><div class="paging"><ul>';
if($page > 1) echo '<li><a href="',sprintf($url, $page-1),'" title="" class="pleft">&nbsp;</a></li>';
echo '<li><a href="',sprintf($url, 1),'" title=""',$page == 1 ? ' class="ac"' : '','>1</a></li>';

$from = $page == $pages ? $page-2 : $page-1;
if($from < 2) $from = 2;
$to = $from+2;
if($to >= $pages) $to = $pages-1;

if($from > 2) echo '<li>...</li>';
for($i = $from; $i <= $to; $i++) echo '<li><a href="',sprintf($url, $i),'" title=""',$i == $page ? ' class="ac"' : '','>',$i,'</a></li>';
if($to < $pages-1) echo '<li>...</li>';

echo '<li><a href="',sprintf($url, $pages),'" title=""',$page == $pages ? ' class="ac"' : '','>',$pages,'</a></li>';
if($page < $pages) echo '<li><a href="',sprintf($url, $page+1),'" title="" class="pright">&nbsp;</a></li>';
echo '</ul>';
?>
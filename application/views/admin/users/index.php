<div class="content-area">
	<div class="path">Reģistrētie lietotāji</div>
	<div class="table">
		<div class="add-btn"><a href="<?=URL::site('admin/users/add');?>" title="Pievienot">Pievienot</a></div>
		
		<div class="table-header-l">
			<div class="table-header">
				<div class="p-40 left"><div>Vārds</div></div>
				<div class="p-20 left"><div>E-pasts</div></div>
				<div class="p-20 left centered">Aktīvs</div>
				<div class="p-20 left centered">Dzēst</div>
			</div>
		</div>
		<div class="table-data">
		<?
		$i = 0;
		foreach($users as $user){
		?>
			<div class="table-item<?if(++$i%2 == 0){?> grey<?}?>">
				<div class="p-40 left cellbg"><div><?=$user->cmsEditLink(HTML::chars($user->username != '' ? '[ADMIN] '.$user->username : ($user->private_name != '' ? $user->private_name : $user->legal_title)));?></div></div>
				<div class="p-20 left cellbg"><div><?=$user->cmsEditLink(HTML::chars($user->email));?></div></div>
				<div class="p-20 left centered cellbg"><img src="<?=URL::base();?>assets/css/cms/img/table-ico-<?if(!$user->active){?>in<?}?>active.png" alt="" /></div>
				<div class="p-20 left centered"><a rel="action:delete" href="<?=URL::site('admin/users/delete/'.$user->id);?>"><img src="<?=URL::base();?>assets/css/cms/img/table-ico-delete.png" alt="" /></a></div>
			</div>
		<?}?>
		</div>
				
			<table style="text-align: left; width: 100%;" border="1"
			 cellpadding="2" cellspacing="2">
			  <tbody>
			    <tr>
			      <td>Uzņēmuma Nosaukums/Vārds Uzvārds</td>
			      <td>E-pasts</td>
			      <td>Konta Numurs</td>
			      <td>Pers. kods / Reģ. nr.</td>
			      <td>Summa</td>
			      <td>Darbi</td>
			      <td>Izrakstīt rēķinu?</td>
			    </tr>
		<?
		$i = 0;
		foreach($users as $user){
			$user_id = $user->id;
			$jobs = ORM::factory('inserts')->where('user_id', '=', $user_id)->find_all()->as_array();
			if(count($jobs) > 0){
				$not_saved = 0;
				$free_job = 1;
				$antalis_free = 1;
				$limbo_free = 2;
				$price = 0;

				foreach($jobs as $job){
					if($job->order_status == 0){
						$not_saved = 1;
					}
					
					if ($job->main_category == 25){ // darba uzdevums - bezmaksas neierobežotā daudzumā
						$price = $price + 0;
					}else{
						if ($job->noprice == 1){ 
							$limbo_free--;
						}
						if ($job->noprice == 1 && $limbo_free > 0){ // viens no bezmaksas Limbo darbiem
							$price = $price + 0;
						/*if ($job->noprice == 1 && $limbo_free >= 1){ 	// ja grib pēc diviem bezmaksas limbo darbiem vēl piešķirt trešo bezmaksas parasto darbu,
							$price = $price + 0;						// jāatkomentē šīs 3 rindiņas un jāizkomentē 4-5 rindiņas kas virs šīm.
							$limbo_free--;*/
						}else{ // Parastais darbs vai, ja bezmaksas limbo darbi iztērēti
							if ($job->antalis == 1 && !empty($job->antalis_type) && $job->antalis_type != 'Ieraksti, kādus Antalis papīrus vai materiālus izmantoji'){
								if ($antalis_free == 1) {
									$price = $price + 0;
									$antalis_free = 0;
								}else{
									if ($free_job == 1){
										$price = $price + 0;
										$free_job = 0;
									}else{
										$price = $price + Kohana::$config->load('site')->get('half_price');
									}
								}
							}else{
								if ($free_job == 1){
									$price = $price + 0;
									$free_job = 0;
								}else{
									$price = $price + Kohana::$config->load('site')->get('item_price');
								}
							}
						}
					} // darba uzdevums
                }?>
			<?}
			if ($price > 0)
			{
			
			?>

			    <tr style="<?if(++$i%2 == 0){?>background-color:<?}?>">
			      <td><?=$user->cmsEditLink(HTML::chars($user->username != '' ? '[ADMIN] '.$user->username : ($user->private_name != '' ? $user->private_name : $user->legal_title)));?></td>
			      <td><? echo $user->email; ?></td>
			      <td><? echo $user->accaunt; ?></td>
			      <td><? echo $user->private_code.' '; echo $user->legal_number; ?></td>
			      <td><? echo ' Summa (2013): '.$price.' Ls';$price=0;?></td>
			      <td>
			      	<? foreach($jobs as $job){
			      		echo 'Kods:'.$job->code.', Darba ID:'.$job->id.'<br />';
						}
			      	?>
			      </td>
			      <td>
			      	<? foreach($jobs as $job){
			      		if ($job->bill > 0){$billie='Jā';}
						}
					echo $billie;
			      	?>
			      </td>
			    </tr>


			<?}?>
		<?}?>
			  </tbody>
			</table>
</div>
<form method="post" id="admin" action="<?=URL::site('admin/users/'.($user ? 'edit/'.$user['id'] : 'add'));?>">
	<div class="insert-area">
		<div class="path">Reģistrētie lietotāji</div>
		<div class="block-insert p-100" style="padding-top:6px;">
			<div class="h-exp" id="content-h">
				<div class="title">Reģistrētie lietotāji</div>
				<div class="arrow"><a href="javascript:;" onclick="admin.block.toggle('content-h','content-area');" title="">&nbsp;</a></div>
			</div>
			<div class="insert-content" id="content-area">
				<?if($user){?>
					<?if($user['private_name'] != ''){?>
						<div class="insert-input">
							<div class="label left p-30">Vārds, uzvārds</div>
							<div class="field left p-70"><?=Form::input('private_name', $user ? $user['private_name'] : '');?></div>
						</div>
						<div class="insert-input">
							<div class="label left p-30">Personas kods</div>
							<div class="field left p-70"><?=Form::input('private_code', $user ? $user['private_code'] : '');?></div>
						</div>
						<div class="insert-input">
							<div class="label left p-30">Nodarbošanās</div>
							<div class="field left p-70"><?=Form::input('private_job', $user ? $user['private_job'] : '');?></div>
						</div>
						<div class="insert-input">
							<div class="label left p-30">E-pasts</div>
							<div class="field left p-70"><?=Form::input('email', $user ? $user['email'] : '');?></div>
						</div>
						<div class="insert-input">
							<div class="label left p-30">Tālrunis</div>
							<div class="field left p-70"><?=Form::input('phone', $user ? $user['phone'] : '');?></div>
						</div>
						<div class="insert-input">
							<div class="label left p-30">Adrese</div>
							<div class="field left p-70"><?=Form::input('address', $user ? $user['address'] : '');?></div>
						</div>
						<div class="insert-input">
							<div class="label left p-30">Banka</div>
							<div class="field left p-70"><?=Form::input('bank', $user ? $user['bank'] : '');?></div>
						</div>
						<div class="insert-input">
							<div class="label left p-30">Konta nr.</div>
							<div class="field left p-70"><?=Form::input('accaunt', $user ? $user['accaunt'] : '');?></div>
						</div>
						<div class="insert-input">
                            <div class="label left p-30">Mainīt paroli</div>
                            <div class="field left p-70"><?=Form::password('password');?></div>
                        </div>
					<?}else{?>
						<div class="insert-input">
							<div class="label left p-30">Uzņēmuma nosaukums</div>
							<div class="field left p-70"><?=Form::input('legal_title', $user ? $user['legal_title'] : '');?></div>
						</div>
						<div class="insert-input">
							<div class="label left p-30">Reģistrācijas numurs</div>
							<div class="field left p-70"><?=Form::input('legal_number', $user ? $user['legal_number'] : '');?></div>
						</div>
						<div class="insert-input">
							<div class="label left p-30">PVN maksātāja numurs</div>
							<div class="field left p-70"><?=Form::input('legal_pvn', $user ? $user['legal_pvn'] : '');?></div>
						</div>
						<div class="insert-input">
							<div class="label left p-30">Kontaktpersona</div>
							<div class="field left p-70"><?=Form::input('legal_contact', $user ? $user['legal_contact'] : '');?></div>
						</div>
						<div class="insert-input">
							<div class="label left p-30">E-pasts</div>
							<div class="field left p-70"><?=Form::input('email', $user ? $user['email'] : '');?></div>
						</div>
						<div class="insert-input">
							<div class="label left p-30">Tālrunis</div>
							<div class="field left p-70"><?=Form::input('phone', $user ? $user['phone'] : '');?></div>
						</div>
						<div class="insert-input">
							<div class="label left p-30">Fiz. adrese</div>
							<div class="field left p-70"><?=Form::input('address', $user ? $user['address'] : '');?></div>
						</div>
						<div class="insert-input">
							<div class="label left p-30">Jurid. adrese</div>
							<div class="field left p-70"><?=Form::input('legal_address', $user ? $user['legal_address'] : '');?></div>
						</div>
						<div class="insert-input">
							<div class="label left p-30">Banka</div>
							<div class="field left p-70"><?=Form::input('bank', $user ? $user['bank'] : '');?></div>
						</div>
						<div class="insert-input">
							<div class="label left p-30">Konta nr.</div>
							<div class="field left p-70"><?=Form::input('accaunt', $user ? $user['accaunt'] : '');?></div>
						</div>
						
						<div class="insert-input">
                            <div class="label left p-30">Mainīt paroli</div>
                            <div class="field left p-70"><?=Form::password('password');?></div>
                        </div>
					<?}?>
				<?}else{?>
				<div class="insert-input">
					<div class="label left p-30">Vārds</div>
					<div class="field left p-70"><?=Form::input('name', $user ? $user['name'] : '');?></div>
				</div>
				<div class="insert-input">
					<div class="label left p-30">Uzvārds</div>
					<div class="field left p-70"><?=Form::input('surname', $user ? $user['surname'] : '');?></div>
				</div>
				<div class="insert-input">
					<div class="label left p-30">E-pasts</div>
					<div class="field left p-70"><?=Form::input('email', $user ? $user['email'] : '');?></div>
				</div>
				<?if($user){?>
				<div class="insert-input">
					<div class="label left p-30">Reģistrācijas datums</div>
					<div class="field left p-70"><?=Form::input('date_created', $user ? date('d.m.Y H:i', strtotime($user['date_created'])) : '', array('disabled' => 'disabled'));?></div>
				</div>
				<div class="insert-input">
					<div class="label left p-30">Pēdējā autorizācija</div>
					<div class="field left p-70"><?=Form::input('date_login', $user ? date('d.m.Y H:i', strtotime($user['date_login'])) : '', array('disabled' => 'disabled'));?></div>
				</div>
				<?}?>
				<div class="insert-input">
					<div class="label left p-30">Lietotājvārds</div>
					<div class="field left p-70"><?=Form::input('username', $user ? $user['username'] : '');?></div>
				</div>
				<div class="insert-input">
					<div class="label left p-30">Parole</div>
					<div class="field left p-70"><?=Form::password('password');?></div>
				</div>
				<div class="insert-input">
					<div class="label left p-30">Tips</div>
					<div class="field left p-70"><?=Form::select('level', array_combine(range(1, 2), range(1, 2)), $user ? $user['level'] : null);?></div>
				</div>
				<?}?>
			</div>
		</div>
	</div>
	<div class="insert-right right">
		<div class="block-insert w-259" style="margin:35px 0 0 1px;">
			<div class="h-exp" id="statuss-h">
				<div class="title">Publicēšana</div>
				<div class="arrow"><a href="javascript:;" onclick="admin.block.toggle('statuss-h','statuss');" title="">&nbsp;</a></div>
			</div>
			<div class="rightblock-content" id="statuss">
				<div class="statuss">
					<div class="label">Statuss:</div>
					<div class="radio left"><input type="radio" name="active" value="1"<?if($user && $user['active']){?> checked="checked"<?}?> /> Aktīvs</div>
					<div class="radio left"><input type="radio" name="active" value="0"<?if(!$user || !$user['active']){?> checked="checked"<?}?> /> Neaktīvs</div>
				</div>
				<?if($user['private_name'] != ''){?>
				<div class="statuss">
					<div class="label">LADC biedrs:</div>
					<div class="radio left"><input type="radio" name="private_member" value="1"<?if($user && $user['private_member']){?> checked="checked"<?}?> /> Jā</div>
					<div class="radio left"><input type="radio" name="private_member" value="2"<?if(!$user || $user['private_member'] == 2){?> checked="checked"<?}?> /> Nē</div>
				</div>
				<?}else{?>
				<div class="statuss">
					<div class="label">LADC biedrs:</div>
					<div class="radio left"><input type="radio" name="legal_member" value="1"<?if($user && $user['legal_member']){?> checked="checked"<?}?> /> Jā</div>
					<div class="radio left"><input type="radio" name="legal_member" value="2"<?if(!$user || $user['legal_member'] == 2){?> checked="checked"<?}?> /> Nē</div>
				</div>
				<?}?>
				<div class="statuss">
					<div class="label">Piekrīt saņemt jaunumus:</div>
					<div class="radio left"><input type="radio" name="spam" value="1"<?if($user && $user['spam']){?> checked="checked"<?}?> /> Jā</div>
					<div class="radio left"><input type="radio" name="spam" value="0"<?if(!$user || $user['spam'] == 0){?> checked="checked"<?}?> /> Nē</div>
				</div>
				<div class="statuss-btns"><button class="save" type="submit" style="margin-left:65px;">Saglabāt</button></div>
				<div class="statuss-btns"><button class="save" type="button" style="margin-left:65px;" onclick="document.location.href = '<?=URL::site('admin/users');?>'; return false;">Atcelt</button></div>
			</div>
		</div>
	</div>
</form>
<script type="text/javascript">
	$(function(){
		$(document.createElement("iframe"))
			.attr({
				id: "formFrame",
				name: "formFrame"
			})
			.hide()
			.appendTo("body");
		$("#admin").attr("target", "formFrame");
	});
</script>
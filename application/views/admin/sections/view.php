<form method="post" id="admin" action="<?=URL::site('admin/sections/'.($section ? 'view/'.(array_key_exists('section_id', $section) ? $section['section_id'] : $section['id']) : 'add/'.$parent_id));?>" enctype="multipart/form-data">
	<div class="insert-area">
		<div class="path"><?=Helper_Admin_Sections::path($section['id']);?></div>
		<div class="block-insert p-100">
			<div class="h-exp" id="content-h">
				<div class="title">Sadaļas pievienošana</div>
				<div class="arrow"><a href="javascript:;" onclick="admin.block.toggle('content-h', 'content-area');" title="">&nbsp;</a></div>
			</div>
			<div class="insert-content" id="content-area">
				<div class="insert-input">
					<div class="label left p-30">Nosaukums</div>
					<div class="field left p-70"><?=Form::input('title', $section ? $section['title'] : '', array('onkeyup' => 'admin.utils.title.rewrite(this)'));?></div>
				</div>
				<div class="insert-input">
					<div class="label left p-30">Saite</div>
					<div class="field left p-57"><?=Form::input('link', $section ? $section['link'] : '');?></div>
					<div class="left p-2">&nbsp;</div>
					<div class="action left p-11"><!--[ <a href="#" title="">Mainīt</a> ]--></div>
				</div>
				<div class="insert-input">
					<div class="label left p-30">Meta apraksts</div>
					<div class="field left p-70"><?=Form::textarea('metadescription', $section ? $section['metadescription'] : '', array('class' => 'p'));?></div>
				</div>
				<div class="insert-input">
					<div class="label left p-30">Meta atslēgas vārdi</div>
					<div class="field left p-70"><?=Form::textarea('metakeywords', $section ? $section['metakeywords'] : '', array('class' => 'p'));?></div>
				</div>
			</div>
		</div>
	</div>
	<div class="insert-right right">
		<div class="block-insert w-259" style="margin:28px 0 0 1px;">
			<div class="h-exp" id="statuss-h">
				<div class="title">Publicēšana</div>
				<div class="arrow"><a href="javascript:;" onclick="admin.block.toggle('statuss-h', 'statuss');">&nbsp;</a></div>
			</div>
			<div class="rightblock-content" id="statuss">
				<div class="statuss" style="margin-bottom: 10px;">
					<div class="label">Statuss:</div>
					<label style="margin-left: 5px;"><?=Form::radio('status', ORM::STATUS_ACTIVE, $section ? $section['status'] == ORM::STATUS_ACTIVE : false, array('style' => 'width: auto;'));?> Publicēts</label>
					<label><?=Form::radio('status', ORM::STATUS_DRAFT, $section ? $section['status'] == ORM::STATUS_DRAFT : true, array('style' => 'width: auto;'));?> Melnraksts</label>
					<label><?=Form::radio('status', ORM::STATUS_HIDDEN, $section ? $section['status'] == ORM::STATUS_HIDDEN : false, array('style' => 'width: auto;'));?> Slēpts</label>
				</div>
				<div class="statuss">
					<div class="label">Modulis:</div>
					<?
					$modules = array('sections' => '');
					foreach(Kohana::config('cms.modules') as $module) $modules[$module['controller']] = $module['title'];
					echo Form::select('module_id', $modules, $section ? $section['module_id'] : null);
					?>
				</div>
				<?=Form::hidden('parent_id', $parent_id);?>
				<?=Form::hidden('lang', $section ? $section['lang'] : current(array_keys($_GET)));?>
				<div class="statuss-btns"><button type="save" class="save" style="margin-left:65px;">Saglabāt</button></div>
			</div>
		</div>
		
		<div class="block-insert w-259" style="margin:15px 0 0 1px;">
			<div class="h-exp" id="statuss-h">
				<div class="title">Lapas fona attēls</div>
				<div class="arrow"><a href="javascript:;" onclick="admin.block.toggle('statuss-h','statuss');" title="">&nbsp;</a></div>
			</div>
			<div class="rightblock-content" id="statuss">
				<div class="statuss" style="margin-bottom: 5px;">
					<div class="label">Fails:</div>
					<?if(!empty($section['image'])){?><img src="<?=URL::base();?>assets/media/images/<?=$section['image'];?>" alt="" style="display: block; margin: 0 auto 10px auto; max-width: 210px;" /><?}?>
					<?=Form::file('image', array('style' => 'margin: 0 0 10px 10px;'));?>
				</div>
			</div>
		</div>
		
		<div class="block-insert w-259" style="margin:28px 0 0 1px;">
			<div class="h-col" id="misc-h">
				<div class="title">MISC</div>
				<div class="arrow"><a href="javascript:;" onclick="admin.block.toggle('misc-h', 'misc');">&nbsp;</a></div>
			</div>
			<div class="rightblock-content" id="misc" style="display: none;">
				<div class="statuss">
					<div class="insert-input">
						<div class="label">controller:</div>
						<div class="field"><?=Form::input('controller', $section ? $section['controller'] : '');?></div>
						<div class="label">action:</div>
						<div class="field"><?=Form::input('action', $section ? $section['action'] : '');?></div>
					</div>
				</div>
			</div>
		</div>
	</div>
</form>
<script type="text/javascript"> admin.tree.active = <?=$section ? (array_key_exists('section_id', $section) ? $section['section_id'] : $section['id']) : $parent_id;?>; </script>
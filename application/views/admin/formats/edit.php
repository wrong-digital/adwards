<form method="post" id="admin" action="<?=URL::site('admin/formats/'.($formats ? 'edit/'.$formats['id'] : 'add'));?>" enctype="multipart/form-data">
	<div class="insert-area">
		<div class="path">Kategorijas un formāti</div>
		<div class="block-insert p-100" style="padding-top:6px;">
			<div class="h-exp" id="content-h">
				<div class="title">Kategorijas un formāti</div>
				<div class="arrow"><a href="javascript:;" onclick="admin.block.toggle('content-h','content-area');" title="">&nbsp;</a></div>
			</div>
			<div class="insert-content" id="content-area">
				<div class="insert-input">
					<div class="label left p-30">Audio fails</div>
					<div class="field left p-70">
						<?
							$c = array('1' => 'Nav vajadzīgs', '2' => 'Pēc izvēles', '3' => 'Obligāts');
							echo Form::select('audio_file', $c, $formats ? $formats['audio_file'] : false, array('style' => 'width:150px;'));
						?>
					</div>
				</div>
				<div class="insert-input">
					<div class="label left p-30">Video fails</div>
					<div class="field left p-70">
						<?
							$c = array('1' => 'Nav vajadzīgs', '2' => 'Pēc izvēles', '3' => 'Obligāts');
							echo Form::select('video_file', $c, $formats ? $formats['video_file'] : false, array('style' => 'width:150px;'));
						?>
					</div>
				</div>
				<div class="insert-input">
					<div class="label left p-30">Arhīva fails</div>
					<div class="field left p-70">
						<?
							$c = array('1' => 'Nav vajadzīgs', '2' => 'Pēc izvēles', '3' => 'Obligāts');
							echo Form::select('zip_file', $c, $formats ? $formats['zip_file'] : false, array('style' => 'width:150px;'));
						?>
					</div>
				</div>								<div class="insert-input">					<div class="label left p-30">Attēli gadagrāmatai</div>					<div class="field left p-70">						<?							$c = array('1' => 'Nav vajadzīgs', '3' => 'Obligāts');							echo Form::select('book_file', $c, $formats ? $formats['book_file'] : false, array('style' => 'width:150px;'));						?>					</div>				</div>								<div class="insert-input">					<div class="label left p-30">Attēli webam</div>					<div class="field left p-70">						<?							$c = array('1' => 'Nav vajadzīgs', '3' => 'Obligāts');							echo Form::select('web_file', $c, $formats ? $formats['web_file'] : false, array('style' => 'width:150px;'));						?>					</div>				</div>
				
				<div class="insert-input" style="padding-top:25px;">
					<div class="label left p-30">Kods</div>
					<div class="field left p-70"><?=Form::input('code', $formats ? $formats['code'] : '');?></div>
				</div>
				<div class="insert-input">
					<div class="label left p-30">Kategorija</div>
					<div class="field left p-70"><?=Form::input('title', $formats ? $formats['title'] : '');?></div>
				</div>
				<div class="insert-input">
					<div class="label left p-30">Žūrijai</div>
					<div class="field left p-70"><?=Form::textarea('jury', $formats ? $formats['jury'] : '', array('class' => 'pb'));?></div>
				</div>
				<div class="insert-input">
					<div class="label left p-30">Gadagrāmatai</div>
					<div class="field left p-70"><?=Form::textarea('book', $formats ? $formats['book'] : '', array('class' => 'pb'));?></div>
				</div>
				<div class="insert-input">
					<div class="label left p-30">Webam</div>
					<div class="field left p-70"><?=Form::textarea('web', $formats ? $formats['web'] : '', array('class' => 'pb'));?></div>
				</div>
				<?=Form::hidden('section_id', $formats ? $formats['section_id'] : $section_id);?>
			</div>
		</div>
	</div>
	<div class="insert-right right">
		<div class="block-insert w-259" style="margin:35px 0 0 1px;">
			<div class="h-exp" id="statuss-h">
				<div class="title">Publicēšana</div>
				<div class="arrow"><a href="javascript:;" onclick="admin.block.toggle('statuss-h','statuss');" title="">&nbsp;</a></div>
			</div>
			<div class="rightblock-content" id="statuss">
				<div class="statuss" style="margin-bottom: 5px;">
					<div class="label">Statuss:</div>
					<label><?=Form::radio('status', ORM::STATUS_ACTIVE, $formats ? $formats['status'] == ORM::STATUS_ACTIVE : false, array('style' => 'width: auto;'));?> Publicēts</label>
					<label><?=Form::radio('status', ORM::STATUS_DRAFT, $formats ? $formats['status'] == ORM::STATUS_DRAFT : true, array('style' => 'width: auto;'));?> Melnraksts</label>
					<label><?=Form::radio('status', ORM::STATUS_HIDDEN, $formats ? $formats['status'] == ORM::STATUS_HIDDEN : false, array('style' => 'width: auto;'));?> Slēpts</label>
				</div>
				<div class="statuss-btns"><button class="save" type="submit" style="margin-left:65px;">Saglabāt</button></div>
			</div>
		</div>
	</div>
</form>
<script type="text/javascript" src="<?=URL::base();?>assets/js/cms/ckeditor/ckeditor.js"></script>
<script type="text/javascript" src="<?=URL::base();?>assets/js/cms/ckeditor/adapters/jquery.js"></script>
<script type="text/javascript">
	admin.tree.active = 14;
	$(function(){
		admin.WYSIWYG.create("textarea.pb");
	});
</script>
<form method="post" id="admin" action="<?=URL::site('admin/galleries/edit_image/'.$image['id']);?>" enctype="multipart/form-data">
	<div class="insert-area">
		<div class="path">Labot attēlu</div>
		<div class="block-insert p-100" style="padding-top:6px;">
			<div class="h-exp" id="content-h">
				<div class="title">Labot attēlu</div>
				<div class="arrow"><a href="javascript:;" onclick="admin.block.toggle('content-h','content-area');" title="">&nbsp;</a></div>
			</div>
			<div class="insert-content" id="content-area">
				<div class="insert-input">
					<div class="label left p-30">Nosaukums</div>
					<div class="field left p-70"><?=Form::input('title', $image ? $image['title'] : '', array());?></div>
				</div>
				<div class="insert-input">
					<div class="label left p-30">Video embed kods (325x235)</div>
					<div class="field left p-70"><?=Form::textarea('embed', $image ? $image['embed'] : '', array('class' => 'pb', 'style' => 'height:125px'));?></div>
				</div>
				<?=Form::hidden('gallery_id', $image['gallery_id']);?>
			</div>
		</div>
	</div>
	<div class="insert-right right">
		<div class="block-insert w-259" style="margin:35px 0 0 1px;">
			<div class="h-exp" id="statuss-h">
				<div class="title">Publicēšana</div>
				<div class="arrow"><a href="javascript:;" onclick="admin.block.toggle('statuss-h','statuss');" title="">&nbsp;</a></div>
			</div>
			<div class="rightblock-content" id="statuss">
				<div class="statuss-btns"><button class="save" type="submit" style="margin-left:65px;">Saglabāt</button></div>
			</div>
		</div>
	</div>
</form>
<script type="text/javascript" src="<?=URL::base();?>assets/js/cms/ckeditor/ckeditor.js"></script>
<script type="text/javascript" src="<?=URL::base();?>assets/js/cms/ckeditor/adapters/jquery.js"></script>
<script type="text/javascript">
	admin.tree.active = 8;
</script>
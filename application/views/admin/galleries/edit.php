<form method="post" id="admin" action="<?=URL::site('admin/galleries/'.($gallery ? 'edit/'.$gallery['id'] : 'add'));?>" enctype="multipart/form-data">
	<div class="insert-area">
		<div class="path">Galerijas</div>
		<div class="block-insert p-100" style="padding-top:6px;">
			<div class="h-exp" id="content-h">
				<div class="title">Galerijas</div>
				<div class="arrow"><a href="javascript:;" onclick="admin.block.toggle('content-h','content-area');" title="">&nbsp;</a></div>
			</div>
			<div class="insert-content" id="content-area">
				<div class="insert-input">
					<div class="label left p-30">Nosaukums</div>
					<div class="field left p-70"><?=Form::input('title', $gallery ? $gallery['title'] : '', array('onkeyup' => 'admin.utils.title.rewrite(this)'));?></div>
				</div>
				<div class="insert-input">
					<div class="label left p-30">Saite</div>
					<div class="field left p-70"><?=Form::input('link', $gallery ? $gallery['link'] : '');?></div>
				</div>
				<div class="insert-input">
					<div class="label left p-30">Apraksts</div>
					<div class="field left p-70"><?=Form::input('description', $gallery ? $gallery['description'] : '');?></div>
				</div>
				<?=Form::hidden('section_id', $gallery ? $gallery['section_id'] : $section_id);?>
				<?=Form::hidden('lang', Request::current()->param('lang'));?>
			</div>
			
			<div class="h-exp" id="content-img">
				<div class="title">Attēli</div>
				<div class="arrow"><a href="javascript:;" onclick="admin.block.toggle('content-img','content-area-img');" title="">&nbsp;</a></div>
			</div>
			<div class="insert-content" id="content-area-img" style="padding-bottom:20px;padding-right:10px;">
				<div class="table">
					<input type="file" name="uploadify" id="uploadify" />
					<div class="table-header-l">
						<div class="table-header">
							<div class="p-80 left"><div>Attēls</div></div>
							<div class="p-20 left centered">Dzēst</div>
						</div>
					</div>
					<div class="table-data">
					<?
					$i = 0;
					if($gallery){
						foreach($images as $image){
						?>
							<div class="table-item<?if(++$i%2 == 0){?> grey<?}?>" id="img" data="<?=$image->id;?>">
								<div class="p-5 left cellbg" style="height:60px;padding:25px 0 0 0;"><span style="padding-left:10px;"><?=$i;?></span></div>
								<div class="p-75 left cellbg">
									<div style="float:left;" class="p-15">
										<a href="/admin/galleries/edit_image/<?=$image->id;?>"><?=HTML::image('assets/upload/gallery/60x60/'.$image->file.'.jpg', array('height' => 60));?></a>
									</div>
									<div class="float:left;" class="p-85">
										<div style="padding:15px 0 7px 0;">Nosaukums: <?=$image->title;?></div>
									</div>
								</div>
								<div class="p-20 left centered"><a rel="action:delete" href="<?=URL::site('admin/galleries_images/delete/'.$image->id);?>"><img src="<?=URL::base();?>assets/css/cms/img/table-ico-delete.png" style="padding-top:20px;" alt="" /></a></div>
							</div>
						<?}
					}?>
					
					<script>
					$(".table-data").sortable({
						update: function() {
							var data = new Array();
							$(".table-data #img").each(function(){
								data.push($(this).attr("data"));
							});
							$.post(admin.url.site+"admin/galleries/sort", {
								data: data
							});
						}
					});
					</script>
					</div>
				</div>
			</div>
		</div>
		
	</div>
    
	<div class="insert-right right">
		<div class="block-insert w-259" style="margin:35px 0 0 1px;">
			<div class="h-exp" id="statuss-h">
				<div class="title">Attēls</div>
				<div class="arrow"><a href="javascript:;" onclick="admin.block.toggle('statuss-h','statuss');" title="">&nbsp;</a></div>
			</div>
			<div class="rightblock-content" id="statuss">
				<div class="statuss" style="margin-bottom: 5px;">
					<div class="label">Attēls:</div>
					<?if(!empty($gallery['image'])){?><img src="<?=URL::base();?>assets/media/images/<?=$gallery['image'];?>" alt="" style="display: block; margin: 0 auto 10px auto; max-width: 210px;" /><?}?>
					<?=Form::file('image', array('style' => 'margin: 0 0 10px 10px;'));?>
				</div>
			</div>
		</div>
		
		<div class="block-insert w-259" style="margin:15px 0 0 1px;">
			<div class="h-exp" id="statuss-h">
				<div class="title">Publicēšana</div>
				<div class="arrow"><a href="javascript:;" onclick="admin.block.toggle('statuss-h','statuss');" title="">&nbsp;</a></div>
			</div>
			<div class="rightblock-content" id="statuss">
				<div class="statuss" style="margin-bottom: 5px;">
					<div class="label">Status:</div>
					<label><?=Form::radio('status', ORM::STATUS_ACTIVE, $gallery ? $gallery['status'] == ORM::STATUS_ACTIVE : false, array('style' => 'width: auto;margin-left: 10px;'));?> Publicēts</label>
					<label><?=Form::radio('status', ORM::STATUS_DRAFT, $gallery ? $gallery['status'] == ORM::STATUS_DRAFT : true, array('style' => 'width: auto;'));?> Melnraksts</label>
					<label><?=Form::radio('status', ORM::STATUS_HIDDEN, $gallery ? $gallery['status'] == ORM::STATUS_HIDDEN : false, array('style' => 'width: auto;'));?> Slēpts</label>
				</div>
				<div class="statuss-btns"><button class="save" type="submit" style="margin-left:65px;">Saglabāt</button></div>
			</div>
		</div>
	</div>
</form>

<script type="text/javascript">
	$(function(){
		$(document.createElement("iframe"))
			.attr({
				id: "formFrame",
				name: "formFrame"
			})
			.hide()
			.appendTo("body");
		$("#admin").attr("target", "formFrame");
		$("#uploadify").uploadify({
			auto: true,
			buttonText: "Upload",
			cancelImg: "<?=URL::base();?>assets/css/cms/img/cancel.png",
			expressInstall: "<?=URL::base();?>assets/swf/cms/expressInstall.swf",
			fileDesc: "Images",
			fileExt: "*.jpg;*.jpeg;*.png;",
			multi: true,
			script: "<?=URL::site('admin/galleries_images/upload/'.$gallery['id']);?>",
			scriptData: {
				XS: "<?=session_id();?>"
			},
			uploader: "<?=URL::base();?>assets/swf/cms/uploadify.swf",
			onAllComplete: function(){
				//document.location.href = document.location.href;
			}
		});
	});
</script>

<script type="text/javascript" src="<?=URL::base();?>assets/js/cms/ckeditor/ckeditor.js"></script>
<script type="text/javascript" src="<?=URL::base();?>assets/js/cms/ckeditor/adapters/jquery.js"></script>
<script type="text/javascript">
	admin.tree.active = <?=$gallery ? $gallery['section_id'] : $section_id;?>;
	$(function(){
		admin.WYSIWYG.create("textarea.pb");
	});
</script>
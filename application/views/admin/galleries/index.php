<div class="content-area">
	<div class="path">Galerijas</div>
	<div class="table">
		<div class="add-btn"><a href="<?=URL::site('admin/galleries/add/'.$section_id);?>" title="Add">Add</a></div>
		<div class="table-header-l">
			<div class="table-header">
				<div class="p-70 left"><div>Nosaukums</div></div>
				<div class="p-20 left centered"><div>Attēli</div></div>
				<div class="p-10 left centered">Dzēst</div>
			</div>
		</div>
		<div class="table-data">
		<?
		$i = 0;
		foreach($galleries as $gallery){
		?>
			<div class="table-item<?if(++$i%2 == 0){?> grey<?}?>">
				<div class="p-70 left cellbg"><div><?=$gallery->cmsEditLink(HTML::chars($gallery->title));?></div></div>
				<div class="p-20 left cellbg centered"><?=$gallery->cmsEditLink($gallery->images->count_all());?></div>
				<div class="p-10 left centered"><?=$gallery->cmsDeleteLink();?></div>
			</div>
		<?}?>
		</div>
	</div>
</div>
<script type="text/javascript">
admin.tree.active = <?=$section_id;?>;
</script>
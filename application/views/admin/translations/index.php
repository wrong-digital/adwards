<form method="post" id="admin" action="<?=URL::site('admin/translations');?>">
	<div class="content-area">
		<div class="path">Tehniskie tulkojumi</div>
		<div class="table">
			<div class="table-header-l">
				<div class="table-header">
					<div class="p-<?=$pw;?> left"><div>Atslēga</div></div>
					<div class="p-<?=$pw;?> left"><div>Sadaļa</div></div>
					<?foreach($languages as $language){?>
					<div class="p-<?=$pw;?> left"><div>Tulkojums (<?=strtoupper($language);?>)</div></div>
					<?}?>
				</div>
			</div>
			<div class="table-data">
			<?
			$i = 0;
			foreach($translations as $translation){
			?>
				<div class="table-item<?if(++$i%2 == 0){?> grey<?}?>">
					<div class="p-<?=$pw;?> left cellbg"><div><?=HTML::chars($translation->key);?></div></div>
					<div class="p-<?=$pw;?> left cellbg"><div><?=$translation->section->loaded() ? HTML::anchor(URL::section($translation->section->id), HTML::chars($translation->section->title)) : '&nbsp;';?></div></div>
					<?foreach($languages as $language){?>
					<div class="p-<?=$pw;?> left cellbg"><div><?=Form::input(sprintf('%s[%s]', $translation->key, $language), $translation->{'str_'.$language});?></div></div>
					<?}?>
				</div>
			<?}?>
			</div>
		</div>
	</div>
	<button class="save" type="submit" style="margin:20px 0 0 35px;">Saglabāt</button>
</form>
<form method="post" id="admin" action="<?=URL::site('admin/settings');?>">
	<div class="insert-area">
		<div class="path">Iestatījumi</div>
		<div class="block-insert p-100">
			<div class="h-exp" id="content-h">
				<div class="title">Iestatījumi</div>
				<div class="arrow"><a href="javascript:;" onclick="admin.block.toggle('content-h', 'content-area');" title="">&nbsp;</a></div>
			</div>
			<div class="insert-content" id="content-area">
				<?foreach($settings as $key=>$value){?>
				<div class="insert-input">
					<div class="label left p-30"><?=$key;?></div>
					<div class="field left p-70"><?=Form::input($key, $value);?></div>
				</div>
				<?}?>
			</div>
		</div>
	</div>
	<div class="insert-right right">
		<div class="block-insert w-259" style="margin:28px 0 0 1px;">
			<div class="h-exp" id="statuss-h">
				<div class="title">Publicēšana</div>
				<div class="arrow"><a href="javascript:;" onclick="admin.block.toggle('statuss-h', 'statuss');">&nbsp;</a></div>
			</div>
			<div class="rightblock-content" id="statuss">
				<div class="statuss-btns"><button type="save" class="save" style="margin-left:65px;">Saglabāt</button></div>
			</div>
		</div>
	</div>
</form>
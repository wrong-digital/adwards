<div class="content-area">
    <div class="path">Iesūtīto darbu apraksti</div>
    <div class="table">
        <div class="add-btn">&nbsp;</div>
        <div class="table-header-l">
            <div class="table-header">
                <div class="p-6 left"><div>ID</div></div>
                <div class="p-22 left"><div>Nosaukums</div></div>
                <div class="p-36 left"><div>Apraksts LV</div></div>
                <div class="p-36 left"><div>Apraksts EN</div></div>
            </div>
        </div>
        <div class="table-data">
        <?
        $i = 0;
        foreach($inserts as $p){
        ?>
            <div class="table-item<?if(++$i%2 == 0){?> grey<?}?>">
                <div class="p-6 left cellbg"><div><a href="/admin/inserts/edit/<?=$p->id;?>" title="">item_<?=$p->id;?></a></div></div>
                <div class="p-22 left cellbg"><div><a title=""><?=$p->title_lv.' /<br />'.$p->title_en;?></a></div></div>
                <div class="p-36 left cellbg"><div><a title=""><?=$p->description_lv;?></a></div></div>
                <div class="p-36 left cellbg"><div><a title=""><?=$p->description_en;?></a></div></div>
            </div>
        <?}?>
        </div>
    </div>
</div>
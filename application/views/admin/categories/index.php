<div class="content-area">
	<div class="path">Kategorijas</div>
	<div class="table">
		<div class="add-btn"><a href="<?=URL::site('admin/categories/add/'.$section_id);?>" title="Pievienot">Pievienot</a></div>
		<div class="table-header-l">
			<div class="table-header">
				<div class="p-70 left"><div>Nosaukums</div></div>
				<div class="p-10 left centered"><div>Apakšsadaļas</div></div>
				<div class="p-10 left centered">Aktīvs</div>
				<div class="p-10 left centered">Dzēst</div>
			</div>
		</div>
		<div class="table-data">
		<?
		$i = 0;
		foreach($categories as $p){
		?>
			<div class="table-item<?if(++$i%2 == 0){?> grey<?}?>" id="prod" data="<?=$p->id;?>">
				<div class="p-70 left cellbg"><div><a href="<?=URL::site('admin/categories/edit/'.$p->id);?>" title=""><?=$p->title;?></a></div></div>
				<div class="p-10 left centered"><div><a href="<?=URL::site('admin/formats/view/'.$p->id);?>" title=""><img src="<?=URL::site();?>assets/css/cms/img/table-ico-list.png" alt="" /></a></div></div>
				<div class="p-10 left centered cellbg"><?=$p->cmsStatusIcon();?></div>
				<div class="p-10 left centered"><a href="<?=URL::site('admin/categories/delete/'.$p->id);?>" title=""><img src="<?=URL::site();?>assets/css/cms/img/table-ico-delete.png" alt="" /></a></div>
			</div>
		<?}?>
		</div>
	</div>
</div>
<script type="text/javascript">
$(".table-data").sortable({
	update: function() {
		var data = new Array();
		$(".table-data #prod").each(function(){
			data.push($(this).attr("data"));
		});
		$.post(admin.url.site+"admin/categories/sort", {
			data: data
		});
	}
});
admin.tree.active = <?=$section_id;?>;
</script>
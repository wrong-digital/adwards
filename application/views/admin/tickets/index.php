<div class="content-area">
	<div class="path">Biļetes</div>
	<p>Šajā lapā ir veiktas izmaiņas speciāli Anetei, lai varētu pārredzamāk savilkt kopā visus, kuriem jāmaksā par Adwards 2013 pasākumiem. Šis man prasīja vismaz pusi dienas, tā ka enjoy.</p>
	<p>Lai dabūtu atpakaļ uz veco versiju, jāatkomentē dažas rindas/bloki failos controller/admin/tickets.php un views/admin/tickets/index.php.</p>
	<p></p>
	<p>No saraksta laukā izfiltrēti LADC biedri, lietotāji, kuri uz lekcijām nav pieteikušies un uz ceremoniju ieeja bija bezmaksas (vai nu ir iesniegti darbi vai arī ielūgums), vai arī apmaksa bijusi veiksmīga (datubāzē uzrādas gan transakcijas ID gan arī Status = 1).</p>
	<p>Sarakstā uzrādās visi, kas bija pieteikušies. Mēs no datubāzes nekādi nevaram zināt, kuri no šiem lietotājiem patiešām bija ieradušies pasākumos, un vai viņiem patiešām ir jāmaksā.</p>
	<p>Nav īsti skaidrs, kas ir noticis ar tiem dažiem ierakstiem, kuri augšā uzrādas bez lietotāja.</p>
	<p>Daudziem lietotājiem ir vairāki ieraksti - Man pagaidām vēl nav īsti skaidrs, kādēļ tā, pajautāšu Aigaram.</p>
	<div class="table">
		<!--<div class="add-btn">&nbsp;</div>
		<div class="table-header-l">
			<div class="table-header">
				<div class="p-30 left"><div>Lietotājs</div></div>
				<div class="p-20 left"><div>Lekcijas</div></div>
				<div class="p-20 left"><div>Ceremonija</div></div>
				<div class="p-20 left"><div>Apmaksa</div></div>
			</div>
		</div>
		<div class="table-data">
		<?
		
		//$i = 0;
		//foreach($tickets as $p){
		?>
			<div class="table-item<?if(++$i%2 == 0){?> grey<?}?>">
				<div class="p-30 left cellbg"><div><a href="/admin/users/edit/<?=$p->user_id;?>" title=""><?=$users[$p->user_id];?></a></div></div>
				<div class="p-20 left cellbg"><div><?=$p->apply_1 == 1 ? 'Jā' : 'Nē';?></div></div>
				<div class="p-20 left cellbg"><div><?=$p->apply_2 == 1 ? 'Jā' : 'Nē';?></div></div>
				<div class="p-20 left cellbg"><div><?=$p->trans_id != '' ? ($p->bill == 1 ? 'Jā (ar rēķinu)' : 'Jā (bez rēķina)') : 'Nē';?></div></div>
			</div>
		<?//}?>
		</div>
		
		-->
		<?
		
		// Varis added for bill generation.
		// !!! IMPORTANT !!! For this to work, you need to comment out lines 6-8 in controller/admin/tickets.php
		
		 ?>

		<div class="table-header-l">
			<div class="table-header">
				<div class="p-5 left"><div>N.p.k</div></div>
				<div class="p-10 left"><div>Lietotājs</div></div>
				<div class="p-7 left"><div>Lekcijas</div></div>
				<div class="p-8 left"><div>Ceremonija</div></div>
				<div class="p-7 left"><div>Laiks</div></div>
				<div class="p-10 left"><div>Apmaksāts?</div></div>
				<div class="p-5 left"><div>Jāmaksā</div></div>
				<!--<div class="p-10 left"><div>Banka</div></div>-->
				<div class="p-15 left"><div>Konts</div></div>
				<div class="p-10 left"><div>P.K / R.N</div></div>
				<div class="p-20 left"><div>Epasts</div></div>
			</div>
		</div>
		<div class="table-data">
		<?
		$i = 0;
		foreach($users as $user){
			$id = $user->id;
			$name = $user->private_name != '' ? $user->private_name : $user->legal_title;
			$bank = $user->bank;
			$acc = $user->accaunt;
			$pcln = $user->private_code != '' ? $user->private_code : $user->legal_number;
			$email = $user->email;
			$primem = $user->private_member;
			$lemem = $user->legal_member;
			$free = 0;
			
			if ($primem != 1 || $lemem != 1){ // not LADC member
				$tickets = ORM::factory('tickets')->where('user_id', '=', $id)->find_all()->as_array();
				if (count($tickets) > 0) { // has tickets registered
					$inserts = ORM::factory('inserts')->where('user_id', '=', $id)->where('order_status', '>', 0)->count_all();
					if ($inserts > 0){ // user has cases submitted
						$free = 1;
					}
					$invite = ORM::factory('invites')->where('email', '=', $email)->count_all();
					if($invite > 0){  // user has an invite
						$free = 1;
					}
					
					foreach($tickets as $p){
						if ($p->trans_id == '' || $p->status == 0){ // payment hasnt been made or transaction hasnt been successfully completed yet
							if ($p->apply_1 == 1 || $p->apply_2 == 1 && $free == 0) { // user has applied for lectures or doesnt have a free pass to ceremony
								$price = 0;
								if ($p->apply_1 == 1){
									if($user->type == 2){
										$price =+ 130;
									}else if ($user->type == 1){
										$price =+ 65;
									}
								}else{
									if ($p->apply_2 == 1 && $free == 0){
										if($user->type == 2){
											$price =+ 18;
										}else{
											$price =+ 12;
										}
									}
								}
								
							?><div class="table-item<?if(++$i%2 == 0){?> grey<?}?>">
								<div class="p-5 left cellbg"><div><?=$i;?></div></div>
								<div class="p-10 left cellbg"><div><a href="<?=URL::site('admin/users/edit/'.$id);?>" title=""><?=$name;?></a></div></div>
								<div class="p-7 left cellbg"><div><?=$p->apply_1 == 1 ? 'Jā' : 'Nē';?></div></div>
								<div class="p-8 left cellbg"><div><?=$p->apply_2 == 1 ? 'Jā' : 'Nē';?></div></div>
								<div class="p-7 left cellbg"><div><?=$p->saved;?></div></div>
								<div class="p-10 left cellbg"><div><?=$p->trans_id != '' ? ($p->bill == 1 ? 'Neveiksmīga transakcija (ar rēķinu)' : 'Neveiksmīga transakcija (bez rēķina)') : 'Nē';?></div></div>
								<div class="p-5 left cellbg"><div><?=$price .' Ls';?></div></div>
								<!--<div class="p-10 left cellbg"><div><?=$bank;?></div></div>-->
								<div class="p-15 left cellbg"><div><?=$acc;?></div></div>
								<div class="p-10 left cellbg"><div><?=$pcln;?></div></div>
								<div class="p-20 left cellbg"><div><?=$email;?></div></div>
							</div><?
							}
						}
					}
				}
			}
		}
		?>
		</div>
		
	</div>
</div>
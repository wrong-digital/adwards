<form method="post" id="admin" action="<?=URL::site('admin/inserts/'.($inserts ? 'edit/'.$inserts['id'] : 'add'));?>" enctype="multipart/form-data">
	<div class="insert-area">
		<div class="path">Iesūtītie darbi</div>
		<div class="block-insert p-100" style="padding-top:6px;">
			<div class="h-exp" id="content-h">
				<div class="title">Iesūtītie darbi</div>
				<div class="arrow"><a href="javascript:;" onclick="admin.block.toggle('content-h','content-area');" title="">&nbsp;</a></div>
			</div>
			<div class="insert-content" id="content-area">
			
				<div class="insert-input">
					<div class="label left p-30">ID</div>
					<div class="field left p-70"><?=$inserts['code'].'_'.$inserts['id'];?></div>
				</div>
				
				<div class="insert-input">
					<div class="label left p-30">Apmaksāts / apstiprināts</div>
					<div class="field left p-70"><?=$inserts['order_status'] > 0 ? 'Jā' : 'Nē';?></div>
				</div>
				
				<div class="insert-input">
					<div class="label left p-30">Limbo darbs</div>
					<div class="field left p-70"><?=$inserts['noprice'] == 1 ? 'Jā' : 'Nē';?></div>
				</div>
				
				<div class="insert-input">
					<div class="label left p-30">Darbs uz Antalis papīra</div>
					<div class="field left p-70"><?=$inserts['antalis'] == 1 ? 'Jā' : 'Nē';?></div>
				</div>
				
				<div class="insert-input">
					<div class="label left p-30">Pamatkategorija</div>
					<div class="field left p-70">
						<?foreach($categories as $c){?>
							<?if($c->id == $inserts['main_category']){?>
								<?=$c->title;?>
							<? break; } ?>
						<?}?>
					</div>
				</div>
				<div class="insert-input">
					<div class="label left p-30">Apakškategorija</div>
					<div class="field left p-70">
						<?foreach($formats as $f){?>
							<?if($f->id == $inserts['sub_category']){?>
								<?=$f->title;?>
							<? break; } ?>
						<?}?>
					</div>
				</div>
				
				
				<div class="insert-input">
					<div class="label left p-30">Nosaukums latviski</div>
					<div class="field left p-70"><?=Form::input('title_lv', $inserts ? $inserts['title_lv'] : '', array());?></div>
				</div>
				<div class="insert-input">
					<div class="label left p-30">Nosaukums angliski</div>
					<div class="field left p-70"><?=Form::input('title_en', $inserts ? $inserts['title_en'] : '', array());?></div>
				</div>
				<div class="insert-input">
					<div class="label left p-30">Klients latviski</div>
					<div class="field left p-70"><?=Form::input('client_lv', $inserts ? $inserts['client_lv'] : '', array());?></div>
				</div>
				<div class="insert-input">
					<div class="label left p-30">Klients angliski</div>
					<div class="field left p-70"><?=Form::input('client_en', $inserts ? $inserts['client_en'] : '', array());?></div>
				</div>
				<div class="insert-input">
					<div class="label left p-30">Produkts latviski</div>
					<div class="field left p-70"><?=Form::input('product_lv', $inserts ? $inserts['product_lv'] : '', array());?></div>
				</div>
				<div class="insert-input">
					<div class="label left p-30">Produkts angliski</div>
					<div class="field left p-70"><?=Form::input('product_en', $inserts ? $inserts['product_en'] : '', array());?></div>
				</div>
				<div class="insert-input">
					<div class="label left p-30">Apraksts latviski</div>
					<div class="field left p-70">
						<?=Form::textarea('description_lv', $inserts ? $inserts['description_lv'] : '', array('style' => 'height:125px;'));?>
					</div>
				</div>
				<div class="insert-input">
					<div class="label left p-30">Apraksts angliski</div>
					<div class="field left p-70">
						<?=Form::textarea('description_en', $inserts ? $inserts['description_en'] : '', array('style' => 'height:125px;'));?>
					</div>
				</div>
				<div class="insert-input">
					<div class="label left p-30">Radošais direktors</div>
					<div class="field left p-70"><?=Form::input('creative_1', $inserts ? $inserts['creative_1'] : '', array());?></div>
				</div>
				<div class="insert-input">
					<div class="label left p-30">Radošais direktors 2</div>
					<div class="field left p-70"><?=Form::input('creative_2', $inserts ? $inserts['creative_2'] : '', array());?></div>
				</div>
				<div class="insert-input">
					<div class="label left p-30">Mākslinieciskais direktors</div>
					<div class="field left p-70"><?=Form::input('art_1', $inserts ? $inserts['art_1'] : '', array());?></div>
				</div>
				<div class="insert-input">
					<div class="label left p-30">>Projekta vadītājs</div>
					<div class="field left p-70"><?=Form::input('art_2', $inserts ? $inserts['art_2'] : '', array());?></div>
				</div>
				<div class="insert-input">
					<div class="label left p-30">Dizainers</div>
					<div class="field left p-70"><?=Form::input('designer_1', $inserts ? $inserts['designer_1'] : '', array());?></div>
				</div>
				<div class="insert-input">
					<div class="label left p-30">Dizainers2</div>
					<div class="field left p-70"><?=Form::input('designer_2', $inserts ? $inserts['designer_2'] : '', array());?></div>
				</div>
				<div class="insert-input">
					<div class="label left p-30">Programmētājs</div>
					<div class="field left p-70"><?=Form::input('programmer_1', $inserts ? $inserts['programmer_1'] : '', array());?></div>
				</div>
				<div class="insert-input">
					<div class="label left p-30">Programmētājs 2</div>
					<div class="field left p-70"><?=Form::input('programmer_2', $inserts ? $inserts['programmer_2'] : '', array());?></div>
				</div>
				<div class="insert-input">
					<div class="label left p-30">Tekstu autors</div>
					<div class="field left p-70"><?=Form::input('text_1', $inserts ? $inserts['text_1'] : '', array());?></div>
				</div>
				<div class="insert-input">
					<div class="label left p-30">Tekstu autors 2</div>
					<div class="field left p-70"><?=Form::input('text_2', $inserts ? $inserts['text_2'] : '', array());?></div>
				</div>
				<div class="insert-input">
					<div class="label left p-30">Fotogrāfs</div>
					<div class="field left p-70"><?=Form::input('photographer_1', $inserts ? $inserts['photographer_1'] : '', array());?></div>
				</div>
				<div class="insert-input">
					<div class="label left p-30">Fotogrāfs 2</div>
					<div class="field left p-70"><?=Form::input('photographer_2', $inserts ? $inserts['photographer_2'] : '', array());?></div>
				</div>
				<div class="insert-input">
					<div class="label left p-30">Režisors</div>
					<div class="field left p-70"><?=Form::input('rezisors_1', $inserts ? $inserts['rezisors_1'] : '', array());?></div>
				</div>
				<div class="insert-input">
					<div class="label left p-30">Cits (norādiet vārdu un lomu projektā)</div>
					<div class="field left p-70"><?=Form::input('rezisors_2', $inserts ? $inserts['rezisors_2'] : '', array());?></div>
				</div>
				<div class="insert-input">
					<div class="label left p-30">Piezīmes</div>
					<div class="field left p-70">
						<?=Form::textarea('notes', $inserts ? $inserts['notes'] : '', array('style' => 'height:125px;'));?>
					</div>
				</div>
				
				<div class="insert-input">&nbsp;</div>
				
				<?if($inserts['zip'] != ''){?>
				<div class="insert-input">
					<div class="label left p-30">Arhīva fails</div>
					<div class="field left p-70"><a href="/assets/upload/jobs/<?=$inserts['zip'];?>" target="_blank">Ielādēt</a></div>
				</div>
				<?}?>
				
				<?if($inserts['audio'] != ''){?>
				<div class="insert-input">
					<div class="label left p-30">Audio fails</div>
					<div class="field left p-70"><a href="/assets/upload/jobs/<?=$inserts['audio'];?>" target="_blank">Ielādēt</a></div>
				</div>
				<?}?>
				
				<?if($inserts['video'] != ''){?>
				<div class="insert-input">
					<div class="label left p-30">Video fails</div>
					<div class="field left p-70"><a href="/assets/upload/jobs/<?=$inserts['video'];?>" target="_blank">Ielādēt</a></div>
				</div>
				<?}?>
				
				<?if($inserts['image_book_1'] != ''){?>		
				<div class="insert-input">
					<div class="label left p-30">Attēls gramatai #1</div>
					<div class="field left p-70"><a href="/assets/upload/jobs/<?=$inserts['image_book_1'];?>" target="_blank">Ielādēt</a></div>
				</div>
				<?}?>
				
				<?if($inserts['image_book_2'] != ''){?>
				<div class="insert-input">
					<div class="label left p-30">Attēls gramatai #2</div>
					<div class="field left p-70"><a href="/assets/upload/jobs/<?=$inserts['image_book_2'];?>" target="_blank">Ielādēt</a></div>
				</div>
				<?}?>
				
				<?if($inserts['image_book_3'] != ''){?>
				<div class="insert-input">
					<div class="label left p-30">Attēls gramatai #3</div>
					<div class="field left p-70"><a href="/assets/upload/jobs/<?=$inserts['image_book_3'];?>" target="_blank">Ielādēt</a></div>
				</div>
				<?}?>
				
				<?if($inserts['image_book_4'] != ''){?>
				<div class="insert-input">
					<div class="label left p-30">Attēls gramatai #4</div>
					<div class="field left p-70"><a href="/assets/upload/jobs/<?=$inserts['image_book_4'];?>" target="_blank">Ielādēt</a></div>
				</div>
				<?}?>
				
				<?if($inserts['image_book_5'] != ''){?>
				<div class="insert-input">
					<div class="label left p-30">Attēls gramatai #5</div>
					<div class="field left p-70"><a href="/assets/upload/jobs/<?=$inserts['image_book_5'];?>" target="_blank">Ielādēt</a></div>
				</div>
				<?}?>
				
				<?if($inserts['image_book_6'] != ''){?>
				<div class="insert-input">
					<div class="label left p-30">Attēls gramatai #6</div>
					<div class="field left p-70"><a href="/assets/upload/jobs/<?=$inserts['image_book_6'];?>" target="_blank">Ielādēt</a></div>
				</div>
				<?}?>
				
				<?if($inserts['image_web_1'] != ''){?>
				<div class="insert-input">
					<div class="label left p-30">Attēls webam #1</div>
					<div class="field left p-70"><a href="/assets/upload/jobs/<?=$inserts['image_web_1'];?>" target="_blank">Ielādēt</a></div>
				</div>
				<?}?>
				
				<?if($inserts['image_web_2'] != ''){?>
				<div class="insert-input">
					<div class="label left p-30">Attēls webam #2</div>
					<div class="field left p-70"><a href="/assets/upload/jobs/<?=$inserts['image_web_2'];?>" target="_blank">Ielādēt</a></div>
				</div>
				<?}?>
				
				<?if($inserts['image_web_3'] != ''){?>
				<div class="insert-input">
					<div class="label left p-30">Attēls webam #3</div>
					<div class="field left p-70"><a href="/assets/upload/jobs/<?=$inserts['image_web_3'];?>" target="_blank">Ielādēt</a></div>
				</div>
				<?}?>
				
				<?if($inserts['image_web_4'] != ''){?>
				<div class="insert-input">
					<div class="label left p-30">Attēls webam #4</div>
					<div class="field left p-70"><a href="/assets/upload/jobs/<?=$inserts['image_web_4'];?>" target="_blank">Ielādēt</a></div>
				</div>
				<?}?>
				
				<?if($inserts['image_web_5'] != ''){?>
				<div class="insert-input">
					<div class="label left p-30">Attēls webam #5</div>
					<div class="field left p-70"><a href="/assets/upload/jobs/<?=$inserts['image_web_5'];?>" target="_blank">Ielādēt</a></div>
				</div>
				<?}?>
				
				<?if($inserts['image_web_6'] != ''){?>
				<div class="insert-input">
					<div class="label left p-30">Attēls webam #6</div>
					<div class="field left p-70"><a href="/assets/upload/jobs/<?=$inserts['image_web_6'];?>" target="_blank">Ielādēt</a></div>
				</div>
				<?}?>
				
			</div>
		</div>
	</div>
	<div class="insert-right right">
		<div class="block-insert w-259" style="margin:35px 0 0 1px;">
			<div class="h-exp" id="statuss-h">
				<div class="title">Publicēšana</div>
				<div class="arrow"><a href="javascript:;" onclick="admin.block.toggle('statuss-h','statuss');" title="">&nbsp;</a></div>
			</div>
			<div class="rightblock-content" id="statuss">
				<div class="statuss" style="margin-bottom: 5px;">
					<div class="label">Statuss:</div>
					<label><?=Form::radio('status', ORM::STATUS_ACTIVE, $inserts ? $inserts['status'] == ORM::STATUS_ACTIVE : false, array('style' => 'width: auto;'));?> Publicēts</label>
					<label><?=Form::radio('status', ORM::STATUS_DRAFT, $inserts ? $inserts['status'] == ORM::STATUS_DRAFT : true, array('style' => 'width: auto;'));?> Melnraksts</label>
					<label><?=Form::radio('status', ORM::STATUS_HIDDEN, $inserts ? $inserts['status'] == ORM::STATUS_HIDDEN : false, array('style' => 'width: auto;'));?> Slēpts</label>
				</div>
				<div class="statuss-btns"><button class="save" type="submit" style="margin-left:65px;">Saglabāt</button></div>
			</div>
		</div>
	</div>
</form>
<script type="text/javascript" src="<?=URL::base();?>assets/js/cms/ckeditor/ckeditor.js"></script>
<script type="text/javascript" src="<?=URL::base();?>assets/js/cms/ckeditor/adapters/jquery.js"></script>
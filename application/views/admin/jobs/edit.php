<form method="post" id="admin" action="<?=URL::site('admin/jobs/'.($jobs ? 'edit/'.$jobs['id'] : 'add'));?>" enctype="multipart/form-data">
	<div class="insert-area">
		<div class="path">Iesniegtie darbi</div>
		<div class="block-insert p-100" style="padding-top:6px;">
			<div class="h-exp" id="content-h">
				<div class="title">Iesniegtie darbi</div>
				<div class="arrow"><a href="javascript:;" onclick="admin.block.toggle('content-h','content-area');" title="">&nbsp;</a></div>
			</div>
			<div class="insert-content" id="content-area">
				<div class="insert-input">
					<div class="label left p-30">Virsraksts</div>
					<div class="field left p-70"><?=Form::input('title', $jobs ? $jobs['title'] : '', array('onkeyup' => 'admin.utils.title.rewrite(this)'));?></div>
				</div>
				<div class="insert-input">
					<div class="label left p-30">Saite</div>
					<div class="field left p-70"><?=Form::input('link', $jobs ? $jobs['link'] : '');?></div>
				</div>
				<div class="insert-input">
					<div class="label left p-30">Kategorija</div>
					<div class="field left p-70">
						<select name="category_id" id="category_id">
						    <?foreach($categories as $c){?>
							<option value="<?=$c->id;?>"<?=$jobs && $jobs['category_id'] == $c->id ? ' selected="selected"' : '';?>><?=$c->title;?></option>
							<?}?>
						</select>
					</div>
				</div>
				<div class="insert-input">
					<div class="label left p-30">Iesniedza</div>
					<div class="field left p-70">
						<select name="user_id" id="user_id">
						    <?foreach($users as $u){?>
							<option value="<?=$u->id;?>"<?=$jobs && $jobs['user_id'] == $u->id ? ' selected="selected"' : '';?>><?=$u->private_name;?><?=$u->legal_title;?></option>
							<?}?>
						</select>
					</div>
				</div>
				<div class="insert-input">
					<div class="label left p-30">Teksts</div>
					<div class="field left p-70"><?=Form::textarea('description', $jobs ? $jobs['description'] : '', array('class' => 'pb'));?></div>
				</div>
				<?=Form::hidden('section_id', $jobs ? $jobs['section_id'] : $section_id);?>
			</div>
		</div>
	</div>
	<div class="insert-right right">
		<div class="block-insert w-259" style="margin:35px 0 0 1px;">
			<div class="h-exp" id="statuss-h">
				<div class="title">Publicēšana</div>
				<div class="arrow"><a href="javascript:;" onclick="admin.block.toggle('statuss-h','statuss');" title="">&nbsp;</a></div>
			</div>
			<div class="rightblock-content" id="statuss">
				<div class="statuss" style="margin-bottom: 5px;">
					<div class="label">Status:</div>
					<label><?=Form::radio('status', ORM::STATUS_ACTIVE, $jobs ? $jobs['status'] == ORM::STATUS_ACTIVE : false, array('style' => 'width: auto;margin-left: 10px;'));?> Publicēts</label>
					<label><?=Form::radio('status', ORM::STATUS_DRAFT, $jobs ? $jobs['status'] == ORM::STATUS_DRAFT : true, array('style' => 'width: auto;'));?> Melnraksts</label>
					<label><?=Form::radio('status', ORM::STATUS_HIDDEN, $jobs ? $jobs['status'] == ORM::STATUS_HIDDEN : false, array('style' => 'width: auto;'));?> Slēpts</label>
				</div>
				<div class="statuss-btns"><button class="save" type="submit" style="margin-left:65px;">Saglabāt</button></div>
			</div>
		</div>
		
		<div class="block-insert w-259" style="margin:15px 0 0 1px;">
			<div class="h-exp" id="statuss-h">
				<div class="title">Attēli</div>
				<div class="arrow"><a href="javascript:;" onclick="admin.block.toggle('statuss-h','statuss');" title="">&nbsp;</a></div>
			</div>
			<div class="rightblock-content" id="statuss">
				<div class="statuss" style="margin-bottom: 5px;">
					<div class="label">Attēls #1:</div>
					<?if(!empty($jobs['image_1_144_126'])){?><img src="<?=URL::base();?>assets/media/images/<?=$jobs['image_1_144_126'];?>" alt="" style="display: block; margin: 0 auto 10px auto; max-width: 240px;" /><?}?>
					<?=Form::file('image_1', array('style' => 'margin: 0 0 10px 10px;'));?>
				</div>
				
				
				<div class="statuss" style="margin-bottom: 5px;">
					<div class="label">Attēls #2:</div>
					<?if(!empty($jobs['image_2_144_126'])){?><img src="<?=URL::base();?>assets/media/images/<?=$jobs['image_2_144_126'];?>" alt="" style="display: block; margin: 0 auto 10px auto; max-width: 240px;" /><?}?>
					<?=Form::file('image_2', array('style' => 'margin: 0 0 10px 10px;'));?>
				</div>
				
				<div class="statuss" style="margin-bottom: 5px;">
					<div class="label">Attēls #3:</div>
					<?if(!empty($jobs['image_3_144_126'])){?><img src="<?=URL::base();?>assets/media/images/<?=$jobs['image_3_144_126'];?>" alt="" style="display: block; margin: 0 auto 10px auto; max-width: 240px;" /><?}?>
					<?=Form::file('image_3', array('style' => 'margin: 0 0 10px 10px;'));?>
				</div>
				
				<div class="statuss" style="margin-bottom: 5px;">
					<div class="label">Attēls #4:</div>
					<?if(!empty($jobs['image_4_144_126'])){?><img src="<?=URL::base();?>assets/media/images/<?=$jobs['image_4_144_126'];?>" alt="" style="display: block; margin: 0 auto 10px auto; max-width: 240px;" /><?}?>
					<?=Form::file('image_4', array('style' => 'margin: 0 0 10px 10px;'));?>
				</div>
				
			</div>
		</div> 
	</div>
</form>
<script type="text/javascript" src="<?=URL::base();?>assets/js/cms/ckeditor/ckeditor.js"></script>
<script type="text/javascript" src="<?=URL::base();?>assets/js/cms/ckeditor/adapters/jquery.js"></script>
<script type="text/javascript">
	admin.tree.active = <?=$jobs ? $jobs['section_id'] : $section_id;?>;
	$(function(){
		admin.WYSIWYG.create("textarea.pb");
	});
</script>
<div class="content-area">
	<div class="path">Iesniegtie darbi</div>
	<div class="table">
		<div class="add-btn"><a href="<?=URL::site('admin/jobs/add/'.$section_id);?>" title="Add">Add</a></div>
		<div class="table-header-l">
			<div class="table-header">
				<div class="p-80 left"><div>Virsraksts</div></div>
				<div class="p-10 left centered">Aktīvs</div>
				<div class="p-10 left centered">Dzēst</div>
			</div>
		</div>
		<div class="table-data">
		<?
		$i = 0;
		foreach($jobs as $jobs_item){
		?>
			<div class="table-item<?if(++$i%2 == 0){?> grey<?}?>">
				<div class="p-80 left cellbg"><div><a href="<?=URL::site('admin/jobs/edit/'.$jobs_item->id);?>" title=""><?=$jobs_item->title;?></a></div></div>
				<div class="p-10 left centered cellbg"><?=$jobs_item->cmsStatusIcon();?></div>
				<div class="p-10 left centered"><a href="<?=URL::site('admin/jobs/delete/'.$jobs_item->id);?>" title=""><img src="<?=URL::site();?>assets/css/cms/img/table-ico-delete.png" alt="" /></a></div>
			</div>
		<?}?>
		</div>
	</div>
</div>
<script type="text/javascript">
admin.tree.active = <?=$section_id;?>;
</script>
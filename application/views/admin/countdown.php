<form method="post" id="countdown-set" action="<?=URL::site('admin/countdown/update');?>" enctype="multipart/form-data">
    <div class="insert-area">
                <div class="path">Atskaite</div>
        <div class="block-insert p-100">

			<div class="h-exp" id="content-h">
                            <div class="title">Laika atskaites uzstādījumi</div>

				<div class="arrow"><a href="javascript:;" onclick="admin.block.toggle('content-h', 'content-area');">&nbsp;</a></div>
                        </div>
                            <div class="insert-content" id="content-area">
        <div class="insert-input">
            <div class="label left p-10">Ziņojums:</div>
            <div class="field left p-70"><input type="text" name="message" placeholder="Ziņojums" value="<?=($message ? $message : null);?>"></div>
        </div>
        <div class="insert-input">
                <input id="timepicker" type="text" name="target" value="<?=$target;?>">
        </div>
        <div class="insert-input">
            <div class="label left p-20">Laika atskaite:</div>
            <div class="field left p-3"><input type="checkbox" name="enabled" id="countdown-enable" <?=($enabled ? 'checked' : null);?>></div>
        </div>
                            </div>
                        </div>
        </div>
</form>

<script>
    $(function() {
        var enabled = <?=$enabled;?>;
        $('#countdown-enable').click(function() {
            if($(this).prop('checked')) { enabled = 1; }
            else { enabled = 0; }
        });
        $('#timepicker').datetimepicker({
            lang: 'lv',
            inline: true,
            minDate:'0'
        });


        $('#countdown-enable').click(function() {
            $.post($('#countdown-set').attr('action'), 
                {
                    enabled: enabled,
                    target: $('input#timepicker').val(),
                    message: $('input[name=message]').val()
                    
                }, function(data){});
                });
        $('#timepicker, input[name=message]').change(function() {
            $.post($('#countdown-set').attr('action'), 
                {
                    enabled: enabled,
                    target: $('input#timepicker').val(),
                    message: $('input[name=message]').val()
                    
                }, function(data){});
        });
    });
</script>
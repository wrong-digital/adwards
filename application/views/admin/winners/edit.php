<form method="post" id="admin" action="<?=URL::site('admin/winners/'.($winner ? 'edit/'.$winner->id : 'add'));?>" enctype="multipart/form-data">

	<div class="insert-area">

		<div class="path"><?=(@$shortlist ? 'Shortlist' : 'Uzvarētāji');?></div>

		<div class="block-insert p-100" style="padding-top:6px;">

			<div class="h-exp" id="content-h">

				<div class="title"><?=(@$shortlist ? 'Nominants' : 'Uzvarētājs');?></div>

				<div class="arrow"><a href="javascript:;" onclick="admin.block.toggle('content-h','content-area');" title="">&nbsp;</a></div>

			</div>

			<div class="insert-content" id="content-area">
                                <?if(!@$shortlist) {?>
                                    <div class="insert-input">
                                            <div class="label left p-30">Gads</div>
                                            <div class="field left p-70">
                                                <select name="year">
                                                    <?foreach($years as $year => $data) {?>
                                                        <option value="<?=$year;?>"<?if($winner && $winner->year == $year) echo ' selected'; ?>><?=$year?></option>
                                                    <?}?>
                                                </select>
                                            </div>
                                    </div>
                                <?}?>
                                <div class="insert-input job_reference" <? if(!($winner && $winner->year == 2015) && !@$shortlist) echo 'style="display: none;"' ?>>
					<div class="label left p-30">Iesniegtais darbs (Adwards2015)</div>
					<div class="field left p-70"><?=Form::input('job', $winner ? $winner->_job->title_lv : null, array('id' => 'job-search'));?></div>
				</div>
                                <?if(!@$shortlist) {?>
                                    <div class="insert-input">
                                            <div class="label left p-30">Godalga</div>
                                            <div class="field left p-70">
                                                <select name="award">
                                                <?foreach($awards as $award) {?>
                                                    <option value="<?=$award->id;?>" class="<?=($award->year > 0 ? $award->year : 'all-years');?>"<?if($winner && $winner->award == $award->id) echo ' selected'; ?>><?=$award->title?></option>
                                                <?}?>
                                                </select>
                                            </div>
                                    </div>
                                <?} else {?>
                                    <div class="insert-input">
                                            <div class="label left p-30">Apakškategorija</div>
                                            <div class="field left p-70"><?=Form::input('sub_category', $winner ? $winner->sub_category : '');?></div>
                                    </div>
                                <?}?>
                                <section class="manual-fields"<? if(($winner && $winner->year == 2015) || @$shortlist) echo 'style="display: none;"' ?>>
                                <div class="insert-input">
					<div class="label left p-30">Kategorija</div>
                                        <div class="field left p-70"><select name="category">
                                        <?foreach($years as $year => $data) {
                                            $n = 0;
                                            foreach($data as $cat) {?>
                                                <option value="<?=$year.'-'.$n;?>" class="<?=$year;?>"<?if($winner && $winner->year == $year && $winner->category == $cat) echo ' selected'; ?>><?=$cat;?></option>
                                            <?$n++;}}?>
                                        </select></div>
				</div>
                                <div class="insert-input">
					<div class="label left p-30">Nosaukums</div>
					<div class="field left p-70"><?=Form::input('title', $winner ? $winner->title : '');?></div>
				</div>
                                <div class="insert-input">
					<div class="label left p-30">Klients</div>
					<div class="field left p-70"><?=Form::input('client', $winner ? $winner->client : '');?></div>
				</div>
                                <div class="insert-input">
					<div class="label left p-30">Iesniedzējs</div>
					<div class="field left p-70"><?=Form::input('author', $winner ? $winner->author : '');?></div>
				</div>
                                </section>

			</div>

		</div>

	</div>
        <div class="insert-right right">

		<div class="block-insert w-259" style="margin:35px 0 0 1px;">

			<div class="h-exp" id="media-h">

				<div class="title">Mediji</div>

				<div class="arrow"><a href="javascript:;" onclick="admin.block.toggle('media-h','winner-media');" title="">&nbsp;</a></div>

			</div>

			<div class="rightblock-content" id="winner-media">
                                <section class="manual-fields">
				<div class="statuss" style="margin-bottom: 5px;">
                                        
					<div class="label">Attēls:</div>

					<?if(!empty($winner->thumbnail)){?><img src="<?=URL::base();?>assets/media/images/<?=$winner->thumbnail;?>" alt="" style="display: block; margin: 0 auto 10px auto; max-width: 240px;" /><?}?>

					<?=Form::file('picture', array('style' => 'margin: 0 0 10px 10px;'));?>
                                        
				</div>
                                <div class="statuss" style="margin-bottom: 5px;">
                                        
					<div class="label">Video:</div>

					<?if(!empty($winner->video)){?><?}?>

					<?=Form::file('video', array('style' => 'margin: 0 0 10px 10px;'));?>
                                        
				</div>
                                <div class="statuss" style="margin-bottom: 5px;">
                                        
					<div class="label">Audio:</div>

					<?if(!empty($winner->audio)){?><?}?>

					<?=Form::file('audio', array('style' => 'margin: 0 0 10px 10px;'));?>
                                        
				</div>
                                </section>

			</div>
                    
                    	<div class="h-exp" id="statuss-h" style="margin-top:10px">

				<div class="title">Publicēšana</div>

				<div class="arrow"><a href="javascript:;" onclick="admin.block.toggle('statuss-h','statuss');" title="">&nbsp;</a></div>

			</div>
                        <div class="rightblock-content" id="statuss">

				<div class="statuss" style="margin-bottom: 5px;">

					<div class="label">Statuss:</div>

					<label><?=Form::radio('status', ORM::STATUS_ACTIVE, $winner ? $winner->status == ORM::STATUS_ACTIVE : false, array('style' => 'width: auto;'));?> Publicēts</label>

					<label><?=Form::radio('status', ORM::STATUS_DRAFT, $winner ? $winner->status == ORM::STATUS_DRAFT : true, array('style' => 'width: auto;'));?> Melnraksts</label>

					<label><?=Form::radio('status', ORM::STATUS_HIDDEN, $winner ? $winner->status == ORM::STATUS_HIDDEN : false, array('style' => 'width: auto;'));?> Slēpts</label>

				</div>				
				<? if(@$shortlist) {
                                    echo Form::hidden('shortlist',1);
                                }?>
				<div class="statuss-btns"><button class="save" type="submit" style="margin-left:65px;">Saglabāt</button></div>

			</div>
		</div>

	</div>
    
</form>

<script>
    $.fn.searchbox = function(settings) {
        var box = this;
        var conf = {
            message: (typeof settings.message === 'undefined') ? null : settings.message,
            url: (typeof settings.url === 'undefined') ? null : settings.url,
            valueIndex: (typeof settings.valueIndex === 'undefined') ? 0 : settings.valueIndex,
            showIndex: (typeof settings.showIndex === 'undefined') ? 0 : settings.showIndex
        };
        if(conf.message != null) {
            box[0].placeholder = conf.message;
        }
        
        var value = box;
        if(conf.valueIndex !== conf.showIndex) {
            var value_holder = document.createElement('input');
                value_holder.type = 'hidden';
                value_holder.name = box[0].name;
                value = $(value_holder);
            box.before(value_holder);
            box[0].name += '_name';
        }
        var results = document.createElement('ul');
            var $results = $(results);
            $results.addClass('searchbox');
            box.after(results);
            
        $results.parent().css({'position': 'relative'});
        this.on('input',function() {
            var field = $(this);
            if(field.val().length > 0) {
                
                $.ajax({
                    url: conf.url,
                    type: 'POST',
                    data: {string: field.val()},
                    success: function(data) {
                        var results = $.parseJSON(data);
                        if(results.length > 0) {
                            $results.empty();
                            for(i in results) {
                                var arr = Object.keys(results[i]).map(function(j) { return results[i][j]; });
                                $results.append('<li data-value='+ arr[conf.valueIndex] +'>'+ arr[conf.showIndex] +'</li>');
                            }
                            $results.show();
                        }
                        else {
                            $results.hide();
                        }
                        
                    }
                });
            }
            else {
                $results.hide();
            }
        });
        $(document).on('click','.searchbox li',function() {
            value.val($(this).data('value'));
            box.val($(this).text());
            $results.hide();
        });
        $(document).click(function(e) {
                if(!$results.is(e.target) && $results.has(e.target).length === 0) {
                    $results.hide();
                }
        });
    };
    
    $(function() {
        var year = $('select[name=year]').val();
        $('select[name=category] option').not('.'+year).wrap('<span/>');
        $('select[name=award] option').not('.all-years').not('.'+year).wrap('<span/>');
        $('#job-search').searchbox({
            message: 'Meklēt darbu pēc nosaukuma vai ID...',
            url: '/admin/winners/search',
            valueIndex: 0,
            showIndex: 1
        });
        
        /*
        $(document).on('click','.searchbox li', function() {
            if($('input[name=job]').val() > 0 && $('section.manual-fields').is(':visible')) {
                $('section.manual-fields').hide().after('<h3 class="autofill">Informācija tiek automātiski aizpildīta no datubāzes</h3>');
            }
        });
        */
        $('input[name=video], input[name=audio]').click(function() {
            if($('input[name=picture]').val() == '') {
                alert('Vēlams augšuplādēt arī attēlu, video un audio nevar izmantot darba thumbnailam!');
            }
        });
        $('select[name=year]').change(function() {
            var year = $(this).val();
            if(year == 2015) {
                $('.job_reference').show();
                if($('section.manual-fields').is(':visible')) {
                    $('section.manual-fields').hide().after('<h3 class="autofill">Pārējā informācija tiek automātiski aizpildīta no datubāzes</h3>');
                }
            }
            else {
                $('.job_reference').hide();
                $('.autofill').remove();
                $('section.manual-fields').show();
                $('input[name=job], input[name=job_name]').val('');
            }
            $('select[name=category] span option, select[name=award] span option').unwrap();
            $('select[name=category] option').not('.'+year).wrap('<span/>');
            $('select[name=award] option').not('.all-years').not('.'+year).wrap('<span/>');
        });
    });
</script>
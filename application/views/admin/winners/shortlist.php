<div class="content-area">
    
	<div class="path">Shortlist</div>

	<div class="table">

		<div class="add-btn"><a href="<?=URL::site('admin/winners/add/'.$section_id);?>" title="Pievienot">Pievienot</a></div>

		<div class="table-header-l">

			<div class="table-header">
                                <div class="p-30 left"><div>Kategorija</div></div>
                                
				<div class="p-40 left"><div>Nosaukums</div></div>

				<div class="p-20 left centered">Iesniedzējs</div>

				<div class="p-10 left centered">Dzēst</div>

				<!--div class="p-10 left centered">Up</div>
				<div class="p-10 left centered">Down</div-->
			</div>

		</div>

		<div class="table-data">

		<?

		$i = 0;
                if($winners) {
		foreach($winners as $winner){
		?>
                        
			<div class="table-item<?if(++$i%2 == 0){?> grey<?}?>">
                                <a href="<?=URL::site('admin/winners/edit/'.$winner->id);?>" title="">
                                <div class="p-30 left cellb"><div><?=$winner->category;?></div></div>

				<div class="p-40 left cellbg"><div><?=$winner->title;?></div></div>
				
				<div class="p-20 left cellbg"><div><?=$winner->author;?></div></div>
                                </a>
                            
				<div class="p-10 left centered cellbg"><a data-action="delete" href="<?=URL::site('admin/winners/delete/'.$winner->id);?>"><img src="/assets/css/cms/img/table-ico-delete.png"></a></div>

			</div>
                        
		<?}
                }
                else {
                    ?><div>Nav neviena ieraksta</div><?
                }
                ?>
                    
		</div>

	</div>
</div>
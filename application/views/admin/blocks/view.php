<form method="post" id="admin" action="<?=URL::site('admin/blocks/view/'.$section_id);?>" enctype="multipart/form-data">
	<div class="insert-area">
		<div class="path">Sākumlapas bloki</div>
		<div class="block-insert p-100" style="padding-top:6px;">
			<div class="h-exp" id="content-h">
				<div class="title">Bloks #1</div>
				<div class="arrow"><a href="javascript:;" onclick="admin.block.toggle('content-h', 'content-area');">&nbsp;</a></div>
			</div>
			<div class="insert-content" id="content-area">
				<div class="insert-input">
					<div class="label left p-30">Virsraksts #1</div>
					<div class="field left p-70"><?=Form::input('title_1', $blocks['title_1']);?></div>
				</div>
				<div class="insert-input">
					<div class="label left p-30">Saite #1</div>
					<div class="field left p-70"><?=Form::input('link_1', $blocks['link_1']);?></div>
				</div>
				<div class="insert-input">
					<div class="label left p-30">Ievads #1</div>
					<div class="field left p-70"><?=Form::input('intro_1', $blocks['intro_1']);?></div>
				</div>
				<div class="insert-input">
					<div class="label left p-30">Embed #1 (287x161)</div>
					<div class="field left p-70"><?=Form::textarea('embed_1', $blocks ? $blocks['embed_1'] : '', array('class' => 'pb', 'style' => 'height:125px;'));?></div>
				</div>
				<div class="insert-input">
					<div class="label left p-30">Attēls #1</div>
					<div class="field left p-70"><?=Form::file('image_1', array('style' => 'margin: 0 0 10px 0'));?></div>
				</div>
				<?if(!empty($blocks['image_1'])){?>
					<div class="insert-input">
						<div class="label left p-30">&nbsp;</div>
						<div class="field left p-70"><img src="<?=URL::base();?>assets/media/images/<?=$blocks['image_1'];?>" alt="" style="display: block; margin: 0 auto 10px auto; max-width: 210px;" /></div>
					</div>
				<?}?>
			</div>
			
			<div class="h-exp" id="content-h">
				<div class="title">Bloks #2</div>
				<div class="arrow"><a href="javascript:;" onclick="admin.block.toggle('content-h', 'content-area');">&nbsp;</a></div>
			</div>
			<div class="insert-content" id="content-area">
				<div class="insert-input">
					<div class="label left p-30">Virsraksts #2</div>
					<div class="field left p-70"><?=Form::input('title_2', $blocks['title_2']);?></div>
				</div>
				<div class="insert-input">
					<div class="label left p-30">Saite #2</div>
					<div class="field left p-70"><?=Form::input('link_2', $blocks['link_2']);?></div>
				</div>
				<div class="insert-input">
					<div class="label left p-30">Ievads #2</div>
					<div class="field left p-70"><?=Form::input('intro_2', $blocks['intro_2']);?></div>
				</div>
				<div class="insert-input">
					<div class="label left p-30">Embed #2 (287x161)</div>
					<div class="field left p-70"><?=Form::textarea('embed_2', $blocks ? $blocks['embed_2'] : '', array('class' => 'pb', 'style' => 'height:125px;'));?></div>
				</div>
				<div class="insert-input">
					<div class="label left p-30">Attēls #2</div>
					<div class="field left p-70"><?=Form::file('image_2', array('style' => 'margin: 0 0 10px 0'));?></div>
				</div>
				<?if(!empty($blocks['image_2'])){?>
					<div class="insert-input">
						<div class="label left p-30">&nbsp;</div>
						<div class="field left p-70"><img src="<?=URL::base();?>assets/media/images/<?=$blocks['image_2'];?>" alt="" style="display: block; margin: 0 auto 10px auto; max-width: 210px;" /></div>
					</div>
				<?}?>
			</div>
			
			<div class="h-exp" id="content-h">
				<div class="title">Bloks #3</div>
				<div class="arrow"><a href="javascript:;" onclick="admin.block.toggle('content-h', 'content-area');">&nbsp;</a></div>
			</div>
			<div class="insert-content" id="content-area">
				<div class="insert-input">
					<div class="label left p-30">Virsraksts #3</div>
					<div class="field left p-70"><?=Form::input('title_3', $blocks['title_3']);?></div>
				</div>
				<div class="insert-input">
					<div class="label left p-30">Saite #3</div>
					<div class="field left p-70"><?=Form::input('link_3', $blocks['link_3']);?></div>
				</div>
				<div class="insert-input">
					<div class="label left p-30">Ievads #3</div>
					<div class="field left p-70"><?=Form::input('intro_3', $blocks['intro_3']);?></div>
				</div>
				<div class="insert-input">
					<div class="label left p-30">Embed #3 (287x161)</div>
					<div class="field left p-70"><?=Form::textarea('embed_3', $blocks ? $blocks['embed_3'] : '', array('class' => 'pb', 'style' => 'height:125px;'));?></div>
				</div>
				<div class="insert-input">
					<div class="label left p-30">Attēls #3</div>
					<div class="field left p-70"><?=Form::file('image_3', array('style' => 'margin: 0 0 10px 0'));?></div>
				</div>
				<?if(!empty($blocks['image_3'])){?>
					<div class="insert-input">
						<div class="label left p-30">&nbsp;</div>
						<div class="field left p-70"><img src="<?=URL::base();?>assets/media/images/<?=$blocks['image_3'];?>" alt="" style="display: block; margin: 0 auto 10px auto; max-width: 210px;" /></div>
					</div>
				<?}?>
			</div>
		</div>
		
		<?=Form::hidden('section_id', $section_id);?>
		
	</div>
	<div class="insert-right right">
		<div class="block-insert w-259" style="margin:35px 0 0 1px;">
			<div class="h-exp" id="statuss-h">
				<div class="title">Publicēšana</div>
				<div class="arrow"><a href="javascript:;" onclick="admin.block.toggle('statuss-h','statuss');" title="">&nbsp;</a></div>
			</div>
			<div class="rightblock-content" id="statuss">
				<div class="statuss-btns"><button class="save" type="submit" style="margin-left:65px;">Saglabāt</button></div>
			</div>
		</div>
	</div>
</form>
<script type="text/javascript" src="<?=URL::base();?>assets/js/cms/ckeditor/ckeditor.js"></script>
<script type="text/javascript" src="<?=URL::base();?>assets/js/cms/ckeditor/adapters/jquery.js"></script>
<script type="text/javascript">
	admin.tree.active = <?=$section_id;?>;
</script>
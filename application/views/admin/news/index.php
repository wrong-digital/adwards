<div class="content-area">
	<div class="path">Jaunumi</div>
	<div class="table">
		<div class="add-btn"><a href="<?=URL::site('admin/news/add/'.$section_id);?>" title="Add">Add</a></div>
		<div class="table-header-l">
			<div class="table-header">
				<div class="p-70 left"><div>Virsraksts</div></div>
				<div class="p-10 left centered">Datums</div>
				<div class="p-10 left centered"><div>Aktīvs</div></div>
				<div class="p-10 left centered">Dzēst</div>
			</div>
		</div>
		<div class="table-data">
		<?
		$i = 0;
		foreach($news as $news_item){
		?>
			<div class="table-item<?if(++$i%2 == 0){?> grey<?}?>">
				<div class="p-70 left cellbg"><div><?=$news_item->cmsEditLink($news_item->title);?></div></div>
				<div class="p-10 left centered cellbg"><div><?=$news_item->cmsEditLink($news_item->date);?></div></div>
				<div class="p-10 left centered cellbg"><?=$news_item->cmsStatusIcon();?></div>
				<div class="p-10 left centered"><?=$news_item->cmsDeleteLink();?></div>
			</div>
		<?}?>
		</div>
	</div>
</div>
<script type="text/javascript">
admin.tree.active = <?=$section_id;?>;
</script>
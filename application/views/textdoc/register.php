	<div class="register">
		
		<div class="area">
			<p style="text-align:center;padding-bottom:7px;"><img src="<?=URL::base();?>assets/css/img/ico-registration.png" alt="" /></p>
			<h1>Reģistrācija</h1>
                        <div class="i-am">Es esmu:</div>
                        <div class="register-dropdown"><span class="legal-status">privātpersona</span><div class="drop-arrow"></div></div>
			<ul class="types">
				<li>
					<a href="javascript:;" title="" onclick="site.types(1,2);">privātpersona</a>
				</li>
				<li>
					<a href="javascript:;" title="" onclick="site.types(2,1);">juridiskā persona</a>
				</li>
			</ul>
			<div class="form">
				<div id="type1">
					<form method="post" action="<?=URL::base().$lang;?>/actions/register">
						<div class="col" style="margin-right:16px;">
							<fieldset>
								<label for="private_name">Vārds, uzvārds</label>
								<input type="text" name="private_name" id="private_name" value="" />
							</fieldset>
							<fieldset>
								<label for="private_code">Personas kods</label>
								<input type="text" name="private_code" id="private_code" value="" />
							</fieldset>
							<fieldset>
								<label for="private_job">Nodarbošanās</label>
								<input type="text" name="private_job" id="private_job" value="" />
							</fieldset>
							<fieldset>
								<label for="email">E-pasts</label>
								<input type="text" name="email" id="email" value="" />
							</fieldset>
							<fieldset>
								<label for="phone">Tālrunis</label>
								<input type="text" name="phone" id="phone" value="" />
							</fieldset>
							
						</div>
						<div class="col" style="margin-left:16px;">
                                                        <fieldset>
								<label for="address">Adrese</label>
								<input type="text" name="address" id="address" value="" />
							</fieldset>
							<fieldset>
								<label for="bank">Banka</label>
								<input type="text" name="bank" id="bank" value="" />
							</fieldset>
							<fieldset>
								<label for="accaunt">Konta nr.</label>
								<input type="text" name="accaunt" id="accaunt" value="" />
							</fieldset>
							<fieldset>
								<label for="password">Parole (vismaz 7 simboli)</label>
								<input type="password" name="password" id="password" value="" />
							</fieldset>
							<fieldset>
								<label for="repeat">Parole atkārtoti</label>
								<input type="password" name="repeat" id="repeat" value="" />
							</fieldset>
							
						</div>
                                                <div class="register-boxes">
                                                        <fieldset>
                                                                    <ul>
                                                                            <li>
                                                                                    <input type="radio" name="private_member" id="private_member" value="1" /> 
                                                                                    <label for="private_member" class="radio">Esmu LADC biedrs</label>
                                                                            </li>
                                                                            <li>
                                                                                    <input type="radio" name="private_member" id="private_member" value="2" checked="checked" /> 
                                                                                    <label for="private_member"  class="radio">Neesmu LADC biedrs</label>
                                                                            </li>
                                                                            <li>
                                                                                    <input type="checkbox" name="spam" id="spam" value="1" checked="checked" /> 
                                                                                    <label for="spam" style="width:300px;" class="checkbox">Es nevēlos saņemt LADC jaunumus</label>
                                                                            </li>
                                                                    </ul>
                                                            </fieldset>
                                                </div>
						<input type="hidden" name="type" value="1" />
						<p>
							<input type="submit" class="submit" value="Reģistrēties" />
						</p>
					</form>
				</div>
				<div id="type2" style="display:none;">
					<form method="post" action="<?=URL::base().$lang;?>/actions/register">
						<div class="col" style="margin-right: 16px;">
							<fieldset>
								<label for="legal_title">Uzņēmuma nosaukums</label>
								<input type="text" name="legal_title" id="legal_title" value="" />
							</fieldset>

							<fieldset>
								<label for="legal_number">Reģistrācijas numurs</label>
								<input type="text" name="legal_number" id="legal_number" value="" />
							</fieldset>
							<fieldset>
								<label for="legal_pvn">PVN maksātāja numurs</label>
								<input type="text" name="legal_pvn" id="legal_pvn" value="" />
							</fieldset>
							<fieldset>
								<label for="email">E-pasts</label>
								<input type="text" name="email" id="email" value="" />
							</fieldset>
							<fieldset>
								<label for="address">Fiz. adrese</label>
								<input type="text" name="address" id="address" value="" />
							</fieldset>
							
							<fieldset>
								<label for="legal_address">Jurid. adrese</label>
								<input type="text" name="legal_address" id="legal_address" value="" />
							</fieldset>
						</div>
						<div class="col" style="margin-left: 16px;">
							<fieldset>
								<label for="phone">Tālrunis</label>
								<input type="text" name="phone" id="phone" value="" />
							</fieldset>
							<fieldset>
								<label for="legal_contact">Kontaktpersona</label>
								<input type="text" name="legal_contact" id="legal_contact" value="" />
							</fieldset>							
							<fieldset>
								<label for="bank">Banka</label>
								<input type="text" name="bank" id="bank" value="" />
							</fieldset>
							
							<fieldset>
								<label for="accaunt">Konta nr.</label>
								<input type="text" name="accaunt" id="accaunt" value="" />
							</fieldset>

							<fieldset>
								<label for="password">Parole (vismaz 7 simboli)</label>
								<input type="password" name="password" id="password" value="" />
							</fieldset>
                                                        <fieldset>
								<label for="repeat">Parole atkārtoti</label>
								<input type="password" name="repeat" id="repeat" value="" />
							</fieldset>

							
						</div>
                                                <div class="register-boxes">
                                                            <fieldset>
                                                                    <ul>
                                                                            <li>
                                                                                    <input type="radio" name="private_member" id="private_member" value="1" /> 
                                                                                    <label for="private_member"  class="radio">Esmu LADC biedrs</label>
                                                                            </li>
                                                                            <li>
                                                                                    <input type="radio" name="private_member" id="private_member" value="2" checked="checked" /> 
                                                                                    <label for="private_member"  class="radio">Neesmu LADC biedrs</label>
                                                                            </li>
                                                                            <li>
                                                                                    <input type="checkbox" name="spam" id="spam" value="1" checked="checked" /> 
                                                                                    <label for="spam" style="width:300px;" class="checkbox">Es nevēlos saņemt LADC jaunumus</label>
                                                                            </li>
                                                                    </ul>
                                                            </fieldset>
                                                </div>
						<input type="hidden" name="type" value="2" />
						<p>
							<input type="submit" class="submit" value="Reģistrēties" />
						</p>
					</form>
				</div>
			</div>
		</div>
	</div>
	<script>
		$(function(){
                        $('.register-dropdown').click(function() {
                            $('ul.types').slideToggle(150);
                        });
                        $('ul.types li').click(function() {
                           $('ul.types').fadeOut(100); 
                           $('.register-dropdown .legal-status').text($(this).children('a').text());
                        });
                    
                        $('input[type="checkbox"]').prop('checked',true);
                        $('.radio').click(function(event) {
                            event.preventDefault();
                            $('.radio-active').removeClass('radio-active');
                            $(this).addClass('radio-active');
                            $(this).prev('input[type=radio]').prop('checked',true);
                        });
                        $('.checkbox').click(function(event) {
                            event.preventDefault();
                            if($(this).hasClass('checkbox-active')) {
                                $('.checkbox-active').removeClass('checkbox-active');
                                $('input[type=checkbox]').prop('checked',true);
                            }
                            else {
                                $(this).addClass('checkbox-active');
                                $(this).prev('input[type=checkbox]').prop('checked',false);
                            }
                            
                        });
			$('input[type=text], input[type=password]').attr('autocomplete','off');			
			//var f = $("#type1");			if(f.length){				var fd = $("form", f);				fd.submit(function(){					$.post(fd.attr("action"), fd.serialize(), function(data){						$("input", fd).removeClass("error");						if(data.status == 'ok'){							window.location.href = window.location.href;						}else if(data.status == 'taken'){							alert(data.text);						}else{							$.each(data.errors, function(k, v){								$("#type1 #"+k).removeClass("error");								$("#type1 #"+k).addClass("error");							});						}					}, "json");					return false;				});			}						var f2 = $("#type2");			if(f2.length){				var fd2 = $("form", f2);				fd2.submit(function(){					$.post(fd2.attr("action"), fd2.serialize(), function(data){						$("input", fd2).removeClass("error");						if(data.status == 'ok'){							window.location.href = window.location.href;						}else if(data.status == 'taken'){							alert(data.text);						}else{							$.each(data.errors, function(k, v){								$("#type2 #"+k).removeClass("error");								$("#type2 #"+k).addClass("error");							});						}					}, "json");					return false;				});			}
                        var f = $("#type1");
                        if(f.length){				
                                var fd = $("form", f);				
                                fd.submit(function(){					
                                        $.post(fd.attr("action"), fd.serialize(), function(data){
                                                $("input", fd).removeClass("error");						
                                                if(data.status == 'ok'){
                                                    window.location.href = window.location.href;
                                                }
                                                else if(data.status == 'taken'){							
                                                        alert(data.text);						
                                                }else{							
                                                        $.each(data.errors, function(k, v){
                                                            $("#type1 #"+k).removeClass("error"); 
                                                            $("#type1 #"+k).addClass("error"); 
                                                        });						
                                                }					
                                        }, "json");					
                                        return false;				
                                });			
                        }						
                        var f2 = $("#type2");			
                        if(f2.length){				
                                var fd2 = $("form", f2);				
                                fd2.submit(function(){					
                                        $.post(fd2.attr("action"), fd2.serialize(), function(data){
                                                console.log(data);
                                                $("input", fd2).removeClass("error");						
                                                if(data.status == 'ok'){
                                                    window.location.href = window.location.href;
                                                }				
                                                else if(data.status == 'taken'){							
                                                        alert(data.text);						
                                                }else{							
                                                        $.each(data.errors, function(k, v){
                                                            $("#type2 #"+k).removeClass("error");
                                                            $("#type2 #"+k).addClass("error");
                                                        });						
                                                }					
                                        }, "json");					
                                        return false;				
                                });			
                        }

                });
	</script>
							
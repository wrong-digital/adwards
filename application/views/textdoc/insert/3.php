<form method="post" id="data" action="<?=URL::base().'actions/save_details';?>">

	<div class="line">
		<?if($item->order_status == 1){?>
			<?if($item->notes != ''){?>
				<p>
					<label for="bill_details">Rekvizīti</label>
				</p>
				<p style="padding:5px 10px 5px 10px;color:#fff;font-size:11px;background:#851b21;"><?=$item->bill_details;?></p>
			<?}?>
		<?}else{?>
			<p>
				<label for="bill_details">Rekvizīti</label>
			</p>
			<p>
				<textarea name="bill_details" id="bill_details" rows="" cols=""><?=$item->bill_details;?></textarea>
			</p>
		<?}?>
	</div>
	
	<input type="hidden" name="id" value="<?=$item->id;?>" />
	<p>
		<input type="submit" class="submit" value="Saglabāt" />
	</p>
</form>

<script>
$(function(){
	
	var fd3 = $("form#data");
	fd3.submit(function(event){
		
		$("input", fd3).removeClass("error");
		$("textarea", fd3).removeClass("error");
		
		site.popup.show('Uzgaidiet...');
			
		$.post(fd3.attr("action"), fd3.serialize(), function(data){
			$("input", fd3).removeClass("error");
			if(data.status == 'ok'){
				window.location.href = '?tab=3&item='+data.id;
			}else{
				site.popup.hide();
				$.each(data.errors, function(k, v){
					$("#insert #"+k).removeClass("error");
					$("#insert #"+k).addClass("error");
				});
				
				$('body,html').animate({
					scrollTop: 450
				}, 800);
			}
		}, "json");
		return false;
	});
})
</script>
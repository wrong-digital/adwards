
<form method="post" id="data" action="<?=URL::base().'actions/save_file_details';?>">

	<?if($format->audio_file > 1){?>
	<div id="digi-audio" class="file-upload" style="padding-top:20px;">
		<div class="file_label">Audio <?if($format->audio_file == 3){?><span style="color:#ff0000;">*</span><?}?></div>	
                <div class="file_info"><p>Audio .MP3 formātā 128KBPS vai augstākā kvalitātē</p></div>
		<div class="uploaded-file" id="uploaded_audio"<?if($item->filename_audio == ''){?> style="display:none;"<?}?>>
			<div class="title"><?=$item->filename_audio;?></div>
			<div class="delete-job-file">
				<a href="<?=URL::base();?>lv/actions/delete_file?id=<?=$item->id;?>&type=audio" title="" data-action="delete"><img src="<?=URL::base();?>assets/css/img/ico-file-delete.png" alt="" /></a>
			</div>		</div>
		<?if($item->filename_audio == ''){?>
		<div class="data" id="data_audio">
                    <div class="file_directions">velc & met <span>vai</span> izvēlies failu no datora (līdz 128mb)</div>
			<p class="file"><div id="audio_uploader"></div></p>
			<div id="status_audio"></div>
                        
		</div>
		<?}?>
		
	</div> 
	<script>
	$(document).ready(function(){
		var settings_audio = {
		    url: "<?=URL::base();?>actions/upload_file/<?=$item->id;?>?type=audio",
		    dragDrop: true,
		    fileName: "file",
		    //allowedTypes:"wav,aiff,mp3,aac",
                    allowedTypes:"mp3",
		    returnType:"json",
			onSuccess:function(files,data,xhr){
		        $('#data_audio').fadeOut(150);
		        $('#uploaded_audio .title').html(data[0]);		        $('#uploaded_audio').fadeIn(150);
		        $.post('<?=URL::base();?>actions/filename/<?=$item->id;?>?type=audio', { filename:data[0] });		    },
		    showDelete: false
		}
		$("#audio_uploader").uploadFile(settings_audio);
	});
	</script>
	<?}

	if($format->video_file > 1){?>
	<div id="digi-video" class="file-upload" style="padding-top:20px;">
		<div class="file_label">Video <?if($format->video_file == 3){?><span style="color:#ff0000;">*</span><?}?></div>
                <div class="file_info"><p>Video .mov vai .mp4 formātā, CODEC H.264, 1280x720 pikseļi vai lielāka Izšķirtspēja</p></div>
		<div class="uploaded-file" id="uploaded_video"<?if($item->filename_video == ''){?> style="display:none;"<?}?>>
			<div class="title"><?=$item->filename_video;?></div>
			<div class="delete-job-file">
				<a href="<?=URL::base();?>lv/actions/delete_file?id=<?=$item->id;?>&type=video" title="" data-action="delete"><img src="<?=URL::base();?>assets/css/img/ico-file-delete.png" alt="" /></a>
			</div>
		</div>
		<?if($item->filename_video == ''){?>
		<div class="data" id="data_video">
                    <div class="file_directions">velc & met <span>vai</span> izvēlies failu no datora (līdz 128mb)</div>
			<p class="file"><div id="video_uploader"></div></p>
			<div id="status_video"></div>
                        
		</div>
		<?}?>
		
	</div>
	<div id="link-to-video" style="padding-top:20px;">
		<div class="file_label">Saite uz video</div>
		<input type="text" name="video_http" id="video_http" style="width:540px;" value="<?=$item->video_http;?>" />
	</div>
	<div class="file-upload" style="padding-top:20px;">
		<div class="file_label">URL (+paroles, ja tādas nepieciešamas)</div>
		<textarea name="url" id="url" rows="" cols="" style="width:540px;height:100px;"><?=$item->url;?></textarea>
	</div> 	
	<script>
	$(document).ready(function(){
		var settings_video = {
		    url: "<?=URL::base();?>actions/upload_file/<?=$item->id;?>?type=video",
		    dragDrop: true,
		    fileName: "file",
		    allowedTypes: "mov,mp4",	
		    returnType: "json",
			onSuccess:function(files,data,xhr){
		        $('#data_video').fadeOut(150);
		        $('#uploaded_video .title').html(data[0]);
		        $('#uploaded_video').fadeIn(150);
		        $.post('<?=URL::base();?>actions/filename/<?=$item->id;?>?type=video', { filename:data[0] });
		    },
		    showDelete:false
		}
		$("#video_uploader").uploadFile(settings_video);
	});
	</script>
	<?}?>
	
	<?if($format->book_file > 1){?>		
		<div id="for-book-img" class="file-upload" style="padding-top:40px;">
			<div class="file_label">Attēli ADWARDS gadagrāmatai (2-6 kadri) <?if($format->book_file == 3){?><span style="color:#ff0000;">*</span><?}?></div>
			<div class="file_info"><p>2-6 kadri vai attēli, TIFF vai EPS (CMYK), izmērs 15 cm (garākā mala), 300 dpi</p></div>
                        <div class="data">
			<?for($i=1;$i<=6;$i++){?>				<div class="uploaded-file" id="uploaded_book_<?=$i;?>"<?if($item->{'filename_image_book_'.$i} == ''){?> style="display:none;margin-bottom:20px;"<?}else{?> style="margin-bottom:20px;"<?}?>>
					<div class="title"><?=$item->{'filename_image_book_'.$i};?></div>
					<div class="delete-job-file">
						<a href="<?=URL::base();?>lv/actions/delete_file?id=<?=$item->id;?>&type=image_book_<?=$i;?>" title="" data-action="delete">
							<img src="<?=URL::base();?>assets/css/img/ico-file-delete.png" alt="" />
						</a>
					</div>
				</div>		
				<?if($item->{'filename_image_book_'.$i} == ''){?>
				<div id="data_book_<?=$i;?>" style="padding-bottom:20px;">
                                    <div class="file_directions">velc & met <span>vai</span> izvēlies failu no datora</div>
					<p class="file"><div id="image_book_<?=$i;?>_uploader"></div></p>
					<div id="status_image_book_<?=$i;?>"></div>
                                        
				</div>
				<?}?>
			<?}?>
                            
			</div>
		</div> 
		
		<script>
		$(document).ready(function(){
			<?for($i=1;$i<=6;$i++){?>
			var settings_book_<?=$i;?> = {
			    url: "<?=URL::base();?>actions/upload_file/<?=$item->id;?>?type=image_book_<?=$i;?>",
			    dragDrop: true,
			    fileName: "file",
			    allowedTypes:"tiff,eps,tif",	
			    returnType:"json",
				onSuccess:function(files,data,xhr){
			        $('#data_book_<?=$i;?>').fadeOut(150);
			        $('#uploaded_book_<?=$i;?> .title').html(data[0]);
			        $('#uploaded_book_<?=$i;?>').fadeIn(150);
			        $.post('<?=URL::base();?>actions/filename/<?=$item->id;?>?type=image_book_<?=$i;?>', { filename:data[0] });
			    },
			    showDelete:false
			}
			$("#image_book_<?=$i;?>_uploader").uploadFile(settings_book_<?=$i;?>);
			<?}?>
		});
		</script>		<?}
																if($format->web_file > 1){?>	
		<div id="for-web-img" class="file-upload" style="padding-top:40px;">
			<div class="file_label">Attēli ADWARDS tīmekļa vietnēm (2-6 kadri) <?if($format->web_file == 3){?><span style="color:#ff0000;">*</span><?}?></div>
			<div class="file_info"><p>2-6 attēli RGB 72dpi, ne mazāku par 1336x900px, ne smagāku par 2mb</p></div>
                        <div class="data">
				
			<?for($i=1;$i<=6;$i++){?>				<div class="uploaded-file" id="uploaded_web_<?=$i;?>"<?if($item->{'filename_image_web_'.$i} == ''){?> style="display:none;margin-bottom:20px;"<?}else{?> style="margin-bottom:20px;"<?}?>>
					<div class="title"><?=$item->{'filename_image_web_'.$i};?></div>
					<div class="delete-job-file">
						<a href="<?=URL::base();?>lv/actions/delete_file?id=<?=$item->id;?>&type=image_web_<?=$i;?>" title="" data-action="delete">
							<img src="<?=URL::base();?>assets/css/img/ico-file-delete.png" alt="" />
						</a>
					</div>
				</div>
				<?if($item->{'filename_image_web_'.$i} == ''){?>
				<div id="data_web_<?=$i;?>" style="padding-bottom:20px;">
                                    <div class="file_directions">velc & met <span>vai</span> izvēlies failu no datora</div>
					<p class="file"><div id="image_web_<?=$i;?>_uploader"></div></p>
					<div id="status_image_web_<?=$i;?>"></div>
                                        
				</div>
				<?}?>
			<?}?>
			
			</div>
		</div> 
		<script>
		$(document).ready(function(){
			<?for($i=1;$i<=6;$i++){?>
			var settings_web_<?=$i;?> = {
			    url: "<?=URL::base();?>actions/upload_file/<?=$item->id;?>?type=image_web_<?=$i;?>",
			    dragDrop: true,
			    fileName: "file",
			    allowedTypes:"jpg,png",	
			    returnType:"json",
				onSuccess:function(files,data,xhr){
			        $('#data_web_<?=$i;?>').fadeOut(150);
			        $('#uploaded_web_<?=$i;?> .title').html(data[0]);
			        $('#uploaded_web_<?=$i;?>').fadeIn(150);
			        $.post('<?=URL::base();?>actions/filename/<?=$item->id;?>?type=image_web_<?=$i;?>', { filename:data[0] });
			    },
			    showDelete:false
			}
			$("#image_web_<?=$i;?>_uploader").uploadFile(settings_web_<?=$i;?>);
			<?}?>
		});
		</script>
	<?}?>
	<?
	if($format->zip_file > 1){?>
	<div class="file-upload" style="padding-top:20px;">
		<div class="file_label">Arhīva fails ar attēliem un citiem failiem <?if($format->zip_file == 3){?><span style="color:#ff0000;">*</span><?}?></div>
                <div class="file_info"><p>ZIP vai RAR arhīvs ar attēliem un/vai citiem failiem žūrijai</p></div>
		<div class="uploaded-file" id="uploaded_zip"<?if($item->filename_zip == ''){?> style="display:none;"<?}?>>
			<div class="title"><?=$item->filename_zip;?></div>
			<div class="delete-job-file">
				<a href="<?=URL::base();?>lv/actions/delete_file?id=<?=$item->id;?>&type=zip" title="" data-action="delete"><img src="<?=URL::base();?>assets/css/img/ico-file-delete.png" alt="" /></a>
			</div>
		</div>
		<?if($item->filename_zip == ''){?>
		<div class="data" id="data_zip">
                    <div class="file_directions">velc & met <span>vai</span> izvēlies failu no datora (līdz 128mb)</div>
			<p class="file"><div id="zip_uploader"></div></p>
			<div id="status_zip"></div>
                        
		</div>
		<?}?>
		
	</div>
	<script>
	$(document).ready(function(){
		var settings_zip = {
		    url: "<?=URL::base();?>actions/upload_file/<?=$item->id;?>?type=zip",
		    dragDrop: true,
		    fileName: "file",
		    allowedTypes:"zip,rar,7z",	
		    returnType:"json",
			onSuccess:function(files,data,xhr){
                        console.log(data);
		        $('#data_zip').fadeOut(150);
		        $('#uploaded_zip .title').html(data[0]);
		        $('#uploaded_zip').fadeIn(150);
		        $.post('<?=URL::base();?>actions/filename/<?=$item->id;?>?type=zip', { filename:data[0] });
		    },
		    showDelete:false
		}
		$("#zip_uploader").uploadFile(settings_zip);
	});
	</script>
	<?}?>
	<?=Form::hidden('id',$item->id);?>
	<p style="padding:45px 0 20px 0;">
		<input type="submit" class="submit" value="Saglabāt" />
	</p>
</form>
<script>
$(function(){
	var fd3 = $("form#data");
	fd3.submit(function(){
		$("input, textarea", fd3).removeClass("error");
		$.post(fd3.attr("action"), fd3.serialize(), function(data){
			$("input, textarea", fd3).removeClass("error");
			if(data.status == 'ok'){
				window.location.href = '<?=URL::section(84);?>';
			}else{
				site.popup.show('Nav pievienoti visi nepieciešamie faili!');
			}
		}, "json");
		return false;
	});
	
	$("a[rel='action:delete'], a[data-action='delete']").click(function(){
		return confirm("Dzēst failu?");
	});
})
</script>
<script src="<?=URL::base();?>assets/js/jquery.uploadfile.js"></script>
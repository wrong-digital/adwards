<form method="post" id="data" name="data" action="<?=URL::base().'actions/save_data';?>">

	<?if($item->id){?>
	<div style="padding-bottom:25px;">
		<p style="padding-bottom:5px;">
			<label>Pieteikuma ID</label>
		</p>
		<p>
			<?=Form::input('',$item->code.'_'.$item->id,array('readonly' => 'readonly'));?>
		</p>
	</div>
	<?}?>
        <div class="pricechecks">
			<p class="noprice-check">
				<?=Form::checkbox('noprice',1,$item->noprice == 1 ? true : false,array('id' => 'noprice', 'style' => 'width:20px;float:left;margin:2px 2px 0 0;'));?>
				<label for="noprice">Limbo darbs</label>
			</p>


			<p class="noprice-check" style="display:none;">
				<?=Form::checkbox('antalis',1,$item->antalis == 1 ? true : false,array('id' => 'antalis', 'style' => 'width:20px;float:left;margin:2px 2px 0 0;'));?>
				<label for="antalis" class="antalis-check">Darbs uz Antalis papīra</label>
			</p>
                        <p class="antalis-check-text" style="display:none;">Ja iesniedzamais darbs drukāts uz Antalis papīra un norādīsi papīra veidu un izmantoto tehnoloģiju (aizpildāmie lauciņi zemāk), arī otro darbu varēsi iesniegt bez maksas. Ja šī informācija netiks norādīta korekti, atlaide netiks piemērota.</p>
        </div>

	<div style="overflow:hidden;">
		<div class="forma-col" style="margin-right:53px;">
			<div id="antalis_type_div_left" style="<?=$item->antalis == 1 ? '' : 'display:none;';?>">
                                <h3>Ieraksti, kādus Antalis papīrus vai materiālus izmantoji*</h3>
				<?=Form::textarea('antalis_type',$item->antalis_type,array('style' => 'height:100px;', 'id' => 'antalis_type'));?>
			</div>

			<div>
				<!-- -->
			</div>
		</div>

		<div class="forma-col">
			<div id="antalis_type_div_right" style="<?=$item->antalis == 1 ? '' : 'display:none;';?>">
                                <h3>Ieraksti, kādas drukas tehnoloģijas izmantoji*</h3>
				<?=Form::textarea('antalis_technology',$item->antalis_technology,array('style' => 'height:100px;', 'id' => 'antalis_technology'));?>
			</div>

			<div>
				<!-- -->
			</div>
		</div>
            <div class="forma-col full-col">
            <div>
                                <div class="sections" id="main_category">
					<div class="selected">
					    <div class="title"><?=$item ? $item->category->title : 'Kategorija';?></div>
					    <div class="ico svg-img">&nbsp;</div>
					</div>
					<ul>
						<?foreach($categories as $c){?>
                                            <li<?php /* ALL BRANDING DISABLED
                                                switch($c->id) {
                                                    case 19:
                                                        echo ' class="branded antalis"';
                                                        break;

                                                    case 28:
                                                        echo ' class="branded"';
                                                        break;

                                                }
                                            */?>>
							<a href="javascript:;" title="<?=$c->title;?>" data-id="<?=$c->id;?>"><?=$c->title;?></a>
						</li>
						<?}?>
					</ul>
					<?=Form::hidden('main_category',$item->main_category);?>
				</div>
            </div>
            <div>
                                <div class="sections" id="sub_category">
					<div class="selected">
					    <div class="title"><?=$item ? strip_tags($item->format->title) : 'Apakškategorija';?></div>
					    <div class="ico svg-img">&nbsp;</div>
					</div>
					<ul>
						<?foreach($subs as $s){?>
						<li>
							<a href="javascript:;" title="<?=$s->title;?>" data-id="<?=$s->id;?>"><?=$s->title;?></a>
						</li>
						<?}?>
					</ul>
					<?=Form::hidden('sub_category',$item->sub_category);?>
				</div>
            </div>
            </div>
	</div>


	<div class="courier"<?if($item->main_category == 19) echo ' style="display:block;"';?>>
                <div style="clear:both;padding:0 0 20px 0;" class="printart_technology">
                        <p>Kategorijas PRINTART UN DIZAINS darbiem jāiesniedz drukāti un/vai produktu paraugi (reāli darbi nevis to attēli) un anketā jānonorāda drukas un/vai pēcapstrādes tehnoloģijas un izmantotie materiāli (aizpildāmais lodziņš zemāk). Ja drukas un produktu reālie paraugi netiks iesūtīti, darbi netiks vērtēti!</p>
                    <h3>Norādi, kādas drukas un/vai pēcapstrādes tehnoloģijas un materiālus izmantoji*</h3>
                    <?=Form::textarea('printart_technology',$item->printart_technology,array('style' => 'height:90px;width:538px;'));?>
                </div>

		<div class="checkbox">
			<?=Form::checkbox('courier',1,$item->courier == 1 ? true : false,array('id' => 'courier'));?>
			<label for="courier">Bezmaksas kurjers no Antalis</label>
		</div>

		<div style="padding:0 0 10px 0;">
                        <h3>Adrese, kur ierasties kurjeram</h3>
			<?=Form::input('courier_address',$item->courier_address,array('id' => 'courier_address', 'style' => 'width:538px;'));?>
		</div>

		<div class="forma-col" style="margin-right:53px;">
                        <h3>Kontaktpersona</h3>
			<?=Form::input('courier_contact',$item->courier_contact,array('id' => 'courier_contact'));?>
		</div>
		<div class="forma-col">
                        <h3>Kontakttālrunis</h3>
			<?=Form::input('courier_phone',$item->courier_phone,array('id' => 'courier_phone'));?>
		</div>

	</div>


	<div class="forma-col field-column" style="margin-right:53px; margin-top: 10px;">
		<div>
                        <h3>Nosaukums latviski *</h3>
			<?=Form::input('title_lv',$item->title_lv,array('id' => 'title_lv'));?>
		</div>
		<div>
                        <h3>Klients latviski *</h3>
			<?=Form::input('client_lv',$item->client_lv,array('id' => 'client_lv'));?>
		</div>
		<div>
                        <h3>Produkts latviski *</h3>
			<?=Form::input('product_lv',$item->product_lv,array('id' => 'product_lv'));?>
		</div>
		<div>
                        <h3>Apraksts latviski *</h3><div class="textcounter"><span id="d_lv">0</span><span class="textmax">/500</span></div>
			<textarea name="description_lv" id="description_lv" rows="" cols="" onkeyup="limitText(this.form.description_lv,this.form.d_lv,500,'d_lv');" onkeydown="limitText(this.form.description_lv,this.form.d_lv,500,'d_lv');"><?=$item->description_lv;?></textarea>

		</div>
		<div>
                        <h3>Radošais direktors / Scenārists</h3>
			<?=Form::input('creative_1',$item->creative_1,array());?>
		</div>
		<div>
                        <h3>Mākslinieciskais direktors</h3>
			<?=Form::input('art_1',$item->art_1,array());?>
		</div>
		<div>
                        <h3>Dizainers</h3>
			<?=Form::input('designer_1',$item->designer_1,array());?>
		</div>
		<div>
                        <h3>Filmas / klipa mākslinieks</h3>
			<?=Form::input('film_artist',$item->film_artist,array());?>
		</div>
		<div>
                        <h3>Programmētājs</h3>
			<?=Form::input('programmer_1',$item->programmer_1,array());?>
		</div>
		<div>
                        <h3>Tekstu autors</h3>
			<?=Form::input('text_1',$item->text_1,array());?>
		</div>
		<div>
                        <h3>Fotogrāfs</h3>
			<?=Form::input('photographer_1',$item->photographer_1,array());?>
		</div>
		<div>
                        <h3>Operators</h3>
			<?=Form::input('operator',$item->operator,array());?>
		</div>
		<div>
                        <h3>Montāžas režisors</h3>
			<?=Form::input('editor',$item->editor,array());?>
		</div>
		<div>
                        <h3>Režisors</h3>
			<?=Form::input('rezisors_1',$item->rezisors_1,array());?>
		</div>
	</div>

	<div class="forma-col field-column"  style="margin-top: 10px;">
		<div>
                        <h3>Nosaukums angliski *</h3>
			<?=Form::input('title_en',$item->title_en,array('id' => 'title_en'));?>
		</div>
		<div>
                        <h3>Klients angliski *</h3>
			<?=Form::input('client_en',$item->client_en,array('id' => 'client_en'));?>
		</div>
		<div>
                        <h3>Produkts angliski *</h3>
			<?=Form::input('product_en',$item->product_en,array('id' => 'product_en'));?>
		</div>
		<div>
                        <h3>Apraksts angliski *</h3><div class="textcounter"><span id="d_en">0</span><span class="textmax">/500</span></div>
			<textarea name="description_en" id="description_en" rows="" cols="" onkeyup="limitText(this.form.description_en,this.form.d_en,500,'d_en');" onkeydown="limitText(this.form.description_en,this.form.d_en,500,'d_en');"><?=$item->description_en;?></textarea>

		</div>
		<div>
                        <h3>Radošais direktors / Scenārists</h3>
			<?=Form::input('creative_2',$item->creative_2,array());?>
		</div>
		<div>
                        <h3>Projekta vadītājs</h3>
			<?=Form::input('art_2',$item->art_2,array());?>
		</div>
		<div>
                        <h3>Dizainers</h3>
			<?=Form::input('designer_2',$item->designer_2,array());?>
		</div>
		<div>
                        <h3>Grafikas dizainers</h3>
			<?=Form::input('grafic_designer',$item->grafic_designer,array());?>
		</div>
		<div>
                        <h3>Programmētājs</h3>
			<?=Form::input('programmer_2',$item->programmer_2,array());?>
		</div>
		<div>
                        <h3>Tekstu autors</h3>
			<?=Form::input('text_2',$item->text_2,array());?>
		</div>
		<div>
                        <h3>Fotogrāfs</h3>
			<?=Form::input('photographer_2',$item->photographer_2,array());?>
		</div>
		<div>
                        <h3>Komponists</h3>
			<?=Form::input('componist',$item->componist,array());?>
		</div>
		<div>
                        <h3>Pēcapstrāde (post)</h3>
			<?=Form::input('post',$item->post,array());?>
		</div>
		<div>
                        <h3>Cits (norādiet vārdu un lomu projektā)</h3>
			<?=Form::input('rezisors_2',$item->rezisors_2,array());?>
		</div>
	</div>

	<div class="notes-container" style="clear:both;padding:0 0 36px 0;">
                <h3>Piezīmes</h3>
		<?=Form::textarea('notes',$item->notes,array('style' => 'height:90px;width:538px;'));?>
	</div>
			<div class='too-late'> Darbu iesniegšana šajā kategorijā jau ir beigusies. Vēl iespējams iesniegt darbus kategorijā 'Darba uzdevumi'</div>
	<?=Form::hidden('id',$item->id);?>
	

	<p style="clear:both;">
		<input type="submit" class="submit" value="Saglabāt" />
	</p>
</form>

<script>
var antalisCheck = false;
$(function(){
        /*
        $('.insert-form :input').attr('disabled','disabled');
                    $('.insert-form input[type=submit]').val('Slēgts');
                    $('.field-column, .notes-container, .pricechecks').hide();
                    $('.too-late').show();
        */
        $('input[name=antalis]').change(function() {
            if($(this).is(':checked')) {
                antalisCheck = true;
                if($('input[name=main_category]').val() == 19) {
                    $('.printart_technology').hide();
                }
            }
            else {
                antalisCheck = false;
                if($('input[name=main_category]').val() == 19) {
                    $('.printart_technology').show();
                }
            }
        });
        $('.sections ul a').click(function() {
            if($(this).data('id') == 19) {
                if(antalisCheck) {
                    $('.printart_technology').hide();
                }
                else {
                    $('.printart_technology').show();
                }
            }
        });
	$('#main_category .selected').click(function(){
		if($('#main_category ul').is(':visible')) {
			$('#main_category .selected .ico').removeClass('down');
			$('#main_category ul').slideUp(250);
		}else{
			$('#main_category .selected .ico').addClass('down');
			$('#main_category ul').slideDown(250);
		}
	})

	$('#sub_category .selected').click(function(){
		if($('#sub_category ul').is(':visible')) {
			$('#sub_category .selected .ico').removeClass('down');
			$('#sub_category ul').slideUp(250);
		}else{
			$('#sub_category .selected .ico').addClass('down');
			$('#sub_category ul').slideDown(250);
		}
	})

	$('#main_category ul li a').click(function(){
		var id = $(this).data('id');
		$('[name=main_category]').val(id);
		$.post('<?=URL::section($section->id);?>', { action:'sub', id:$(this).data('id') }, function(data){
			$('#sub_category ul').hide();
			$('#sub_category ul').html(data);
			$('#sub_category .selected .title').html('Apakškategorija');
			$('#sub_category ul li a').click(function(){
				var id = $(this).data('id');
				$('[name=sub_category]').val(id);
				$('#sub_category .selected .title').html($(this).html());
				$('#sub_category .selected .ico').removeClass('down');
				$('#sub_category ul').slideUp(250);
			});
		});
		$('#main_category .selected .title').html($(this).html());
		$('#main_category .selected .ico').removeClass('down');
		$('#main_category ul').slideUp(250);


		if(id == 19){
			$('.courier').slideDown(250);
                        $('#main_category .selected').css('background','url(/assets/css/img/bg-antalis-small.png) no-repeat');
                        $('#main_category .selected .title').css('color','#fff');
		}
			/*
                else if(id == 28) {
                        $('#main_category .selected').css('background','url(/assets/css/img/bg-videsrekl-small.png) no-repeat');
                        $('#main_category .selected .title').css('color','#fff');
                }
				*/
		else {
			$('.courier').hide(150);
                        $('#main_category .selected').css('background','#d1dadf');
                        $('#main_category .selected .title').css('color','#5a5a5a');
		}

                /* CLOSE */

                /*
                    $('.insert-form :input').attr('disabled','disabled');
                    $('.insert-form input[type=submit]').val('Slēgts');
                    $('.field-column, .notes-container, .pricechecks').hide();
                    $('.too-late').show();
                    */


	});

	$('#sub_category ul li a').click(function(){
		var id = $(this).data('id');
		$('[name=sub_category]').val(id);
		$('#sub_category .selected .title').html($(this).html());
		$('#sub_category .selected .ico').removeClass('down');
		$('#sub_category ul').slideUp(250);

	});

	$("#antalis").change(function(){
		if(document.data.elements['antalis'].checked) {
			$("#antalis_type_div_left, #antalis_type_div_right").slideDown(250);
		}else{
			$("#antalis_type_div_left, #antalis_type_div_right").slideUp(250);
		}
	});

	<?if($item->order_status == 1){?>$("input, select", fd3).attr('disabled','disabled');<?}?>
	var fd3 = $("form#data");
	fd3.submit(function(event){
		$("div", fd3).removeClass("error");
		$("input", fd3).removeClass("error");
		$("textarea", fd3).removeClass("error");
		$.post(fd3.attr("action"), fd3.serialize(), function(data){
			$("input", fd3).removeClass("error");
			if(data.status == 'ok'){
				window.location.href = '?tab=<?=$item->id ? 1 : 2;?>&item='+data.id;
			}else{
				$.each(data.errors, function(k, v){
					$("#"+k).removeClass("error");
					$("#"+k).addClass("error");
				});
				$('body,html').animate({
					scrollTop: 320
				}, 800);
			}
		}, "json");
		return false;
	});
})
</script>

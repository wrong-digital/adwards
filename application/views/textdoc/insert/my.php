	<div class="register">
		<div class="leftside" style="height:auto;">
			<ul class="submenu">
				<?if($section->id == 3 || $section->parent_id == 3){?>
					<?=View::factory('global/leftmenu');?>
				<?}?>
				<?=Helper_Section::public_tree($section->parent_id == 0 ? $section->id : $section->parent_id);?>
			</ul>
		</div>
		<div class="jobs-section">
                    <!--
			<div class="insert-new">
				<div class="add-button">
					<a href="<?=URL::section(83);?>" title="">Pievienot darbu +</a>
				</div>
				<div class="info">
					<?=$section->textdoc->content;?>
				</div>
			</div>
                    -->
			<?if(count($jobs) > 0){?>
			<h2>Neapstiprinātie darbi</h2>

			<?foreach($jobs as $job){?>
			<div class="saved-job">
				<div class="info">
					<h2><?=strip_tags($job->title_lv);?></h2>
					<p><?=strip_tags($job->description_lv);?></p>
					
					<?if($job->validate_job($job->id) == 'false'){?><p style="padding:0 0 5px 0;color:#ff0000;">Nav pievienoti visi nepieciešamie faili!</p><?}?>
					<div class="buttons">
						<a href="<?=URL::section(83);?>?item=<?=$job->id;?>" title="" class="edit">Labot informāciju</a>
                                                <a href="<?=URL::section(83);?>?item=<?=$job->id;?>&tab=2" title="" class="edit">Labot failus</a>
                                                <a href="<?=URL::base();?>lv/actions/delete_job?delete=<?=$job->id;?>" title="" class="delete" data-action="delete">Dzēst pieteikumu</a>
					</div>
				</div>
			</div>
			<?}?>

			<form method="post" id="checkout" action="<?=URL::base();?>lv/actions/submit">
				<div class="checkout">
					<?if($price > 0){?>
					<div class="price">
						<h2>Dalības maksa: <?=number_format($price,2);?> EUR</h2>
						<p>Darbu ID: <?=$ids;?></p>
					</div>
					<div class="details">
						<h2>Mūsu rekvizīti:</h2>
						<p>Biedrība "Latvian Art Directors Club"<br/>13. janvāra iela 33, Rīga, LV-1050<br/>Reģ. Nr. 40008092003<br/>PVN Nr. LV40008092003<br/>Konta Nr. LV86HABA0551010412001<br/>AS Swedbank<br/>Kods: HABALV22</p>
					</div>
					<div class="details">
						<h2>Jūsu rekvizīti:</h2>
						<textarea name="details" rows="" cols=""><?=$userdata->type == 1 ? $userdata->private_name : $userdata->legal_title;?>&#10;<?=$userdata->type == 1 ? $userdata->private_code : $userdata->legal_number;?>&#10;<?=$userdata->bank;?>&#10;<?=$userdata->accaunt;?>&#10;<?=$userdata->type == 1 ? $userdata->address : $userdata->legal_address;?></textarea>
                                                <span class="edit-userdata">Labot</span>
                                        </div>
					<?}?>
					<input type="hidden" name="price" value="<?=number_format($price,2);?>" />
					<input type="submit"<?if($price == 0){?> style="margin:0 0 0 150px;"<?}?>value="Apstiprināt" />
				</div>
			</form>
			<script>
				$(function(){
					var checkout = $("form#checkout");
					checkout.submit(function(event){
						$("textarea", checkout).removeClass("error");
						$.post(checkout.attr("action"), checkout.serialize(), function(data){
							if(data.status == 'ok'){
								window.location.href = window.location.href;
							}else if(data.status == 'files'){
								site.popup.show('Nav pievienoti visi nepieciešamie faili!');
							}else{
								$.each(data.errors, function(k, v){
									$("[name="+k+"]").addClass("error");
								});
							}
						}, "json");
						return false;
					});
				})
			</script>
			<?}?>
			<?if(count($saved) > 0){?>
			<h2 style="font-size:25px;padding:55px 0 15px 0;">Apstiprinātie darbi</h2>
			<?}?>
                        <div class="accepted-jobs">
			<?foreach($saved as $job){?>
			<div class="saved-job">
				<div class="info">
					<h2><?=strip_tags($job->title_lv);?></h2>
					<p><?=strip_tags($job->description_lv);?></p>
				</div>
			</div>
			<?}?>
                        </div>
		</div>
	</div>
	<script>
		$(function(){
                        $('.edit-userdata').click(function() {
                            $('textarea[name=details]').focus();
                        });
                    
			$("a[rel='action:delete'], a[data-action='delete']").click(function(){
				return confirm("Dzēst pieteikumu?");
			});
		});
	</script>
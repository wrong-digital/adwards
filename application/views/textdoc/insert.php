	<div class="register">
		<div class="leftside">
			<ul class="submenu">
				<?if($section->id == 3 || $section->parent_id == 3){?>
					<?=View::factory('global/leftmenu');?>
				<?}?>
				<?=Helper_Section::public_tree($section->parent_id == 0 ? $section->id : $section->parent_id);?>
			</ul>
		</div>
		
		<div class="insert-form">
			<ul class="tabs">
				<li>
					<a href="?tab=1<?=$item->id ? '&item='.$item->id : '';?>" title="" class="<?=$tab == 1 ? 'ac' : '';?>">Darba pamatinformācija</a>
				</li>
				<li>
					<a href="<?=$item->id ? '?tab=2&item='.$item->id : 'javascript:;';?>"<?if(!$item){?> onclick="$('.overlay').fadeIn(150, function(){ site.popup.show('Vispirms jāsaglabā pamatinformācija'); });"<?}?> title="" class="<?=$tab == 2 ? 'ac' : '';?>">Faili</a>
				</li>
			</ul>
			<div class="forma">
							
				<?
				echo View::factory('textdoc/insert/'.($tab == 1 ? 1 : 2))
					->set('item',$item)
					->set('subs',$subs)
					->set('format',$format)
					->set('categories',$categories);
				?>
												
			</div>
		</div>
	</div>
	<div class="register">		<div class="area">			<p style="text-align:center;padding-bottom:7px;"><img src="<?=URL::base();?>assets/css/img/ico-registration.png" alt="" /></p>			<h1>Profils</h1>			<div class="form" id="update_profile">
				<form method="post" action="<?=URL::base().$lang;?>/actions/profile">
					<?if($user->type == 1){?>
						<div class="col" style="margin-right: 45px;">
							<fieldset>
								<label for="private_name">Vārds, uzvārds</label>
								<input type="text" name="private_name" id="private_name" value="<?=$user->private_name;?>" />
							</fieldset>
							<fieldset>
								<label for="private_code">Personas kods</label>
								<input type="text" name="private_code" id="private_code" value="<?=$user->private_code;?>" />
							</fieldset>
							<fieldset>
								<label for="private_job">Nodarbošanās</label>
								<input type="text" name="private_job" id="private_job" value="<?=$user->private_job;?>" />
							</fieldset>
							<fieldset>
								<label for="email">E-pasts</label>
								<input type="text" name="email" id="email" value="<?=$user->email;?>" />
							</fieldset>
							<fieldset>
								<label for="phone">Tālrunis</label>
								<input type="text" name="phone" id="phone" value="<?=$user->phone;?>" />
							</fieldset>
							<fieldset>
								<label for="address">Adrese</label>
								<input type="text" name="address" id="address" value="<?=$user->address;?>" />
							</fieldset>
						</div>
						<div class="col" style="border-left:1px solid #5a5165;padding-left:45px;">
							<fieldset>
								<label for="bank">Banka</label>
								<input type="text" name="bank" id="bank" value="<?=$user->bank;?>" />
							</fieldset>
							<fieldset>
								<label for="accaunt">Konta nr.</label>
								<input type="text" name="accaunt" id="accaunt" value="<?=$user->accaunt;?>" />
							</fieldset>
							<fieldset>
								<label for="password">Mainīt paroli</label>
								<input type="password" name="password" id="password" value="" autocomplete="off" />
							</fieldset>
							<fieldset>
								<ul>
									<li>
										<input type="radio" name="private_member" id="private_member_1" value="1"<?=$user->private_member == 1 ? ' checked="checked"' : '';?> /> 
										<label for="private_member_1">Esmu LADC biedrs</label>
									</li>
									<li>
										<input type="radio" name="private_member" id="private_member_2" value="2"<?=$user->id == '' || $user->legal_member == 0 || $user->private_member == 2 ? ' checked="checked"' : '';?> /> 
										<label for="private_member_2">Neesmu LADC biedrs</label>
									</li>
									<li>
										<input type="checkbox" name="spam" id="spam" value="1"<?=$user->spam == 1 ? ' checked="checked"' : '';?> /> 
										<label for="spam" style="width:210px;">Es piekrītu saņemt jaunumus no LADC</label>
									</li>
								</ul>
							</fieldset>
						</div>
						
						<input type="hidden" name="type" value="1" />
						<p>
							<input type="submit" class="submit" value="Saglabāt" />
						</p>
					<?}else{?>
						<div class="col" style="margin-right: 45px;">
							<fieldset>
								<label for="legal_title">Uzņēmuma nosaukums</label>
								<input type="text" name="legal_title" id="legal_title" value="<?=$user->legal_title;?>" />
							</fieldset>
							
							<fieldset>
								<label for="legal_number">Reģistrācijas numurs</label>
								<input type="text" name="legal_number" id="legal_number" value="<?=$user->legal_number;?>" />
							</fieldset>						
							<fieldset>
								<label for="legal_pvn">PVN maksātāja numurs</label>
								<input type="text" name="legal_pvn" id="legal_pvn" value="<?=$user->legal_pvn;?>" />
							</fieldset>
							<fieldset>
								<label for="email">E-pasts</label>
								<input type="text" name="email" id="email" value="<?=$user->email;?>" />
							</fieldset>
							<fieldset>
								<label for="address">Fiz. adrese</label>
								<input type="text" name="address" id="address" value="<?=$user->address;?>" />
							</fieldset>
							<fieldset>
								<label for="legal_address">Jurid. adrese</label>
								<input type="text" name="legal_address" id="legal_address" value="<?=$user->legal_address;?>" />
							</fieldset>
						</div>

						<div class="col" style="border-left:1px solid #5a5165;padding-left:45px;">
							<fieldset>
								<label for="phone">Tālrunis</label>
								<input type="text" name="phone" id="phone" value="<?=$user->phone;?>" />
							</fieldset>
							<fieldset>
								<label for="legal_contact">Kontaktpersona</label>
								<input type="text" name="legal_contact" id="legal_contact" value="<?=$user->legal_contact;?>" />
							</fieldset>														<fieldset>								<label for="bank">Banka</label>								<input type="text" name="bank" id="bank" value="<?=$user->bank;?>" />							</fieldset>							
							<fieldset>
								<label for="accaunt">Konta nr.</label>
								<input type="text" name="accaunt" id="accaunt" value="<?=$user->accaunt;?>" />
							</fieldset>
							<fieldset>
								<label for="password">Mainīt paroli</label>
								<input type="password" name="password" id="password" value="" autocomplete="off" />
							</fieldset>
							<fieldset>
								<ul>
									<li>
										<input type="radio" name="legal_member" id="legal_member" value="1"<?=$user->legal_member == 1 ? ' checked="checked"' : '';?> /> 
										<label for="legal_member_1">Esmu LADC biedrs</label>
									</li>
									<li>
										<input type="radio" name="legal_member" id="legal_member" value="2"<?=$user->id == '' || $user->legal_member == 0 || $user->legal_member == 2 ? ' checked="checked"' : '';?> /> 
										<label for="legal_member_2">Neesmu LADC biedrs</label>
									</li>									
									<li>
										<input type="checkbox" name="spam" id="spam" value="1"<?=$user->spam == 1 ? ' checked="checked"' : '';?> /> 
										<label for="spam" style="width:210px;">Es piekrītu saņemt jaunumus no LADC</label>
									</li>
								</ul>
							</fieldset>
						</div>
						<input type="hidden" name="type" value="2" />								
						<p>
							<input type="submit" class="submit" value="Saglabāt" />
						</p>
					<?}?>
				</form>
			</div>
		</div>
	</div>
	<script>
	$(function(){
		var update = $("#update_profile");
		if(update.length){
			var updated = $("form", update);
			updated.submit(function(){
				$.post(updated.attr("action"), updated.serialize(), function(data){
					$("input", updated.removeClass("error"));
					if(data.status == 'ok'){
						site.popup.hide();
					}else if(data.status == 'taken'){
						alert(data.text);
					}else{
						$.each(data.errors, function(k, v){
							$("#"+k).removeClass("error");
							$("#"+k).addClass("error");
						});
					}
				}, "json");
				return false;
			});
		}
	})
	</script>
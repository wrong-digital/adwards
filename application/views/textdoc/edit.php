
				<div class="page">
					<div class="leftside">
						<ul><?=Helper_Section::public_tree($section->parent_id == 0 ? $section->id : $section->parent_id);?></ul>
						<?if(count($jobs) > 0){
							$not_saved = 0;
							$free_job = 1;
							$antalis_free = 1;
							$limbo_free = 2;
							$price = 0;
							?>
							<div class="jobs">
						    <h3>Pievienotie darbi</h3>
							<?foreach($jobs as $job){?>
							<div class="item">
							    <div class="title">
								    <a href="<?=URL::section($section->id);?>?edit=<?=$job->id;?>" title=""><?=$job->title_lv;?></a>
								</div>
								<?
								
								if($job->order_status == 0){
									$not_saved = 1;
									?><div class="icons">
									    <a href="<?=URL::section($section->id);?>?edit=<?=$job->id;?>" title=""><img src="/assets/css/img/ico-edit.png" alt="" /></a>
									    <a href="<?=URL::section($section->id);?>?delete=<?=$job->id;?>" title=""><img src="/assets/css/img/ico-delete.png" style="margin-left:2px;" alt="" /></a>
									</div><?
								}
								
								if ($job->main_category == 25){ // darba uzdevums - bezmaksas neierobežotā daudzumā
									$price = $price + 0;
								}else{
									if ($job->noprice == 1){ 
										$limbo_free--;
									}
									if ($job->noprice == 1 && $limbo_free > 0){ // viens no bezmaksas Limbo darbiem
										$price = $price + 0;
									/*if ($job->noprice == 1 && $limbo_free >= 1){ 	// ja grib pēc diviem bezmaksas limbo darbiem vēl piešķirt trešo bezmaksas parasto darbu,
										$price = $price + 0;						// jāatkomentē šīs 3 rindiņas un jāizkomentē 4-5 rindiņas kas virs šīm.
										$limbo_free--;*/
									}else{ // Parastais darbs vai, ja bezmaksas limbo darbi iztērēti
										if ($job->antalis == 1 && !empty($job->antalis_type) && $job->antalis_type != 'Ieraksti, kādus Antalis papīrus vai materiālus izmantoji'){
											if ($antalis_free == 1) {
												$price = $price + 0;
												$antalis_free = 0;
											}else{
												if ($free_job == 1){
													$price = $price + 0;
													$free_job = 0;
												}else{
													if($job->order_status == 0 && $job->bill != 1){
														$price = $price + Kohana::$config->load('site')->get('half_price');
													}else{
														$price = $price + 0;
													}
												}
											}
										}else{
											if ($free_job == 1){
												$price = $price + 0;
												$free_job = 0;
											}else{
												if($job->order_status == 0 && $job->bill != 1){
													$price = $price + Kohana::$config->load('site')->get('item_price');
												}else{
													$price = $price + 0;
												}
											}
										}
									}
								} // darba uzdevums
                            ?>
							</div>
							<?}?>
                            				
							<?if($price > 0){?>
								<div class="order-details">
									<div class="billing">
										<div class="checkbox"><input type="checkbox" name="bill" id="bill" value="1" /></div>
										<div class="label">Vēlos saņemt rēķinu</div>
									</div>
									<div class="total"><?=$price;?> Ls</div>
									<div class="purchase">
										<a href="javascript:;" onclick="javascript:site.ibis.go(<?=Session::instance()->get('user_uid');?>);" title="Apmaksāt">Apmaksāt</a>
									</div>
								</div>
							<?}?>
							
							<?if($price == 0 && $not_saved == 1){?>
								<div class="order-details">
									<div class="purchase">
										<a href="<?=URL::section($section->id);?>?action=submit" title="Apstiprināt">Apstiprināt</a>
									</div>
								</div>
							<?}?>
						</div>
						<?}?>
						
						<p>&nbsp;</p>
					</div>					
					
					<div class="page-content" id="insert">
					    <h1><?=$item->id == '' ? e($section->title) : 'Labot darbu - '.$item->title_lv;?></h1>
						<?$time = time();?>
						<?$end = mktime(0,0,0,4,6,2013);?>
						<?if(Session::instance()->get('user_uid') == ''){?>
						<p><?=l('musit_login');?></p>
						<?}elseif($time > $end && $_SERVER['REMOTE_ADDR'] != '62.63.138.162' && $_SERVER['REMOTE_ADDR'] != '77.93.5.158'){?>
						<p>Darbu iesniegšanas termiņš ir beidzies. Gaidām jūs nākamgad!</p>
						<?}else{?>
						<form method="post" name="myform" id="myform" action="<?=URL::section($section->id);?>?edit=<?=$item->id;?>" enctype="multipart/form-data">
						<div class="form">
							<div>
							    <p>
									<label for="">Pieteikuma ID</label>
								</p>
								<p>
								    <input type="text" name="idn" readonly="readonly" class="idn" value="<?=$item->code.'_'.$item->id;?>" />
								</p>
							</div>
							
							<div style="padding:15px 0 20px 0;">
								<p>
									<input type="checkbox" name="noprice"<?if($item->order_status == 1){?> disabled="disabled"<?}?> style="width:20px;" id="noprice" value="1"<?=$item->noprice == 1 ? ' checked="checked"' : '';?> />
									<label for="noprice">Limbo darbs</label>
								</p>
							</div>
							
							<div style="padding:0 0 20px 0;">
								<p>
									<input onchange="javascript" type="checkbox" name="antalis"<?if($item->order_status == 1){?> disabled="disabled"<?}?> style="width:20px;" id="antalis" value="1"<?=$item->antalis == 1 ? ' checked="checked"' : '';?> />
									<label for="antalis">Darbs uz Antalis papīra</label>
								</p>
							</div>
							<div id="antalis_type_div" class="antalis_type_div" style="padding:0 0 20px 0;display:none;">
								<p>
									<input type="text" name="antalis_type" id="antalis_type" value="<?=$item->antalis_type;?>"<?if($item->order_status == 1){?> disabled="disabled"<?}?> />
								</p>
							</div>
							
						    <div class="line">
						    	<div class="left">
								    <p>
										<label for="main_category">Pamatkategorija <span>*</span></label>
									</p>
									<p class="input" style="background:none;">
									    <select name="main_category" id="main_category"<?if($item->order_status == 1){?> disabled="disabled"<?}?> onchange="this.className='';site.load.subsections('<?=URL::section($section->id);?>');">
										    <option value="">Izvēlieties pamata kategoriju</option>
											<?foreach($categories as $c){?>
											<option value="<?=$c->id;?>"<?=$item->main_category == $c->id ? ' selected="selected"' : '';?>><?=$c->title;?></option>
											<?}?>
										</select>
									</p>
								</div>
								<div class="right" id="courier_button" style="display:none;">
									<a class="courier_button" href="javascript:;" title="Bezmaksas kurjers no Antalis" id="request_courier">Bezmaksas kurjers no Antalis</a>
								</div>
							</div>
							
							<div>
							    <p>
									<label for="sub_category">Apakškategorija <span>*</span></label>
								</p>
								<p class="input" style="background:none;">
								    <select name="sub_category" id="sub_category"<?if($item->order_status == 1){?> disabled="disabled"<?}?> onchange="site.load.getformats('<?=URL::section($section->id);?>');">
									    <option value="">Izvēlieties apakškategoriju</option>
										<?foreach($subs as $s){?>
										<?
										if($item->sub_category == $s->id){
											$for_book = $s->book;
											$for_web = $s->web;
										}
										?>
										<option value="<?=$s->id;?>"<?=$item->sub_category == $s->id ? ' selected="selected"' : '';?>><?=$s->title;?></option>
										<?}?>
									</select>
								</p>
							</div>
							
							<div class="line">
								<div class="left">
									<p>
										<label for="">Nosaukums latviski <span>*</span></label>
									</p>
									<p>
										<input type="text" name="title_lv" id="title_lv" value="<?=$item->title_lv;?>"<?if($item->order_status == 1){?> disabled="disabled"<?}?> />
									</p>
								</div>
								<div class="right">
									<p>
										<label for="">Nosaukums angliski <span>*</span></label>
									</p>
									<p>
										<input type="text" name="title_en" id="title_en" value="<?=$item->title_en;?>"<?if($item->order_status == 1){?> disabled="disabled"<?}?> />
									</p>
								</div>
							</div>
							
							<div class="line">
								<div class="left">
									<p>
										<label for="">Klients latviski <span>*</span></label>
									</p>
									<p>
										<input type="text" name="client_lv" id="client_lv" value="<?=$item->client_lv;?>"<?if($item->order_status == 1){?> disabled="disabled"<?}?> />
									</p>
								</div>
								<div class="right">
									<p>
										<label for="">Klients angliski <span>*</span></label>
									</p>
									<p>
										<input type="text" name="client_en" id="client_en" value="<?=$item->client_en;?>"<?if($item->order_status == 1){?> disabled="disabled"<?}?> />
									</p>
								</div>
							</div>
							
							<div class="line">
								<div class="left">
									<p>
										<label for="">Produkts latviski <span>*</span></label>
									</p>
									<p>
										<input type="text" name="product_lv" id="product_lv" value="<?=$item->product_lv;?>"<?if($item->order_status == 1){?> disabled="disabled"<?}?> />
									</p>
								</div>
								<div class="right">
									<p>
										<label for="">Produkts angliski <span>*</span></label>
									</p>
									<p>
										<input type="text" name="product_en" id="product_en" value="<?=$item->product_en;?>"<?if($item->order_status == 1){?> disabled="disabled"<?}?> />
									</p>
								</div>
							</div>
							
							<div class="line">
								<div class="left">
									<?if($item->order_status == 1){?>
										<?if($item->description_lv != ''){?>
											<p style="overflow:hidden;">
												<label for="description_lv" style="float:left;">Apraksts latviski <span>*</span></label>
												<span id="d_lv" style="float:right;width:40px;text-align:right;font-size:11px;">500</span>
											</p>
											<p style="padding:5px 10px 5px 10px;color:#fff;font-size:11px;background:#851b21;">
												<?=$item->description_lv;?>
											</p>
										<?}?>
									<?}else{?>
										<p style="overflow:hidden;">
												<label for="description_lv" style="float:left;">Apraksts latviski <span>*</span></label>
												<span id="d_lv" style="float:right;width:40px;text-align:right;font-size:11px;">500</span>
											</p>
										<p>
											<textarea name="description_lv" id="description_lv" rows="" cols="" onkeyup="limitText(this.form.description_lv,this.form.d_lv,500,'d_lv');" onkeydown="limitText(this.form.description_lv,this.form.d_lv,500,'d_lv');"><?=$item->description_lv;?></textarea>
										</p>
									<?}?>
								</div>
								<div class="right">
									<?if($item->order_status == 1){?>
										<?if($item->description_en != ''){?>
											<p style="overflow:hidden;">
												<label for="description_en" style="float:left;">Apraksts angliski <span>*</span></label>
												<span id="d_en" style="float:right;width:40px;text-align:right;font-size:11px;">500</span>
											</p>
											<p style="padding:5px 10px 5px 10px;color:#fff;font-size:11px;background:#851b21;">
												<?=$item->description_en;?>
											</p>
										<?}?>
									<?}else{?>
										<p style="overflow:hidden;">
												<label for="description_en" style="float:left;">Apraksts angliski <span>*</span></label>
												<span id="d_en" style="float:right;width:40px;text-align:right;font-size:11px;">500</span>
											</p>
										<p>
											<textarea name="description_en" id="description_en" rows="" cols="" onkeyup="limitText(this.form.description_en,this.form.d_en,500,'d_en');" onkeydown="limitText(this.form.description_en,this.form.d_en,500,'d_en');"><?=$item->description_en;?></textarea>
										</p>
									<?}?>
								</div>
							</div>
							
							<div class="line">
								<div class="left">
									<p>
										<label for="">Radošais direktors</label>
									</p>
									<p>
										<input type="text" name="creative_1" id="creative_1" value="<?=$item->creative_1;?>"<?if($item->order_status == 1){?> disabled="disabled"<?}?> />
									</p>
								</div>
								<div class="right">
									<p>
										<label for="">Radošais direktors 2</label>
									</p>
									<p>
										<input type="text" name="creative_2" id="creative_2" value="<?=$item->creative_2;?>"<?if($item->order_status == 1){?> disabled="disabled"<?}?> />
									</p>
								</div>
							</div>
							
							<div class="line">
								<div class="left">
									<p>
										<label for="">Mākslinieciskais direktors</label>
									</p>
									<p>
										<input type="text" name="art_1" id="art_1" value="<?=$item->art_1;?>"<?if($item->order_status == 1){?> disabled="disabled"<?}?> />
									</p>
								</div>
								<div class="right">
									<p>
										<label for="">Projekta vadītājs</label>
									</p>
									<p>
										<input type="text" name="art_2" id="art_2" value="<?=$item->art_2;?>"<?if($item->order_status == 1){?> disabled="disabled"<?}?> />
									</p>
								</div>
							</div>
							
							<div class="line">
								<div class="left">
									<p>
										<label for="">Dizainers</label>
									</p>
									<p>
										<input type="text" name="designer_1" id="designer_1" value="<?=$item->designer_1;?>"<?if($item->order_status == 1){?> disabled="disabled"<?}?> />
									</p>
								</div>
								<div class="right">
									<p>
										<label for="">Dizainers 2</label>
									</p>
									<p>
										<input type="text" name="designer_2" id="designer_2" value="<?=$item->designer_2;?>"<?if($item->order_status == 1){?> disabled="disabled"<?}?> />
									</p>
								</div>
							</div>
							
							<div class="line">
								<div class="left">
									<p>
										<label for="">Programmētājs</label>
									</p>
									<p>
										<input type="text" name="programmer_1" id="programmer_1" value="<?=$item->programmer_1;?>"<?if($item->order_status == 1){?> disabled="disabled"<?}?> />
									</p>
								</div>
								<div class="right">
									<p>
										<label for="">Programmētājs 2</label>
									</p>
									<p>
										<input type="text" name="programmer_2" id="programmer_2" value="<?=$item->programmer_2;?>"<?if($item->order_status == 1){?> disabled="disabled"<?}?> />
									</p>
								</div>
							</div>
							
							<div class="line">
								<div class="left">
									<p>
										<label for="">Tekstu autors</label>
									</p>
									<p>
										<input type="text" name="text_1" id="text_1" value="<?=$item->text_1;?>"<?if($item->order_status == 1){?> disabled="disabled"<?}?> />
									</p>
								</div>
								<div class="right">
									<p>
										<label for="">Tekstu autors 2</label>
									</p>
									<p>
										<input type="text" name="text_2" id="text_2" value="<?=$item->text_2;?>"<?if($item->order_status == 1){?> disabled="disabled"<?}?> />
									</p>
								</div>
							</div>
							
							<div class="line">
								<div class="left">
									<p>
										<label for="">Fotogrāfs</label>
									</p>
									<p>
										<input type="text" name="photographer_1" id="photographer_1" value="<?=$item->photographer_1;?>"<?if($item->order_status == 1){?> disabled="disabled"<?}?> />
									</p>
								</div>
								<div class="right">
									<p>
										<label for="">Fotogrāfs 2</label>
									</p>
									<p>
										<input type="text" name="photographer_2" id="photographer_2" value="<?=$item->photographer_2;?>"<?if($item->order_status == 1){?> disabled="disabled"<?}?> />
									</p>
								</div>
							</div>
							
							<div class="line">
								<div class="left">
									<p>
										<label for="">Režisors</label>
									</p>
									<p>
										<input type="text" name="rezisors_1" id="rezisors_1" value="<?=$item->rezisors_1;?>"<?if($item->order_status == 1){?> disabled="disabled"<?}?> />
									</p>
								</div>
								<div class="right">
									<p>
										<label for="">Cits (norādiet vārdu un lomu projektā)</label>
									</p>
									<p>
										<input type="text" name="rezisors_2" id="rezisors_2" value="<?=$item->rezisors_2;?>"<?if($item->order_status == 1){?> disabled="disabled"<?}?> />
									</p>
								</div>
							</div>
                            <div class="line" id="printed-1" style="display: none;">
                            	
							<div class="label"><label>ŽŪRIJAS VĒRTĒJUMAM</label></div>

                            <div class="line" id="printed-1" style="display: none;">
                                        <label style="text-transform:none;">Drukātie paraugi uzlikti uz A2 vai A3 planšetes jānogādā LADC birojā. Antalis visiem drukas darbiem, kas jānogādā uz LADC biroju, nodrošina bezmaksas kurjeru. Nepalaid garām iespēju izmantot bezmaksas kurjera pakalpojumu!</label>
                            </div>   
                            <div class="line" id="printed-2" style="display: none;">
                                        <label style="text-transform:none;">Drukātie paraugi jānogādā LADC birojā. Antalis visiem drukas darbiem, kas jānogādā uz LADC biroju, nodrošina bezmaksas kurjeru. Nepalaid garām iespēju izmantot bezmaksas kurjera pakalpojumu!</label>
                            </div>
                            <div class="line" id="printed-3" style="display: none;">
                                        <label style="text-transform:none;">Produkcijas paraugi jānogādā LADC birojā. Antalis visiem drukas darbiem, kas jānogādā uz LADC biroju, nodrošina bezmaksas kurjeru. Nepalaid garām iespēju izmantot bezmaksas kurjera pakalpojumu!</label>
                            </div>
                            </div>
                                                    
							<div id="all-files" style="<?if($item->main_category == 19){?>display:none;<?}?>">
								<div id="digi-audio" class="file-upload" style="padding-top:20px;">
									<div class="label"><label for="audio">Audio ielāde</label></div>
									<div class="data">
										<p class="file">
											<input type="file" name="audio" id="audio" value="" />
										</p>
										<?if($item->audio != ''){?>
										<p class="delete">
											<a href="<?=URL::section($section->id);?>?delfile=<?=$item->id;?>&type=audio" title=""><img src="/assets/css/img/ico-delete.png" style="margin-left:2px;" alt="" /></a>
										</p>
										<?}?>
									</div>
									<div class="info">
										<p>Formāts wav/.aiff / MP3 / AAC Tax 128KBPS vai lielāks</p>
									</div>
								</div> 
							
								<div id="digi-video" class="file-upload" style="padding-top:20px;">
									<div class="label"><label for="video">Video ielāde</label></div>
									<div class="data">
										<p class="file">
											<input type="file" name="video" id="video" value="" />
										</p>
										<?if($item->video != ''){?>
										<p class="delete">
											<a href="<?=URL::section($section->id);?>?delfile=<?=$item->id;?>&type=video" title=""><img src="/assets/css/img/ico-delete.png" style="margin-left:2px;" alt="" /></a>
										</p>
										<?}?>
									</div>
									<div class="info" id="info_video">
										<p>Augšupielādēt video .mov formātā CODEC H.264, 720x576 pikseļi (4:3) vai 1024x576 (16:9)<br/><br/>Papildus video var augšupielādēt pēc pieteikuma saglabāšanas</p>
									</div>
								</div> 
								
								<?if($item->video != '' || $item->video_2 != ''){?>
								<div class="file-upload">
									<div class="label"><label for="video">Video ielāde #2</label></div>
									<div class="data">
										<p class="file">
											<input type="file" name="video_2" id="video_2" value="" />
										</p>
										<?if($item->video_2 != ''){?>
										<p class="delete">
											<a href="<?=URL::section($section->id);?>?delfile=<?=$item->id;?>&type=video_2" title=""><img src="/assets/css/img/ico-delete.png" style="margin-left:2px;" alt="" /></a>
										</p>
										<?}?>
									</div>
								</div> 
								<?}?>
								
								<?if($item->video_2 != '' || $item->video_3 != ''){?>
								<div class="file-upload">
									<div class="label"><label for="video">Video ielāde #3</label></div>
									<div class="data">
										<p class="file">
											<input type="file" name="video_3" id="video_3" value="" />
										</p>
										<?if($item->video_3 != ''){?>
										<p class="delete">
											<a href="<?=URL::section($section->id);?>?delfile=<?=$item->id;?>&type=video_3" title=""><img src="/assets/css/img/ico-delete.png" style="margin-left:2px;" alt="" /></a>
										</p>
										<?}?>
									</div>
								</div> 
								<?}?>
								
								<?if($item->video_3 != '' || $item->video_4 != ''){?>
								<div class="file-upload">
									<div class="label"><label for="video">Video ielāde #4</label></div>
									<div class="data">
										<p class="file">
											<input type="file" name="video_4" id="video_4" value="" />
										</p>
										<?if($item->video_4 != ''){?>
										<p class="delete">
											<a href="<?=URL::section($section->id);?>?delfile=<?=$item->id;?>&type=video_4" title=""><img src="/assets/css/img/ico-delete.png" style="margin-left:2px;" alt="" /></a>
										</p>
										<?}?>
									</div>
								</div> 
								<?}?>
								
								<div id="link-to-video">
									<p>
										<label for="video_http">Saite uz video</label>
									</p>
									<p>
										<input type="text" name="video_http" id="video_http" value="<?=$item->video_http;?>" />
									</p>
								</div>
							</div>
							
							<div id="digital-files" style="<?if($item->main_category != 19){?>display:none;<?}?>">
								<div class="file-upload" style="padding-top:20px;">
									<div class="label"><label for="video">Video ielāde</label></div>
									<div class="data">
										<p class="file">
											<input type="file" name="video_digital" id="video_digital" value="" />
										</p>
										<?if($item->video_digital != ''){?>
										<p class="delete">
											<a href="<?=URL::section($section->id);?>?delfile=<?=$item->id;?>&type=video_digital" title=""><img src="/assets/css/img/ico-delete.png" style="margin-left:2px;" alt="" /></a>
										</p>
										<?}?>
									</div>
									<div class="info" id="info_video">
										<p>Augšupielādēt video .mov formātā CODEC H.264, 720x576 pikseļi (4:3) vai 1024x576 (16:9).<br/><br/>Vai URL (+paroles, ja tādas nepieciešamas)</p>
									</div>
								</div>
								
								<div>
									<p>
										<label for="video_http">Saite uz video</label>
									</p>
									<p>
										<input type="text" name="video_digital_http" id="video_digital_http" value="<?=$item->video_digital_http;?>" />
									</p>
								</div>
								
								<div class="file-upload">
									<div class="label"><label for="audio">URL (+paroles, ja tādas nepieciešamas)</label></div>
									<div class="data">
										<p class="file" style="width:310px;">
											<textarea name="url" id="url" rows="" cols=""><?=$item->url;?></textarea>
										</p>
									</div>
								</div> 
							</div>
							
							<div class="file-upload" style="padding-top:20px;" id="zip_upload">
								<div class="label"><label for="zip">Arhīva fails ar attēliem un citiem failiem</label></div>
								<div class="data">
									<p class="file">
										<input type="file" name="zip" id="zip" value="" />
									</p>
									<?if($item->zip != ''){?>
									<p class="delete">
										<a href="<?=URL::section($section->id);?>?delfile=<?=$item->id;?>&type=zip" title=""><img src="/assets/css/img/ico-delete.png" style="margin-left:2px;" alt="" /></a>
									</p>
									<?}?>
								</div>
								<div class="info">
								    <p>ZIP vai RAR arhīvs ar attēliem un/vai citiem failiem žūrijai (dažādi formāti - .jpg, .tiff, .ppt, .mov utt.), pēc kategorijai atbilstošajiem formātiem un specifikācijām. <a href="kategorijas-un-formati" target="_blank">Vairāk info par formātiem</a></p>
								</div>
							</div> 
							
							<!--<div class="file-upload" style="padding-top:20px;display:none;" id="zip_upload2">
								<div class="label"><label for="audio">Arhīvs ar failiem</label></div>
								<div class="data">
									<p class="file">
										<input type="file" name="zip" id="zip" value="" />
									</p>
									<?if($item->zip != ''){?>
									<p class="delete">
										<a href="<?=URL::section($section->id);?>?delfile=<?=$item->id;?>&type=zip" title=""><img src="/assets/css/img/ico-delete.png" style="margin-left:2px;" alt="" /></a>
									</p>
									<?}?>
								</div>
								<div class="info">
								    <p>ZIP vai RAR arhīvs ar failiem
                                                                    </p>
								</div>
							</div> -->
							
							<div id="for-book-img" class="file-upload" style="padding-top:20px;">
								<div class="label"><label>Attēli ADWARDS GADAGRĀMATAI</label></div>
								<div class="data">
									<div class="item">
										<p class="file">
											<input type="file" name="image_book_1" id="image_book_1" value="" />
										</p>
										<?if($item->image_book_1 != ''){?>
										<p class="delete">
											<a href="<?=URL::section($section->id);?>?delfile=<?=$item->id;?>&type=image_book_1" title=""><img src="/assets/css/img/ico-delete.png" style="margin-left:2px;" alt="" /></a>
										</p>
										<?}?>
									</div>
									<div class="item">
										<p class="file">
											<input type="file" name="image_book_2" id="image_book_2" value="" />
										</p>
										<?if($item->image_book_2 != ''){?>
										<p class="delete">
											<a href="<?=URL::section($section->id);?>?delfile=<?=$item->id;?>&type=image_book_2" title=""><img src="/assets/css/img/ico-delete.png" style="margin-left:2px;" alt="" /></a>
										</p>
										<?}?>
									</div>
									<div class="item">
										<p class="file">
											<input type="file" name="image_book_3" id="image_book_3" value="" />
										</p>
										<?if($item->image_book_3 != ''){?>
										<p class="delete">
											<a href="<?=URL::section($section->id);?>?delfile=<?=$item->id;?>&type=image_book_3" title=""><img src="/assets/css/img/ico-delete.png" style="margin-left:2px;" alt="" /></a>
										</p>
										<?}?>
									</div>
									<div class="item">
										<p class="file">
											<input type="file" name="image_book_4" id="image_book_4" value="" />
										</p>
										<?if($item->image_book_4 != ''){?>
										<p class="delete">
											<a href="<?=URL::section($section->id);?>?delfile=<?=$item->id;?>&type=image_book_4" title=""><img src="/assets/css/img/ico-delete.png" style="margin-left:2px;" alt="" /></a>
										</p>
										<?}?>
									</div>
									<div class="item">
										<p class="file">
											<input type="file" name="image_book_5" id="image_book_5" value="" />
										</p>
										<?if($item->image_book_5 != ''){?>
										<p class="delete">
											<a href="<?=URL::section($section->id);?>?delfile=<?=$item->id;?>&type=image_book_5" title=""><img src="/assets/css/img/ico-delete.png" style="margin-left:2px;" alt="" /></a>
										</p>
										<?}?>
									</div>
									<div class="item">
										<p class="file">
											<input type="file" name="image_book_6" id="image_book_6" value="" />
										</p>
										<?if($item->image_book_6 != ''){?>
										<p class="delete">
											<a href="<?=URL::section($section->id);?>?delfile=<?=$item->id;?>&type=image_book_6" title=""><img src="/assets/css/img/ico-delete.png" style="margin-left:2px;" alt="" /></a>
										</p>
										<?}?>
									</div>
								</div>
								<div class="info" id="info_book">
								    <?=$for_book;?>
								</div>
							</div> 
							
							
							<div id="for-web-img" class="file-upload" style="padding-top:20px;">
								<div class="label"><label>Attēli ADWARDS TĪMEKĻA VIETNĒM</label></div>
								<div class="data">
									<div class="item">
										<p class="file">
											<input type="file" name="image_web_1" id="image_web_1" value="" />
										</p>
										<?if($item->image_web_1 != ''){?>
										<p class="delete">
											<a href="<?=URL::section($section->id);?>?delfile=<?=$item->id;?>&type=image_web_1" title=""><img src="/assets/css/img/ico-delete.png" style="margin-left:2px;" alt="" /></a>
										</p>
										<?}?>
									</div>
									<div class="item">
										<p class="file">
											<input type="file" name="image_web_2" id="image_web_2" value="" />
										</p>
										<?if($item->image_web_2 != ''){?>
										<p class="delete">
											<a href="<?=URL::section($section->id);?>?delfile=<?=$item->id;?>&type=image_web_2" title=""><img src="/assets/css/img/ico-delete.png" style="margin-left:2px;" alt="" /></a>
										</p>
										<?}?>
									</div>
									<div class="item">
										<p class="file">
											<input type="file" name="image_web_3" id="image_web_3" value="" />
										</p>
										<?if($item->image_web_3 != ''){?>
										<p class="delete">
											<a href="<?=URL::section($section->id);?>?delfile=<?=$item->id;?>&type=image_web_3" title=""><img src="/assets/css/img/ico-delete.png" style="margin-left:2px;" alt="" /></a>
										</p>
										<?}?>
									</div>
									<div class="item">
										<p class="file">
											<input type="file" name="image_web_4" id="image_web_4" value="" />
										</p>
										<?if($item->image_web_4 != ''){?>
										<p class="delete">
											<a href="<?=URL::section($section->id);?>?delfile=<?=$item->id;?>&type=image_web_4" title=""><img src="/assets/css/img/ico-delete.png" style="margin-left:2px;" alt="" /></a>
										</p>
										<?}?>
									</div>
									<div class="item">
										<p class="file">
											<input type="file" name="image_web_5" id="image_web_5" value="" />
										</p>
										<?if($item->image_web_5 != ''){?>
										<p class="delete">
											<a href="<?=URL::section($section->id);?>?delfile=<?=$item->id;?>&type=image_web_5" title=""><img src="/assets/css/img/ico-delete.png" style="margin-left:2px;" alt="" /></a>
										</p>
										<?}?>
									</div>
									<div class="item">
										<p class="file">
											<input type="file" name="image_web_6" id="image_web_6" value="" />
										</p>
										<?if($item->image_web_6 != ''){?>
										<p class="delete">
											<a href="<?=URL::section($section->id);?>?delfile=<?=$item->id;?>&type=image_web_6" title=""><img src="/assets/css/img/ico-delete.png" style="margin-left:2px;" alt="" /></a>
										</p>
										<?}?>
									</div>
								</div>
								<div class="info" id="info_web">
								   <?=$for_web;?>
								</div>
							</div> 
														
							<?if($item->order_status == 1){?>
								<?if($item->notes != ''){?>
								<div style="padding-top:20px;">
									<p>
										<label for="">Piezīmes</label>
									</p>
									<p style="width:282px;padding:5px 10px 5px 10px;color:#fff;font-size:11px;background:#851b21;">
										<?=$item->notes;?>
									</p>
								</div>
								<?}?>
							<?}else{?>
							<div style="padding-top:20px;">
							    <p>
									<label for="">Piezīmes</label>
								</p>
								<p>
								   <textarea name="notes" id="notes" rows="" cols=""><?=$item->notes;?></textarea>
								</p>
							</div>
							<?}?>
														
							<input type="hidden" name="id" value="<?=$item->id;?>" />
							
							<?$hash_id = md5($item->id.'QWCcc4234');?>
							
							<?if($item->order_status == 0){?>
							<p>
								<input type="submit" class="submit" value="Saglabāt" />
							</p>
							<p>
								<span style="position:relative;left:-213px;"><a href="<?=URL::section(53);?>?id=<?=$hash_id;?>" target="_blank" title="Drukāt" class="print">Drukāt</a></span>
							</p>
							<?}else{?>
							<p>
								<a href="<?=URL::section(53);?>?id=<?=$hash_id;?>" target="_blank" title="Drukāt" class="print">Drukāt</a>
							</p>

							<?}?>
							
						</div>
						</form>
						<?}?>
					</div>
				</div>
				
				<script>
				$(function() {
					var m = $(".leftside ul").height();
					var j = $(".jobs").height();
					var h = $(".page-content").height();
					var n = h - m - j - 11;
					$(".leftside p").height(n);
				});
				</script>
				
				<?if($message_update == 1){?>
				<script>
				$(function() {
					site.popup.show('<?=l('job_update_ok');?>');
				});
				</script>
				<?}?>
			</div>
			<div class="courier-request-box" id="courier-request-box" style="position:absolute;top:820px;">
			<form id="cform" method="post" action="<?=URL::section(62);?>">
				<div style="width:417px;padding:none;margin:none;border-left:20px solid #851b21;">
				<p>
				    <label for="address">Adrese, kur ierasties kurjeram:</label>
					<input class="input-long" type="text" name="address" id="address" />
				</p>					
					<div style="width:204px;float:left;padding:none;margin:none;">
						<p>
						    <label for="contact">Kontaktpersona:</label>
							<input type="text" name="contact" id="contact" />
						</p>
					</div>
					<div style="width:204px;float:right;padding:none;margin:none;">
						<p>
						    <label for="phone">Kontakttālrunis:</label>
							<input type="text" name="phone" id="phone" />
						</p>
					</div>
				</div>
				<div style="width:100%;float:left;">
					<input type="submit" class="submit" value="Sūtīt" />
				</div>
			</form>
			</div>
			<?echo "\n\t",HTML::script('assets/js/edit.js?v=13');?>
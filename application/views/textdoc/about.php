
	<?foreach($sections as $category){?>
		<div id="section-<?=$category->id;?>" class="adwards-section" style="min-height:670px;background:url(<?=URL::base().'assets/media/images/'.$category->image;?>) no-repeat center center fixed; background-size: cover;">
			<h2><?=$category->title;?></h2>
		</div>
		<div class="adwards-section-wrapper">
			<div class="adwards-section-content">
				<?$i=0;if($category->id == 13){?>
					<div class="jury section-wide">
						<?foreach($people as $item){?>
						<div class="item" style="<?=++$i%4==0?'margin-right:0;':'';?>background:url(<?=URL::base().'assets/media/images/'.$item->image_214_299;?>) no-repeat;">
							 <a href="<?=URL::site('lv/par-adwards/zurija').'/'.$item->link;?>" class="jury-link">
                                                             <div class="over" data-num="<?=$i;?>" data-id="<?=$item->id;?>">
							 	<h3><?=$item->title;?></h3>
							 	<h4><?=$item->job;?></h4>
                                                             </div></a>
						</div>
						<?}?>
					</div>
				<?}else{?>
                                    <div<?if($category->id == 41) echo ' class="section-wide"';?>>
                                        <?=e($category->textdoc->content)->safe();?>
                                    </div>
				<?}?>
			</div>
		</div>
	<?}?>
	<!--<p class="to-top"><img src="<?=URL::base();?>assets/css/img/btn-to-top.png" alt="" /></p>-->
	<script>
        var juryView = false;
        var scrollPos = 0;
	$(function(){
                var iOS = /(iPad|iPhone|iPod)/g.test( navigator.userAgent );
                if(iOS) {
                    $('.adwards-section').each(function() {
                        var x = (1600 - $(window).width())/2;
                        $(this).css({
                            'height': '670px',
                            'width': '100%',
                            'background-size': '1600px 670px', 
                            'background-position': -x+'px '+$(this).offset().top+'px'
                        });
                    });
                }
                
                function topVisible(elem)
                {
                    var docViewTop = $(window).scrollTop();
                    var elemTop = $(elem).offset().top;
                    return (docViewTop > (elemTop - (2 * $(window).height())));
                }
                function bottomVisible(elem)
                {
                    var docViewTop = $(window).scrollTop();
                    var elemBottom = $(elem).offset().top + $(elem).height();
                    return (docViewTop < (elemBottom + ($(window).height())));
                }
                function visible(elem)
                {
                    var docViewTop = $(window).scrollTop();
                    var docViewBottom = docViewTop + $(window).height();

                    var elemTop = $(elem).offset().top;
                    var elemBottom = elemTop + $(elem).height();
                   
                    return (((docViewBottom > elemTop) && (docViewTop < elemTop)) || ((docViewTop < elemBottom) && (docViewBottom > elemBottom)));
                    
                }
            
                var sections = [];
                $('.adwards-section').each(function() {
                    var obj = $(this);
                    var section = {
                        id: obj.attr('id').match(/\d+$/)[0],
                        object: obj[0],
                        url: '',
                        title: ''
                    };
                    $.post('<?=URL::section(2);?>', {id:section.id}, function(data){
                        section.url = data.url;
                        section.title = data.title;
                    });
                    sections.push(section);
                });
                
                <?if($sect == 'jury') {
                    if($jury) {?>
                        var initialMember = <?=$jury; }?>;
                    $('html, body').animate({
                        scrollTop: $(".jury").offset().top
                    }, 0);
                <?}
                else if($sect) {?>
                    $('html, body').animate({
                        scrollTop: $("#section-<?=$sect;?>").offset().top
                    }, 0);
                <?}?>
                $('a.jury-link').click(function(event) {
                    event.preventDefault();
                });
                // 'div#section-'+sections[i])[0]
                $(window).scroll(function(event) {
                    for(i in sections) {
                        if(visible(sections[i].object)) {
                                var state = {
                                    "Section": sections[i].title
                                  };
                                  history.pushState(state, "Par Adwards", sections[i].url);
                                  //expect(history.state).toEqual(state);
                        }
                    }
                    var st = $(this).scrollTop();
                    var scrolldif = st - scrollPos;
                    var height = $('.jury-popup').height();
                    var top = $('.jury-popup').offset().top;
                    var bottom = top + height;
                    var bdif = st - (bottom-500);
                    var tdif = -(st - (top-100));
                    var cur = parseInt($('.jury-left, .jury-right').css('top'));
                    if(juryView === true && scrollPos > 0) {
                        if(st > scrollPos) {
                            if(bdif > 0) {
                                    $('.jury-left, .jury-right').css({'top': (320-bdif)+'px'});
                                }
                                else if(tdif > 0) {
                                    $('.jury-left, .jury-right').css({'top': (320+tdif)+'px'});
                                }
                                else {
                                    $('.jury-left, .jury-right').css({'top': '320px'});                                    
                                }
                            if(!bottomVisible('.jury-popup')) {
                                $('.overlay, .jury-popup, .jury-left, .jury-right').fadeOut(150,function() {
                                    $('.jury-left, .jury-right').css({'top': '320px'});  
                                }); 
                                juryView = false;
                            }
                        } //down
                        else if(st < scrollPos){
                             if(tdif > 0) {
                                    $('.jury-left, .jury-right').css({'top': (cur-scrolldif)+'px'});
                                }
                                else if(bdif > 0 ) {
                                    $('.jury-left, .jury-right').css({'top': (cur-scrolldif)+'px'});
                                }
                                else {
                                    $('.jury-left, .jury-right').css({'top': '320px'});  
                                }
                            if(!topVisible('.jury-popup')) {
                                $('.overlay, .jury-popup, .jury-left, .jury-right').fadeOut(150,function() {
                                    $('.jury-left, .jury-right').css({'top': '320px'});  
                                }); 
                                juryView = false;
                            }
                        } //up
                    }
                    scrollPos = st;
                });
                	
                function juryPositioning() {
                    $('.jury-popup').offset({
                        top: $(window).scrollTop()
                    });
                }
                        
		$('.jury-left, .jury-right').click(function(){
			$('.jury-popup .c').fadeOut(150);
                        //$('.jury-right').fadeOut(75);
                        //$('.jury-left').fadeOut(75);
			var current = $('[data-id='+$('input[name=selected_jury]').val()+']');
			if($(this).attr('class') == 'jury-left'){
                                var prev = current.closest('.item').prev('.item');
                                if(prev.length > 0) {
                                    var obj = prev.find('.over');
                                }
                                else {
                                    var obj = $('.item').filter(':last').find('.over');
                                }
				//var obj = current.closest('.item').prev('.item').find('.over');
                                
			}else{
                                var next = current.closest('.item').next('.item');
                                if(next.length > 0) {
                                    var obj = next.find('.over');
                                }
                                else {
                                    var obj = $('.item').filter(':first').find('.over');
                                }
				//var obj = next.find('.over');
                                
			}
			
			var id = obj.data('id');
			var num = obj.data('num');
			/*
			if(num > 1){
				$('.jury-left').fadeIn();
			}else{
				$('.jury-left').fadeOut();
			}
				
			if(num < <?=count($people);?>){
				$('.jury-right').fadeIn();
			}else{
				$('.jury-right').fadeOut();
			}
			*/
			$('input[name=selected_jury]').val(id);
			
			$.post('<?=URL::section(13);?>', {id:id}, function(data){
				$('.jury-popup h2').html(data.name);
				$('.jury-popup h3').html(data.job);
				$('.jury-popup .about div div').html(data.text);
				$('.jury-popup .image').html(data.image);
				$('input[name=selected_jury]').val(data.id);
				$('.jury-popup .c').fadeIn(150);
                                  var state = {
                                    "Jury": data.name
                                  };
                                  history.pushState(state, "Par Adwards", data.url);
                                  //expect(history.state).toEqual(state);
			},'json');
		});
		
		$('.over').click(function(){
			var id = $(this).data('id');
			var num = $(this).data('num');
                        juryView = true;
                        $('.jury-left').fadeIn();
                        $('.jury-right').fadeIn();
                        /*
			if(num > 1) 
				$('.jury-left').fadeIn();
				
			if(num < <?=count($people);?>)
				$('.jury-right').fadeIn();
				*/
			$('input[name=selected_jury]').val(id);

			$.post('<?=URL::section(13);?>', {id:id}, function(data){
				$('.jury-popup h2').html(data.name);
				$('.jury-popup h3').html(data.job);
				$('.jury-popup .about div div').html(data.text);
				$('.jury-popup .image').html(data.image);
				$('.overlay, .jury-popup').fadeIn(function(){
					
				});
                                 var state = {
                                    "Jury": data.name
                                  };
                                  history.pushState(state, "Par Adwards", data.url);
                                  //expect(history.state).toEqual(state);
                              },'json');
                              juryPositioning();
		});
		$('.jury-popup .close a, .overlay').click(function(){
			$('.overlay, .jury-popup, .jury-left, .jury-right').fadeOut(150,function() {
                            $('.jury-left, .jury-right').css({'top': '320px'});  
                        }); 
                        juryView = false;
		});
                if(typeof initialMember !== 'undefined')
                {
                    $('div.over[data-id="'+initialMember+'"]').click();
                }

                
	});	
		
	jQuery(".adwards-section h2").fitText();
	</script>

	<div class="textdoc">
		<div class="leftside">
			<ul class="submenu">
				<?if($section->id == 3 || $section->parent_id == 3){?>
					<?=View::factory('global/leftmenu');?>
				<?}?>
				<?=Helper_Section::public_tree($section->parent_id == 0 ? $section->id : $section->parent_id);?>
			</ul>
		</div>
		<div class="page-content">
			<?if($section->image){?>
				<div class="page-image"><img src="<?=URL::base().'assets/media/images/'.$section->image;?>" alt="" /></div>
			<?}?>
			<?=e($section->textdoc->content)->safe();?>
		</div>
	</div>
	
	
	
<?

return array(

	'sections' => array(

		array(

			'title'      => 'Saturs',

			'controller' => 'admin/start',

			'ico'        => 'assets/css/cms/img/ico-content.png'

		),

		array(

			'title'      => 'Tehniskie tulkojumi',

			'controller' => 'admin/translations',

			'ico'        => 'assets/css/cms/img/ico-tech.png'

		),

		/*

		array(

			'title'      => 'Statistika',

			'controller' => 'admin/statistics',

			'ico'        => 'assets/css/cms/img/ico-stats.png'

		)

		*/

	),

	'widgets' => array(

		/*

		array(

			'title'      => 'Latplanta',

			'controller' => 'admin/about',

			'ico'        => 'assets/css/cms/img/ico-users.png'

		),

		array(

			'title'      => 'Baneri',

			'controller' => 'admin/banners',

			'ico'        => 'assets/css/cms/img/ico-banners.png'

		),

		array(

			'title'      => 'Iepakojumi',

			'controller' => 'admin/types',

			'ico'        => 'assets/css/cms/img/ico-users.png'

		),

		*/

		array(

			'title'      => 'Lietotāji',

			'controller' => 'admin/users',

			'ico'        => 'assets/css/cms/img/ico-users.png'

		),

		array(

			'title'      => 'Iesūtītie darbi',

			'controller' => 'admin/inserts',

			'ico'        => 'assets/css/cms/img/ico-newsletters.png'

		),

		array(

			'title'      => 'Biļetes',

			'controller' => 'admin/tickets',

			'ico'        => 'assets/css/cms/img/ico-polls.png'

		)

	),

	'modules' => array(

		array(

			'id'         => 1,

			'title'      => 'Teksta dokuments',

			'controller' => 'textdoc'

		),

		array(

			'id'         => 2,

			'title'      => 'Jaunumi',

			'controller' => 'news'

		),

		array(

			'id'         => 3,

			'title'      => 'Žūrija',

			'controller' => 'people'

		),

		array(

			'id'         => 4,

			'title'      => 'Galerijas',

			'controller' => 'galleries'

		),

		array(

			'id'         => 5,

			'title'      => 'Programma',

			'controller' => 'programm'

		),

		array(

			'id'         => 6,

			'title'      => 'Kategorijas un formāti',

			'controller' => 'categories'

		),

		array(

			'id'         => 7,

			'title'      => 'Sākumlapas bloki',

			'controller' => 'blocks'

		),

		array(

			'id'         => 8,

			'title'      => 'Iesniegtie darbi',

			'controller' => 'jobs'

		),

		array(

			'id'         => 9,

			'title'      => 'Pirmā apakšsadaļa',

			'controller' => 'subsection'

		),

		array(

			'id'         => 10,

			'title'      => 'Uzvarētāji',

			'controller' => 'winners'

		)

	)

);

?>
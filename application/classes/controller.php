<?
class Controller extends Kohana_Controller {
	protected $section;
	
	public function before(){
		parent::before();
		if($this->request->directory() == 'admin') return;
		$this->section = ORM::factory('section', $this->request->param('section'));
		View::set_global(array(
			'id'      => $this->request->param('id'),
			'lang'    => $this->request->param('lang'),
			'section' => $this->section
		));
		Helper_Translations::instance($this->request->param('lang'), $this->section);
		class_exists('Helper_View');
	}
}
?>
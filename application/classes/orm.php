<?


class ORM extends Kohana_ORM {


	const STATUS_HIDDEN = 1;


	const STATUS_DRAFT  = 2;


	const STATUS_ACTIVE = 3;


	


	protected $_cmsEditAction = 'edit';


	


	public function delete_all(){


		return DB::delete($this->_table_name);


	}


	


	public function getUrl(){


		return URL::section($this->section_id).'/'.$this->link.'.html';


	}


	


	public function cmsStatusIcon(){


		$icons = array(


			self::STATUS_HIDDEN => 'inactive',


			self::STATUS_DRAFT  => 'draft',


			self::STATUS_ACTIVE => 'active'


		);


		return HTML::image('assets/css/cms/img/table-ico-'.$icons[$this->status].'.png');


	}


	


	public function cmsEditUrl($absolute = true){


		$uri = Route::get('admin')->uri(array(


			'controller' => $this->getController(),


			'action'     => $this->_cmsEditAction,


			'id'         => $this->pk()


		));


		if($absolute) $uri = URL::site($uri);


		return $uri;


	}


	


	public function cmsEditLink($content = null){


		if(is_null($content)) $content = HTML::chars($this);


		return HTML::anchor($this->cmsEditUrl(false), (string)$content);


	}


	


	public function cmsDeleteLink(){


		return HTML::anchor(Route::get('admin')->uri(array(


			'controller' => $this->getController(),


			'action'     => 'delete',


			'id'         => $this->pk()


		)), HTML::image('assets/css/cms/img/table-ico-delete.png'), array('data-action' => 'delete'));


	}


	


	protected function getController(){


		$model = get_class($this);


		$model = strtolower(substr($model, strpos($model, '_')+1));


		return Inflector::plural($model);


	}


}


?>
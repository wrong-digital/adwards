<?
class Model_News extends ORM {
	
	protected $_sorting = array(
		'date' => 'DESC',
		'id' => 'DESC'
	);
	
	public function main_news(){
		return 
			ORM::factory('news')
				->where('status', '=', ORM::STATUS_ACTIVE)
				->where('mainpage', '=', 2)
				->find_all();
	}
	
}
?>
<?
class Model_Gallery extends ORM {
	protected $_created_column = array(
		'column' => 'date',
		'format' => 'Y-m-d H:i:s'
	);
	protected $_sorting = array(
		'id' => 'DESC'
	);
	protected $_has_many = array(
		'images' => array(
			'model' => 'galleries_image'
		),
        'gallery' =>array()
	);
    protected $_belongs_to = array(
		'category' => array()
	);
}
?>
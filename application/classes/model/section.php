<?
class Model_Section extends ORM {
	protected $_created_column = array(
		'column' => 'date_created',
		'format' => 'Y-m-d H:i:s'
	);
	protected $_updated_column = array(
		'column' => 'date_updated',
		'format' => 'Y-m-d H:i:s'
	);
	protected $_sorting = array('list_order' => 'ASC');
	protected $_has_one = array(
		'textdoc' => array()
	);
	protected $_belongs_to = array(
		'gallery' => array()
	);
	protected $_cmsEditAction = 'view';
	
	public function __toString(){
		return (string)$this->title;
	}
	
	public function save(Validation $validation = null){
		if(!$this->loaded()) $this->user_id = (int)Session::instance()->get('admin_uid');
		return parent::save($validation);
	}
}
?>
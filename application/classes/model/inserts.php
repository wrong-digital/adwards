<?
class Model_Inserts extends ORM {

	protected $_table_name = 'inserts';

	protected $_belongs_to = array(
		'format' => array(
			'model' => 'formats',
			'foreign_key' => 'sub_category'
		),
		'category' => array(
			'model' => 'categories',
			'foreign_key' => 'main_category'
		)
	);


	// Validate uploaded files to allow submit
	public static function validate_job($id){

		$job = ORM::factory('inserts', $id);
		$valid = true;

		// Get job format && required files
		$format = ORM::factory('formats')->where('id', '=', $job->sub_category)->find();

		if($format->audio_file == 3 && $job->filename_audio == '') $valid = false;
		if($format->video_file == 3 && $job->filename_video == '') $valid = false;
		if($format->zip_file == 3 && $job->filename_zip == '') $valid = false;

		if($format->book_file == 3){
			$count = 0;
			for($i=1;$i<=6;$i++){
				if($job->{'filename_image_book_'.$i} != '') $count++;
			}
			if($count < 2) $valid = false;
		}

		if($format->web_file == 3){
			$count = 0;
			for($i=1;$i<=6;$i++){
				if($job->{'filename_image_web_'.$i} != '') $count++;
			}
			if($count < 2) $valid = false;
		}

		return $valid == true ? 'true' : 'false';

	}


	// Calculate order price
	public static function calculate_price($jobs){

		$price = 0;
		$free_job = 1;
		$antalis_free = 1;
		$limbo_free = 2;


		// skat. komentus tālāk
		$now = new DateTime();
		$price_change = new DateTime('2016-08-02 23:59:59');


		foreach($jobs as $job){
			/*
			 * Old jobs safeguard...
			 */


			// if($job->votes > 0) continue; //WTF kapec parbauda votes? tautas balsojumu!!!    order_status japarbauda 
			if($job->order_status > 0) continue;

			// ...

			if ($job->main_category == 25){ // darba uzdevums - bezmaksas neierobežotā daudzumā
				$price = $price + 0;
			}
			else {

				if ($job->noprice == 1){
					$limbo_free--;
				}



// echo "<pre>"; var_dump($job->title_lv); echo"</pre>";
// echo "<pre>"; var_dump($job->votes); echo"</pre>";


				if ($job->noprice == 1 && $limbo_free > 0){ // viens no bezmaksas Limbo darbiem

					if($now < $price_change) {
						$price = $price + 0;
					} else {
						$price = $price + Kohana::$config->load('site')->get('item_price'); // application/config/site.php
					}


					/*if ($job->noprice == 1 && $limbo_free >= 1){ 	// ja grib pēc diviem bezmaksas limbo darbiem vēl piešķirt trešo bezmaksas parasto darbu,
					$price = $price + 0;						// jāatkomentē šīs 3 rindiņas un jāizkomentē 4-5 rindiņas kas virs šīm.
					$limbo_free--;*/



				} else { // Parastais darbs vai, ja bezmaksas limbo darbi iztērēti

					// ANTALIS DARBI DISABLED

					if(false) {
					//if ($job->antalis == 1){
						if ($antalis_free == 1) {
							$price = $price + 0;
							$antalis_free = 0;
						}else{
							if ($free_job == 1){
								$price = $price + 0;
								$free_job = 0;
							}else{
								if($job->order_status == 0 && $job->bill != 1){
									$price = $price + Kohana::$config->load('site')->get('half_price');
								}else{
									$price = $price + 0;
								}
							}
						}
					}

					// PARASTIE VAI MAKSAS LIMBO
					else{




						if ($free_job == 1){

							/**
							* Līdz 2016. gada 1. augusta plkst. 23.59 pirmā darba iesniegšana visiem festivāla dalībniekiem ir bez maksas, savukārt par katru nākamo darbu jāmaksā EUR 87.00 (PVN iekļauts cenā). (šis paliek, kā bija)
							* Sākot 2016. gada 2. augustu par pirmā darba iesniegšanu jāmaksā EUR 55.00 (PVN iekļauts cenā), savukārt par katra nākamā darba iesniegšanu jāmaksā EUR 100.00 (PVN iekļauts cenā).
							*/


							if($now < $price_change) {
								$price = $price + 0;
							} else {
								$price = $price + Kohana::$config->load('site')->get('first_price'); // application/config/site.php
							}

							$free_job = 0;

						}else{
							if($job->order_status == 0 && $job->bill != 1){

								if($now < $price_change) {
									$price = $price + Kohana::$config->load('site')->get('half_price');
								} else {
									$price = $price + Kohana::$config->load('site')->get('item_price');
								}

							}else{
								$price = $price + 0;
							}
						}
					}
				}


			}
		}

		return $price;
	}
}
?>

<?php

class Model_Winners extends ORM
{
    protected $_table_name = 'winners';
    protected $_belongs_to = array(
            '_award' => array('model' => 'award', 'foreign_key' => 'award'),
            '_job' => array('model' => 'inserts', 'foreign_key' => 'job')
        );
}


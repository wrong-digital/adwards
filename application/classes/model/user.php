<?
class Model_User extends ORM {
	const USER = 1;
	const ADMIN = 2;
	
	protected $_created_column = array(
		'column' => 'date_created',
		'format' => 'Y-m-d H:i:s'
	);
	
	public function __toString(){
		return (string)$this->username;
	}
	
	public function isAdmin(){
		if(!$this->loaded()) return false;
		return $this->level == self::ADMIN;
	}
	
	public static function password_salt($length = 10){
		$chars = array_merge(range(0, 9), range('a', 'z'), range('A', 'Z'), array('`', '~', '!', '@', '#', '$', '%', '^', '&', '*', '(', ')', '-', '_', '=', '+', '[', ']', '{', '}', ';', ':', '\'', '"', '\\', '|', ',', '.', '<', '>', '/', '?'));
		shuffle($chars);
		return implode(array_slice($chars, 0, $length));
	}
	
	public static function password_hash($password, $salt){
		return hash_hmac('sha512', $password, $salt);
	}
}
?>
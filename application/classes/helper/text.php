<?
class Helper_Text {
	
	public static function rewrite($url){
		
		$patterns = array (
        '/А/','/а/','/Б/','/б/','/В/','/в/','/Г/','/г/','/Д/','/д/','/Е/','/е/','/Ж/','/ж/','/З/','/з/',
        '/И/','/и/','/Й/','/й/','/К/','/к/','/Л/','/л/','/М/','/м/','/Н/','/н/','/О/','/о/','/П/','/п/',
        '/Р/','/р/','/С/','/с/','/Т/','/т/','/У/','/у/','/Ф/','/ф/','/Х/','/х/','/Ц/','/ц/','/Ч/','/ч/',
        '/Ш/','/ш/','/Щ/','/щ/','/Э/','/э/','/Ю/','/ю/','/Я/','/я/',
        '/"/','/ /','/,/','/:/',
        '/A/','/B/','/C/','/D/','/E/',
        '/F/','/G/','/H/','/I/','/J/','/K/','/L/','/M/','/N/','/O/','/P/','/R/','/S/','/T/','/U/','/V/',
        '/Z/','/W/','/Y/','/Ā/','/Č/','/Ē/','/Ģ/','/Ī/','/Ķ/','/Ļ/','/Ņ/','/Š/','/Ū/','/Ž/','/ā/','/č/',
        '/ē/','/ģ/','/ī/','/ķ/','/ļ/','/ņ/','/š/','/ū/','/ž/');
        
        $replace = array (
        'a', 'a', 'b', 'b', 'v', 'v', 'g', 'g', 'd', 'd', 'e', 'e', 'zh', 'zh', 'z', 'z','i', 'i', 'j', 
        'j', 'k', 'k', 'l', 'l', 'm', 'm', 'n', 'n', 'o', 'o', 'p', 'p', 'r', 'r', 's', 's', 't', 't', 
        'u', 'u', 'f', 'f', 'h', 'h', 'c', 'c', 'ch', 'ch', 'sh', 'sh', 'shch', 'shch', 'e', 'e', 'ju', 
        'ju', 'ja', 'ja',
        '', '-', '', '',
        'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k','l', 'm', 
        'n', 'o', 'p', 'r', 's', 't', 'u', 'v', 'z', 'w', 'y', 'a', 'c', 'e', 'g', 'i', 'k', 'l', 'n', 
        's', 'u', 'z', 'a', 'c', 'e', 'g', 'i', 'k', 'l', 'n', 's', 'u', 'z');
    
        $rewrited_url = preg_replace($patterns, $replace, $url);
        $rewrited_url = str_replace(".", "_", $rewrited_url); 
        
		$string = $rewrited_url;
		$i=0;
        while ($i < strlen($string)) 
        {
          if (ctype_alnum($string[$i]) || $string[$i]==" " || $string[$i]=="-" || $string[$i]==".") $new .= $string[$i];
          $i++;
          }; // while
        $new = str_replace("  ", " ", $new); 
        $new = str_replace(" ", "_", $new); 
		$new = str_replace("-", "_", $new);
        $new = str_replace(".", "_", $new); 
        $new = strtolower($new);
        
        return $new;	
	}
	
	public static function embedYoutube($text,$width,$height){
		$search = '%          # Match any youtube URL in the wild.
			(?:https?://)?    # Optional scheme. Either http or https
			(?:www\.)?        # Optional www subdomain
			(?:               # Group host alternatives
			  youtu\.be/      # Either youtu.be,
			| youtube\.com    # or youtube.com
			  (?:             # Group path alternatives
				/embed/       # Either /embed/
			  | /v/           # or /v/
			  | /watch\?v=    # or /watch\?v=
			  )               # End path alternatives.
			)                 # End host alternatives.
			([\w\-]{10,12})   # Allow 10-12 for 11 char youtube id.
			\b                # Anchor end to word boundary.
			%x';

		$replace = '<iframe width="'.$width.'" height="'.$height.'" src="http://www.youtube.com/embed/$1" frameborder="0" allowfullscreen></iframe>';

		return preg_replace($search, $replace, $text);
	}
	
	
	public static function newsDate($date){
		
		$d = explode('-',$date);
		
		return $d[2].'.'.$d[1].'.'.$d[0];
		
	}
	
	public static function jobSaver($id){
	
		$data = ORM::factory('user')->where('id', '=', $id)->find();
		
		return $data->private_name.$data->legal_title;
	
	}
	
	
	public static function newsLink($id){
	
		$data = ORM::factory('news')->where('id', '=', $id)->find();
		return $data->link;
		
	}
	
	
	public static function flvFilename($file){
	
		$flv = explode('.',$file);
		return $flv[0].'.flv';
		
	}

}
?>
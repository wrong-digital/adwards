<?


class Helper_Admin_Sections {


	public static $data = array();


	


	public static function cache(){


		foreach(ORM::factory('section')->find_all() as $section) self::$data[$section->id] = $section->as_array();


	}


	


	private static function data($id){


		return array_key_exists($id, self::$data) ? (object)self::$data[$id] : false;


	}


	


	public static function path($id){


		if(!$s = self::data($id)) return '';


		$r = self::path($s->parent_id);


		$r.= sprintf(' <img src="%sassets/css/cms/img/path-arrow.png" alt="" /> <a href="%s">%s</a>', URL::base(), URL::site('admin/'.$s->module_id.'/view/'.$s->id), $s->title);


		return $r;


	}


}





Helper_Admin_Sections::cache();


?>
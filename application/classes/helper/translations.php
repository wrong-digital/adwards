<?


class Helper_Translations {


	private $lang;


	private $section;


	private $translations = null;


	


	private static $instance;


	


	public static function instance($lang = null, $section = null){


		if(!isset(self::$instance)) self::$instance = new self($lang, $section);


		return self::$instance;


	}


	


	public function __construct($lang, $section){


		$this->lang = $lang;


		$this->section = $section;


		$file = APPPATH.'i18n/'.$this->lang.'.json';


		if(!file_exists($file) || !$data = file_get_contents($file)){


			$this->translations = array();


			return $this;


		}


		$this->translations = json_decode($data, true);


		return $this;


	}


	


	private function getKey($key){


		if(array_key_exists($key, $this->translations) && $this->translations[$key] !== '') return $this->translations[$key];


		$t = new Model_Translation();


		$t->key = $key;


		$t->section_id = $this->section->id;


		try{


			$t->save();


		}catch(Database_Exception $e){}


		return '{{ '.$key.' }}';


	}


	


	public function get($key){


		$str = HTML::chars($this->getKey($key));


		// admin functions in progress


		return $str;


	}


	


	public function write($key, array $args = null){


		$str = $this->get($key);


		if($args) $str = vsprintf($str, $args);


		return $str;


	}


}





function l($key){


	if(func_num_args() > 1){


		$args = func_get_args();


		unset($args[0]);


	}else{


		$args = array();


	}


	return Helper_Translations::instance()->write($key, $args);


}


?>
<?
class Helper_Section {
	public static $link = array();
	public static $tree = array();
	public static $data = array();
	public static $cached = false;
	
	private static $classes = array(
		ORM::STATUS_ACTIVE => 'active',
		ORM::STATUS_HIDDEN => 'hidden',
		ORM::STATUS_DRAFT  => 'draft'
	);
	
	public static function cache($lang = null, $active = true){
		if(is_null($lang)) $lang = Request::current()->param('lang');
		$sections = ORM::factory('section')->where('lang', '=', $lang);
		if($active) $sections->where('status', 'IN', array(ORM::STATUS_HIDDEN, ORM::STATUS_ACTIVE));
		foreach($sections->find_all() as $section){
			self::$link[$section->id] = array(
				'pid'  => $section->parent_id,
				'link' => $section->link
			);
			self::$tree[$section->parent_id][$section->id] = $section->link;
			self::$data[$section->id] = $section->as_array();
		}
	}
	
	public static function build_link($id){
		if(!array_key_exists($id, self::$link)) return '';
		$url = self::build_link(self::$link[$id]['pid']);
		$url.= '/'.self::$link[$id]['link'];
		return $url;
	}
	
	public static function parse_link($segments, $i = 0, $pid = 0){
		if(!array_key_exists($pid, self::$tree) || !array_key_exists($i, $segments)) return $pid;
		if(!$n = array_search($segments[$i], self::$tree[$pid])) return $pid;
		return self::parse_link($segments, ++$i, $n);
	}
	
	public static function data($id){
		return array_key_exists($id, self::$data) ? (object)self::$data[$id] : false;
	}
	
	public static function print_tree($l, $p = 0){
		if(!self::$cached){
			self::$tree = self::$link = self::$data = array();
			self::cache($l, false);
			self::$cached = true;
		}
		if(!array_key_exists($p, self::$tree)) return '<ul><li class="last"><a class="add disabled" href="'.URL::site('admin/sections/add/'.$p).'?'.$l.'">Pievienot sadaļu</a></li></ul>';
		$r = '<ul>';
		foreach(array_keys(self::$tree[$p]) as $s){
			$data = self::data($s);
			$r.= sprintf('<li id="s%d" class="closed %s"><a href="%s">%s</a>%s</li>', $s, self::$classes[$data->status], $data->module_id != 'subsection' ? URL::site('admin/'.$data->module_id.'/view/'.$s) : '#', $data->title, self::print_tree($l, $s));
		}
		$r.= '<li class="last"><a class="add disabled" href="'.URL::site('admin/sections/add/'.$p).'?'.$l.'">Pievienot sadaļu</a></li></ul>';
		return $r;
	}
	 
	public static function public_tree($p = 0){
		
		if(self::parent_id($p) > 0){
			$p = self::parent_id($p);
		}
		
		$c = array();
		self::getCurrents($c);
		if(!array_key_exists($p, self::$tree)) return '';
		$r = '';
		$tpl = '<li><a href="%s" title="" class="%s">%s</a>%s</li>';
		$i = 1;
		foreach(array_keys(self::$tree[$p]) as $s){
			$data = self::data($s);  
			if(empty($data->title) || $data->status != ORM::STATUS_ACTIVE || $data->id == 77 && Session::instance()->get('user_uid') > 0) continue;
			$r.= sprintf($tpl, $s == 61 ? '/2012' : URL::section($s), in_array($data->id, $c) || count($c) == 0 && $i == 1 ? 'ac' : '', $data->title, in_array($data->id, $c) || count($c) == 0 && $i == 1 ? self::submenu($s) : '');
			$i++;
		}
		return $r;
	}
	
	
	public static function submenu($p){
				
		$c = array();
		self::getCurrents($c);
		if(!array_key_exists($p, self::$tree)) return '';
		$r = '<ul>';
		$tpl = '<li><a href="%s" title="" class="%s">%s</a></li>';
		$i = 1;
		foreach(array_keys(self::$tree[$p]) as $s){
			$data = self::data($s);
			if(empty($data->title) || $data->status != ORM::STATUS_ACTIVE) continue;
			$r.= sprintf($tpl, URL::section($s), in_array($data->id, $c) || count($c) == 0 && $i == 1 ? 'ac' : '', $data->title);
			$i++;
		}
		$r.= '</ul>';
		return $r;
	}
	
	public static function top_menu($p = 0){
		$c = array();
		self::getCurrents($c);
		if(!array_key_exists($p, self::$tree)) return '';
		$r = '<ul>';
		$tpl = '<li><a href="%s" target="%s" title="" class="%s">%s</a></li>';
		$i = 1;
		foreach(array_keys(self::$tree[$p]) as $s){
			$data = self::data($s);
			if(empty($data->title) || $data->status != ORM::STATUS_ACTIVE) continue;
                        if(true/*$data->id != 59*/) {
			$r.= sprintf($tpl, $data->id == 6 ? 'http://www.ladc.lv' : URL::section($s), $data->id == 6 ? '_blank' : '', in_array($data->id, $c) ? 'ac' : '', $data->title);
                        }
                        $i++;
		}
		$r.= '</ul>';
		return $r;
	}
	
	public static function path($id = null){
		if(is_null($id)){
			$id = Request::current()->param('section');
			if(!$id) return '';
		}
		if(!$s = self::data($id)) return '';
		$r = self::path($s->parent_id);
		$r.= sprintf('%s<a href="%s">%s</a>', $s->parent_id > 0 ? ' / ' : '', URL::section($id), $s->title);
		return $r;
	}
	
	public static function title($id){
		return self::data($id)->title;
	}
	
	private static function getCurrents(&$c, $p = null){
		if(is_null($p)) $p = Request::current()->param('section');
		if(!$p) return;
		$c[] = $p;
		self::getCurrents($c, self::data($p)->parent_id);
	}
	
	public static function parent_id($id){
		return self::data($id)->parent_id;
	}
	
	
	public static function absolute_parent_id($id){
		$parent_id = $id > 0 ? self::data($id)->parent_id : '';
		if($parent_id > 0){
			$parent_id = self::absolute_parent_id($parent_id);
		}
		
		return $parent_id == 0 ? $id : $parent_id;
	}
	
	public static function section_image($id){
		$id = $id == '' ? 1 : $id;
		$data = ORM::factory('section')->where('id', '=', $id)->find();
		echo $data->image;
	}
}

Helper_Section::cache();
?>
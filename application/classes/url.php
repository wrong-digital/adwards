<?


class URL extends Kohana_URL {


	public static function section($id, $language = null){


		$url = self::site();


		if(count(Kohana::config('site.languages')) > 1) $url.= (is_null($language) ? Request::current()->param('lang') : $language).'/';


		$url.= substr(Helper_Section::build_link($id), 1);


		return $url;


	}


}


?>
<?


class Social_Draugiem {


	private static $instances = array();


	


	private $app_id;


	private $app_key;


	


	const URL_API  = 'http://api.draugiem.lv/json/?';


	const URL_AUTH = 'http://api.draugiem.lv/authorize/?';


	


	public function __construct($config){


		$config = Kohana::config($config == 'iframe' ? 'socials.integrated.draugiem' : 'socials.draugiem');


		$this->app_id = $config['id'];


		$this->app_key = $config['key'];


		return $this;


	}


	


	public static function instance($type = 'pp'){


		if(!isset(self::$instances[$type])) self::$instances[$type] = new self($type);


		return self::$instances[$type];


	}


	


	public function getLoginUrl(){


		$cnf = Kohana::config('socials.site');


		$redirect = $cnf['domain'].'socials/draugiem/authorize';


		return self::URL_AUTH.http_build_query(array(


			'app'      => $this->app_id,


			'redirect' => $redirect,


			'hash'     => md5($this->app_key.$redirect)


		));


	}


	


	public function api($action, $params){


		$params['action'] = $action;


		$params['app'] = $this->app_key;


		if(!($response = file_get_contents(self::URL_API.http_build_query($params)))) return false;


		if(!($data = json_decode($response))) return false;


		if(isset($data->error)) return false;


		return $data;


	}


}


?>
<?
class Controller_Textdoc extends Controller_Template {
	
	public $template = 'global/template';
	
	public function action_section(){
            
                if(Request::initial()->method() == Request::POST && Request::initial()->is_ajax()) {
                    $section = ORM::factory('section',$_POST['id']);
                    $out['url'] = URL::site('lv/par-adwards/').'/'.$section->link;
                    $out['title'] = $section->title;
                    header('Content-Type: application/json');			
                    die(json_encode($out));	
                }
		
		if($this->section->id == 2 || $this->section->parent_id == 2){

	        if($this->section->parent_id > 0) {
	            $sect = $this->section->id;
	        }

			$zurija_section_id = 13;

			$view = View::factory('textdoc/about')
				->set('people', ORM::factory('people')
				->where('status', '=', ORM::STATUS_ACTIVE)
				->where('section_id', '=', $zurija_section_id)
				->order_by('list_order', 'ASC')->find_all()->as_array()
			)
				->set('sections', ORM::factory('section')
					->where('parent_id', '=', 2)
					->where('status', '=', ORM::STATUS_ACTIVE)
					->order_by('list_order', 'ASC')
					->find_all()
			);
                        
                        if($sect) {
                            $view->set('sect',$sect);
                        }
		}else{
			$view = View::factory('textdoc/section')
				->set('text', $this->section->textdoc);
		}
		
		$this->template->title = HTML::chars($this->section->title);
		$this->template->title = $this->section->title;
		$this->template->content = $view;
	}	
	
	public function action_register(){
		
		if(Request::initial()->method() == Request::POST && Request::initial()->is_ajax()){
			$this->register();
		}else{
			if(Session::instance()->get('user_uid') > 0){
				die(Header("Location: ".URL::section(3)));
			}else{
				$this->template->title = HTML::chars($this->section->title);
				$view = View::factory('textdoc/register');
				$this->template->content = $view;
			}
		}
	
	}
	
	public function action_courier(){
		
		if(Request::initial()->method() == Request::POST && Request::initial()->is_ajax()){
			$this->courier();
		}
	}
	
	// mani iesniegtie darbi
	public function action_my(){
		
		if(Session::instance()->get('user_uid') == ''){
			die(Header("Location: ".URL::section(3)));
		}
		
		// Get IDs
		$ids = '';
		$data = ORM::factory('inserts')
			->where('user_id', '=', Session::instance()->get('user_uid'))
			->where('order_status', '!=', 1)
			->find_all();
		$i = 1;
		foreach($data as $insert){
			$ids = $i == 1 ? $insert->code.'_'.$insert->id : $ids.', '.$insert->code.'_'.$insert->id;
			$i++;
		}
		
		//
		$inserts = new Model_Inserts();
		
		$all_jobs = ORM::factory('inserts')
			->where('user_id', '=', Session::instance()->get('user_uid'))
			->find_all();
		
		$jobs = ORM::factory('inserts')
			->where('user_id', '=', Session::instance()->get('user_uid'))
			->where('order_status', '=', 0)
			->order_by('id', 'DESC')
			->find_all();
			
		$saved = ORM::factory('inserts')
			->where('user_id', '=', Session::instance()->get('user_uid'))
			->where('order_status', '=', 1)
			->order_by('id', 'DESC')
			->find_all();
		
		$this->template->css = array('insert');
		$this->template->title = HTML::chars($this->section->title);
		$view = View::factory('textdoc/insert/my')
			->set('userdata', ORM::factory('user')->where('id', '=', Session::instance()->get('user_uid'))->find())
			->set('status', 1)
			->set('ids', $ids)
			->set('price', $inserts->calculate_price($all_jobs))
			->set('saved', $saved)
			->set('jobs', $jobs);
		
		$this->template->content = $view;
	}
	
	
	// Darba iesniegšana
	public function action_insert(){
				
		if(Session::instance()->get('user_uid') == ''){
			die(Header("Location: ".URL::section(3)));
		}
		
		if(Request::initial()->method() == Request::POST){
			if(isset($_POST['action'])){
				
				// Apakškategorijas drop down menu
				if($_POST['action'] == 'sub'){					
					$data = ORM::factory('formats')
						->where('section_id', '=', $_POST['id'])
						->where('status', '=', ORM::STATUS_ACTIVE)
						->order_by('list_order', 'ASC')
						->find_all();
					
					foreach($data as $format){?>
					<li>
						<a href="javascript:;" title="<?=strip_tags($format->title);?>" data-id="<?=$format->id;?>"><?=strip_tags($format->title);?></a>
					</li>
					<?}
				}
				
				die;
				
			// Saglabā informāciju
			}else{
				$this->insert();
			}
			
		}else{
		
			$job = $subs = '';
			
			$status = Arr::get($_GET, 'status', '');
			$submit = Arr::get($_GET, 'submit', '');
			$edit_id = Arr::get($_GET, 'item', '');
			$delete_id = Arr::get($_GET, 'delete', '');
			$delfile = Arr::get($_GET, 'delfile', '');
			$type = Arr::get($_GET, 'type', '');
			$action = Arr::get($_GET, 'action', '');
			
			// Apstiprina iesniegtos darbu
			if($action == 'submit'){
				
				// Set insert status as submitted
				$data = ORM::factory('inserts')
					->where('user_id', '=', Session::instance()->get('user_uid'))
					->where('order_status', '!=', 1)
					->find_all();
				$i = 1;
				foreach($data as $insert){
					$ids = $i == 1 ? $insert->code.'_'.$insert->id : $ids.', '.$insert->code.'_'.$insert->id;
					$insert->order_status = 1;
					$insert->save();
					$i++;
				}
				
				$userdata = ORM::factory('user')->where('id', '=', Session::instance()->get('user_uid'))->find();
						
				// Send email to user
				$html = '<html><body>';
				$html .= '<div style="padding:20px;font-family:Tahoma;font-size:12px;"><p>Paldies, jūsu darbs ir apstiprināts</p><p>Jūsu darba(-u) ID: '.$ids.'</p></div>';
				$html .= '</body></html>';
				
				$mail = new phpmailer;
				$mail->CharSet    = 'UTF-8';
				$mail->Subject    = l('submit_ok_subject');
				$mail->IsMail();
				$mail->MsgHTML($html);
				$mail->AddAddress($userdata->email);
				$mail->SetFrom('no-reply@adwards.lv', l('order_ok_from'));
				$mail->AddReplyTo('no-reply@adwards.lv', l('order_ok_from'));
				$mail->Send();
				
				Session::instance()->set('submit_ok', 1);
				$this->request->redirect(URL::section(50));
			}
			
			
			// Dzēšam iesniegto darbu
			if($delfile != '' && $type != ''){
				
				$data = ORM::factory('inserts')
					->where('id', '=', $delfile)
					->where('user_id', '=', Session::instance()->get('user_uid'))
					->find()
				;
				
				if($data->id > 0){
					$_POST[$type] = '';
					$data->values($_POST);
					$data->save();
					$this->request->redirect(URL::section(50).'?edit='.$delfile);
				}
			}
			
			
			// Set edit OR insert template
			if($edit_id > 0){
				
				$job = ORM::factory('inserts')
					->where('id', '=', $edit_id)
					->where('user_id', '=', Session::instance()->get('user_uid'))
					->find();

				$subs = ORM::factory('formats')
					->where('section_id', '=', $job->main_category)
					->where('status', '=', ORM::STATUS_ACTIVE)
					->order_by('list_order', 'ASC')
					->find_all()->as_array();
					
			}else if($delete_id > 0){
				
				$check = ORM::factory('inserts')
					->where('id', '=', $delete_id)
					->where('user_id', '=', Session::instance()->get('user_uid'))
					->count_all();
				
				if($check > 0){
					$job = ORM::factory('inserts', $delete_id);
					if($job->loaded()) $job->delete();
				}
			}
								
			$jobs = ORM::factory('inserts')->where('user_id', '=', Session::instance()->get('user_uid'))->find_all();
			
			$this->template->css = array('insert');
			$this->template->title = HTML::chars($this->section->title);
			$view = View::factory('textdoc/insert')
				->set('not_saved', 0)
				->set('price', Model_Inserts::calculate_price($jobs))
				->set('time', time())
				->set('end', mktime(0,0,0,4,6,2013))
				
				->set('categories', ORM::factory('categories')->where('status', '=', ORM::STATUS_ACTIVE)->order_by('list_order', 'ASC')->find_all()->as_array())
				->set('message', Session::instance()->get('insert_ok'))
				->set('message_update', Session::instance()->get('update_ok'))
				->set('item', $job)
				->set('format', $job ? ORM::factory('formats')->where('id', '=', $job->sub_category)->find() : '')
				->set('tab', Arr::get($_GET, 'tab', 1))
				->set('status', $status)
				->set('submit', Session::instance()->get('submit_ok'))
				->set('subs', $subs)
				->set('jobs', $jobs);
				
			Session::instance()->set('insert_ok',0);
			Session::instance()->set('update_ok',0);
			Session::instance()->set('submit_ok',0);
			$this->template->content = $view;
		}
	
	}
	
	
	
	public function insert(){
		
		if($_POST['id'] > 0){
		
			$out = array();
			$validation = Validation::factory($_POST)
				->rule('main_category', 'not_empty')
				->rule('sub_category', 'not_empty')
				->rule('title_lv', 'not_empty')
				->rule('title_en', 'not_empty')
				->rule('client_lv', 'not_empty')
				->rule('client_en', 'not_empty')
				->rule('product_lv', 'not_empty')
				->rule('product_en', 'not_empty')
				->rule('description_lv', 'not_empty')
				->rule('description_en', 'not_empty');
		
			if($validation->check()){
				
				$_POST['noprice'] = isset($_POST['noprice']) ? $_POST['noprice'] : 0;
				$_POST['antalis'] = isset($_POST['antalis']) ? $_POST['antalis'] : 0;
					
				$code = ORM::factory('formats')->where('id', '=', $_POST['sub_category'])->find();
				$_POST['code'] = $code->code;
					
				$job = ORM::factory('inserts',$_POST['id']);
				$job->values($_POST);
				$job->save();
						
				Session::instance()->set('update_ok',1);
				$this->request->redirect(URL::section(50).'?edit='.$_POST['id']);
					
				$out['status'] = 'ok';		
			}else{
				$out['status'] = false;
				$out['errors'] = array();
				foreach($validation->errors() as $key=>$value) $out['errors'][$key] = $value[0];
			}
			
			header('Content-Type: application/json');
			die(json_encode($out));
			
		}else{
			
			$out = array();
			$validation = Validation::factory($_POST)
				->rule('main_category', 'not_empty')
				->rule('sub_category', 'not_empty')
				->rule('title_lv', 'not_empty')
				->rule('title_en', 'not_empty')
				->rule('client_lv', 'not_empty')
				->rule('client_en', 'not_empty')
				->rule('product_lv', 'not_empty')
				->rule('product_en', 'not_empty')
				->rule('description_lv', 'not_empty')
				->rule('description_en', 'not_empty');
		
			if($validation->check()){
									
				$code = ORM::factory('formats')->where('id', '=', $_POST['sub_category'])->find();
				$_POST['code'] = $code->code;
                                                
				$job = new Model_Inserts();
				$job->values($_POST);
				$job->save();
                                                
                //Aicis added for hash_id
                $hash_id = md5($job->id.'QWCcc4234');
                $job_title = $job->product_lv;
                $job->hash_id = $hash_id;
                                                
                $job->save();
				
				$out['status'] = 'ok';
				header('Content-Type: application/json');
				die(json_encode($out));
						
				/*
                $userdata = ORM::factory('user')->where('id', '=', Session::instance()->get('user_uid'))->find();
                $user_email = $userdata->email;
                $title = $userdata->private_name;
                                                
                $html = '<html><body>';
				$html .= '<div style="padding:20px;font-family:Tahoma;font-size:12px;"><p>'.l('register_title').' '.$title.'!</p><p>'.l('job_register_text').': <a href="http://www.adwards.lv/lv/sakums/pdf?id='.$hash_id.'">'.$job_title.'</a></p></div>';
                $html .= '</body></html>';

                $mail = new phpmailer;
                $mail->CharSet    = 'UTF-8';
                $mail->Subject    = l('job_register_subject');
                $mail->IsQmail();
                $mail->MsgHTML($html);
                $mail->AddAddress($user_email);
                $mail->SetFrom('no-reply@adwards.lv', l('register_from'));
                $mail->AddReplyTo('no-reply@adwards.lv', l('register_from'));
                $mail->Send();
				*/
          
			}else{
				$out['status'] = false;
				$out['errors'] = array();
				foreach($validation->errors() as $key=>$value) $out['errors'][$key] = $value[0];
				header('Content-Type: application/json');
				die(json_encode($out));
			}
			
		}
	
	}
	
	
	
	public function action_profile(){
		
		if(Request::initial()->method() == Request::POST && Request::initial()->is_ajax()){
			$this->update();
		}else{
			if(Session::instance()->get('user_uid') == ''){
				$this->request->redirect('/');
			}else{
				$this->template->title = HTML::chars($this->section->title);
				$view = View::factory('textdoc/profile')
					->set('user', ORM::factory('user')->where('id', '=', Session::instance()->get('user_uid'))->find());
				$this->template->content = $view;
			}
		}
	
	}
	
	
	
	public function update(){
		
		if($_POST['type'] == 1){
			$validation = Validation::factory($_POST)
				->rule('private_name', 'not_empty')
				->rule('private_code', 'not_empty')
				->rule('private_job', 'not_empty')
				->rule('email', 'not_empty')
				->rule('email', 'Valid::email')
				->rule('phone', 'not_empty')
				->rule('phone', 'Valid::phone')
				->rule('address', 'not_empty')
				->rule('bank', 'not_empty')
				->rule('accaunt', 'not_empty')
				->rule('password', 'min_length', array(':value', 7));
			$title = $_POST['private_name'];
		}else{
			$validation = Validation::factory($_POST)
				->rule('legal_title', 'not_empty')
				->rule('legal_number', 'not_empty')
				->rule('legal_pvn', 'not_empty')
				->rule('legal_contact', 'not_empty')
				->rule('email', 'not_empty')
				->rule('email', 'Valid::email')
				->rule('phone', 'not_empty')
				->rule('phone', 'Valid::phone')
				->rule('address', 'not_empty')
				->rule('legal_address', 'not_empty')
				->rule('bank', 'not_empty')
				->rule('accaunt', 'not_empty')
				->rule('password', 'min_length', array(':value', 7));
			$title = $_POST['legal_title'];
		}
		
		$out = array();
		if($validation->check()){
			
			$_POST['spam'] = isset($_POST['spam']) ? $_POST['spam'] : 0;
			
			$user = ORM::factory('user')
				->where('level', '=', 3)
				->where('email', '=', $_POST['email'])
				->where('id', '!=', Session::instance()->get('user_uid'))
				->count_all();
			if($user > 0){
				$out['status'] = 'taken';
				$out['text'] = l('register_taken');
			}else{
				$user = ORM::factory('user',Session::instance()->get('user_uid'));
				
				$_POST['password'] != '' ? $_POST['password_salt'] = Model_User::password_salt() : '';
				$_POST['password'] != '' ? $_POST['password'] = Model_User::password_hash($_POST['password'], $_POST['password_salt']) : '';
				
				if($_POST['password'] == ''){
					unset($_POST['password']);
				}
					
				$user->values($_POST);
				$user->save();
			
				$out['status'] = 'ok';
				$out['text'] = l('update_ok');
				$out['update'] = true;
			}
		}else{
			$out['status'] = false;
			$out['errors'] = array();
			foreach($validation->errors() as $key=>$value) $out['errors'][$key] = $value[0];
		}
		header('Content-Type: application/json');
		die(json_encode($out));
			
	}
	
	
	
	public function action_login(){
	
		if(Request::initial()->method() == Request::POST && Request::initial()->is_ajax()){
			$validation = Validation::factory($_POST)
				->rule('email', 'not_empty')
				->rule('email', 'Valid::email')
				->rule('password', 'not_empty');
			$out = array();
			if($validation->check()){
				$user = ORM::factory('user', array('email' => $_POST['email']));
				if(!$user->loaded()){
				    $out['status'] = 'error';
					$out['text'] = l('user_not_exist');
				}else{
					if($user->password != Model_User::password_hash($_POST['password'], $user->password_salt)) {
						$out['status'] = 'error';
						$out['text'] = l('user_not_exist');
					}else{
						Session::instance()
							->set('user_uid', $user->id)
							->set('user_name', $user->private_name != '' ? ($user->type == 2 && $user->legal_title == '' ? $user->private_name : $user->legal_title) : $user->legal_title);
						$user->date_login = DB::expr('NOW()');
						$user->save();
						$out['status'] = 'ok';
					}
				}
			}else{
				$out['status'] = false;
				$out['errors'] = array();
				foreach($validation->errors() as $key=>$value) $out['errors'][$key] = $value[0];
			}
			header('Content-Type: application/json');
			die(json_encode($out));
		}
	
	}
	
	
	
	public function action_logout(){
	
		Session::instance()->set('user_uid', '');
		Session::instance()->set('user_name', '');
		header("Location: ".$_SERVER['HTTP_REFERER']);
		die;
	
	}
	
	
		
	public function register(){
		
		if($_POST['type'] == 1){
			$validation = Validation::factory($_POST)
				->rule('private_name', 'not_empty')
				->rule('private_code', 'not_empty')
				->rule('private_job', 'not_empty')
				->rule('email', 'not_empty')
				->rule('email', 'Valid::email')
				->rule('phone', 'not_empty')
				->rule('phone', 'Valid::phone')
				->rule('address', 'not_empty')
				->rule('bank', 'not_empty')
				->rule('accaunt', 'not_empty')
				->rule('password', 'not_empty')
				->rule('password', 'min_length', array(':value', 7));
			$title = $_POST['private_name'];
		}else{
			$validation = Validation::factory($_POST)
				->rule('legal_title', 'not_empty')
				->rule('legal_number', 'not_empty')
				->rule('legal_pvn', 'not_empty')
				->rule('legal_contact', 'not_empty')
				->rule('email', 'not_empty')
				->rule('email', 'Valid::email')
				->rule('phone', 'not_empty')
				->rule('phone', 'Valid::phone')
				->rule('address', 'not_empty')
				->rule('legal_address', 'not_empty')
				->rule('accaunt', 'not_empty')
				->rule('password', 'not_empty')
				->rule('password', 'min_length', array(':value', 7));
			$title = $_POST['legal_title'];
		}
		
		$out = array();
		if($validation->check()){
			
			$user = ORM::factory('user')->where('level', '=', 3)->where('email', '=', $_POST['email'])->count_all();
			if($user > 0){
				$out['status'] = 'taken';
				$out['text'] = l('register_taken');
			}else{
				$user = ORM::factory('user');
				
				$_POST['level'] = 3;
				$_POST['password_salt'] = Model_User::password_salt();
				$_POST['password'] = Model_User::password_hash($_POST['password'], $_POST['password_salt']);
					
				$user->values($_POST);
				$user->save();
			
				$out['status'] = 'ok';
				$out['text'] = l('register_ok');
				
				Session::instance()
					->set('reg_ok', 1)
					->set('user_uid', $user->id)
					->set('user_name', $_POST['private_name'] != '' ? $_POST['private_name'] : $_POST['legal_title']);
			
				$html = '<html><body>';
				$html .= '<div style="padding:20px;font-family:Tahoma;font-size:12px;"><p>'.l('register_title').' '.$title.'!</p><p>'.l('register_text').'</p></div>';
				$html .= '</body></html>';
				
				$mail = new phpmailer;
				$mail->CharSet    = 'UTF-8';
				$mail->Subject    = l('register_subject');
				$mail->IsQmail();
				$mail->MsgHTML($html);
				$mail->AddAddress($_POST['email']);
				$mail->SetFrom('no-reply@adwards.lv', l('register_from'));
				$mail->AddReplyTo('no-reply@adwards.lv', l('register_from'));
				$mail->Send();
			}
		}else{
			$out['status'] = false;
			$out['errors'] = array();
			foreach($validation->errors() as $key=>$value) $out['errors'][$key] = $value[0];
		}
		header('Content-Type: application/json');
		die(json_encode($out));
			
	}
	
	public function courier(){
		
		$validation = Validation::factory($_POST)
			->rule('address', 'not_empty')
			->rule('contact', 'not_empty')
			->rule('phone', 'Valid::phone');

		$out = array();
		if($validation->check()){
			$u_id = Session::instance()->get('user_uid');
			$user = ORM::factory('user', $u_id);
			$u_email = $user->email;
			
			$courier = ORM::factory('courier');
			$courier->values($_POST);
			$courier->user_id = $u_id;
			$courier->email = $u_email;
			$courier->date_time = date("Y-m-d H:i:s");
			$courier->save();
		
			$html_courier = '<html><body>';
			$html_courier .= '<div style="padding:20px;font-family:Tahoma;font-size:12px;"><p>Sveiki,</p><p>'.l('courier_request_intro').'</p>';
			$html_courier .= '<p>Adrese: '.$_POST['address'].'<br />Kontaktpersona: '.$_POST['contact'].'<br />Kontakttālrunis: '.$_POST['phone'].'</p></div>';
			$html_courier .= '</body></html>';
			
			$mail_courier = new phpmailer;
			$mail_courier->CharSet    = 'UTF-8';
			
			$mail_courier->Subject    = l('courier_request_subject');
			$mail_courier->IsQmail();
			$mail_courier->MsgHTML($html_courier);
			$mail_courier->AddAddress(l('courier_email'));
			$mail_courier->SetFrom($u_email, $_POST['contact']);
			$mail_courier->AddReplyTo($u_email, $_POST['contact']);
			if(!$mail_courier->Send()){
				die("Mailer Error: " . $mail_courier->ErrorInfo);
			}else{
				$html_cust = '<html><body>';
				$html_cust .= '<div style="padding:20px;font-family:Tahoma;font-size:12px;"><p>Sveiki,</p><p>'.l('courier_request_confirmation_line_1').'</p>';
				$html_cust .= '<p>'.l('courier_request_confirmation_line_2').'</p></div>';
				$html_cust .= '</body></html>';
				
				$mail_cust = new phpmailer;
				$mail_cust->CharSet    = 'UTF-8';
				$mail_cust->Subject    = l('courier_request_confirmation_subject');
				$mail_cust->IsQmail();
				$mail_cust->MsgHTML($html_cust);
				$mail_cust->AddAddress($u_email);
				$mail_cust->SetFrom('no-reply@adwards.lv', 'Adwards');
				$mail_cust->AddReplyTo('no-reply@adwards.lv', 'Adwards');
				if(!$mail_cust->Send())
				die("Mailer Error: " . $mail_cust->ErrorInfo);
				
				$out['status'] = 'ok';
				$out['text'] = l('courier_request_confirmation_line_1').' '.l('courier_request_confirmation_line_2');
			}
				
		}else{
			$out['status'] = false;
			$out['errors'] = array();
			foreach($validation->errors() as $key=>$value) $out['errors'][$key] = $value[0];
		}
		header('Content-Type: application/json');
		die(json_encode($out));
			
	}
	
	public function action_pdf(){
		
		$id = Arr::get($_GET,'id');
                //$user=Arr::get($_GET,'admin');
		//die(var_dump($user));
                //if ($user=='true'){
                //     $job = ORM::factory('inserts')
		//	->where('id', '=', $id)
		//	->find();
                //}else {
                    $job = ORM::factory('inserts')
			->where('hash_id', '=', $id)
			->find();
                //}
                
		
			
		$maincat = ORM::factory('categories')->where('id', '=', $job->main_category)->find();
		$maincat = $maincat->title;
		
		$subcat = ORM::factory('formats')->where('id', '=', $job->sub_category)->find();
		$subcat = $subcat->title;
			
		if($job->id > 0){

			$html = '
			<html>
				<head>
				<style>
					html,body { margin: 0 0 25px 10px; padding: 0; font-family: Tahoma; font-size: 10px; color: #000000; }
					.title { text-transform: uppercase; padding: 10px 0 3px 0; }
					.input { border: 1px solid #585961; padding: 5px; border-left: 5px solid #585961; }
				</style>
				<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
			</head>
			<body>
				
					<table cellpadding="0" cellspacing="0" width="505" align="center">
				
					    <tr>
						    <td width="239" align="left" valign="top" style="padding:20px 0 0 0;"><strong>Darba pieteikums - '.$job->code.'_'.$job->id.'</strong></td>
							<td width="25">&nbsp;</td>
						    <td width="239" align="right" valign="top"><img src="/assets/css/img/adwards-logo2015.png" alt="" /></td>
						</tr>
						
						<tr>
						    <td colspan="3" height="40">&nbsp;</td>
						</tr>
						
						<tr>
						    <td width="239" align="left" class="title">Pamatkategorija</td>
							<td width="25">&nbsp;</td>
						    <td width="239" align="center" valign="top">&nbsp;</td>
						</tr>
						<tr>
						    <td width="239" align="left" class="input">'.$maincat.'</td>
							<td width="25">&nbsp;</td>
						    <td width="239" align="center" valign="top">&nbsp;</td>
						</tr>
						<tr>
						    <td colspan="3">&nbsp;</td>
						</tr>
						
						<tr>
						    <td width="239" align="left" class="title">Apakškategorija</td>
							<td width="25">&nbsp;</td>
						    <td width="239" align="center" valign="top">&nbsp;</td>
						</tr>
						<tr>
						    <td width="239" align="left" class="input">'.$subcat.'</td>
							<td width="25">&nbsp;</td>
						    <td width="239" align="center" valign="top">&nbsp;</td>
						</tr>
						<tr>
						    <td colspan="3">&nbsp;</td>
						</tr>
						
						<tr>
						    <td width="239" align="left" class="title">Nosaukums latviski</td>
							<td width="25">&nbsp;</td>
						    <td width="239" align="left" class="title">Nosaukums angliski</td>
						</tr>
						<tr>
						    <td width="239" align="left" class="input">'.$job->title_lv.'</td>
							<td width="25">&nbsp;</td>
						    <td width="239" align="left" class="input">'.$job->title_en.'</td>
						</tr>
						<tr>
						    <td colspan="3">&nbsp;</td>
						</tr>
						
						<tr>
						    <td width="239" align="left" class="title">Klients latviski</td>
							<td width="25">&nbsp;</td>
						    <td width="239" align="left" class="title">Klients angliski</td>
						</tr>
						<tr>
						    <td width="239" align="left" class="input">'.$job->client_lv.'</td>
							<td width="25">&nbsp;</td>
						    <td width="239" align="left" class="input">'.$job->client_lv.'</td>
						</tr>
						<tr>
						    <td colspan="3">&nbsp;</td>
						</tr>
						
						<tr>
						    <td width="239" align="left" class="title">Produkts latviski</td>
							<td width="25">&nbsp;</td>
						    <td width="239" align="left" class="title">Produkts angliski</td>
						</tr>
						<tr>
						    <td width="239" align="left" class="input">'.$job->product_lv.'</td>
							<td width="25">&nbsp;</td>
						    <td width="239" align="left" class="input">'.$job->product_en.'</td>
						</tr>
						<tr>
						    <td colspan="3">&nbsp;</td>
						</tr>
						';
						
						if($job->description_lv != '' || $job->description_en != ''){
						$html .= '
						<tr>
						    <td width="239" align="left" class="title">Apraksts latviski</td>
							<td width="25">&nbsp;</td>
						    <td width="239" align="left" class="title">Apraksts angliski</td>
						</tr>
						<tr>
						    <td width="239" align="left" class="input">'.$job->description_lv.'</td>
							<td width="25">&nbsp;</td>
						    <td width="239" align="left" class="input">'.$job->description_en.'</td>
						</tr>
						<tr>
						    <td colspan="3">&nbsp;</td>
						</tr>
						';
						}
						
						if($job->creative_1 != '' || $job->creative_1 != ''){
						$html .= '
						<tr>
						    <td width="239" align="left" class="title">Radošais direktors</td>
							<td width="25">&nbsp;</td>
						    <td width="239" align="left" class="title">Radošais direktors 2</td>
						</tr>
						<tr>
						    <td width="239" align="left" class="input">'.$job->creative_1.'</td>
							<td width="25">&nbsp;</td>
						    <td width="239" align="left" class="input">'.$job->creative_2.'</td>
						</tr>
						<tr>
						    <td colspan="3">&nbsp;</td>
						</tr>
						';
						}
						
						if($job->art_1 != '' || $job->art_2 != ''){
						$html .= '
						<tr>
						    <td width="239" align="left" class="title">Mākslinieciskais direktors</td>
							<td width="25">&nbsp;</td>
						    <td width="239" align="left" class="title">Projekta vadītājs</td>
						</tr>
						<tr>
						    <td width="239" align="left" class="input">'.$job->art_1.'</td>
							<td width="25">&nbsp;</td>
						    <td width="239" align="left" class="input">'.$job->art_2.'</td>
						</tr>
						<tr>
						    <td colspan="3">&nbsp;</td>
						</tr>
						';
						}
						
						if($job->designer_1 != '' || $job->designer_2 != ''){
						$html .= '
						<tr>
						    <td width="239" align="left" class="title">Dizainers</td>
							<td width="25">&nbsp;</td>
						    <td width="239" align="left" class="title">Dizainers 2</td>
						</tr>
						<tr>
						    <td width="239" align="left" class="input">'.$job->designer_1.'</td>
							<td width="25">&nbsp;</td>
						    <td width="239" align="left" class="input">'.$job->designer_2.'</td>
						</tr>
						<tr>
						    <td colspan="3">&nbsp;</td>
						</tr>
						';
						}
						
						if($job->programmer_1 != '' || $job->programmer_2 != ''){
						$html .= '
						<tr>
						    <td width="239" align="left" class="title">Programmētājs</td>
							<td width="25">&nbsp;</td>
						    <td width="239" align="left" class="title">Programmētājs 2</td>
						</tr>
						<tr>
						    <td width="239" align="left" class="input">'.$job->programmer_1.'</td>
							<td width="25">&nbsp;</td>
						    <td width="239" align="left" class="input">'.$job->programmer_2.'</td>
						</tr>
						<tr>
						    <td colspan="3">&nbsp;</td>
						</tr>
						';
						}
						
						if($job->text_1 != '' || $job->text_2 != ''){
						$html .= '
						<tr>
						    <td width="239" align="left" class="title">Tekstu autors</td>
							<td width="25">&nbsp;</td>
						    <td width="239" align="left" class="title">Tekstu autors 2</td>
						</tr>
						<tr>
						    <td width="239" align="left" class="input">'.$job->text_1.'</td>
							<td width="25">&nbsp;</td>
						    <td width="239" align="left" class="input">'.$job->text_2.'</td>
						</tr>
						<tr>
						    <td colspan="3">&nbsp;</td>
						</tr>
						';
						}
						
						if($job->photographer_1 != '' || $job->photographer_2 != ''){
						$html .= '
						<tr>
						    <td width="239" align="left" class="title">Fotogrāfs</td>
							<td width="25">&nbsp;</td>
						    <td width="239" align="left" class="title">Fotogrāfs 2</td>
						</tr>
						<tr>
						    <td width="239" align="left" class="input">'.$job->photographer_1.'</td>
							<td width="25">&nbsp;</td>
						    <td width="239" align="left" class="input">'.$job->photographer_2.'</td>
						</tr>
						<tr>
						    <td colspan="3">&nbsp;</td>
						</tr>
						';
						}
						
						if($job->rezisors_1 != '' || $job->rezisors_2 != ''){
						$html .= '
						<tr>
						    <td width="239" align="left" class="title">Režisors</td>
							<td width="25">&nbsp;</td>
						    <td width="239" align="left" class="title">Cits (norādiet vārdu un lomu projektā)</td>
						</tr>
						<tr>
						    <td width="239" align="left" class="input">'.$job->rezisors_1.'</td>
							<td width="25">&nbsp;</td>
						    <td width="239" align="left" class="input">'.$job->rezisors_2.'</td>
						</tr>
						<tr>
						    <td colspan="3">&nbsp;</td>
						</tr>
						';
						}
						
						if($job->notes != ''){
						$html .= '
						<tr>
						    <td width="239" align="left" class="title">Piezīmes</td>
							<td width="25">&nbsp;</td>
						    <td width="239" align="left" class="title">&nbsp;</td>
						</tr>
						<tr>
						    <td width="239" align="left" class="input">'.$job->notes.'</td>
							<td width="25">&nbsp;</td>
						    <td width="239" align="left">&nbsp;</td>
						</tr>
						';
						}
						
						$html .= '
					</table>
				
			</body>
			</html>
			';
							
				
			require_once(DOCROOT.'/application/classes/mpdf/mpdf.php');
			$mpdf = new mPDF();
			$mpdf->WriteHTML($html);
			$mpdf->Output();
		
		}else{
			
			$this->request->redirect('/');

		}
	}
	
}
?>
<?
class Controller_Galleries extends Controller_Template {
	public $template = 'global/template';
	
	public function action_section(){
		
		if(Request::initial()->method() == Request::POST && Request::initial()->is_ajax()){
			
			$o = '';
			
			if($_POST['data'] == 'prev' && $_POST['start'] == 0){
				$start = 0;
			}else{
				$start = $_POST['data'] == 'next' ? $_POST['start'] + 8 : $_POST['start'] - 8;
			}
			
	
			$images = ORM::factory('galleries_image')
					->where('gallery_id', '=', $_POST['id'])
					->order_by('list_order', 'ASC')
					->limit(8)->offset($start)
					->find_all();
			
			$a=0;
			$c = $start;
			foreach($images as $image){
				$o .= "
				<li".($a == 7 ? ' style="margin-right:0;"' : '').">
					<a href='javascript:;' id='thumb".$image->id."' data-nr='".($c+1)."' data-title='".$image->title."' data-img='assets/upload/gallery/323x235/".$image->file.".jpg' onclick='site.load.thumb(".$image->id.");' class='ad-thumb-list-a'>
						<img src='".URL::base()."assets/upload/gallery/60x60/".$image->file.".jpg' alt='".$image->title."' />
					</a>
				</li>
				";
				$a++;
				$c++;
			}
			
			$out['start'] = $start;
			$out['html'] = $o;
			header('Content-Type: application/json');
			die(json_encode($out));
			
		}else{
		
			$current = ORM::factory('gallery')
						->where('status', '=', ORM::STATUS_ACTIVE)
						->where('section_id', '=', $this->section->id)
						->order_by('id', 'DESC')
						->limit(1)->find()->as_array()
					;
			$images = ORM::factory('galleries_image')
						->where('gallery_id', '=', $current['id'])
						->order_by('list_order', 'ASC')
						->limit(8)
						->find_all()->as_array()
					;
					
			$allimages = ORM::factory('galleries_image')->where('gallery_id', '=', $current['id'])->count_all();
			
			$this->template->content = View::factory('galleries/list')
				->set('galleries', ORM::factory('gallery')
				->where('status', '=', ORM::STATUS_ACTIVE)
				->where('section_id', '=', $this->section->id)
				->where('id', '!=', $current['id'])
				->order_by('id', 'DESC')->find_all()->as_array())
				->set('images', $images)
				->set('allimages', $allimages)
				->set('current', $current);
				
			$this->template->title = $this->section->title;
			
		}
	}
	
	
	public function action_item(){
		
		$allimages = '';
		$current = ORM::factory('gallery')
					->where('status', '=', ORM::STATUS_ACTIVE)
					->where('section_id', '=', $this->section->id)
					->where('link', '=', $this->request->param('id'))
					->order_by('id', 'DESC')
					->limit(1)->find()->as_array()
				;
				
		$allimages = ORM::factory('galleries_image')->where('gallery_id', '=', $current['id'])->count_all();
		
		$images = ORM::factory('galleries_image')
					->where('gallery_id', '=', $current['id'])
					->order_by('list_order', 'ASC')
					->limit(8)
					->find_all()->as_array()
				;
		
		$this->template->content = View::factory('galleries/list')
			->set('galleries', ORM::factory('gallery')
			->where('status', '=', ORM::STATUS_ACTIVE)
			->where('section_id', '=', $this->section->id)
			->where('id', '!=', $current['id'])
			->order_by('id', 'DESC')->find_all()->as_array())
			->set('current', $current)
			->set('allimages', $allimages)
			->set('images', $images);
			
		$this->template->title = $current['title'];
	}
}
?>
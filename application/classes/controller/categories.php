<?
class Controller_Categories extends Controller_Template {
	public $template = 'global/template';
	
	public function action_section(){
		
		if(Request::initial()->method() == Request::POST && Request::initial()->is_ajax()){
			
			$formats = 
				ORM::factory('formats')
					->where('status', '=', ORM::STATUS_ACTIVE)
					->where('section_id', '=', $_POST['id'])
					->order_by('list_order', 'ASC')
					->find_all();
			
			foreach($formats as $format){?>
				<div class="format" data-id="<?=$format->id;?>">
					<div class="title"><?=strip_tags($format->title);?></div>
					<div class="ico">&nbsp;</div>
				</div>
				<div class="format-content" data-content="<?=$format->id;?>">
					<h3><?=l('for_jury');?></h3>
					<?=$format->jury;?>
					<h3><?=l('for_book');?></h3>
					<?=$format->book;?>
					<h3><?=l('for_web');?></h3>
					<?=$format->web;?>
				</div>
			<?}
			
			die;
		}
		
		$first = ORM::factory('categories')
			->where('status', '=', ORM::STATUS_ACTIVE)
			->where('section_id', '=', $this->section->id)
			->order_by('list_order', 'ASC')->limit(1)->find();
						
		$this->template->content = View::factory('categories/list')
			->set('categories', ORM::factory('categories')
				->where('status', '=', ORM::STATUS_ACTIVE)
				->where('section_id', '=', 14)
				->order_by('list_order', 'ASC')
				->find_all()->as_array())
			->set('formats', ORM::factory('formats')
				->where('status', '=', ORM::STATUS_ACTIVE)
				->where('section_id', '=', $first->id)
				->order_by('list_order', 'ASC')
				->find_all()->as_array());
		
		$this->template->title = $this->section->title;
		
	}
	
	
	public function action_item(){
	
		$item = ORM::factory('categories')
			->where('link', '=', $this->request->param('id'))
			->where('status', '=', ORM::STATUS_ACTIVE)->find()->as_array();
				
		$view = View::factory('categories/list')
            ->set('item', $item)
            ->set('section_id', $this->section->parent_id)
			->set('categories', ORM::factory('categories')
				->where('status', '=', ORM::STATUS_ACTIVE)
				->where('section_id', '=', 14)
				->order_by('list_order', 'ASC')
				->find_all()->as_array())
			->set('formats', ORM::factory('formats')
				->where('status', '=', ORM::STATUS_ACTIVE)
				->where('section_id', '=', $item['id'])
				->order_by('list_order', 'ASC')
				->find_all()->as_array());
		
		$this->template->content = $view;
	}
}
?>
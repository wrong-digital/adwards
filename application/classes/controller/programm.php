<?
class Controller_Programm extends Controller_Template {
	public $template = 'global/template';
	
	public function action_section(){
			$plist = ORM::factory('programm')
				->where('status', '=', ORM::STATUS_ACTIVE)
				->where('section_id', '=', $this->section->id)
                                ->where('start_date','IS NOT',NULL)
				->order_by('start_date', 'ASC')
                                ->order_by('list_order', 'ASC')->find_all()->as_array();
                        
                        /*
                        $clones = [];
                        foreach($plist as $event) {
                            if($event->end_date != null) {
                                $start = new DateTime($event->start_date);
                                $end = new DateTime($event->end_date);
                                $diff = $end->diff($start)->format("%a");
                                for($d = 1; $d <= intval($diff); $d++) {
                                    $clone = clone $event;
                                    $clone->end_date = NULL;
                                    $date = new DateTime($event->start_date);
                                    $date->modify('+'.$d.' day');
                                    $clone->start_date = strval($date->format('Y-m-d'));
                                    var_dump($clone->start_date);
                                    $clones[] = $clone;
                                }
                            }
                        }
                        foreach($clones as $clone) {
                            $date = $clone->start_date;
                            
                        }
                         
                         */
			$lecturers_section = ORM::factory('section')
					->where('parent_id','=',$this->section->id)
					->where('module_id','=','people')
					->find();

			if($lecturers_section) {
				$lecturers = ORM::factory('people')
						->where('status', '=', ORM::STATUS_ACTIVE)
						->where('section_id', '=', $lecturers_section->id)
						->order_by('list_order', 'ASC')->find_all()->as_array();
			} else {
				$lecturers = [];
			}
                        
			$this->template->content = View::factory('programm/list')
				->set('plist', $plist)
				->set('lecturers', $lecturers);
			
			$this->template->title = $this->section->title;
			
		/*
		if(Request::initial()->method() == Request::POST && Request::initial()->is_ajax()){
			$this->apply();
			die;
		}else{
			
			$status = Arr::get($_GET, 'status', '');
			$apply_1 = Arr::get($_GET, 'apply_1', '');
			
			if($status == 'ok'){
						
				$userdata = ORM::factory('user')->where('id', '=', Session::instance()->get('user_uid'))->find();
										
				$html = '<html><body>';
				
				if($apply_1 != 1){
					$html .= '<div style="padding:20px;font-family:Tahoma;font-size:12px;">
						<p>Cien. '.($userdata->private_name != '' ? $userdata->private_name : $userdata->legal_title).'! <br/>Paldies, ka pieteicāties Adwards2013 radošajai lekciju dienai / radošajai lekciju dienai un noslēguma ceremonijai / noslēguma ceremonijai.</p>
						<p>Adwards2013 radošā lekciju diena notiks 12. aprīlī no plkst. 9:00 līdz 17:30 Konferenču centrā Citadele (Republikas laukums 2A). Adwards2013 Kill Your Monsters noslēguma ceremonija notiks 12.aprīlī plkst. 20:00 (sāksies precīzi laikā, ieeja no plkst. 19:00) koncertzālē Palladium (Marijas iela 21). <br/>Pēc ceremonijas gaidīsim visus bārā MiiT (Lāčplēša ielā 10), kur "Kill Your Monsters" zīmē ballēsimies līdz rīta gaismai.</p>
						<p>Sirsnībā</p>
						<p>Jūsu LADC</p>
						<p>Vairāk par visu – www.adwards.lv, anete@ladc.lv</p>
						</div>';
				}else{
					$html .= '<div style="padding:20px;font-family:Tahoma;font-size:12px;">
						<p>Cien. '.($userdata->private_name != '' ? $userdata->private_name : $userdata->legal_title).'! <br/>Paldies, ka pieteicāties Adwards2013 radošajai lekciju dienai / radošajai lekciju dienai un noslēguma ceremonijai / noslēguma ceremonijai.</p>
						<p>Adwards2013 radošā lekciju diena notiks 12. aprīlī no plkst. 9:00 līdz 17:30 Konferenču centrā Citadele (Republikas laukums 2A). Adwards2013 Kill Your Monsters noslēguma ceremonija notiks 12.aprīlī plkst. 20:00 (sāksies precīzi laikā, ieeja no plkst. 19:00) koncertzālē Palladium (Marijas iela 21). <br/>Pēc ceremonijas gaidīsim visus bārā MiiT (Lāčplēša ielā 10), kur "Kill Your Monsters" zīmē ballēsimies līdz rīta gaismai.</p>
						<p>Sirsnībā</p>
						<p>Jūsu LADC</p>
						<p>Vairāk par visu – www.adwards.lv, anete@ladc.lv</p>
						</div>';
				}
				$html .= '</body></html>';
				
				$mail = new phpmailer;
				$mail->CharSet    = 'UTF-8';
				$mail->Subject    = l('event_ok_subject');
				$mail->IsQmail();
				$mail->MsgHTML($html);
				$mail->AddAddress($userdata->email);
				$mail->SetFrom('no-reply@adwards.lv', l('event_ok_from'));
				$mail->AddReplyTo('no-reply@adwards.lv', l('event_ok_from'));
				$mail->Send();
							
			}
			
			$first = ORM::factory('programm')
				->where('status', '=', ORM::STATUS_ACTIVE)
				->where('section_id', '=', $this->section->id)
				->order_by('list_order', 'ASC')->limit(1)->find();
											
			$this->template->content = View::factory('programm/list')
				->set('categories', ORM::factory('programm')
					->where('status', '=', ORM::STATUS_ACTIVE)
					->where('section_id', '=', $this->section->id)
					->order_by('list_order', 'ASC')
					->find_all()->as_array())
				->set('items', ORM::factory('programmitems')
					->where('status', '=', ORM::STATUS_ACTIVE)
					->where('section_id', '=', $first->id)
					->order_by('list_order', 'ASC')
					->find_all()->as_array())
				->set('status', $status);
			
			$this->template->title = $this->section->title;
			
		}
		 */
	}
	
	/*
	public function action_item(){
	
		if(Request::initial()->method() == Request::POST && Request::initial()->is_ajax()){
			$this->apply();
			die;
		}else{
		
			$item = ORM::factory('programm')
				->where('link', '=', $this->request->param('id'))
				->find()->as_array();
									
			$view = View::factory('programm/list')
				->set('item', $item)
				->set('section_id', $this->section->parent_id)
				->set('categories', ORM::factory('programm')
					->where('status', '=', ORM::STATUS_ACTIVE)
					->where('section_id', '=', $this->section->id)
					->order_by('list_order', 'ASC')
					->find_all()->as_array())
				->set('items', ORM::factory('programmitems')
					->where('status', '=', ORM::STATUS_ACTIVE)
					->where('section_id', '=', $item['id'])
					->order_by('list_order', 'ASC')
					->find_all()->as_array());
			
			$this->template->content = $view;
		}
	}
	
	
	public function apply(){
	
		if(isset($_POST['apply_1']) || isset($_POST['apply_2'])){
			
			$_POST['apply_1'] = isset($_POST['apply_1']) ? 1 : '';
			$_POST['apply_2'] = isset($_POST['apply_2']) ? 1 : '';
			
			$user = ORM::factory('user', Session::instance()->get('user_uid'));
			
			// Check if user allready have ticket, show error
			if($_POST['apply_1'] == 1){
				$saved = ORM::factory('tickets')
					->where('status', '=', 1)
					->where('apply_1', '=', 1)
					->where('user_id', '=', Session::instance()->get('user_uid'))
					->count_all();
			}
			
			if($_POST['apply_2'] == 1){
				$saved = ORM::factory('tickets')
					->where('status', '=', 1)
					->where('apply_2', '=', 1)
					->where('user_id', '=', Session::instance()->get('user_uid'))
					->count_all();
			}
				
			if($saved == 0){
			
				// LADC member
				if($user->private_member == 1 || $user->legal_member == 1){
				
					$html = '<html><body>';
					$html .= '<div style="padding:20px;font-family:Tahoma;font-size:12px;">
						<p>Cien. '.($userdata->private_name != '' ? $userdata->private_name : $userdata->legal_title).'! <br/>Paldies, ka pieteicāties Adwards2013 radošajai lekciju dienai / radošajai lekciju dienai un noslēguma ceremonijai / noslēguma ceremonijai.</p>
						<p>Adwards2013 radošā lekciju diena notiks 12. aprīlī no plkst. 9:00 līdz 17:30 Konferenču centrā Citadele (Republikas laukums 2A). Adwards2013 Kill Your Monsters noslēguma ceremonija notiks 12.aprīlī plkst. 20:00 (sāksies precīzi laikā, ieeja no plkst. 19:00) koncertzālē Palladium (Marijas iela 21). <br/>Pēc ceremonijas gaidīsim visus bārā MiiT (Lāčplēša ielā 10), kur "Kill Your Monsters" zīmē ballēsimies līdz rīta gaismai.</p>
						<p>Sirsnībā</p>
						<p>Jūsu LADC</p>
						<p>Vairāk par visu – www.adwards.lv, anete@ladc.lv</p>
						</div>';
					$html .= '</body></html>';
					
					$mail = new phpmailer;
					$mail->CharSet    = 'UTF-8';
					$mail->Subject    = l('event_ok_subject');
					$mail->IsQmail();
					$mail->MsgHTML($html);
					$mail->AddAddress($user->email);
					$mail->SetFrom('no-reply@adwards.lv', l('event_ok_from'));
					$mail->AddReplyTo('no-reply@adwards.lv', l('event_ok_from'));
					$mail->Send();
					
					$out['text'] = 'Paldies, jūs esat veiksmīgi pieteicies ADwards 2013 pasākumiem un drīz saņemsiet apstiprinājumu uz jūsu norādīto e-pasta adresi. Lūgums to izdrukāt un, ierodoties uz pasākumu, uzrādīt ADwards organizatoriem.';
					
					mysql_query("INSERT INTO `tickets` (`status`, `user_id`, `saved`, `apply_1`, `apply_2`) VALUES ('1', '".Session::instance()->get('user_uid')."', now(), '".$_POST['apply_1']."', '".$_POST['apply_2']."')");
					
				}else{
			
					// Not LADC memeber, but have uploaded case
					$free = '';
					if($_POST['apply_2'] == 1){
						$inserts = ORM::factory('inserts')->where('user_id', '=', Session::instance()->get('user_uid'))->where('order_status', '>', 0)->count_all();
						$free = $inserts > 0 ? 1 : '';
						
						// If friends email
						if($_POST['add_email'] != '' && $free == 1){
							$exist = ORM::factory('invites')->where('email', '=', $_POST['add_email'])->find();
							if($exist->id == ''){
								$data['user_id'] = Session::instance()->get('user_uid');
								$data['email'] = $_POST['add_email'];
								$data['status'] = 0;
								$exist->values($data);
								$exist->save();
								
								$html = '<html><body>';
								$html .= '<div style="padding:20px;font-family:Tahoma;font-size:12px;">
									<p>'.($user->private_name != '' ? $user->private_name : $user->legal_title).' ir uzaicinājis Jūs uz ADwards 2013 uzvarētāju apbalvošanas ceremoniju. Lai mēs varētu iekļaut Jūs viesu sarakstā, lūdzam Jūs pieteikties pasākumam ADwards vortālā http://adwards.lv/lv/pasakumi/pasakumi.  Lai pieteiktos pasākumam, Jums jābūt reģistrētam lietotājam. Reģistrācija: http://adwards.lv/lv/sakums/registracija</p>
									<p>Paldies un gaidām jūs apbalvošanas ceremonijā!</p>
									<p>ADwards 2013</p>
									</div>';
								$html .= '</body></html>';
								
								$mail = new phpmailer;
								$mail->CharSet    = 'UTF-8';
								$mail->Subject    = 'Ielūgums uz ADwards 2013 uzvarētāju apbalvošanas ceremoniju';
								$mail->IsQmail();
								$mail->MsgHTML($html);
								$mail->AddAddress($_POST['add_email']);
								$mail->SetFrom('no-reply@adwards.lv', l('event_ok_from'));
								$mail->AddReplyTo('no-reply@adwards.lv', l('event_ok_from'));
								$mail->Send();
								
							}
						}
					}
					
					// Check if user have invite
					$invite = ORM::factory('invites')->where('email', '=', $user->email)->find();
					if($invite->id != ''){
						$data['status'] = 1;
						$invite->values($data);
						$invite->save();
						
						$free = 1;
					}
					
					
					if($free == 1 && $_POST['apply_1'] != 1){
				
						$html = '<html><body>';
						$html .= '<div style="padding:20px;font-family:Tahoma;font-size:12px;">
						<p>Cien. '.($userdata->private_name != '' ? $userdata->private_name : $userdata->legal_title).'! <br/>Paldies, ka pieteicāties Adwards2013 radošajai lekciju dienai / radošajai lekciju dienai un noslēguma ceremonijai / noslēguma ceremonijai.</p>
						<p>Adwards2013 radošā lekciju diena notiks 12. aprīlī no plkst. 9:00 līdz 17:30 Konferenču centrā Citadele (Republikas laukums 2A). Adwards2013 Kill Your Monsters noslēguma ceremonija notiks 12.aprīlī plkst. 20:00 (sāksies precīzi laikā, ieeja no plkst. 19:00) koncertzālē Palladium (Marijas iela 21). <br/>Pēc ceremonijas gaidīsim visus bārā MiiT (Lāčplēša ielā 10), kur "Kill Your Monsters" zīmē ballēsimies līdz rīta gaismai.</p>
						<p>Sirsnībā</p>
						<p>Jūsu LADC</p>
						<p>Vairāk par visu – www.adwards.lv, anete@ladc.lv</p>
						</div>';
						$html .= '</body></html>';
						
						$mail = new phpmailer;
						$mail->CharSet    = 'UTF-8';
						$mail->Subject    = l('event_ok_subject');
						$mail->IsQmail();
						$mail->MsgHTML($html);
						$mail->AddAddress($user->email);
						$mail->SetFrom('no-reply@adwards.lv', l('event_ok_from'));
						$mail->AddReplyTo('no-reply@adwards.lv', l('event_ok_from'));
						$mail->Send();
					
						$out['text'] = 'Paldies, jūs esat veiksmīgi pieteicies ADwards 2013 apbalvošanas ceremonijai un drīz saņemsiet apstiprinājumu uz jūsu norādīto e-pasta adresi. Lūgums to izdrukāt un, ierodoties uz pasākumu, uzrādīt ADwards organizatoriem.';
						
						mysql_query("INSERT INTO `tickets` (`status`, `user_id`, `saved`, `apply_1`, `apply_2`) VALUES ('1', '".Session::instance()->get('user_uid')."', now(), '".$_POST['apply_1']."', '".$_POST['apply_2']."')");

					}else{
							
						$payment = '/ibis/do.php?user='.Session::instance()->get('user_uid').'&t=event&type='.($user->private_name != '' ? 'private' : 'legal').'&free='.$free.'&apply_1='.$_POST['apply_1'].'&apply_2='.$_POST['apply_2'];
						$out['text'] = 'Lai pabeigtu pieteikšanās procesu, lūdzam apmaksāt dalību pasākumā. Jums būs nepieciešama norēķinu karte un divas minūtes laika. Ja esat atzīmējis, ka vēlaties saņemt rēķinu, to nosūtīsim uz jūsu norādīto pasta adresi.<br/><br/><input type="checkbox" id="bill" value="1" /> Vēlos saņemt rēķinu<br/><br/><a href="javascript:;" onclick="javascript:site.ibis.events(\''.$payment.'\');" title="Apmaksāt" class="pay">Apmaksāt</a>';
					
					}
					
				}
		
				$out['status'] = 'ok';
				
			}else{
				$out['text'] = 'Viens reģistrēts lietotājs var iegādāties tikai vienu biļeti uz katru pasākumu. Katrai personai, kas vēlās ierasties uz pasākumu ir jāpiereģistrējas adwards.lv vortālā un tādā pat veidā jāviec maksājums.';
				$out['status'] = 'error';
			}
		}else{
			$out['text'] = 'Izvēlieties pasākumu!';
			$out['status'] = 'error';
		}
		
		header('Content-Type: application/json');
		die(json_encode($out));
	}
	*/
}
?>
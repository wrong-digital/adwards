<?
class Controller_Actions  extends Controller {

	public function action_xls(){

		$users = ORM::factory('user')->find_all()->as_array();
		foreach($users as $user){
			$users[$user->id] = $user->private_name != '' ? $user->private_name : $user->legal_title;
		}

		$categories = ORM::factory('categories')->find_all();
		foreach($categories as $category){
			$c[$category->id] = $category->title;
		}

		$formats = ORM::factory('formats')->find_all();
		foreach($formats as $format){
			$f[$format->id] = $format->title;
		}


		$html = '<table>';
		$data =
			ORM::factory('inserts')
				->where('order_status', '=', 1)
				->order_by('main_category','asc')
				->order_by('sub_category','asc')
				->find_all();

		foreach($data as $item){
			$html .= '<tr>';
				$html .= '<td>'.$item->id.'</td>';
				$html .= '<td>'.$item->code.'_'.$item->id.'</td>';
				$html .= '<td>'.$users[$item->user_id].'</td>';
				$html .= '<td>'.$c[$item->main_category].'</td>';
				$html .= '<td>'.$f[$item->sub_category].'</td>';
				$html .= '<td>'.($item->noprice == 1 ? 'Jā' : 'Nē').'</td>';
				$html .= '<td>'.($item->antalis == 1 ? 'Jā' : 'Nē').'</td>';
				$html .= '<td>'.$item->antalis_type.'</td>';
				$html .= '<td>'.$item->antalis_technology.'</td>';
				$html .= '<td>'.$item->title_lv.'</td>';
				$html .= '<td>'.$item->title_en.'</td>';
				$html .= '<td>'.$item->client_lv.'</td>';
				$html .= '<td>'.$item->client_en.'</td>';
				$html .= '<td>'.$item->product_lv.'</td>';
				$html .= '<td>'.$item->product_en.'</td>';
				$html .= '<td>'.$item->description_lv.'</td>';
				$html .= '<td>'.$item->description_en.'</td>';
				$html .= '<td>'.$item->creative_1.'</td>';
				$html .= '<td>'.$item->creative_2.'</td>';
				$html .= '<td>'.$item->art_1.'</td>';
				$html .= '<td>'.$item->art_2.'</td>';
				$html .= '<td>'.$item->designer_1.'</td>';
				$html .= '<td>'.$item->designer_2.'</td>';
				$html .= '<td>'.$item->programmer_1.'</td>';
				$html .= '<td>'.$item->programmer_2.'</td>';
				$html .= '<td>'.$item->text_1.'</td>';
				$html .= '<td>'.$item->text_2.'</td>';
				$html .= '<td>'.$item->photographer_1.'</td>';
				$html .= '<td>'.$item->photographer_2.'</td>';
				$html .= '<td>'.$item->rezisors_1.'</td>';
				$html .= '<td>'.$item->rezisors_2.'</td>';
				$html .= '<td>'.$item->componist.'</td>';
				$html .= '<td>'.$item->operator.'</td>';
				$html .= '<td>'.$item->film_artist.'</td>';
				$html .= '<td>'.$item->grafic_designer.'</td>';
				$html .= '<td>'.$item->editor.'</td>';
				$html .= '<td>'.$item->post.'</td>';
				$html .= '<td>'.$item->notes.'</td>';
			$html .= '</tr>';

		}

		$html .= '</table>';

		echo $html;

		die;

	}

	public function action_files(){

		$data = ORM::factory('inserts')->where('order_status', '=', 1)->where('id', '>', 744)->find_all();
		foreach($data as $item){
			$dir = $item->code . '_' . $item->id;

			if (!file_exists(DOCROOT.'assets/upload/export/'.$dir)) {
				mkdir(DOCROOT . 'assets/upload/export/' . $dir, 0777, true);


				if ($item->zip) copy(DOCROOT . 'assets/upload/jobs/' . $item->zip, DOCROOT . 'assets/upload/export/' . $dir . '/' . $item->zip);
				if ($item->audio) copy(DOCROOT . 'assets/upload/jobs/' . $item->audio, DOCROOT . 'assets/upload/export/' . $dir . '/' . $item->audio);
				if ($item->video) copy(DOCROOT . 'assets/upload/jobs/' . $item->video, DOCROOT . 'assets/upload/export/' . $dir . '/' . $item->video);
				if ($item->image_book_1) copy(DOCROOT . 'assets/upload/jobs/' . $item->image_book_1, DOCROOT . 'assets/upload/export/' . $dir . '/' . $item->image_book_1);
				if ($item->image_book_2) copy(DOCROOT . 'assets/upload/jobs/' . $item->image_book_2, DOCROOT . 'assets/upload/export/' . $dir . '/' . $item->image_book_2);
				if ($item->image_book_3) copy(DOCROOT . 'assets/upload/jobs/' . $item->image_book_3, DOCROOT . 'assets/upload/export/' . $dir . '/' . $item->image_book_3);
				if ($item->image_book_4) copy(DOCROOT . 'assets/upload/jobs/' . $item->image_book_4, DOCROOT . 'assets/upload/export/' . $dir . '/' . $item->image_book_4);
				if ($item->image_book_5) copy(DOCROOT . 'assets/upload/jobs/' . $item->image_book_5, DOCROOT . 'assets/upload/export/' . $dir . '/' . $item->image_book_5);
				if ($item->image_book_6) copy(DOCROOT . 'assets/upload/jobs/' . $item->image_book_6, DOCROOT . 'assets/upload/export/' . $dir . '/' . $item->image_book_6);
				if ($item->image_web_1) copy(DOCROOT . 'assets/upload/jobs/' . $item->image_web_1, DOCROOT . 'assets/upload/export/' . $dir . '/' . $item->image_web_1);
				if ($item->image_web_2) copy(DOCROOT . 'assets/upload/jobs/' . $item->image_web_2, DOCROOT . 'assets/upload/export/' . $dir . '/' . $item->image_web_2);
				if ($item->image_web_3) copy(DOCROOT . 'assets/upload/jobs/' . $item->image_web_3, DOCROOT . 'assets/upload/export/' . $dir . '/' . $item->image_web_3);
				if ($item->image_web_4) copy(DOCROOT . 'assets/upload/jobs/' . $item->image_web_4, DOCROOT . 'assets/upload/export/' . $dir . '/' . $item->image_web_4);
				if ($item->image_web_5) copy(DOCROOT . 'assets/upload/jobs/' . $item->image_web_5, DOCROOT . 'assets/upload/export/' . $dir . '/' . $item->image_web_5);
				if ($item->image_web_6) copy(DOCROOT . 'assets/upload/jobs/' . $item->image_web_6, DOCROOT . 'assets/upload/export/' . $dir . '/' . $item->image_web_6);

				echo $dir . "\n";
			}
			else {
				echo "EXISTS \n";
			}
			//sleep(1);
		}

		die;
	}


	public function action_aCjPrRFS() {
		$data = ORM::factory('inserts')
				->where('order_status','=',1)
				->where('id','>',744)
				->order_by('user_id')
				->order_by('id')
				->find_all()
				->as_array();

		$user = -1;
		$limbo_jobs = 0;
		$job = 1;

		echo '<html><head><meta charset="utf-8" />'
		 . '<style>table,th,td { border: 1px solid black; } th { background: #999; }</style></head><body>';
		echo '<table><thead><tr><th>ID</th><th>User ID</th><th>User</th><th>Pēc 2. augusta</th><th>Limbo</th><th>Price</th><th>Title</th></tr></thead><tbody>';

		foreach($data as $item) {
			if($user != $item->user_id) {
				$user = $item->user_id;
				$job = 0;
				$limbo_jobs = 0;

				$user_obj = ORM::factory('user')
						->where('id','=',$item->user_id)
						->find();

				$user_color = 'rgb(' . rand(128,255) . ',' . rand(128,255) . ',' . rand(128,255) . ')';
			}

			$after = ($item->id > 854);
			$limbo = ($item->noprice == 1);


			if($limbo) {
				if ($after) { // Pēc 2. augusta
					if($limbo_jobs == 0) {
						$item->price = 0;
					} else {
						$item->price = 100;
					}

				} else {
					if($limbo_jobs < 2) {
						$item->price = 0;
					} else {
						$item->price = 87;
					}
				}

				$limbo_jobs++;
			}

			// ne-Limbo savs aprēķins
			else {
				if ($after) { // Pēc 2. augusta
					if($job == 0) {
						$item->price = 55;
					} else {
						$item->price = 100;
					}

				} else {
					if($job == 0) {
						$item->price = 0;
					} else {
						$item->price = 87;
					}
				}

				$job++;
			}


			echo '<tr style="background: ' . $user_color . ';"><td>' . $item->code . '_' . $item->id . '</td><td>'
					. $item->user_id . '</td><td>'
					. ($user_obj->legal_title ? $user_obj->legal_title : $user_obj->private_name)
					. '</td><td>'  . ($item->id > 854 ? "Jā" : "Nē")
					. '</td><td>' . ($item->noprice ? "Jā" : "Nē") . '</td><td>'
					. $item->price . '</td><td>'
					. $item->title_lv . '</td></tr>';
		}

		echo '</tbody></table></body></html>';
	}

	public function action_profile(){

		if(Request::initial()->method() == Request::POST && Request::initial()->is_ajax()){
			if($_POST['type'] == 1){
				$validation = Validation::factory($_POST)
					->rule('private_name', 'not_empty')
					->rule('private_code', 'not_empty')
					->rule('private_job', 'not_empty')
					->rule('email', 'not_empty')
					->rule('email', 'Valid::email')
					->rule('phone', 'not_empty')
					->rule('phone', 'Valid::phone')
					->rule('address', 'not_empty')
					->rule('bank', 'not_empty')
					->rule('accaunt', 'not_empty')
					->rule('password', 'min_length', array(':value', 7));
				$title = $_POST['private_name'];
			}else{
				$validation = Validation::factory($_POST)
					->rule('legal_title', 'not_empty')
					->rule('legal_number', 'not_empty')
					->rule('legal_pvn', 'not_empty')
					->rule('legal_contact', 'not_empty')
					->rule('email', 'not_empty')
					->rule('email', 'Valid::email')
					->rule('phone', 'not_empty')
					->rule('phone', 'Valid::phone')
					->rule('address', 'not_empty')
					->rule('legal_address', 'not_empty')
					->rule('bank', 'not_empty')
					->rule('accaunt', 'not_empty')
					->rule('password', 'min_length', array(':value', 7));
				$title = $_POST['legal_title'];
			}

			$out = array();
			if($validation->check()){

				$_POST['spam'] = isset($_POST['spam']) ? $_POST['spam'] : 0;

				$user = ORM::factory('user')
					->where('level', '=', 3)
					->where('email', '=', $_POST['email'])
					->where('id', '!=', Session::instance()->get('user_uid'))
					->count_all();
				if($user > 0){
					$out['status'] = 'taken';
					$out['text'] = l('register_taken');
				}else{
					$user = ORM::factory('user',Session::instance()->get('user_uid'));

					$_POST['password'] != '' ? $_POST['password_salt'] = Model_User::password_salt() : '';
					$_POST['password'] != '' ? $_POST['password'] = Model_User::password_hash($_POST['password'], $_POST['password_salt']) : '';

					if($_POST['password'] == ''){
						unset($_POST['password']);
					}

					$user->values($_POST);
					$user->save();

					$out['status'] = 'ok';
					$out['text'] = l('update_ok');
					$out['update'] = true;
				}
			}else{
				$out['status'] = false;
				$out['errors'] = array();
				foreach($validation->errors() as $key=>$value) $out['errors'][$key] = $value[0];
			}
			header('Content-Type: application/json');
			die(json_encode($out));
		}else{
			if(Session::instance()->get('user_uid') == ''){
				die;
			}else{
				$view = View::factory('textdoc/profile')
					->set('user', ORM::factory('user')->where('id', '=', Session::instance()->get('user_uid'))->find());
				echo $view;
				die;
			}
		}

	}


	public function action_register(){

        if(Request::initial()->method() == Request::POST && Request::initial()->is_ajax()){
			if($_POST['type'] == 1){
				$validation = Validation::factory($_POST)
					->rule('private_name', 'not_empty')
					->rule('private_code', 'not_empty')
					->rule('private_job', 'not_empty')
					->rule('email', 'not_empty')
					->rule('email', 'Valid::email')
					->rule('phone', 'not_empty')
					->rule('phone', 'Valid::phone')
					->rule('address', 'not_empty')
					->rule('bank', 'not_empty')
					->rule('accaunt', 'not_empty')
					->rule('password', 'not_empty')
					->rule('password', 'min_length', array(':value', 7));
				$title = $_POST['private_name'];
			}else{
				$validation = Validation::factory($_POST)
					->rule('legal_title', 'not_empty')
					->rule('legal_number', 'not_empty')
					->rule('legal_pvn', 'not_empty')
					->rule('legal_contact', 'not_empty')
					->rule('email', 'not_empty')
					->rule('email', 'Valid::email')
					->rule('phone', 'not_empty')
					->rule('phone', 'Valid::phone')
					->rule('address', 'not_empty')
					->rule('legal_address', 'not_empty')
					->rule('accaunt', 'not_empty')
					->rule('password', 'not_empty')
					->rule('password', 'min_length', array(':value', 7));
				$title = $_POST['legal_title'];
			}

			$out = array();
			if($validation->check()){

				$user = ORM::factory('user')->where('level', '=', 3)->where('email', '=', $_POST['email'])->count_all();
				if($user > 0){
					$out['status'] = 'taken';
					$out['text'] = l('register_taken');
				}else{
					$user = ORM::factory('user');

					$_POST['level'] = 3;
					$_POST['password_salt'] = Model_User::password_salt();
					$_POST['password'] = Model_User::password_hash($_POST['password'], $_POST['password_salt']);

					$user->values($_POST);
					$user->save();

					$out['status'] = 'ok';
					$out['text'] = l('register_ok');

					Session::instance()
						->set('reg_ok', 1)
						->set('user_uid', $user->id)
						->set('user_name', $_POST['private_name'] != '' ? $_POST['private_name'] : $_POST['legal_title']);

					$html = '<html><body>';
					$html .= '<div style="padding:20px;font-family:Tahoma;font-size:12px;"><p>'.l('register_title').' '.$title.'!</p><p>'.l('register_text').'</p></div>';
					$html .= '</body></html>';

					$mail = new phpmailer;
					$mail->CharSet    = 'UTF-8';
					$mail->Subject    = l('register_subject');
					$mail->IsQmail();
					$mail->MsgHTML($html);
					$mail->AddAddress($_POST['email']);
					$mail->SetFrom('no-reply@adwards.lv', l('register_from'));
					$mail->AddReplyTo('no-reply@adwards.lv', l('register_from'));
					$mail->Send();
				}
			}else{
				$out['status'] = false;
				$out['errors'] = array();
				foreach($validation->errors() as $key=>$value) $out['errors'][$key] = $value[0];
			}
			header('Content-Type: application/json');
			die(json_encode($out));
		}

		echo View::factory('textdoc/register');
		die;
	}

	public function action_submit(){
		$out = array();
		$ids = '';
		$pdfs = '';
		$out['status'] = false;
		if($_POST['price'] == 0){
			$_POST['details'] = 1;
		}

		// Validate uplaoded files
		$data =
			ORM::factory('inserts')
				->where('user_id', '=', Session::instance()->get('user_uid'))
				->where('order_status', '!=', 1)
				->find_all();

		foreach($data as $job){
			if($job->validate_job($job->id) == 'false'){
				$out['status'] = 'files';
				header('Content-Type: application/json');
				die(json_encode($out));
			}
		}

		$validation = Validation::factory($_POST)->rule('details', 'not_empty');
		if($validation->check()){

			// Set insert status as submitted
			$data = ORM::factory('inserts')
				->where('user_id', '=', Session::instance()->get('user_uid'))
				->where('order_status', '!=', 1)
				->find_all();
			$i = 1;
			foreach($data as $insert){
				$ids = $i == 1 ? $insert->code.'_'.$insert->id : $ids.', '.$insert->code.'_'.$insert->id;
				$pdfs .= '<a href="http://www.adwards.lv/lv/actions/pdf?id='.$insert->hash_id.'" title="" target="_blank">'.$insert->code.'_'.$insert->id.'</a><br/>';
				$insert->order_status = 1;
				$insert->save();
				$i++;
			}

			$userdata = ORM::factory('user')->where('id', '=', Session::instance()->get('user_uid'))->find();

			// Send email to user
			$html = '<html><body>';
			$html .= '<div style="padding:20px;font-family:Tahoma;font-size:12px;"><p>Paldies, Jūsu darbs ir iesniegts XI radošās izcilības festivālam Adwards2016.</p><p>Jūsu darba(-u) ID: '.$ids.'</p><p>Anketas PDF formātā apskatāmas šeit:</p><p>'.$pdfs.'</p></div>';
			$html .= '</body></html>';
			$mail = new phpmailer;
			$mail->CharSet    = 'UTF-8';
			$mail->Subject    = l('submit_ok_subject');
			$mail->IsQmail();
			$mail->MsgHTML($html);
			$mail->AddAddress($userdata->email);
			$mail->SetFrom('no-reply@adwards.lv', l('order_ok_from'));
			$mail->AddReplyTo('no-reply@adwards.lv', l('order_ok_from'));
			$mail->Send();

			// Save order in DB, send email to LADC
			if($_POST['price'] > 0){

				$order = ORM::factory('orders');
				$order->user_id = Session::instance()->get('user_uid');
				$order->ids = $ids;
				$order->details = $_POST['details'];
				$order->saved = DB::expr('NOW()');

				$html = '<html><body>';
				$html .= '<div style="padding:20px;font-family:Tahoma;font-size:12px;"><p>Summa: '.$_POST['price'].'</p><p>Darba(-u) ID: '.$ids.'</p><p>Rekvizīti, komentāri: '.$_POST['details'].'</p></div>';
				$html .= '</body></html>';
				$mail = new phpmailer;
				$mail->CharSet    = 'UTF-8';
				$mail->Subject    = 'Iesniegti jauni Adwards darbi';
				$mail->IsQmail();
				$mail->MsgHTML($html);

				/** Grāmatvedības adrese (?) */
				$mail->AddAddress('agnese@ladc.lv');
				$mail->AddAddress('kantoris@bendikaskantoris.lv');

				$mail->SetFrom('no-reply@adwards.lv', l('order_ok_from'));
				$mail->AddReplyTo('no-reply@adwards.lv', l('order_ok_from'));
				$mail->Send();
			}

			// Send courier data
			foreach($data as $insert){

				if($insert->courier == 1 && $insert->main_category == 19){

					// Send mail to Antalis
					$html_courier = '<html><body>';
					$html_courier .= '<div style="padding:20px;font-family:Tahoma;font-size:12px;"><p>Sveiki,</p><p>'.l('courier_request_intro').'</p>';
					$html_courier .= '<p>Adrese: '.$insert->courier_address.'<br />Kontaktpersona: '.$insert->courier_contact.'<br />Kontakttālrunis: '.$insert->courier_phone.'</p></div>';
					$html_courier .= '</body></html>';

					$mail_courier = new phpmailer;
					$mail_courier->CharSet    = 'UTF-8';

					$mail_courier->Subject    = l('courier_request_subject');
					$mail_courier->IsQmail();
					$mail_courier->MsgHTML($html_courier);
					$mail_courier->AddAddress(l('courier_email'));
					$mail_courier->SetFrom($userdata->email, $insert->courier_contact);
					$mail_courier->AddReplyTo($userdata->email, $insert->courier_contact);
					$mail_courier->Send();

					// Send mail to client
					$html_cust = '<html><body>';
					$html_cust .= '<div style="padding:20px;font-family:Tahoma;font-size:12px;"><p>Sveiki,</p><p>'.l('courier_request_confirmation_line_1').'</p>';
					$html_cust .= '<p>'.l('courier_request_confirmation_line_2').'</p></div>';
					$html_cust .= '</body></html>';

					$mail_cust = new phpmailer;
					$mail_cust->CharSet    = 'UTF-8';
					$mail_cust->Subject    = l('courier_request_confirmation_subject');
					$mail_cust->IsQmail();
					$mail_cust->MsgHTML($html_cust);
					$mail_cust->AddAddress($userdata->email);
					$mail_cust->SetFrom('no-reply@adwards.lv', 'Adwards');
					$mail_cust->AddReplyTo('no-reply@adwards.lv', 'Adwards');
					$mail_cust->Send();
				}

			}

			$out['status'] = 'ok';
		}else{
			$out['errors'] = array();
			foreach($validation->errors() as $key=>$value) $out['errors'][$key] = $value[0];
		}
		header('Content-Type: application/json');
		die(json_encode($out));
	}

	/**
	 * Saglabājam darba pamat informāciju
	 */
	public function action_save_data(){

		$out = array();
		$out['status'] = false;

		if($_POST['antalis_type'] == 'Ieraksti, kādus Antalis papīrus vai materiālus izmantoji'){
			$_POST['antalis_type'] = '';
		}

		if($_POST['antalis_technology'] == 'Ieraksti, kādas drukas tehnoloģijas izmantoji'){
			$_POST['antalis_technology'] = '';
		}

		$validation = Validation::factory($_POST)
			->rule('main_category', 'not_empty')
			->rule('sub_category', 'not_empty')
			->rule('title_lv', 'not_empty')
			->rule('title_en', 'not_empty')
			->rule('client_lv', 'not_empty')
			->rule('client_en', 'not_empty')
			->rule('product_lv', 'not_empty')
			->rule('product_en', 'not_empty')
			->rule('description_lv', 'not_empty')
			->rule('description_en', 'not_empty');

		if(isset($_POST['antalis'])){
			$validation->rule('antalis_type', 'not_empty')->rule('antalis_technology', 'not_empty');
		}

		if(isset($_POST['courier'])){
			$validation->rule('courier_address', 'not_empty')->rule('courier_contact', 'not_empty')->rule('courier_phone', 'not_empty');
		}
                if(isset($_POST['courier']) && !isset($_POST['antalis'])) {
                    $validation->rule('printart_technology', 'not_empty');
                }

		if($validation->check()){

			$_POST['courier'] = $_POST['courier'] ? $_POST['courier'] : '';
			$_POST['noprice'] = $_POST['noprice'] ? $_POST['noprice'] : '';
			$_POST['antalis'] = $_POST['antalis'] ? $_POST['antalis'] : '';

			// Update data
			if($_POST['id']){
				$job =
					ORM::factory('inserts')
						->where('user_id', '=', Session::instance()->get('user_uid'))
						->where('id', '=', $_POST['id'])
						->find()
					;

				if($job->id){
					$job->values($_POST);
					$job->save();
				}

			// Save as new
			}else{
				$code = ORM::factory('formats')->where('id', '=', $_POST['sub_category'])->find();
				$_POST['code'] = $code->code;
				$_POST['user_id'] = Session::instance()->get('user_uid');

				$job = new Model_Inserts();
				$job->values($_POST);
				$job->save();

	            //Aicis added for hash_id
	            $hash_id = md5($job->id.'QWCcc4234');
	            $job_title = $job->product_lv;
	            $job->hash_id = $hash_id;
	            $job->save();
	        }

			$out['status'] = 'ok';
			$out['id'] = $job->id;
		}else{
			$out['errors'] = array();
			foreach($validation->errors() as $key=>$value) $out['errors'][$key] = $value[0];
		}

		header('Content-Type: application/json');
		die(json_encode($out));
	}


	/**
	 * Saglabājam rēķina rekvizītus darbam
	 */
	public function action_save_file_details(){
		$out = array();
		$job = 	ORM::factory('inserts')
			->where('user_id', '=', Session::instance()->get('user_uid'))
			->where('id', '=', $_POST['id'])
			->find();
		if($job->validate_job($job->id) == 'false'){
			$out['status'] = false;
		}else{
			// Update data
			$job->values($_POST);
			$job->save();
			$out['status'] = 'ok';
		}
		header('Content-Type: application/json');
		die(json_encode($out));
	}

	/**
	 * Saglabājam rēķina rekvizītus darbam
	 */
	public function action_save_details(){
		$out = array();
		$out['status'] = false;
		$validation = Validation::factory($_POST)
			->rule('bill_details', 'not_empty');
		if($validation->check()){
			// Update data
			$job = ORM::factory('inserts')->where('id', '=', $_POST['id'])->find();
			if($job->id){
				$job->values($_POST);
				$job->save();
			}
			$out['status'] = 'ok';
			$out['id'] = $job->id;
		}else{
			$out['files'] = 'error';
			$out['errors'] = array();
			foreach($validation->errors() as $key=>$value) $out['errors'][$key] = $value[0];
		}

		header('Content-Type: application/json');
		die(json_encode($out));
	}
		public function action_filename($id){			$field = Arr::get($_GET, 'type');				$job = ORM::factory('inserts',$id);		$job->{'filename_'.$field} = $_POST['filename'];		$job->save();				die;			}

	/**
	 * Ielādējam darba failu
	 */
	public function action_upload_file($id){

		$field = Arr::get($_GET, 'type');
		$upload_dir = DOCROOT."assets/upload/jobs/";

		if(isset($_FILES["file"])){

			$ret = array();
			$error = $_FILES["file"]["error"];

			if(!is_array($_FILES["file"]["name"])){
				$ext = substr($_FILES['file']['name'], strrpos($_FILES['file']['name'], '.') + 1);
				$file = md5(microtime()).'.'.$ext;
				Upload::save($_FILES['file'], $file, $upload_dir);
				//move_uploaded_file($_FILES["file"]["tmp_name"],$upload_dir.$file);
				$job = ORM::factory('inserts',$id);
				//$job->{'filename_'.$field} = $_FILES['file']['name'];
				$job->$field = $file;
				$job->save();

				$ret[]= $_FILES["file"]["name"];
			}
			else
			{
			    $fileCount = count($_FILES["file"]["name"]);
			    for($i=0; $i < $fileCount; $i++){
			  		$fileName = $_FILES["file"]["name"][$i];
                                        move_uploaded_file($_FILES["file"]["tmp_name"][$i],$upload_dir.$fileName);
			  		$ret[]= $fileName;
			  	}
			}

		    echo json_encode($ret);
		 }
		die();
	}


	/**
	 * Dzēšam failu
	 */
	public function action_delete_file(){

		$id = Arr::get($_GET, 'id', '');
		$type = Arr::get($_GET, 'type', '');

		$data = ORM::factory('inserts')
			->where('id', '=', $id)
			->where('user_id', '=', Session::instance()->get('user_uid'))
			->find()
		;

		if($data->id > 0){
			$_POST[$type] = $_POST['filename_'.$type] = '';
			$data->values($_POST);
			$data->save();
			unlink(DOCROOT."assets/upload/jobs/".$data->$type);
		}

		die(header("Location: ".URL::section(83)."?tab=2&item=".$data->id));

	}


	public function action_delete_job(){

		$delete_id = Arr::get($_GET, 'delete', '');
		$check = ORM::factory('inserts')
			->where('id', '=', $delete_id)
			->where('user_id', '=', Session::instance()->get('user_uid'))
			->count_all();

		if($check > 0){
			$job = ORM::factory('inserts', $delete_id);
			if($job->loaded()) $job->delete();
		}

		die(header("Location: ".URL::section(84)));
	}


    /*
     * Paroles atjaunināšana
     */
    public function action_restorepass(){

        if(Request::initial()->method() == Request::POST && Request::initial()->is_ajax()){
            //Helper_Translations::instance('lv', null, true);
            $validation = Validation::factory($_POST)
                ->rule('email', 'not_empty')
                ->rule('email', 'Valid::email');
            $out = array();
            if($validation->check()){

                $user = ORM::factory('user')->where('email', '=', $_POST['email'])->count_all();
                if($user < 1){
                    $out['status'] = 'dont_exist';
                    $out['text'] = l('login_restore_dontexist');
                }else{
                    $user = ORM::factory('user')->where('email', '=', $_POST['email'])->find();
                    $alphabet = "abcdefghijklmnopqrstuwxyzABCDEFGHIJKLMNOPQRSTUWXYZ0123456789";
                    $pass = array();
                    $alphaLength = strlen($alphabet) - 1;
                    for ($i = 0; $i < 8; $i++){
                        $n = rand(0, $alphaLength);
                        $pass[] = $alphabet[$n];
                    }
                    $newpass = implode($pass);

                    $_POST['password_salt'] = Model_User::password_salt();
                    $_POST['password'] = Model_User::password_hash($newpass, $_POST['password_salt']);

                    $user->password_salt = $_POST['password_salt'];
                    $user->password = $_POST['password'];
                    $user->save();


                    $out['status'] = 'ok';
                    $out['text'] = l('login_restore_ok');

                    $title = $user->type==1 ? $user->private_name : $user->legal_title;
                    $html = '<html><body>';
                    $html .= '<div style="padding:20px;font-family:Tahoma;font-size:12px;"><p>'.l('login_restore_title').' '.$title.'!</p><p>'.l('login_restore_text').'</p>';
                    $html .= '<p>'.l('login_restore_newpass').': '.$newpass.'</p>';
                    $html .= '</div></body></html>';

                    $mail = new phpmailer;
                    $mail->CharSet    = 'UTF-8';
                    $mail->Subject    = l('login_restore_subject');
                    $mail->IsQmail();
                    $mail->MsgHTML($html);
                    $mail->AddAddress($_POST['email']);
                    $mail->SetFrom('no-reply@adwards.lv', l('register_from'));
                    $mail->AddReplyTo('no-reply@adwards.lv', l('register_from'));
                    $mail->Send();
                }
            }else{
                $out['status'] = false;
                $out['errors'] = array();
                foreach($validation->errors() as $key=>$value) $out['errors'][$key] = $value[0];
            }
            header('Content-Type: application/json');
            die(json_encode($out));

        }else{
            if(Session::instance()->get('user_uid') > 0){
                $this->request->redirect('/');
            }else{
                $this->template->title = HTML::chars($this->section->title);
                $view = View::factory('textdoc/register');
                $this->template->content = $view;
            }
        }

    }


	public function action_pdf(){

		$id = Arr::get($_GET,'id');
                //$user=Arr::get($_GET,'admin');
		//die(var_dump($user));
                //if ($user=='true'){
                //     $job = ORM::factory('inserts')
		//	->where('id', '=', $id)
		//	->find();
                //}else {
                    $job = ORM::factory('inserts')
			->where('hash_id', '=', $id)
			->find();
                //}



		$maincat = ORM::factory('categories')->where('id', '=', $job->main_category)->find();
		$maincat = $maincat->title;

		$subcat = ORM::factory('formats')->where('id', '=', $job->sub_category)->find();
		$subcat = $subcat->title;

		if($job->id > 0){

			$html = '
			<html>
				<head>
				<style>
					html,body { margin: 0 0 25px 10px; padding: 0; font-family: Tahoma; font-size: 10px; color: #000000; }
					.title { text-transform: uppercase; padding: 10px 0 3px 0; }
					.input { border: 1px solid #585961; padding: 5px; border-left: 5px solid #585961; }
				</style>
				<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
			</head>
			<body>

					<table cellpadding="0" cellspacing="0" width="505" align="center">

						<tr>
						    <td colspan="3" align="center"><img src="/assets/css/img/adwards-logo2016.png" style="width: 120px; height: auto;" alt="" /></td>
						</tr>

					    <tr>
						    <td width="239" align="left" valign="top" style="padding:20px 0 0 0;"><strong>Darba pieteikums - '.$job->code.'_'.$job->id.'</strong></td>
							<td width="25">&nbsp;</td>
						    <td width="239" align="right" valign="top"></td>
						</tr>

						<tr>
						    <td colspan="3" height="40">&nbsp;</td>
						</tr>

						<tr>
						    <td width="239" align="left" class="title">Pamatkategorija</td>
							<td width="25">&nbsp;</td>
						    <td width="239" align="center" valign="top">&nbsp;</td>
						</tr>
						<tr>
						    <td width="239" align="left" class="input">'.$maincat.'</td>
							<td width="25">&nbsp;</td>
						    <td width="239" align="center" valign="top">&nbsp;</td>
						</tr>
						<tr>
						    <td colspan="3">&nbsp;</td>
						</tr>

						<tr>
						    <td width="239" align="left" class="title">Apakškategorija</td>
							<td width="25">&nbsp;</td>
						    <td width="239" align="center" valign="top">&nbsp;</td>
						</tr>
						<tr>
						    <td width="239" align="left" class="input">'.$subcat.'</td>
							<td width="25">&nbsp;</td>
						    <td width="239" align="center" valign="top">&nbsp;</td>
						</tr>
						<tr>
						    <td colspan="3">&nbsp;</td>
						</tr>

						<tr>
						    <td width="239" align="left" class="title">Nosaukums latviski</td>
							<td width="25">&nbsp;</td>
						    <td width="239" align="left" class="title">Nosaukums angliski</td>
						</tr>
						<tr>
						    <td width="239" align="left" class="input">'.$job->title_lv.'</td>
							<td width="25">&nbsp;</td>
						    <td width="239" align="left" class="input">'.$job->title_en.'</td>
						</tr>
						<tr>
						    <td colspan="3">&nbsp;</td>
						</tr>

						<tr>
						    <td width="239" align="left" class="title">Klients latviski</td>
							<td width="25">&nbsp;</td>
						    <td width="239" align="left" class="title">Klients angliski</td>
						</tr>
						<tr>
						    <td width="239" align="left" class="input">'.$job->client_lv.'</td>
							<td width="25">&nbsp;</td>
						    <td width="239" align="left" class="input">'.$job->client_lv.'</td>
						</tr>
						<tr>
						    <td colspan="3">&nbsp;</td>
						</tr>

						<tr>
						    <td width="239" align="left" class="title">Produkts latviski</td>
							<td width="25">&nbsp;</td>
						    <td width="239" align="left" class="title">Produkts angliski</td>
						</tr>
						<tr>
						    <td width="239" align="left" class="input">'.$job->product_lv.'</td>
							<td width="25">&nbsp;</td>
						    <td width="239" align="left" class="input">'.$job->product_en.'</td>
						</tr>
						<tr>
						    <td colspan="3">&nbsp;</td>
						</tr>
						';

						if($job->description_lv != '' || $job->description_en != ''){
						$html .= '
						<tr>
						    <td width="239" align="left" class="title">Apraksts latviski</td>
							<td width="25">&nbsp;</td>
						    <td width="239" align="left" class="title">Apraksts angliski</td>
						</tr>
						<tr>
						    <td width="239" align="left" class="input">'.$job->description_lv.'</td>
							<td width="25">&nbsp;</td>
						    <td width="239" align="left" class="input">'.$job->description_en.'</td>
						</tr>
						<tr>
						    <td colspan="3">&nbsp;</td>
						</tr>
						';
						}

						if($job->creative_1 != '' || $job->creative_2 != ''){
						$html .= '
						<tr>
						    <td width="239" align="left" class="title">Radošais direktors / Scenārists</td>
							<td width="25">&nbsp;</td>
						    <td width="239" align="left" class="title">Radošais direktors / Scenārists</td>
						</tr>
						<tr>
						    <td width="239" align="left" class="input">'.$job->creative_1.'</td>
							<td width="25">&nbsp;</td>
						    <td width="239" align="left" class="input">'.$job->creative_2.'</td>
						</tr>
						<tr>
						    <td colspan="3">&nbsp;</td>
						</tr>
						';
						}

						if($job->art_1 != '' || $job->art_2 != ''){
						$html .= '
						<tr>
						    <td width="239" align="left" class="title">Mākslinieciskais direktors</td>
							<td width="25">&nbsp;</td>
						    <td width="239" align="left" class="title">Projekta vadītājs</td>
						</tr>
						<tr>
						    <td width="239" align="left" class="input">'.$job->art_1.'</td>
							<td width="25">&nbsp;</td>
						    <td width="239" align="left" class="input">'.$job->art_2.'</td>
						</tr>
						<tr>
						    <td colspan="3">&nbsp;</td>
						</tr>
						';
						}

						if($job->designer_1 != '' || $job->designer_2 != ''){
						$html .= '
						<tr>
						    <td width="239" align="left" class="title">Dizainers</td>
							<td width="25">&nbsp;</td>
						    <td width="239" align="left" class="title">Dizainers</td>
						</tr>
						<tr>
						    <td width="239" align="left" class="input">'.$job->designer_1.'</td>
							<td width="25">&nbsp;</td>
						    <td width="239" align="left" class="input">'.$job->designer_2.'</td>
						</tr>
						<tr>
						    <td colspan="3">&nbsp;</td>
						</tr>
						';
						}

						if($job->film_artist != '' || $job->grafic_designer != ''){
						$html .= '
						<tr>
						    <td width="239" align="left" class="title">Filmas / klipa mākslinieks</td>
							<td width="25">&nbsp;</td>
						    <td width="239" align="left" class="title">Grafikas dizainers</td>
						</tr>
						<tr>
						    <td width="239" align="left" class="input">'.$job->film_artist.'</td>
							<td width="25">&nbsp;</td>
						    <td width="239" align="left" class="input">'.$job->grafic_designer.'</td>
						</tr>
						<tr>
						    <td colspan="3">&nbsp;</td>
						</tr>
						';
						}

						if($job->programmer_1 != '' || $job->programmer_2 != ''){
						$html .= '
						<tr>
						    <td width="239" align="left" class="title">Programmētājs</td>
							<td width="25">&nbsp;</td>
						    <td width="239" align="left" class="title">Programmētājs</td>
						</tr>
						<tr>
						    <td width="239" align="left" class="input">'.$job->programmer_1.'</td>
							<td width="25">&nbsp;</td>
						    <td width="239" align="left" class="input">'.$job->programmer_2.'</td>
						</tr>
						<tr>
						    <td colspan="3">&nbsp;</td>
						</tr>
						';
						}

						if($job->text_1 != '' || $job->text_2 != ''){
						$html .= '
						<tr>
						    <td width="239" align="left" class="title">Tekstu autors</td>
							<td width="25">&nbsp;</td>
						    <td width="239" align="left" class="title">Tekstu autors</td>
						</tr>
						<tr>
						    <td width="239" align="left" class="input">'.$job->text_1.'</td>
							<td width="25">&nbsp;</td>
						    <td width="239" align="left" class="input">'.$job->text_2.'</td>
						</tr>
						<tr>
						    <td colspan="3">&nbsp;</td>
						</tr>
						';
						}

						if($job->photographer_1 != '' || $job->photographer_2 != ''){
						$html .= '
						<tr>
						    <td width="239" align="left" class="title">Fotogrāfs</td>
							<td width="25">&nbsp;</td>
						    <td width="239" align="left" class="title">Fotogrāfs</td>
						</tr>
						<tr>
						    <td width="239" align="left" class="input">'.$job->photographer_1.'</td>
							<td width="25">&nbsp;</td>
						    <td width="239" align="left" class="input">'.$job->photographer_2.'</td>
						</tr>
						<tr>
						    <td colspan="3">&nbsp;</td>
						</tr>
						';
						}

						if($job->operator != '' || $job->componist != ''){
						$html .= '
						<tr>
						    <td width="239" align="left" class="title">Operators</td>
							<td width="25">&nbsp;</td>
						    <td width="239" align="left" class="title">Komponists</td>
						</tr>
						<tr>
						    <td width="239" align="left" class="input">'.$job->operator.'</td>
							<td width="25">&nbsp;</td>
						    <td width="239" align="left" class="input">'.$job->componist.'</td>
						</tr>
						<tr>
						    <td colspan="3">&nbsp;</td>
						</tr>
						';
						}

						if($job->editor != '' || $job->post != ''){
						$html .= '
						<tr>
						    <td width="239" align="left" class="title">Montāžas režisors</td>
							<td width="25">&nbsp;</td>
						    <td width="239" align="left" class="title">Pēcapstrāde (post)</td>
						</tr>
						<tr>
						    <td width="239" align="left" class="input">'.$job->editor.'</td>
							<td width="25">&nbsp;</td>
						    <td width="239" align="left" class="input">'.$job->post.'</td>
						</tr>
						<tr>
						    <td colspan="3">&nbsp;</td>
						</tr>
						';
						}

						if($job->rezisors_1 != '' || $job->rezisors_2 != ''){
						$html .= '
						<tr>
						    <td width="239" align="left" class="title">Režisors</td>
							<td width="25">&nbsp;</td>
						    <td width="239" align="left" class="title">Cits (norādiet vārdu un lomu projektā)</td>
						</tr>
						<tr>
						    <td width="239" align="left" class="input">'.$job->rezisors_1.'</td>
							<td width="25">&nbsp;</td>
						    <td width="239" align="left" class="input">'.$job->rezisors_2.'</td>
						</tr>
						<tr>
						    <td colspan="3">&nbsp;</td>
						</tr>
						';
						}

						if($job->notes != ''){
						$html .= '
						<tr>
						    <td width="239" align="left" class="title">Piezīmes</td>
							<td width="25">&nbsp;</td>
						    <td width="239" align="left" class="title">&nbsp;</td>
						</tr>
						<tr>
						    <td width="239" align="left" class="input">'.$job->notes.'</td>
							<td width="25">&nbsp;</td>
						    <td width="239" align="left">&nbsp;</td>
						</tr>
						';
						}

						$html .= '
					</table>

			</body>
			</html>
			';


			require_once(DOCROOT.'/application/classes/mpdf/mpdf.php');
			$mpdf = new mPDF();
			$mpdf->WriteHTML($html);
			$mpdf->Output();

		}else{

			$this->request->redirect('/');

		}
	}
}
?>

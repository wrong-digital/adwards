<?
class Controller_Decode extends Controller {
	public function action_section(){
		$section = ORM::factory('section', Helper_Section::parse_link(explode('/', $this->request->param('path'))));
		if(!$section->loaded()) throw new Http_Exception_404('The requested URL :uri was not found on this server.', array(':uri' => $this->request->param('path')));
		$this->response->body(Request::factory(Route::get('decoded')->uri(array(
				'lang'       => $this->request->param('lang'),
				'controller' => empty($section->controller) ? $section->module_id : $section->controller,
				'action'     => empty($section->action) ? 'section' : $section->action,
				'section'    => $section->id
			)))
			->execute()
			->send_headers()
			->body());
	}
	
	public function action_item(){
		$section = ORM::factory('section', Helper_Section::parse_link(explode('/', $this->request->param('path'))));
		if(!$section->loaded()) throw new Http_Exception_404('The requested URL :uri was not found on this server.', array(':uri' => $this->request->param('path')));
		$this->response->body(Request::factory(Route::get('decoded')->uri(array(
				'lang'       => $this->request->param('lang'),
				'controller' => empty($section->controller) ? $section->module_id : $section->controller,
				'action'     => empty($section->action) ? 'item' : $section->action,
				'section'    => $section->id,
				'id'         => $this->request->param('id')
			)))
			->execute()
			->send_headers()
			->body());
	}
}
?>
<?
class Controller_Profile extends Controller {


    public function action_archive(){
        
        $f = array(
            'zip','video','video_2','video_3','video_4','video_digital','audio',
            'image_book_1','image_book_2','image_book_3','image_book_4','image_book_5',
            'image_book_6','image_web_1','image_web_2','image_web_3','image_web_4','image_web_5','image_web_6'
        );
        
        $inserts = ORM::factory('inserts')->order_by('id', 'ASC')->find_all()->as_array();
        foreach($inserts as $i){
            
            if(!is_dir(DOCROOT.'assets/upload/jobs/item_'.$i->id)){
                mkdir(DOCROOT.'assets/upload/jobs/item_'.$i->id);
                chmod(DOCROOT.'assets/upload/jobs/item_'.$i->id);
            }
            
            foreach($f as $field){
                if($i->{$field} && !file_exists(DOCROOT.'assets/upload/jobs/item_'.$i->id.'/'.$i->{$field})) 
                    copy(DOCROOT.'assets/upload/jobs/'.$i->{$field}, DOCROOT.'assets/upload/jobs/item_'.$i->id.'/'.$i->{$field});
            }
        }
        
        die();
    }
    
	public function action_getvideo(){
		$video = ORM::factory('galleries_image')->where('id', '=', $_POST['id'])->find();
		echo $video->embed;
	}
	
    public function action_mailtest(){
        $html = '<html><body>';
        $html .= '</body></html>';
                                
        $mail = new phpmailer;
        $mail->CharSet    = 'UTF-8';
        $mail->Subject    = 'Ielūgums uz ADwards 2012 uzvarētāju apbalvošanas ceremoniju';
        $mail->IsMail();
        $mail->MsgHTML($html);
        $mail->AddAddress('aigars@digibrand.lv');
        $mail->SetFrom('no-reply@adwards.lv', 'ADwards 2012');
        $mail->AddReplyTo('no-reply@adwards.lv', 'ADwards 2012');
        
        if(!$mail->Send()) {
            die("Mailer Error: " . $mail->ErrorInfo);
        }
        
        die('sent');
    }
    
	public function action_vote(){
		
		if(Session::instance()->get('uid') > 0){
			$vote = ORM::factory('votes')
				->where('item_id', '=', $_POST['id'])
				->where('user_id', '=', Session::instance()->get('uid'))
				->where('network', '=', Session::instance()->get('network'))
				->find();
			if($vote->id == ''){
				$data['item_id'] =  $_POST['id'];
				$data['user_id'] =  Session::instance()->get('uid');
				$data['network'] =  Session::instance()->get('network');
				$data['saved'] = DB::expr('now()');
				$vote->values($data);
				$vote->save();
				
				$item = ORM::factory('inserts')
					->where('id', '=', $_POST['id'])
			 		->find();
				$item_data['votes'] = $item->votes + 1;
				$item->values($item_data);
				$item->save();
			
				$out['votes'] = $item_data['votes'];
				$out['text'] = 'Paldies, balsojums pieņemts!';
			
				header('Content-Type: application/json');
				die(json_encode($out));
			}
		}
	}
	
	 
	public function action_bills(){
		 
		$query = mysql_query("
			SELECT * FROM inserts WHERE bill = 1 GROUP BY trans_id ASC
		");
		while($row = mysql_fetch_array($query)){
			$q2 = mysql_query("
				SELECT id, amount, result FROM ibis_transaction WHERE trans_id = '".$row['trans_id']."'
			");
			$row2 = mysql_fetch_array($q2);
			
			$q3 = mysql_query("
				SELECT * FROM users WHERE id = '".$row['user_id']."'
			");
			$row3 = mysql_fetch_array($q3);
			
			echo $row3['email'].' '.$row['trans_id'].' '.$row2['amount'].' '.$row2['result'].'<br/>';
		}
	 
	}
	
	public function action_transactions(){
		 
		$query = mysql_query("
			SELECT * FROM ibis_transaction WHERE result = 'OK'
		");
		while($row = mysql_fetch_array($query)){
			$q2 = mysql_query("
				SELECT * FROM inserts WHERE trans_id = '".$row['trans_id']."'
			");
			while($row2 = mysql_fetch_array($q2)){
				echo $row['trans_id'].' '.$row['amount'].' '.$row2['user_id'].' '.$row2['title_lv'].'<br/>';
			}
		}
	
	}
	
	public function action_all(){
		 
		$query = mysql_query("
			SELECT * FROM inserts
			ORDER BY user_id
		");
		echo '<table>';
		while($row = mysql_fetch_array($query)){
			
			$q2 = mysql_query("
				SELECT private_name, legal_title, address, legal_address
				FROM users 
				WHERE id = '".$row['user_id']."'
			");
			$row2 = mysql_fetch_row($q2);
			echo '<tr><td>'.$row['code'].'_'.$row['id'].'</td><td>'.$row['user_id'].'</td><td>'.$row2[0].' '.$row2[1].'</td><td>'.$row2[2].'</td><td>'.$row2[3].'</td></tr>';			
		}
		echo '<table>';
	
	}
	
	
	public function action_tickets(){
	
		$query = mysql_query("
			SELECT DISTINCT user_id
			FROM tickets 
			WHERE status = 1
			AND apply_2 = 1
		");
		while($row = mysql_fetch_array($query)){
										
			$user = ORM::factory('user', $row['user_id']);
			$name = $user->private_name != '' ? $user->private_name : $user->legal_title;
			
			echo $name.', '.$user->email.'<br/>';
			
			
		}
		
	}
	
	
	public function action_exist(){
	
		$query = mysql_query("
			SELECT * FROM inserts
		");
		while($row = mysql_fetch_array($query)){
			
			if($row['image_web_1'] != ''){
				if(!file_exists(DOCROOT."assets/upload/jobs/".$row['image_web_1'])){
					echo $row['id'].'  '.$row['user_id'].' '.DOCROOT."assets/upload/jobs/".$row['image_web_1'].'<br/>';
				}
			}
		
		}
		
	}
	
	
	public function action_sendmail(){
			
		die;
		
		$query = mysql_query("
			SELECT * FROM tickets WHERE status = 1 AND saved > '2012-03-16' AND id < 242
		");
		while($row = mysql_fetch_array($query)){
					
			$html = '';
					
			$user = ORM::factory('user', $row['user_id']);
			if($user->private_member == 1 || $user->legal_member == 1){
				
				/*
				$html = '<html><body>';
				$html .= '<div style="padding:20px;font-family:Tahoma;font-size:12px;">
					<p>Cien. '.($user->private_name != '' ? $user->private_name : $user->legal_title).'! <br/><br/>Atvainojiet par tehniskajām kļūmēm iepriekš sūtītajos atkārtotajos apstiprinājumos uz pieteikšanos pasākumiem. Šis ir atkārtotais apstiprinājuma e-pasts. Neaizmirstiet, ka arī atzīmētajiem līdznācējiem (ja tādi ir) ir jāreģistrējas ADwards weblapā un jāpiesakās online.</p>
					<p>ADwards 2012 radošā diena ir 2012. gada 23. marts no plkst. 9.00 līdz plkst. 18.00 Splendid Palace Mazajā zālē (Elizabetes iela 61). ADwards 2012 apbalvošanas ceremonija notiks 2012. gada 23. martā plkst. 19.30 (ieeja no 19.00) koncertzālē Palladium (Marijas iela 21)</p>
					<p>Sirsnībā</p>
					<p>Jūsu LADC</p>
					<p>Vairāk par visu – www.adwards.lv, olga@ladc.lv</p>
					</div>';
				$html .= '</body></html>';
					
				$mail = new phpmailer;
				$mail->CharSet    = 'UTF-8';
				$mail->Subject    = 'Dalība ADwards 2012 pasākumos';
				$mail->IsQmail();
				$mail->MsgHTML($html);
				$mail->AddAddress($user->email);
				$mail->SetFrom('no-reply@adwards.lv', 'ADwards 2012');
				$mail->AddReplyTo('no-reply@adwards.lv', 'ADwards 2012');
				$mail->Send();
				$mail->ClearAddresses();
				
				echo 'Sent to LADC member: '.$user->id.' '.$user->email.'<br/>';
				
				sleep(1);
				*/
				
			}else{
				
				if($row['apply_1'] == 1 && $row['apply_2'] == 1){
					$html = '<html><body>';
					$html .= '<div style="padding:20px;font-family:Tahoma;font-size:12px;">
						<p>Cien. '.($user->private_name != '' ? $user->private_name : $user->legal_title).'! <br/><br/>Atvainojiet par tehniskajām kļūmēm iepriekš sūtītajos atkārtotajos apstiprinājumos uz pieteikšanos pasākumiem. Šis ir atkārtotais apstiprinājuma e-pasts. Neaizmirstiet, ka arī atzīmētajiem līdznācējiem (ja tādi ir) ir jāreģistrējas ADwards weblapā un jāpiesakās online.</p>
						<p>Priecājamies apstiprināt, ka jūs esat veiksmīgi pieteicies ADwards 2012 pasākumiem. Lūgums izdrukāt šo apstiprinājumu un, ierodoties uz pasākumu, to uzrādīt ADwards organizatoriem.</p>
						<p>ADwards 2012 radošā diena ir 2012. gada 23. marts no plkst. 9.00 līdz plkst. 18.00 Splendid Palace Mazajā zālē (Elizabetes iela 61). ADwards 2012 apbalvošanas ceremonija notiks 2012. gada 23. martā plkst. 19.30 (ieeja no 19.00) koncertzālē Palladium (Marijas iela 21)</p>
						<p>Sirsnībā</p>
						<p>Jūsu LADC</p>
						<p>Vairāk par visu – www.adwards.lv, olga@ladc.lv</p>
						</div>';
					$html .= '</body></html>';
				}else{
					if($row['apply_1'] == 1){
						$html = '<html><body>';
						$html .= '<div style="padding:20px;font-family:Tahoma;font-size:12px;">
							<p>Cien. '.($user->private_name != '' ? $user->private_name : $user->legal_title).'! <br/><br/>Atvainojiet par tehniskajām kļūmēm iepriekš sūtītajos atkārtotajos apstiprinājumos uz pieteikšanos pasākumiem. Šis ir atkārtotais apstiprinājuma e-pasts. Neaizmirstiet, ka arī atzīmētajiem līdznācējiem (ja tādi ir) ir jāreģistrējas ADwards weblapā un jāpiesakās online.</p>
							<p>Priecājamies apstiprināt, ka jūs esat veiksmīgi pieteicies ADwards 2012 pasākumiem. Lūgums izdrukāt šo apstiprinājumu un, ierodoties uz pasākumu, to uzrādīt ADwards organizatoriem.</p>
							<p>ADwards 2012 radošā diena ir 2012. gada 23. marts no plkst. 9.00 līdz plkst. 18.00 Splendid Palace Mazajā zālē (Elizabetes iela 61). ADwards 2012 apbalvošanas ceremonija notiks 2012. gada 23. martā plkst. 19.30 (ieeja no 19.00) koncertzālē Palladium (Marijas iela 21)</p>
							<p>Sirsnībā</p>
							<p>Jūsu LADC</p>
							<p>Vairāk par visu – www.adwards.lv, olga@ladc.lv</p>
							</div>';
						$html .= '</body></html>';
					}else{
						$html .= '<div style="padding:20px;font-family:Tahoma;font-size:12px;">
							<p>Cien. '.($user->private_name != '' ? $user->private_name : $user->legal_title).'! <br/><br/>Atvainojiet par tehniskajām kļūmēm iepriekš sūtītajos atkārtotajos apstiprinājumos uz pieteikšanos pasākumiem. Šis ir atkārtotais apstiprinājuma e-pasts. Neaizmirstiet, ka arī atzīmētajiem līdznācējiem (ja tādi ir) ir jāreģistrējas ADwards weblapā un jāpiesakās online.</p>
							<p>Priecājamies apstiprināt, ka jūs esat veiksmīgi pieteicies ADwards 2012 pasākumiem. Lūgums izdrukāt šo apstiprinājumu un, ierodoties uz pasākumu, to uzrādīt ADwards organizatoriem.</p>
							<p>ADwards 2012 radošā diena ir 2012. gada 23. marts no plkst. 9.00 līdz plkst. 18.00 Splendid Palace Mazajā zālē (Elizabetes iela 61). ADwards 2012 apbalvošanas ceremonija notiks 2012. gada 23. martā plkst. 19.30 (ieeja no 19.00) koncertzālē Palladium (Marijas iela 21)</p>
							<p>Sirsnībā</p>
							<p>Jūsu LADC</p>
							<p>Vairāk par visu – www.adwards.lv, olga@ladc.lv</p>
							</div>';
						$html .= '</body></html>';
					}
				} 
							
				
				$mail = new phpmailer;
				$mail->CharSet    = 'UTF-8';
				$mail->Subject    = 'Dalība ADwards 2012 pasākumos';
				$mail->IsQmail();
				$mail->MsgHTML($html);
				$mail->AddAddress($user->email);
				$mail->SetFrom('no-reply@adwards.lv', 'ADwards 2012');
				$mail->AddReplyTo('no-reply@adwards.lv', 'ADwards 2012');
				$mail->Send();
				$mail->ClearAddresses();
				
				echo 'Sent to NON member: '.$user->id.' '.$user->email.'<br/>';
				
				sleep(1);
							
			}
			
		}
		
		die;
		
		$user = ORM::factory('user', 120);
		
		$html = '<html><body>';
		$html .= '<div style="padding:20px;font-family:Tahoma;font-size:12px;">
			<p>'.($user->private_name != '' ? $user->private_name : $user->legal_title).' ir uzaicinājis Jūs uz ADwards 2012 uzvarētāju apbalvošanas ceremoniju. Lai mēs varētu iekļaut Jūs viesu saraksta, lūdzam Jūs pieteikties pasākumam ADwards vortālā http://adwards.lv/lv/pasakumi/pasakumi.  Lai pieteiktos pasākumam, Jums jābūt reģistrētam lietotājam. Reģistrācija: http://adwards.lv/lv/sakums/registracija</p>
			<p>Paldies un gaidām jūs apbalvošanas ceremonijā!</p>
			<p>ADwards 2012</p></div>';
		$html .= '</body></html>';
								
		$mail = new phpmailer;
		$mail->CharSet    = 'UTF-8';
		$mail->Subject    = 'Ielūgums uz ADwards 2012 uzvarētāju apbalvošanas ceremoniju';
		$mail->IsQmail();
		$mail->MsgHTML($html);
		$mail->AddAddress('ilja.olijevskis@mediacom.lv');
		$mail->SetFrom('no-reply@adwards.lv', 'ADwards 2012');
		$mail->AddReplyTo('no-reply@adwards.lv', 'ADwards 2012');
		$mail->Send();
		
		echo $html;
		
		/*
		$mail = new phpmailer;
		$mail->CharSet    = 'UTF-8';
		$mail->Subject    = 'test subject';
		$mail->IsQmail();
		$mail->MsgHTML('<p>paragrāfs</p>');
		$mail->AddAddress('aigars@digibrand.lv');
		$mail->SetFrom('no-reply@adwards.lv', 'Adwards 2012');
		$mail->AddReplyTo('no-reply@adwards.lv', 'Adwards 2012');
		$mail->Send();
		*/
		
	}
	
}
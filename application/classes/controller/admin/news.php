<?
class Controller_Admin_News extends Controller_Admin {
	public function action_view($section_id){
		$this->template->content = View::factory('admin/news/index')->set('section_id', $section_id)->set('news', ORM::factory('news')->where('section_id', '=', $section_id)->find_all()->as_array());
	}
	
	public function action_add($id){
		if($this->request->method() == Request::POST){
                        
                    /*
			if(!empty($_FILES['image']) && $_FILES['image']['size'] > 0 && $_FILES['image']['error'] == 0){
				$ext = substr($_FILES['image']['name'], strrpos($_FILES['image']['name'], '.') + 1);
				$image_285_150 = md5(microtime()).'.'.$ext;
				Image::factory($_FILES['image']['tmp_name'])
					->resize(284, 150, Image::INVERSE)
					->crop(284, 150)
					->save(DOCROOT.'assets/media/images/'.$image_285_150);
				$_POST['image_285_150'] = $image_285_150;
				
				$image_527_235 = md5(microtime()).'.'.$ext;
				Image::factory($_FILES['image']['tmp_name'])
					->resize(670, 275, Image::INVERSE)
					->crop(670, 275)
					->save(DOCROOT.'assets/media/images/'.$image_527_235);
				$_POST['image_527_235'] = $image_527_235;
				
				$image_1005_277 = md5(microtime()).'.'.$ext;
				Image::factory($_FILES['image']['tmp_name'])
					->resize(1002, 277, Image::INVERSE)
					->crop(1002, 277)
					->save(DOCROOT.'assets/media/images/'.$image_1005_277);
				$_POST['image_1005_277'] = $image_1005_277;
			}
                     
                     */
                        $_POST['image_285_150'] = $_POST['image_sm'];
                        $_POST['image_527_235'] = $_POST['image_md'];
                        $_POST['image_1005_277'] = $_POST['image_lg'];

			$news = new Model_News();
			$news->values($_POST);
			$news->save();
			$this->request->redirect('admin/news/view/'.$_POST['section_id']);
		}
		$this->template->content = View::factory('admin/news/edit')
			->set('news', false)
			->set('section_id', $id);
	}
        
        public function action_cropper() {
                if($this->request->method() == Request::POST){
                    
                        if(!empty($_FILES['image_small']) && $_FILES['image_small']['size'] > 0 && $_FILES['image_small']['error'] == 0){
				$ext = substr($_FILES['image_small']['name'], strrpos($_FILES['image_small']['name'], '.') + 1);
				$image_285_150 = md5(microtime()).'.'.$ext;
				Image::factory($_FILES['image_small']['tmp_name'])
					->resize(308, 163, Image::INVERSE)
					->crop(308, 163)
					->save(DOCROOT.'assets/media/images/'.$image_285_150);
				$out['image'] = $image_285_150;
			}
                        if(!empty($_FILES['image_medium']) && $_FILES['image_medium']['size'] > 0 && $_FILES['image_medium']['error'] == 0){
                            $ext = substr($_FILES['image_medium']['name'], strrpos($_FILES['image_medium']['name'], '.') + 1);
                            $image_527_235 = md5(microtime()).'.'.$ext;
				Image::factory($_FILES['image_medium']['tmp_name'])
					->resize(648, 343, Image::INVERSE)
					->crop(648, 343)
					->save(DOCROOT.'assets/media/images/'.$image_527_235);
				$out['image'] = $image_527_235;
			}
                        if(!empty($_FILES['image_large']) && $_FILES['image_large']['size'] > 0 && $_FILES['image_large']['error'] == 0){
				$ext = substr($_FILES['image_large']['name'], strrpos($_FILES['image_large']['name'], '.') + 1);
				$image_1005_277 = md5(microtime()).'.'.$ext;
				Image::factory($_FILES['image_large']['tmp_name'])
					->resize(986, 460, Image::INVERSE)
					->crop(986, 460)
					->save(DOCROOT.'assets/media/images/'.$image_1005_277);
				$out['image'] = $image_1005_277;
			}
                        header('Content-Type: application/json');			
                        die(json_encode($out));		
                }
        }
        
	
	public function action_edit($id){
		$news = ORM::factory('news', $id);
		if(!$news->loaded()) $this->request->redirect('admin/news/view');
		if($this->request->method() == Request::POST){
			
                    /*
			if(!empty($_FILES['image']) && $_FILES['image']['size'] > 0 && $_FILES['image']['error'] == 0){
				$ext = substr($_FILES['image']['name'], strrpos($_FILES['image']['name'], '.') + 1);
				$image_285_150 = md5(microtime()).'.'.$ext;
				Image::factory($_FILES['image']['tmp_name'])
					->resize(284, 150, Image::INVERSE)
					->crop(284, 150)
					->save(DOCROOT.'assets/media/images/'.$image_285_150);
				$_POST['image_285_150'] = $image_285_150;
				
				$image_527_235 = md5(microtime()).'.'.$ext;
				Image::factory($_FILES['image']['tmp_name'])
					->resize(670, 275, Image::INVERSE)
					->crop(670, 275)
					->save(DOCROOT.'assets/media/images/'.$image_527_235);
				$_POST['image_527_235'] = $image_527_235;
				
				$image_1005_277 = md5(microtime()).'.'.$ext;
				Image::factory($_FILES['image']['tmp_name'])
					->resize(1002, 277, Image::INVERSE)
					->crop(1002, 277)
					->save(DOCROOT.'assets/media/images/'.$image_1005_277);
				$_POST['image_1005_277'] = $image_1005_277;
			}
                     
                     */
                        $_POST['image_285_150'] = $_POST['image_sm'];
                        $_POST['image_527_235'] = $_POST['image_md'];
                        $_POST['image_1005_277'] = $_POST['image_lg'];
			
			$news->values($_POST);
			$news->save();
			$this->request->redirect('admin/news/view/'.$_POST['section_id']);
		}
		$this->template->content = View::factory('admin/news/edit')
			->set('news', $news->as_array());
	}
	
	public function action_delete($id){
		$news = ORM::factory('news', $id);
		$section_id = $news->section_id;
		if(!$news->loaded()) throw new Http_Exception_404('[CMS]: Item :id not found', array(':id' => $id));
		$news->delete();
		$this->request->redirect('admin/news/view/'.$section_id);
	}
	
	
	public function action_deleteimg($id){
		$news = ORM::factory('news', $id);
		if(!$news->loaded()) $this->request->redirect('admin/news/view');
		
		unlink(DOCROOT.'assets/media/images/'.$news->image_285_150);
		unlink(DOCROOT.'assets/media/images/'.$news->image_527_235);
		
		$data['image_285_150'] = '';
		$data['image_527_235'] = '';
		$news->values($data);
		$news->save();
		$this->request->redirect('admin/news/edit/'.$id);
	}
	
}
?>
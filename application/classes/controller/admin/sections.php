<?
class Controller_Admin_Sections extends Controller_Admin {
	public function action_tree($language){
		die(Helper_Section::print_tree($language));
	}
	
	public function action_sort(){
		DB::update('sections')
			->set(array('list_order' => 0))
			->execute();
		if(empty($_POST['data'])) exit;
		foreach($_POST['data'] as $pid=>$sections){
			if(empty($sections)) continue;
			foreach($sections as $key=>$value){
				if(empty($value)) continue;
				$section = ORM::factory('section', (int)substr($value, 1));
				$section->list_order = $key;
				$section->save();
			}
		}
		exit;
	}
	
	public function action_add($id){
		if($this->request->method() == Request::POST){
		
			if(!empty($_FILES['image']) && $_FILES['image']['size'] > 0 && $_FILES['image']['error'] == 0){
				$ext = substr($_FILES['image']['name'], strrpos($_FILES['image']['name'], '.') + 1);
				$img = md5(microtime()).'.'.$ext;
				$_POST['image'] = $img;
				Upload::save($_FILES['image'],$img,DOCROOT.'assets/media/images/');				
			}
			
			$section = new Model_Section();
			$section->values($_POST);
			$section->save();
			$this->request->redirect('admin/'.$section->module_id.'/view/'.$section->id);
		}
		$this->template->content = (string)View::factory('admin/sections/view')
			->set('section', false)
			->set('parent_id', $id);
	}
	
	public function action_view($id){
		$section = ORM::factory('section', $id);
		if(!$section->loaded()) throw new Http_Exception_404('[CMS]: Section :id not found', array(':id' => $id));
		if($this->request->method() == Request::POST){
			
			if(!empty($_FILES['image']) && $_FILES['image']['size'] > 0 && $_FILES['image']['error'] == 0){
				$ext = substr($_FILES['image']['name'], strrpos($_FILES['image']['name'], '.') + 1);
				$img = md5(microtime()).'.'.$ext;
				$_POST['image'] = $img;
				Upload::save($_FILES['image'],$img,DOCROOT.'assets/media/images/');				
			}
		
			$section->values($_POST);
			$section->save();
		}
		$this->template->content = (string)View::factory('admin/sections/view')
			->set('section', $section->as_array())
			->set('parent_id', $section->parent_id);
	}
	
	public function action_delete($id){
		$section = ORM::factory('section', $id);
		if(!$section->loaded()) throw new Http_Exception_404('[CMS]: Section :id not found', array(':id' => $id));
		$section->delete();
		exit;
	}
}
?>
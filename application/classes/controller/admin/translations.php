<?
class Controller_Admin_Translations extends Controller_Admin {
	public function action_index(){
		$languages = array_keys(Kohana::$config->load('site')->get('languages'));
		if($this->request->method() == Request::POST){
			$cache = array();
			foreach($_POST as $key=>$value){
				$updates = array();
				foreach($value as $k=>$v){
					$updates['str_'.$k] = $v;
					$cache[$k][$key] = $v;
				}
				DB::update('translations')
					->set($updates)
					->where('key', '=', $key)
					->execute();
			}
			foreach($cache as $key=>$value) file_put_contents(APPPATH.'i18n/'.$key.'.json', json_encode($value));
		}
		$this->template->content = View::factory('admin/translations/index')
			->set('pw', round(100/(count($languages)+2)))
			->set('languages', $languages)
			->set('translations', ORM::factory('translation')->find_all()->as_array());
	}
}
?>
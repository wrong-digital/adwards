<?
class Controller_Admin_Formats extends Controller_Admin {
	public function action_view($section_id){
		$this->template->content = 
			View::factory('admin/formats/index')
				->set('section_id', $section_id)
				->set('formats', ORM::factory('formats')->where('section_id', '=', $section_id)->order_by('list_order', 'ASC')->find_all()->as_array());
	}
	
	public function action_add($id){
		if($this->request->method() == Request::POST){
			$next = ORM::factory('formats')->where('section_id', '=', $_POST['section_id'])->order_by('list_order', 'DESC')->limit(1)->find();
			$formats = new Model_Formats();
			$_POST['list_order'] = $next->list_order+1;
			
			$formats->values($_POST);
			$formats->save();
			$this->request->redirect('admin/formats/view/'.$_POST['section_id']);
		}
		
		$this->template->content = View::factory('admin/formats/edit')
			->set('formats', false)
			->set('section_id', $id);
	}
	
	public function action_edit($id){
		$formats = ORM::factory('formats', $id);
		if(!$formats->loaded()) $this->request->redirect('admin/formats/view');
		
		if($this->request->method() == Request::POST){
			$formats->values($_POST);
			$formats->save();
			$this->request->redirect('admin/formats/view/'.$_POST['section_id']);
		}
		
		$this->template->content = View::factory('admin/formats/edit')
			->set('formats', $formats->as_array());
	}
	
	public function action_delete($id){
		$formats = ORM::factory('formats', $id);
		$section_id = $formats->section_id;
		if(!$formats->loaded()) throw new Http_Exception_404('[CMS]: Item :id not found', array(':id' => $id));
		$formats->delete();
		$this->request->redirect('admin/formats/view/'.$section_id);
	}
	
	public function action_sort(){
		if(empty($_POST['data'])) exit;
		foreach($_POST['data'] as $key=>$id){
			$formats = ORM::factory('formats', $id);
			$formats->list_order = $key;
			$formats->save();
		}
		die;
	}
	
}
?>
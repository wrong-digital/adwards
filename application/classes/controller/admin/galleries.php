<?
class Controller_Admin_Galleries extends Controller_Admin {

	public function action_view($section_id){
		$this->template->content = View::factory('admin/galleries/index')
			->set('section_id', $section_id)
			->set('galleries', ORM::factory('gallery')
			->where('section_id', '=', $section_id)
			->find_all()
			->as_array());
	}
	
	
	public function action_add($id){
		if($this->request->method() == Request::POST){
		
			if(!empty($_FILES['image']) && $_FILES['image']['size'] > 0 && $_FILES['image']['error'] == 0){
				$ext = substr($_FILES['image']['name'], strrpos($_FILES['image']['name'], '.') + 1);
				$img = md5(microtime()).'.'.$ext;
				Image::factory($_FILES['image']['tmp_name'])
					->resize(107, 107, Image::INVERSE)
					->crop(107, 107)
					->save(DOCROOT.'assets/media/images/'.$img);
				$_POST['image'] = $img;
			}

            $gallery = new Model_Gallery();
            $gallery->values($_POST);
			$gallery->save();                       
                      
			$this->request->redirect('admin/galleries/view/'.$_POST['section_id']);
		}
        
		$this->template->js = array('jquery.uploadify', 'swfobject');
		$this->template->content = View::factory('admin/galleries/edit')
			->set('gallery', false)
			->set('section_id', $id);		
	}
	
	
	public function action_edit($id){
        $gallery = ORM::factory('gallery', $id);
        if($this->request->method() == Request::POST){
			
			if(!$gallery->loaded()) $this->request->redirect('admin/galleries');

            if(!empty($_FILES['image']) && $_FILES['image']['size'] > 0 && $_FILES['image']['error'] == 0){
				$ext = substr($_FILES['image']['name'], strrpos($_FILES['image']['name'], '.') + 1);
				$img = md5(microtime()).'.'.$ext;
				Image::factory($_FILES['image']['tmp_name'])
					->resize(107, 107, Image::INVERSE)
					->crop(107, 107)
					->save(DOCROOT.'assets/media/images/'.$img);
				$_POST['image'] = $img;
			}
			
            $gallery->values($_POST);
			$gallery->save();
			$this->request->redirect('admin/galleries/view/'.$_POST['section_id']);
		}
		
		$this->template->css = array('uploadify');
		$this->template->js = array('jquery.uploadify', 'swfobject');
		$this->template->content = View::factory('admin/galleries/edit')
			->set('gallery', $gallery->as_array())
			->set('images', $gallery->images->order_by('list_order', 'ASC')->find_all()->as_array());
	}
	
	
	public function action_edit_image($id){
	    $image = ORM::factory('galleries_image', $id);
		
		if($this->request->method() == Request::POST){
			if(!$image->loaded()) $this->request->redirect('admin/galleries');
            $image->values($_POST);
			$image->save();
			$this->request->redirect('admin/galleries/edit/'.$_POST['gallery_id']);
		}
		
        $this->template->content = 
			View::factory('admin/galleries/edit_image')
				->set('image', $image->as_array());
	}

	
    public function action_delete($id){
		$gallery = ORM::factory('gallery', $id);
		$section_id = $gallery->section_id;
		if(!$gallery->loaded()) throw new Http_Exception_404('[CMS]: Item :id not found', array(':id' => $id));
		$gallery->delete();
		$this->request->redirect('admin/galleries/view/'.$section_id);
	}
	
	
	public function action_sort(){
		if(empty($_POST['data'])) exit;
		foreach($_POST['data'] as $key=>$id){
			$images = ORM::factory('galleries_image', $id);
			$images->list_order = $key;
			$images->save();
		}
		die;
	}
}
?>
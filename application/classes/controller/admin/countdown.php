<?php

class Controller_Admin_Countdown extends Controller_Admin {
    
    public function action_view() {
        $countdown = ORM::factory('countdown')->find();
        $this->template->content =
                View::factory('admin/countdown')
                    ->set('enabled',$countdown->enabled)
                    ->set('target', $countdown->target)
                    ->set('message', $countdown->message);
    }
    
    public function action_update() {
        if($this->request->method() == Request::POST && $this->request->is_ajax()) {
            $enabled = $_POST['enabled'];
            $target = HTML::chars($_POST['target']);
            $message = HTML::chars($_POST['message']);
            $countdown = ORM::factory('countdown',1);
            $countdown->enabled = $enabled;
            $countdown->target = $target;
            $countdown->message = $message;
            $countdown->save();
            $out['state'] = $countdown->enabled;
            header('Content-Type: application/json');
            die(json_encode($out));
            
        }
    }
}
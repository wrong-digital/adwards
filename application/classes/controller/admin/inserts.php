<?
class Controller_Admin_Inserts extends Controller_Admin {
	public function action_index(){
		
		$users = ORM::factory('user')->find_all()->as_array();
                $emails = array();
		foreach($users as $user){
			$users[$user->id] = $user->private_name != '' ? $user->private_name : $user->legal_title;
                        $emails[$user->id] = $user->email;
		}
		
		$this->template->content = 
			View::factory('admin/inserts/index')
				->set('users', $users)
                                ->set('emails', $emails)
				->set('inserts', ORM::factory('inserts')->where('user_id', '>', 0)->order_by('id', 'DESC')->find_all()->as_array());
	}
    
    public function action_apraksti(){
        
        $users = ORM::factory('user')->find_all()->as_array();
        foreach($users as $user){
            $users[$user->id] = $user->private_name != '' ? $user->private_name : $user->legal_title;
        }
        
        $this->template->content = 
            View::factory('admin/inserts/apraksti')
                ->set('users', $users)
                ->set('inserts', ORM::factory('inserts')->where('user_id', '>', 0)->order_by('id', 'asc')->find_all()->as_array());
    }
	
	public function action_edit($id){
		$inserts = ORM::factory('inserts', $id);
		if(!$inserts->loaded()) $this->request->redirect('admin/inserts');
		if($this->request->method() == Request::POST){
			$inserts->values($_POST);
			$inserts->save();
			$this->request->redirect('admin/inserts');
		}
		
		$this->template->content = View::factory('admin/inserts/edit')
			->set('inserts', $inserts->as_array())
			->set('formats', ORM::factory('formats')->order_by('list_order', 'ASC')->find_all()->as_array())
			->set('categories', ORM::factory('categories')->order_by('list_order', 'ASC')->find_all()->as_array());
	}
}
?>
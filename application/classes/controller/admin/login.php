<?
class Controller_Admin_Login extends Controller {
	public function action_index(){
		$this->response->body((string)View::factory('admin/login'));
	}
	
	public function action_go(){
		$un = Arr::get($_POST, 'username');
		$pw = Arr::get($_POST, 'password');
		if(empty($un) || empty($pw)) die(json_encode(array('status' => false, 'code' => 1)));
		$user = ORM::factory('user', array('username' => $un));
		if(!$user->loaded()) die(json_encode(array('status' => false, 'code' => 2)));
		if(!$user->active) die(json_encode(array('status' => false, 'code' => 3)));
		if($user->password != Model_User::password_hash($pw, $user->password_salt)) die(json_encode(array('status' => false, 'code' => 4)));
		Session::instance()
			->set('admin_uid', $user->id)
			->set('admin_level', $user->level);
		$user->date_login = DB::expr('NOW()');
		$user->save();
		die(json_encode(array('status' => true)));
	}
	
	public function action_logout(){
		Session::instance()->destroy();
		$this->request->redirect();
	}
}
?>
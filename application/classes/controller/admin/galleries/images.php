<?
class Controller_Admin_Galleries_Images extends Controller_Admin {
	public function before(){
		/*if(!empty($_POST['XS'])){
            session_name('XS');
            session_id($_POST['XS']);
        }
		parent::before();
         * 
         */
	}
	
	public function action_upload($id){
		$gallery = ORM::factory('gallery', $id);
		if(!$gallery->loaded()) die('false');
		if(empty($_FILES['Filedata']) || $_FILES['Filedata']['size'] <= 0 || $_FILES['Filedata']['error'] != 0) die('false');
        $config = Kohana::$config->load('modules/gallery')->get('images');
		$filename = Helper_Gallery::getFilename();
		foreach($config as $image){
			$img = Image::factory($_FILES['Filedata']['tmp_name']);
			if($image['crop']){
				$img
					->resize($image['width'], $image['height'], Image::INVERSE)
					->crop($image['width'], $image['height']);
			}else{
				$img->resize($image['width'], $image['height']);
			}
			$ext = array_key_exists('ext', $image) ? $image['ext'] : 'jpg';
			$img->save(DOCROOT.'assets/upload/gallery/'.$image['path'].'/'.$filename.'.'.$ext);
		}
		$thumb = new Model_Galleries_Image();
		$thumb->gallery_id = (int)$id;
		$thumb->file = $filename;
		$thumb->save();
		die('true');
	}
	
	public function action_delete($id){
		$image = ORM::factory('galleries_image', $id);
		if(!$image->loaded()) $this->request->redirect('admin/galleries');
		$config = Kohana::$config->load('modules/gallery')->get('images');
		foreach($config as $img){
			$ext = array_key_exists('ext', $img) ? $img['ext'] : 'jpg';
			@unlink(DOCROOT.'assets/upload/gallery/'.$img['path'].'/'.$image->file.'.'.$ext);
		}
		$gid = $image->gallery_id;
		$image->delete();
		$this->request->redirect('admin/galleries/edit/'.$gid);
	}
}
?>
<?
class Controller_Admin_Textdoc extends Controller_Admin {
	public function action_view($id){
		$section = ORM::factory('section', $id);
		if(!$section->loaded()) throw new Http_Exception_404('[CMS]: Section :id not found', array(':id' => $id));
		$textdoc = ORM::factory('textdoc', array('section_id' => $id));
		if($this->request->method() == Request::POST){
			$contents = array(
				'date'       => date('Y-m-d H:i:s'),
				'user_id'    => Session::instance()->get('admin_uid'),
				'section_id' => $section->id,
				'content'    => $_POST['content']
			);
			if(!$textdoc->loaded()) $textdoc = new Model_Textdoc();
			$section->values($_POST);
			$section->save();
			$textdoc->values($contents);
			$textdoc->save();
		}
		$this->template->content = View::factory('admin/textdoc/view')
			->set('section', $section->as_array())
			->set('textdoc', $textdoc->loaded() ? $textdoc->as_array() : false);
	}
}
?>
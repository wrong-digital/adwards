<?
class Controller_Admin_Start extends Controller_Admin {
	public function action_index(){
		$countdown = ORM::factory('countdown')->find();
                $this->template->content =
                        View::factory('admin/countdown')
                            ->set('enabled',$countdown->enabled)
                            ->set('target', $countdown->target)
                            ->set('message', $countdown->message);
	}
}
?>
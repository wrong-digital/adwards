<?
class Controller_Admin_Settings extends Controller_Admin {
	public function action_index(){
		if($this->request->method() == Request::POST){
			foreach($_POST as $key=>$value){
				$s = ORM::factory('settings', $key);
				if(!$s->loaded()) continue;
				$s->value = $value;
				$s->save();
			}
		}
		$this->template->content = View::factory('admin/settings/index')
			->set('settings', ORM::factory('settings')->find_all()->as_array('key', 'value'));
	}
}
?>
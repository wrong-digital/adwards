<?php


class Controller_Admin_Winners extends Controller_Admin
{
    private $years = [
            2006 => [
                'Ideju kategorija - TV',
                'Ideju kategorija - Drukātā reklāma',
                'Ideju kategorija - Radioreklāma',
                'Ideju kategorija - Tiešais mārketings',
                'Ideju kategorija - Sociālā reklāma',
                'Meistarības kategorija - TV',
                'Meistarības kategorija - Grafiskais dizains',
                'Meistarības kategorija - Industriālais dizains',
                'Meistarības kategorija - Fotogrāfija',
                'Meistarības kategorija - Animācija',
                'Meistarības kategorija - Reklāmas teksts'
              ],
            2007 => [
                'Industriālais dizains',
                'Interaktīvais dizains',
                'Grafiskais dizains',
                'Sociālā reklāma',
                'Drukātā reklāma',
                'Integrētās kampaņas',
                'TV',
                'Vides objekti'
              ],
            2008 => [
                'Speciālās kategorijas',
                'Žūrijas īpašie apbalvojumi',
                'Integrētā kampaņa',
                'TV',
                'Drukātā reklāma',
                'Tiešais mārketings',
                'Sociālā reklāma',
                'Grafiskais dizains',
                'Iepakojuma dizains',
                'Industriālais dizains',
                'Plakāti / vides reklāma',
                'Reklāmas teksts'
              ],
            2009 => [
                'Integrētā kampaņa',
                'Tiešais mārketings',
                'Sociālā reklāma',
                'Grafiskais dizains',
                'Animācija',
                'Interaktīvais dizains',
                'Industriālais dizains',
                'TV'
              ],
            2010 => [
                'Industriālais dizains',
                'Grafiskais dizains',
                'Fotogrāfija',
                'Interaktīvais dizains',
                'Radio reklāma',
                'Drukātā reklāma',
                'Integrētā kampaņa',
                'Televīzija',
                'Speciālās balvas'
              ],
            2011 => [
                'TV',
                'Grafiskais dizains',
                'Digitālie risinājumi',
                'Speciālās balvas',
                'Darba uzdevumi'
              ],
            2012 => [
                'Integrētā kampaņa',
                'TV reklāmas',
                'Valsts pakalpojumi un labdarība',
                'Reklāmas plakāti',
                'Reklāma laikrakstos, žurnālos un informatīvos tirdzniecības materiālos',
                'Tīmekļa vietnes un mikrovietnes',
                'Reklāmkarogi, Rich media, animētas apsveikuma kartītes',
                'Social Network Solutions',
                'Grafiskais dizains',
                'Redakcijas darbs/grāmatas/korporatīvā izdevējdarbība/katalogi',
                'Korporatīvā identitāte/korporatīvais dizains/zīmolvedība',
                'Ilustrācija un fotogrāfija',
                'Industriālais dizains',
                'Animācija',
                'Ambient Media',
                'Pasākumi (korporatīvie, sabiedriskie, patērētāju, sociālie/kultūras)',
                'PR Kampaņas',
                'Limbo',
                'Darba uzdevumi'
              ],
            2013 => [
                'Integrētā kampaņa',
                'TV un Radio',
                'Interaktīvie un digitālie risinājumi',
                'Reklāmas spēles',
                'Citi digitālie risinājumi',
                'Netradicionālā komunikācija',
                'Pasākumi',
                'Ambient',
                'Sabiedriskās attiecības',
                'Vides reklāma',
                'Grafiskais dizains',
                'Vides, sabiedrisko telpu un komerciālo telpu dizains',
                'Ilustrācija',
                'Grāmatas, katalogi un kalendāri',
                'Iepakojuma dizains',
                'Logo/vizuālā identitāte',
                'Produktu dizains',
                'Radošie uzdevumi'
              ],
            2014 => [
                'Integrētā kampaņa',
                'TV un Radio',
                'Grafiskie video risinājumi',
                'Interaktīvie un digitālie risinājumi',
                'Tīmekļa lapa',
                'Citi digitālie risinājumi',
                'Mobilās aplikācijas',
                'Digitālais dizains',
                'Netradicionālā komunikācija',
                'Branded content and entertainment',
                'Print Art un dizains',
                'Izdevniecību nozare',
                'Kalendāri',
                'Vizuālā identitāte',
                'Komerciālais iepakojums',
                'Fotogrāfija',
                'Produktu dizains'
              ],
            2015 => [],
            2016 => ['Integrētā kampaņa',
                'TV un Radio',
                'Grafiskie video risinājumi',
                'Interaktīvie un digitālie risinājumi',
                'Tīmekļa lapa',
                'Citi digitālie risinājumi',
                'Mobilās aplikācijas',
                'Digitālais dizains',
                'Netradicionālā komunikācija',
                'Branded content and entertainment',
                'Print Art un dizains',
                'Izdevniecību nozare',
                'Kalendāri',
                'Vizuālā identitāte',
                'Komerciālais iepakojums',
                'Fotogrāfija',
                'Zīmola stāsts',
                'Vides reklāma',
                'Sabiedriskās attiecības',
                'Pasākumi',
                'Produktu dizains']
            ];
    
    public function action_view($section_id)
    {
        switch($section_id)
        {
            case 90:
                $this->template->content =
                    View::factory('admin/winners/shortlist')
                        ->set('section_id',$section_id)
                        ->set('winners', ORM::factory('winners')->where('shortlist','=',1)->order_by('year','DESC')->find_all()->as_array());
                break;
            default:
                $this->template->content =
                    View::factory('admin/winners/index')
                        ->set('section_id',$section_id)
                        ->set('winners', ORM::factory('winners')->where('shortlist','=',0)->order_by('year','DESC')->find_all()->as_array());
        }

    }
    public function action_add($section_id)
    {        
        if($this->request->method() == Request::POST)
        {
                        $winner	= new Model_Winners();
            
                        if($_POST['job'] != '') {
                            $job = ORM::factory('inserts',$_POST['job']);
                            if(!$job->loaded()) $this->request->redirect('admin/winners/add');
                            $_POST['title'] = $job->title_lv;
                            $_POST['client'] = $job->client_lv;
                            $user = ORM::factory('user',$job->user_id);
                            if($user->private_name != '') {
                                $_POST['author'] = $user->private_name;
                            }
                            else {
                                $_POST['author'] = $user->legal_title;
                            }
                            $_POST['category'] = ORM::factory('categories',$job->main_category)->title;
                            if($_POST['shortlist'] != 1) {
                                $_POST['sub_category'] = ORM::factory('formats',$job->sub_category)->title;
                            }
                            if($job->video != '') {
                                $_POST['video'] = 'assets/upload/jobs/'.$job->video;                 
                            }
                            if($job->audio != '') {
                                $_POST['audio'] = 'assets/upload/jobs/'.$job->audio;
                            }
                            if($job->image_web_1 != '') { $pic = $job->image_web_1; }
                                else if($job->image_web_2 != '') { $pic = $job->image_web_2; }
                                else if($job->image_web_3 != '') { $pic = $job->image_web_3; }
                                else if($job->image_web_4 != '') { $pic = $job->image_web_4; }
                                else if($job->image_web_5 != '') { $pic = $job->image_web_5; }
                                else if($job->image_web_6 != '') { $pic = $job->image_web_6; }
                                else { $pic = ''; }
                            if($pic != '') {
                                $ext = substr($pic, strrpos($pic, '.') + 1);

                                    $picture = md5(microtime()).'.'.$ext;
                                    $thumbnail = md5(microtime()).'.'.$ext;
                                    $bigthumb = md5(microtime()).'.'.$ext;
                                    Image::factory(DOCROOT.'assets/upload/jobs/'.$pic)
                                            ->save(DOCROOT.'assets/media/images/'.$picture);
                                    Image::factory(DOCROOT.'assets/upload/jobs/'.$pic)
                                            ->resize(648, 343, Image::INVERSE)
                                            ->crop(648, 343)
                                            ->save(DOCROOT.'assets/media/images/'.$bigthumb);
                                    Image::factory(DOCROOT.'assets/upload/jobs/'.$pic)
                                            ->resize(308, 163, Image::INVERSE)
                                            ->crop(308, 163)
                                            ->save(DOCROOT.'assets/media/images/'.$thumbnail);

                                    $_POST['picture'] = $picture;
                                    $_POST['big_thumb'] = $bigthumb;
                                    $_POST['thumbnail'] = $thumbnail;
                            }
                        }
                        else {
                            if(!empty($_FILES['picture']) && $_FILES['picture']['size'] > 0 && $_FILES['picture']['error'] == 0){
                                    $ext = substr($_FILES['picture']['name'], strrpos($_FILES['picture']['name'], '.') + 1);

                                    $picture = md5(microtime()).'.'.$ext;
                                    $thumbnail = md5(microtime()).'.'.$ext;
                                    $bigthumb = md5(microtime()).'.'.$ext;
                                    Image::factory($_FILES['picture']['tmp_name'])
                                            ->save(DOCROOT.'assets/media/images/'.$picture);
                                    Image::factory($_FILES['picture']['tmp_name'])
                                            ->resize(648, 343, Image::INVERSE)
                                            ->crop(648, 343)
                                            ->save(DOCROOT.'assets/media/images/'.$bigthumb);
                                    Image::factory($_FILES['picture']['tmp_name'])
                                            ->resize(308, 163, Image::INVERSE)
                                            ->crop(308, 163)
                                            ->save(DOCROOT.'assets/media/images/'.$thumbnail);

                                    $_POST['picture'] = $picture;
                                    $_POST['big_thumb'] = $bigthumb;
                                    $_POST['thumbnail'] = $thumbnail;
                            }
                            if(!empty($_FILES['video']) && $_FILES['video']['size'] > 0 && $_FILES['video']['error'] == 0){
                                    $ext = substr($_FILES['video']['name'], strrpos($_FILES['video']['name'], '.') + 1);
                                    
                                    $dir = 'assets/media/multi/';
                                    $video = md5(microtime()).'.'.$ext;
                                    Upload::save($_FILES['video'],$video,DOCROOT.$dir);
                                    $_POST['video'] = $dir.$video;
                                    
                                    
                            }	
                            if(!empty($_FILES['audio']) && $_FILES['audio']['size'] > 0 && $_FILES['audio']['error'] == 0){
                                    $ext = substr($_FILES['audio']['name'], strrpos($_FILES['audio']['name'], '.') + 1);
                                    $dir = 'assets/media/multi/';
                                    $audio = md5(microtime()).'.'.$ext;
                                    Upload::save($_FILES['audio'],$audio,DOCROOT.$dir);
                                    $_POST['audio'] = $dir.$audio;
                            }
                            $catRaw = explode('-',$_POST['category']);
                            $_POST['category'] = $this->years[(int)$catRaw[0]][(int)$catRaw[1]];
                            
                        }
                        if($_POST['shortlist'] != 1) {
                            $_POST['shortlist'] = 0;
                        }
                        else {
                            $_POST['year'] = 2016;
                            
                        }
                        foreach($_POST as $postvar) {
                            if($postvar == '') {
                                $postvar = null;
                            }
                        }
			
			$winner->values($_POST);
                        if($_POST['job'] == '') {
                            $winner->job = NULL;
                        }
			$winner->save();
                        if($_POST['shortlist'] != 1) {
                            $this->request->redirect('admin/winners/view/');
                        }
                        else {
                            $this->request->redirect('admin/winners/view/90');
                        }
			
        }
        else 
        {
            if($section_id == 90) {
                $shortlist = true;
            }
            else {
                $shortlist = false;
            }
            $awards = ORM::factory('award')->find_all()->as_array();
            $this->template->content =
                    View::factory('admin/winners/edit')
                        ->set('years',$this->years)
                        ->set('categories',['lulz'])
                        ->set('awards',$awards)
                        ->set('shortlist',$shortlist);
        }
    }
    public function action_edit($id)
    {
        error_reporting(E_ALL);
        ini_set('display_errors', 1);
            
        $awards = ORM::factory('award')->find_all()->as_array();
        $winner	= ORM::factory('winners',$id);
        if(!$winner->loaded()) $this->request->redirect('admin/winners/view');
        if($this->request->method() == Request::POST)
        {
                        if($_POST['job'] != '') {
                            $job = ORM::factory('inserts',$_POST['job']);
                            if(!$job->loaded()) $this->request->redirect('admin/winners/add');
                            $_POST['title'] = $job->title_lv;
                            $_POST['client'] = $job->client_lv;
                            $user = ORM::factory('user',$job->user_id);
                            if($user->private_name != '') {
                                $_POST['author'] = $user->private_name;
                            }
                            else {
                                $_POST['author'] = $user->legal_title;
                            }
                            $_POST['category'] = ORM::factory('categories',$job->main_category)->title;
                            $_POST['sub_category'] = ORM::factory('formats',$job->sub_category)->title;
                            
                            if($job->video != '') {
                                $_POST['video'] = 'assets/upload/jobs/'.$job->video;                 
                            }
                            if($job->audio != '') {
                                $_POST['audio'] = 'assets/upload/jobs/'.$job->audio;
                            }
                            if($job->image_web_1 != '') { $pic = $job->image_web_1; }
                                else if($job->image_web_2 != '') { $pic = $job->image_web_2; }
                                else if($job->image_web_3 != '') { $pic = $job->image_web_3; }
                                else if($job->image_web_4 != '') { $pic = $job->image_web_4; }
                                else if($job->image_web_5 != '') { $pic = $job->image_web_5; }
                                else if($job->image_web_6 != '') { $pic = $job->image_web_6; }
                                else { $pic = ''; }
                            if($pic != '') {
                                $ext = substr($pic, strrpos($pic, '.') + 1);

                                    $picture = md5(microtime()).'.'.$ext;
                                    $thumbnail = md5(microtime()).'.'.$ext;
                                    $bigthumb = md5(microtime()).'.'.$ext;
                                    Image::factory(DOCROOT.'assets/upload/jobs/'.$pic)
                                            ->save(DOCROOT.'assets/media/images/'.$picture);
                                    Image::factory(DOCROOT.'assets/upload/jobs/'.$pic)
                                            ->resize(648, 343, Image::INVERSE)
                                            ->crop(648, 343)
                                            ->save(DOCROOT.'assets/media/images/'.$bigthumb);
                                    Image::factory(DOCROOT.'assets/upload/jobs/'.$pic)
                                            ->resize(308, 163, Image::INVERSE)
                                            ->crop(308, 163)
                                            ->save(DOCROOT.'assets/media/images/'.$thumbnail);

                                    $_POST['picture'] = $picture;
                                    $_POST['big_thumb'] = $bigthumb;
                                    $_POST['thumbnail'] = $thumbnail;
                            }
                        }
                        else {
                        if(!empty($_FILES['picture']) && $_FILES['picture']['size'] > 0 && $_FILES['picture']['error'] == 0){
				$ext = substr($_FILES['picture']['name'], strrpos($_FILES['picture']['name'], '.') + 1);
				
				$picture = md5(microtime()).'.'.$ext;
                                $thumbnail = md5(microtime()).'.'.$ext;
                                $bigthumb = md5(microtime()).'.'.$ext;
                                // DELETE OLD
                                unlink(DOCROOT.'assets/media/images/'.$winner->picture);
                                unlink(DOCROOT.'assets/media/images/'.$winner->thumbnail);
                                unlink(DOCROOT.'assets/media/images/'.$winner->big_thumb);
                                
				Image::factory($_FILES['picture']['tmp_name'])
					->save(DOCROOT.'assets/media/images/'.$picture);
                                Image::factory($_FILES['picture']['tmp_name'])
                                        ->resize(648, 343, Image::INVERSE)
                                        ->crop(648, 343)
                                        ->save(DOCROOT.'assets/media/images/'.$bigthumb);
				Image::factory($_FILES['picture']['tmp_name'])
					->resize(308, 163, Image::INVERSE)
					->crop(308, 163)
					->save(DOCROOT.'assets/media/images/'.$thumbnail);
                                
				$_POST['picture'] = $picture;
                                $_POST['big_thumb'] = $bigthumb;
                                $_POST['thumbnail'] = $thumbnail;
			}
                        if(!empty($_FILES['video']) && $_FILES['video']['size'] > 0 && $_FILES['video']['error'] == 0){
				$ext = substr($_FILES['video']['name'], strrpos($_FILES['video']['name'], '.') + 1);
				$dir = 'assets/media/multi/';
                                $video = md5(microtime()).'.'.$ext;
                                // DELETE OLD
                                unlink(DOCROOT.'assets/media/multi/'.$winner->video);
				Upload::save($_FILES['video'],$video,DOCROOT.$dir);
                                $_POST['video'] = $dir.$video;
			}	
                        if(!empty($_FILES['audio']) && $_FILES['audio']['size'] > 0 && $_FILES['audio']['error'] == 0){
				$ext = substr($_FILES['audio']['name'], strrpos($_FILES['audio']['name'], '.') + 1);
				$dir = 'assets/media/multi/';
                                $audio = md5(microtime()).'.'.$ext;
                                // DELETE OLD
                                unlink(DOCROOT.'assets/media/multi/'.$winner->audio);
				Upload::save($_FILES['audio'],$audio,DOCROOT.$dir);
                                $_POST['audio'] = $dir.$audio;
			}
                            $catRaw = explode('-',$_POST['category']);
                            $_POST['category'] = $this->years[(int)$catRaw[0]][(int)$catRaw[1]];
                        }
                            $winner->values($_POST);
                            if($_POST['job'] == '') {
                                $winner->job = NULL;
                            }
                            $winner->save();
                            $this->request->redirect('admin/winners/view/');
                        }
        
        else
        {
            $this->template->content =
                View::factory('admin/winners/edit')
                    ->set('winner', $winner)
                    ->set('years',$this->years)
                    ->set('categories',['lulz'])
                    ->set('awards',$awards);            
        }
    }
    
    public function action_search()
    {
        if($this->request->method() == Request::POST && $this->request->is_ajax())
        {
            $this->auto_render = false;
            $string = $_POST['string'];
            if((string)(int)$string == $string) {
                $results = ORM::factory('inserts',(int)$string);
                if($results->loaded()) {
                    $response[] = ['id' => $results->id, 'name' => $results->title_lv];
                }
            }
            else {
                $results = ORM::factory('inserts')->where('title_lv','like','%'.$string.'%')->limit(10)->find_all()->as_array();
            
            $response = [];
            foreach($results as $res) {
                $response[] = ['id' => $res->id, 'name' => $res->title_lv];
            }
            }
            $this->response->body(json_encode($response));

        }
        else {
            die('Whoops...');
        }
    }
    public function action_delete($id)
    {
        $winner = ORM::factory('winners',$id);
	if(!$winner->loaded()) throw new Http_Exception_404('[CMS]: Item :id not found', array(':id' => $id));
	$winner->delete();
	$this->request->redirect('admin/winners/view/');
    }
}

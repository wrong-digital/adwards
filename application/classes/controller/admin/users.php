<?
class Controller_Admin_Users extends Controller_Admin {
	public function action_index(){
		$users = ORM::factory('user');
		$this->template->content = (string)View::factory('admin/users/index')
			->set('users', $users->find_all()->as_array());
	}
	
	public function action_edit($id){
		$user = ORM::factory('user', $id);
		if(!$user->loaded()) $this->request->redirect('admin/users');
		if($this->request->method() == Request::POST){
			if(!empty($_POST['email']) && !filter_var($_POST['email'], FILTER_VALIDATE_EMAIL)) die('<script type="text/javascript"> alert("Ievadītā e-pasta adrese nav derīga!"); parent.$("input[name=\'email\']").focus(); </script>');
			if(empty($_POST['password'])){
				unset($_POST['password']);
			}else{
				$_POST['password_salt'] = Model_User::password_salt();
				$_POST['password'] = Model_User::password_hash($_POST['password'], $_POST['password_salt']);
			}
			$user->values($_POST);
			$user->save();
			die('<script type="text/javascript"> parent.document.location.href = "'.URL::site('admin/users/edit/'.$user->id).'"; </script>');
		}
		$this->template->content = (string)View::factory('admin/users/edit')->set('user', $user->as_array());
	}
	
	public function action_add($id){
		if($this->request->method() == Request::POST){
			if(empty($_POST['username'])) die('<script type="text/javascript"> alert("Ievadi lietotājvārdu!"); parent.$("input[name=\'username\']").focus(); </script>');
			if(empty($_POST['password'])) die('<script type="text/javascript"> alert("Ievadi Paroli!"); parent.$("input[name=\'password\']").focus(); </script>');
			if(!empty($_POST['email']) && !filter_var($_POST['email'], FILTER_VALIDATE_EMAIL)) die('<script type="text/javascript"> alert("Ievadītā e-pasta adrese nav derīga!"); parent.$("input[name=\'email\']").focus(); </script>');
			$check = ORM::factory('user', array(
				'username' => $_POST['username']
			));
			if($check->loaded()) die('<script type="text/javascript"> alert("Šāds lietotājvārds jau ir reģistrēts!"); parent.$("#input[name=\'username\']").focus(); </script>');
			$user = new Model_User();
			$_POST['password_salt'] = Model_User::password_salt();
			$_POST['password'] = Model_User::password_hash($_POST['password'], $_POST['password_salt']);
			$user->values($_POST);
			$user->save();
			die('<script type="text/javascript"> parent.document.location.href = "'.URL::site('admin/users').'"; </script>');
		}
		$this->template->content = (string)View::factory('admin/users/edit')->set('user', false);
	}
	
	public function action_status($id){
		$user = ORM::factory('user', $id);
		if(!$user->loaded()) exit;
		$user->active = (int)Arr::get($_GET, 's');
		$user->save();
		exit;
	}
	
	public function action_delete($id){
		$user = ORM::factory('user', $id);
		if(!$user->loaded()) exit;
		$user->delete();
		$this->request->redirect('admin/users');
	}
}
?>
<?
class Controller_Admin_Categories extends Controller_Admin {
	public function action_view($section_id){
		$this->template->content = 
			View::factory('admin/categories/index')
				->set('section_id', $section_id)
				->set('categories', ORM::factory('categories')->where('section_id', '=', $section_id)->order_by('list_order', 'ASC')->find_all()->as_array());
	}
	
	public function action_add($id){
		if($this->request->method() == Request::POST){
			$next = ORM::factory('categories')->where('section_id', '=', $_POST['section_id'])->order_by('list_order', 'DESC')->limit(1)->find();
			$categories = new Model_Categories();
			$_POST['list_order'] = $next->list_order+1;
			
			$categories->values($_POST);
			$categories->save();
			$this->request->redirect('admin/categories/view/'.$_POST['section_id']);
		}
		
		$this->template->content = View::factory('admin/categories/edit')
			->set('categories', false)
			->set('section_id', $id);
	}
	
	public function action_edit($id){
		$categories = ORM::factory('categories', $id);
		$section_id = $categories->section_id;
		if(!$categories->loaded()) $this->request->redirect('admin/categories/view');
		if($this->request->method() == Request::POST){
			$categories->values($_POST);
			$categories->save();
			$this->request->redirect('admin/categories/view/'.$section_id);
		}
		
		$this->template->content = View::factory('admin/categories/edit')
			->set('categories', $categories->as_array());
	}
	
	public function action_delete($id){
		$categories = ORM::factory('categories', $id);
		$section_id = $categories->section_id;
		if(!$categories->loaded()) throw new Http_Exception_404('[CMS]: Item :id not found', array(':id' => $id));
		$categories->delete();
		$this->request->redirect('admin/categories/view/'.$section_id);
	}
	
	public function action_sort(){
		if(empty($_POST['data'])) exit;
		foreach($_POST['data'] as $key=>$id){
			$categories = ORM::factory('categories', $id);
			$categories->list_order = $key;
			$categories->save();
		}
		die;
	}
	
}
?>
<?
class Controller_Admin_Jobs extends Controller_Admin {
	public function action_view($section_id){
		$this->template->content = View::factory('admin/jobs/index')
			->set('section_id', $section_id)
			->set('jobs', ORM::factory('jobs')->where('section_id', '=', $section_id)->order_by('list_order', 'ASC')->find_all()->as_array());
	}
	
	public function action_add($id){
		if($this->request->method() == Request::POST){
			
			$next = ORM::factory('jobs')->where('section_id', '=', $_POST['section_id'])->order_by('list_order', 'DESC')->limit(1)->find();
			
			
			
			for($i=1;$i<=4;$i++){
				if(!empty($_FILES['image_'.$i]) && $_FILES['image_'.$i]['size'] > 0 && $_FILES['image_'.$i]['error'] == 0){
					$ext = substr($_FILES['image_'.$i]['name'], strrpos($_FILES['image_'.$i]['name'], '.') + 1);
					
					$image_144_126 = md5(microtime()).'.'.$ext;
					Image::factory($_FILES['image_'.$i]['tmp_name'])
						->resize(144, 126, Image::INVERSE)
						->crop(144, 126)
						->save(DOCROOT.'assets/media/images/'.$image_144_126);
					$_POST['image_'.$i.'_144_126'] = $image_144_126;
					
					$image_370_274 = md5(microtime()).'.'.$ext;
					Image::factory($_FILES['image_'.$i]['tmp_name'])
						->resize(370, 274, Image::INVERSE)
						->crop(370, 274)
						->save(DOCROOT.'assets/media/images/'.$image_370_274);
					$_POST['image_'.$i.'_370_274'] = $image_370_274;
					
					$image_750_555 = md5(microtime()).'.'.$ext;
					Image::factory($_FILES['image_'.$i]['tmp_name'])
						->resize(750, 555, Image::INVERSE)
						->crop(750, 555)
						->save(DOCROOT.'assets/media/images/'.$image_750_555);
					$_POST['image_'.$i.'_750_555'] = $image_750_555;
				}
			}
				
			
			$jobs = new Model_Jobs();
			$_POST['list_order'] = $next->list_order+1;
			
			$jobs->values($_POST);
			$jobs->save();
			$this->request->redirect('admin/jobs/view/'.$_POST['section_id']);
		}
		$this->template->content = View::factory('admin/jobs/edit')
			->set('jobs', false)
			->set('section_id', $id)
			->set('users', ORM::factory('user')->where('level', '>', 2)->order_by('name', 'ASC')->find_all()->as_array())
			->set('categories', ORM::factory('categories')->where('section_id', '=', 14)->order_by('list_order', 'ASC')->find_all()->as_array());
	}
	
	public function action_edit($id){
		$jobs = ORM::factory('jobs', $id);
		if(!$jobs->loaded()) $this->request->redirect('admin/jobs/view');
		if($this->request->method() == Request::POST){
			
			for($i=1;$i<=4;$i++){
				if(!empty($_FILES['image_'.$i]) && $_FILES['image_'.$i]['size'] > 0 && $_FILES['image_'.$i]['error'] == 0){
					
					list($width, $height, $type, $attr) = getimagesize($_FILES['image_'.$i]['tmp_name']);
					
					$ext = substr($_FILES['image_'.$i]['name'], strrpos($_FILES['image_'.$i]['name'], '.') + 1);
					
					$image_144_126 = md5(microtime()).'.'.$ext;
					Image::factory($_FILES['image_'.$i]['tmp_name'])
						->resize(144, 126, Image::INVERSE)
						->crop(144, 126)
						->save(DOCROOT.'assets/media/images/'.$image_144_126);
					$_POST['image_'.$i.'_144_126'] = $image_144_126;
					
					$image_370_274 = md5(microtime()).'.'.$ext;
					Image::factory($_FILES['image_'.$i]['tmp_name'])
						->resize(370, 274, Image::INVERSE)
						->crop(370, 274)
						->save(DOCROOT.'assets/media/images/'.$image_370_274);
					$_POST['image_'.$i.'_370_274'] = $image_370_274;
					
					$image_750_555 = md5(microtime()).'.'.$ext;
					if($width > $height){
						Image::factory($_FILES['image_'.$i]['tmp_name'])
							->resize(750, 555, Image::INVERSE)
							->crop(750, 555)
							->save(DOCROOT.'assets/media/images/'.$image_750_555);
					}else{
						Image::factory($_FILES['image_'.$i]['tmp_name'])
							->resize(447, 640, Image::INVERSE)
							->crop(447, 640)
							->save(DOCROOT.'assets/media/images/'.$image_750_555);
					}
					$_POST['image_'.$i.'_750_555'] = $image_750_555;
				}
			}
			
			$jobs->values($_POST);
			$jobs->save();
			$this->request->redirect('admin/jobs/view/'.$_POST['section_id']);
		}
		$this->template->content = View::factory('admin/jobs/edit')
			->set('jobs', $jobs->as_array())
			->set('users', ORM::factory('user')->where('level', '>', 2)->order_by('name', 'ASC')->find_all()->as_array())
			->set('categories', ORM::factory('categories')->where('section_id', '=', 14)->order_by('list_order', 'ASC')->find_all()->as_array());
	}
	
	public function action_delete($id){
		$jobs = ORM::factory('jobs', $id);
		$section_id = $jobs->section_id;
		if(!$jobs->loaded()) throw new Http_Exception_404('[CMS]: Item :id not found', array(':id' => $id));
		$jobs->delete();
		$this->request->redirect('admin/jobs/view/'.$section_id);
	}
	
	
	public function action_deleteimg($id){
		$jobs = ORM::factory('jobs', $id);
		if(!$jobs->loaded()) $this->request->redirect('admin/jobs/view');
		
		unlink(DOCROOT.'assets/media/images/'.$jobs->image_144_126);
		unlink(DOCROOT.'assets/media/images/'.$jobs->image_527_235);
		
		$data['image_144_126'] = '';
		$data['image_527_235'] = '';
		$jobs->values($data);
		$jobs->save();
		$this->request->redirect('admin/jobs/edit/'.$id);
	}
	
}
?>
<?
class Controller_Admin_Programm extends Controller_Admin {
	public function action_view($section_id){
		$this->template->content = 
			View::factory('admin/programm/index')
				->set('section_id', $section_id)
				->set('programm', ORM::factory('programm')->where('section_id', '=', $section_id)->order_by('list_order', 'ASC')->find_all()->as_array());
	}
	
	public function action_add($id){
		if($this->request->method() == Request::POST){
			
			if(!empty($_FILES['image']) && $_FILES['image']['size'] > 0 && $_FILES['image']['error'] == 0){
				$ext = substr($_FILES['image']['name'], strrpos($_FILES['image']['name'], '.') + 1);
				
				$image_100_100	= md5(microtime()).'.'.$ext;
				
				Image::factory($_FILES['image']['tmp_name'])
					->resize(138, 138, Image::INVERSE)
					->crop(138, 138)
					->save(DOCROOT.'assets/media/images/'.$image_100_100);
				$_POST['img']	= $image_100_100;
			}			
			
			$next = ORM::factory('programm')->where('section_id', '=', $_POST['section_id'])->order_by('list_order', 'DESC')->limit(1)->find();
			$programm		= new Model_Programm();
			$_POST['list_order']	= $next->list_order+1;

			//$_POST['title']	= strtoupper($_POST['title'], 'UTF-8');
			//$_POST['subtitle']	= strtoupper($_POST['subtitle'], 'UTF-8');
			if($_POST['end_date'] == '' || $_POST['end_date'] == '0000-00-00')
                        {
                            $_POST['end_date'] = null;
                        }
			$programm->values($_POST);
			$programm->save();
			$this->request->redirect('admin/programm/view/'.$_POST['section_id']);
		}
		
		$this->template->content = View::factory('admin/programm/edit')
			->set('programm', false)
			->set('section_id', $id);
	}
	
	public function action_edit($id){
		$programm = ORM::factory('programm', $id);
		$section_id = $programm->section_id;
		if(!$programm->loaded()) $this->request->redirect('admin/programm/view');
		if($this->request->method() == Request::POST){

			if(!empty($_FILES['image']) && $_FILES['image']['size'] > 0 && $_FILES['image']['error'] == 0){
				$ext = substr($_FILES['image']['name'], strrpos($_FILES['image']['name'], '.') + 1);
				
				$image_100_100 = md5(microtime()).'.'.$ext;
				
				Image::factory($_FILES['image']['tmp_name'])
					->resize(138, 138, Image::INVERSE)
					->crop(138, 138)
					->save(DOCROOT.'assets/media/images/'.$image_100_100);
				$_POST['img'] = $image_100_100;
			}			
			
			
			//$_POST['title']	= strtoupper($_POST['title'], 'UTF-8');
			//$_POST['subtitle'] = strtoupper($_POST['subtitle'], 'UTF-8');
			if($_POST['end_date'] == '' || $_POST['end_date'] == '0000-00-00')
                        {
                            $_POST['end_date'] = null;
                        }
			$programm->values($_POST);
			$programm->save();
			$this->request->redirect('admin/programm/view/'.$section_id);
		}
		
		$this->template->content = View::factory('admin/programm/edit')
			->set('programm', $programm->as_array());
	}
	
	public function action_delete($id){
		$programm = ORM::factory('programm', $id);
		$section_id = $programm->section_id;
		if(!$programm->loaded()) throw new Http_Exception_404('[CMS]: Item :id not found', array(':id' => $id));
		$programm->delete();
		$this->request->redirect('admin/programm/view/'.$section_id);
	}
	
	public function action_sort(){
		if(empty($_POST['data'])) exit;
		foreach($_POST['data'] as $key=>$id){
			$programm = ORM::factory('programm', $id);
			$programm->list_order = $key;
			$programm->save();
		}
		die;
	}
	
	public function action_deleteimg($id){
		$programm = ORM::factory('programm', $id);
		if(!$programm->loaded()) $this->request->redirect('admin/programm/view');
		
		unlink(DOCROOT.'assets/media/images/'.$programm->img);
		
		$data['img'] = '';
		$programm->values($data);
		$programm->save();
		$this->request->redirect('admin/programm/edit/'.$id);
	}	
	
	/*31.mart.14*/
	/*
	public function action_up($id){
		$crr				= ORM::factory('programm')->where('list_order', '=', $id)->find();
		$section_id			= $crr->section_id;
		
		$prev				= ORM::factory('programm')->where('list_order', '=', $crr->id - 1)->find();
		if($prev->loaded()){
			$id0			= $crr->list_order;
			$id1			= $prev->list_order;
			
			$crr->list_order		= -1;
			$prev->list_order		= -2;
			$crr->save();
			$prev->save();
			
			$crr->list_order		= $id1;
			$prev->list_order		= $id0;
			
			$crr->save();
			$prev->save();
		}
		
		$this->request->redirect('admin/programm/view/'.$section_id);
	}
	
	public function action_down($id){
		$crr				= ORM::factory('programm')->where('list_order', '=', $id)->find();
		$section_id			= $crr->section_id;
		
		$prev				= ORM::factory('programm')->where('list_order', '=', $crr->id + 1)->find();
		if($prev->loaded()){
			$id0			= $crr->list_order;
			$id1			= $prev->list_order;
			
			$crr->list_order		= -1;
			$prev->list_order		= -2;
			$crr->save();
			$prev->save();
			
			$crr->list_order		= $id1;
			$prev->list_order		= $id0;
			
			$crr->save();
			$prev->save();
		}
		
		$this->request->redirect('admin/programm/view/'.$section_id);
	}	
	*/
}
?>
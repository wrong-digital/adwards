<?
class Controller_Admin_Programmitems extends Controller_Admin {
	public function action_view($section_id){
		$this->template->content = 
			View::factory('admin/programmitems/index')
				->set('section_id', $section_id)
				->set('programmitems', ORM::factory('programmitems')->where('section_id', '=', $section_id)->order_by('list_order', 'ASC')->find_all()->as_array());
	}
	
	public function action_add($id){
		if($this->request->method() == Request::POST){
			$next = ORM::factory('programmitems')->where('section_id', '=', $_POST['section_id'])->order_by('list_order', 'DESC')->limit(1)->find();
			$programmitems = new Model_Programmitems();
			$_POST['list_order'] = $next->list_order+1;
			
			$programmitems->values($_POST);
			$programmitems->save();
			$this->request->redirect('admin/programmitems/view/'.$_POST['section_id']);
		}
		
		$this->template->content = View::factory('admin/programmitems/edit')
			->set('news', ORM::factory('news')->order_by('date', 'DESC')->find_all()->as_array())
			->set('programmitems', false)
			->set('section_id', $id);
	}
	
	public function action_edit($id){
		$programmitems = ORM::factory('programmitems', $id);
		if(!$programmitems->loaded()) $this->request->redirect('admin/programmitems/view');
		
		if($this->request->method() == Request::POST){
			$programmitems->values($_POST);
			$programmitems->save();
			$this->request->redirect('admin/programmitems/view/'.$_POST['section_id']);
		}
		
		$this->template->content = View::factory('admin/programmitems/edit')
			->set('programmitems', $programmitems->as_array())
			->set('news', ORM::factory('news')->order_by('date', 'DESC')->find_all()->as_array());
	}
	
	public function action_delete($id){
		$programmitems = ORM::factory('programmitems', $id);
		$section_id = $programmitems->section_id;
		if(!$programmitems->loaded()) throw new Http_Exception_404('[CMS]: Item :id not found', array(':id' => $id));
		$programmitems->delete();
		$this->request->redirect('admin/programmitems/view/'.$section_id);
	}
	
	public function action_sort(){
		if(empty($_POST['data'])) exit;
		foreach($_POST['data'] as $key=>$id){
			$programmitems = ORM::factory('programmitems', $id);
			$programmitems->list_order = $key;
			$programmitems->save();
		}
		die;
	}
	
}
?>
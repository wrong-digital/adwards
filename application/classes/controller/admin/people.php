<?
class Controller_Admin_People extends Controller_Admin {
	public function action_view($section_id){
		$section = ORM::factory('section', $section_id);

		$this->template->content = 
			View::factory('admin/people/index')
				->set('section_id', $section_id)
				->set('section_title', $section->title)
				->set('people', ORM::factory('people')->where('section_id', '=', $section_id)->order_by('list_order', 'ASC')->find_all()->as_array());
	}
	
	public function action_add($id){
		if($this->request->method() == Request::POST){
			$next = ORM::factory('people')->where('section_id', '=', $_POST['section_id'])->order_by('list_order', 'DESC')->limit(1)->find();
			
			if(!empty($_FILES['image']) && $_FILES['image']['size'] > 0 && $_FILES['image']['error'] == 0){
				$ext = substr($_FILES['image']['name'], strrpos($_FILES['image']['name'], '.') + 1);
				$image_289_138 = md5(microtime()).'.'.$ext;
				Image::factory($_FILES['image']['tmp_name'])
					->resize(648, 444, Image::INVERSE)
					->crop(648, 444)
					->save(DOCROOT.'assets/media/images/'.$image_289_138);
				$_POST['image_289_138'] = $image_289_138;
				
				$image_214_299 = md5(microtime()).'.'.$ext;
				Image::factory($_FILES['image']['tmp_name'])
					->resize(223, 223, Image::INVERSE)
					->crop(223, 223)
					->save(DOCROOT.'assets/media/images/'.$image_214_299);
				$_POST['image_214_299'] = $image_214_299;
			}
			
			$people = new Model_People();
			$_POST['list_order'] = $next->list_order+1;
			
			$people->values($_POST);
			$people->save();
			$this->request->redirect('admin/people/view/'.$_POST['section_id']);
		}

		$section = ORM::factory('section', $people->section_id);
		
		$this->template->content = View::factory('admin/people/edit')
			->set('people', false)
			->set('section_title', $section->title)
			->set('section_id', $id);
	}
	
	public function action_edit($id){
		$people = ORM::factory('people', $id);
		if(!$people->loaded()) $this->request->redirect('admin/people/view');
		
		if($this->request->method() == Request::POST){
			
			if(!empty($_FILES['image']) && $_FILES['image']['size'] > 0 && $_FILES['image']['error'] == 0){
				$ext = substr($_FILES['image']['name'], strrpos($_FILES['image']['name'], '.') + 1);
				$image_289_138 = md5(microtime()).'.'.$ext;
				Image::factory($_FILES['image']['tmp_name'])
					->resize(685, 470, Image::INVERSE)
					->crop(685, 470)
					->save(DOCROOT.'assets/media/images/'.$image_289_138);
				$_POST['image_289_138'] = $image_289_138;
				
				$image_214_299 = md5(microtime()).'.'.$ext;
				Image::factory($_FILES['image']['tmp_name'])
					->resize(225, 225, Image::INVERSE)
					->crop(225, 225)
					->save(DOCROOT.'assets/media/images/'.$image_214_299);
				$_POST['image_214_299'] = $image_214_299;
			}
			
			$people->values($_POST);
			$people->save();
			$this->request->redirect('admin/people/view/'.$_POST['section_id']);
		}

		$section = ORM::factory('section', $people->section_id);

		$this->template->content = View::factory('admin/people/edit')
			->set('section_title', $section->title)
			->set('people', $people->as_array());
	}
	
	public function action_delete($id){
		$people = ORM::factory('people', $id);
		$section_id = $people->section_id;
		if(!$people->loaded()) throw new Http_Exception_404('[CMS]: Item :id not found', array(':id' => $id));
		$people->delete();
		$this->request->redirect('admin/people/view/'.$section_id);
	}
	
	public function action_sort(){
		if(empty($_POST['data'])) exit;
		foreach($_POST['data'] as $key=>$id){
			$people = ORM::factory('people', $id);
			$people->list_order = $key;
			$people->save();
		}
		die;
	}
	
}
?>
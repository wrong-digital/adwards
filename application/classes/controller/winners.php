<?php
class Controller_Winners extends Controller_Template
{
    public $template = 'global/template';
    
    private $categories = [
            2006 => [
                'Ideju kategorija - TV',
                'Ideju kategorija - Drukātā reklāma',
                'Ideju kategorija - Radioreklāma',
                'Ideju kategorija - Tiešais mārketings',
                'Ideju kategorija - Sociālā reklāma',
                'Meistarības kategorija - TV',
                'Meistarības kategorija - Grafiskais dizains',
                'Meistarības kategorija - Industriālais dizains',
                'Meistarības kategorija - Fotogrāfija',
                'Meistarības kategorija - Animācija',
                'Meistarības kategorija - Reklāmas teksts'
              ],
            2007 => [
                'Industriālais dizains',
                'Interaktīvais dizains',
                'Grafiskais dizains',
                'Sociālā reklāma',
                'Drukātā reklāma',
                'Integrētās kampaņas',
                'TV',
                'Vides objekti'
              ],
            2008 => [
                'Speciālās kategorijas',
                'Žūrijas īpašie apbalvojumi',
                'Integrētā kampaņa',
                'TV',
                'Drukātā reklāma',
                'Tiešais mārketings',
                'Sociālā reklāma',
                'Grafiskais dizains',
                'Iepakojuma dizains',
                'Industriālais dizains',
                'Plakāti / vides reklāma',
                'Reklāmas teksts'
              ],
            2009 => [
                'Integrētā kampaņa',
                'Tiešais mārketings',
                'Sociālā reklāma',
                'Grafiskais dizains',
                'Animācija',
                'Interaktīvais dizains',
                'Industriālais dizains',
                'TV'
              ],
            2010 => [
                'Industriālais dizains',
                'Grafiskais dizains',
                'Fotogrāfija',
                'Interaktīvais dizains',
                'Radio reklāma',
                'Drukātā reklāma',
                'Integrētā kampaņa',
                'Televīzija',
                'Speciālās balvas'
              ],
            2011 => [
                'TV',
                'Grafiskais dizains',
                'Digitālie risinājumi',
                'Speciālās balvas',
                'Darba uzdevumi'
              ],
            2012 => [
                'Integrētā kampaņa',
                'TV reklāmas',
                'Valsts pakalpojumi un labdarība',
                'Reklāmas plakāti',
                'Reklāma laikrakstos, žurnālos un informatīvos tirdzniecības materiālos',
                'Tīmekļa vietnes un mikrovietnes',
                'Reklāmkarogi, Rich media, animētas apsveikuma kartītes',
                'Social Network Solutions',
                'Grafiskais dizains',
                'Redakcijas darbs/grāmatas/korporatīvā izdevējdarbība/katalogi',
                'Korporatīvā identitāte/korporatīvais dizains/zīmolvedība',
                'Ilustrācija un fotogrāfija',
                'Industriālais dizains',
                'Animācija',
                'Ambient Media',
                'Pasākumi (korporatīvie, sabiedriskie, patērētāju, sociālie/kultūras)',
                'PR Kampaņas',
                'Limbo',
                'Darba uzdevumi'
              ],
            2013 => [
                'Integrētā kampaņa',
                'TV un Radio',
                'Interaktīvie un digitālie risinājumi',
                'Reklāmas spēles',
                'Citi digitālie risinājumi',
                'Netradicionālā komunikācija',
                'Pasākumi',
                'Ambient',
                'Sabiedriskās attiecības',
                'Vides reklāma',
                'Grafiskais dizains',
                'Vides, sabiedrisko telpu un komerciālo telpu dizains',
                'Ilustrācija',
                'Grāmatas, katalogi un kalendāri',
                'Iepakojuma dizains',
                'Logo/vizuālā identitāte',
                'Produktu dizains',
                'Radošie uzdevumi'
              ],
            2014 => [
                'Integrētā kampaņa',
                'TV un Radio',
                'Grafiskie video risinājumi',
                'Interaktīvie un digitālie risinājumi',
                'Tīmekļa lapa',
                'Citi digitālie risinājumi',
                'Mobilās aplikācijas',
                'Digitālais dizains',
                'Netradicionālā komunikācija',
                'Branded content and entertainment',
                'Print Art un dizains',
                'Izdevniecību nozare',
                'Kalendāri',
                'Vizuālā identitāte',
                'Komerciālais iepakojums',
                'Fotogrāfija',
                'Produktu dizains'
              ],
            2015 => [],
            2016 => [
                'Integrētā kampaņa',
                'TV un Radio',
                'Grafiskie video risinājumi',
                'Interaktīvie un digitālie risinājumi',
                'Tīmekļa lapa',
                'Citi digitālie risinājumi',
                'Mobilās aplikācijas',
                'Digitālais dizains',
                'Netradicionālā komunikācija',
                'Branded content and entertainment',
                'Print Art un dizains',
                'Izdevniecību nozare',
                'Kalendāri',
                'Vizuālā identitāte',
                'Komerciālais iepakojums',
                'Fotogrāfija',
                'Zīmola stāsts',
                'Vides reklāma',
                'Sabiedriskās attiecības',
                'Pasākumi',
                'Produktu dizains'
                ]
            ];
    
    public function action_section()
    {
        if(Request::initial()->method() == Request::GET && Request::initial()->is_ajax()){
            $id = $_GET['id'];
            if(isset($_GET['all']) && $_GET['all'] = true) {
                $all = true;
            }
            else {
                $all = false;
            }
            $winner = ORM::factory('winners',$id);
            if($winner->loaded()) {
                if($winner->video != null) {
                    $ext = substr($winner->video, strrpos($winner->video, '.') + 1);
                    $media = ['type' => 'video','url' => $winner->video, 'ext' => $ext];
                }
                else if($winner->audio != null) {
                    $ext = substr($winner->audio, strrpos($winner->audio, '.') + 1);
                    $media = ['type' => 'audio','url' => $winner->audio, 'ext' => $ext];
                }
                else if($winner->picture != null){
                    $media = ['type' => 'picture','url' => $winner->picture];
                }
                else {
                    $media = ['type' => 'none'];
                }
                if($all) {
                    $itm = ORM::factory('inserts',$winner->job);
                    $allmedia = array(
                    	'thumbs'		=> array($itm->image_1_284_150, $itm->image_2_284_150, $itm->image_3_284_150, $itm->image_4_284_150, $itm->image_5_284_150, $itm->image_6_284_150),
			'pictures'		=> array($itm->image_1_668_376, $itm->image_2_668_376, $itm->image_3_668_376, $itm->image_4_668_376, $itm->image_5_668_376, $itm->image_6_668_376),
                        'video'             => $itm->video,
                        'video_2'           => $itm->video_2,
                        'video_3'           => $itm->video_3,
                        'video_4'           => $itm->video_4,
			'audio'	=> $itm->audio
                    );
                    $media['allmedia'] = $allmedia;
                }
                $media['title'] = $winner->title;
                $media['client'] = $winner->client;
                $media['author'] = $winner->author;
            }
            else {
                $media = ['type' => 'failed'];
            }
            header('Content-Type: application/json');
            die(json_encode($media));
        }


        if($this->section->id == 90) {  
            // IF SHORTLIST - used 'winners' module
            $categories = [];
            $cats = ORM::factory('categories')->find_all()->as_array();
            foreach($cats as $cat) {
                $winners = ORM::factory('winners')->where('year','=',2015)->where('category','=',$cat->title)->where('shortlist','=',1)->order_by('sub_category','DESC')->order_by('id','ASC')->find_all();
                $categories[] = ['name' => $cat->title, 'winners' => $winners];
            }
        
            $this->template->content = View::factory('winners/shortlist')
                    ->set('categories',$categories)
                    ->set('year',2015);
            $this->template->title = $this->section->title;
        }
        else {
            $this->request->redirect('lv/uzvaretaji/2016.html');
        }


    }
    
    public function action_item()
    {
        $years = [2016,2015,2014,2013,2012,2011,2010,2009,2008,2007,2006];
        
        $year = (int)$this->request->param('id');
        

        /*
        if($year == 1337001) {
            error_reporting(E_ALL);
ini_set('display_errors', 1);
            $inserts = ORM::factory('inserts')->find_all()->as_array();
            //for($ii = 15; $ii < 800; $ii++){
            foreach($inserts as $i) {
			//$i = $inserts[$ii];
                        if($i->id == 730) {
			echo ("start: id[".$i->id."]");
			
                        $vidname = substr($i->video, 0, -4);
			if($vidname != ""){
				$i->video_flv	= $vidname.".flv";
				$i->save();
			}
                        
			echo (DOCROOT.'assets/upload/jobs/'.$i->image_web_1);
			
			if($i->image_web_1 != '' && file_exists(DOCROOT.'assets/upload/jobs/'.$i->image_web_1) ){
				echo "!!";
				
				$ext		= substr($i->image_web_1, strrpos($i->image_web_1, '.') + 1);
				$image_284_150	= md5(microtime()).'.'.$ext;
				Image::factory(DOCROOT.'assets/upload/jobs/'.$i->image_web_1)
					->resize(308, 163, Image::INVERSE)
					->crop(308, 163)
					->save(DOCROOT.'assets/upload/jobs/resize/img/'.$image_284_150);
				
				
				$image_668_376	= md5(microtime()).'.'.$ext;
				Image::factory(DOCROOT.'assets/upload/jobs/'.$i->image_web_1)
					->resize(NULL, 428)
					//->crop(818, 428)
					->save(DOCROOT.'assets/upload/jobs/resize/img/'.$image_668_376);
				
				$i->image_1_284_150 = $image_284_150;
				$i->image_1_668_376 = $image_668_376;
				$i->save();
				echo "save1/";
			}
			
			if($i->image_web_2 != '' && file_exists(DOCROOT.'assets/upload/jobs/'.$i->image_web_2)){
				$ext		= substr($i->image_web_2, strrpos($i->image_web_2, '.') + 1);
				
				$image_284_150	= md5(microtime()).'.'.$ext;
				Image::factory(DOCROOT.'assets/upload/jobs/'.$i->image_web_2)
					->resize(308, 163, Image::INVERSE)
					->crop(308, 163)
					->save(DOCROOT.'assets/upload/jobs/resize/img/'.$image_284_150);
					
				$image_668_376	= md5(microtime()).'.'.$ext;
				Image::factory(DOCROOT.'assets/upload/jobs/'.$i->image_web_2)
					->resize(NULL, 428)
					//->crop(818, 428)
					->save(DOCROOT.'assets/upload/jobs/resize/img/'.$image_668_376);
										
				$i->image_2_284_150 = $image_284_150;
				$i->image_2_668_376 = $image_668_376;
				$i->save();
				echo "save2/";
			}
			
			if($i->image_web_3 != '' && file_exists(DOCROOT.'assets/upload/jobs/'.$i->image_web_3)){
				$ext		= substr($i->image_web_3, strrpos($i->image_web_3, '.') + 1);
				
				$image_284_150	= md5(microtime()).'.'.$ext;
				Image::factory(DOCROOT.'assets/upload/jobs/'.$i->image_web_3)
					->resize(308, 163, Image::INVERSE)
					->crop(308, 163)
					->save(DOCROOT.'assets/upload/jobs/resize/img/'.$image_284_150);
					
				$image_668_376	= md5(microtime()).'.'.$ext;
				Image::factory(DOCROOT.'assets/upload/jobs/'.$i->image_web_3)
					->resize(NULL, 428)
					//->crop(818, 428)
					->save(DOCROOT.'assets/upload/jobs/resize/img/'.$image_668_376);
										
				$i->image_3_284_150 = $image_284_150;
				$i->image_3_668_376 = $image_668_376;
				$i->save();
				echo "save3/";
			}
			
			if($i->image_web_4 != '' && file_exists(DOCROOT.'assets/upload/jobs/'.$i->image_web_4)){
				$ext		= substr($i->image_web_4, strrpos($i->image_web_4, '.') + 1);
				
				$image_284_150	= md5(microtime()).'.'.$ext;
				Image::factory(DOCROOT.'assets/upload/jobs/'.$i->image_web_4)
					->resize(308, 163, Image::INVERSE)
					->crop(308, 163)
					->save(DOCROOT.'assets/upload/jobs/resize/img/'.$image_284_150);
					
				$image_668_376	= md5(microtime()).'.'.$ext;
				Image::factory(DOCROOT.'assets/upload/jobs/'.$i->image_web_4)
					->resize(NULL, 428)
					//->crop(818, 428)
					->save(DOCROOT.'assets/upload/jobs/resize/img/'.$image_668_376);
										
				$i->image_4_284_150 = $image_284_150;
				$i->image_4_668_376 = $image_668_376;
				$i->save();
				echo "save4/";
			}
			
			if($i->image_web_5 != '' && file_exists(DOCROOT.'assets/upload/jobs/'.$i->image_web_5)){
				$ext		= substr($i->image_web_5, strrpos($i->image_web_5, '.') + 1);
				
				$image_284_150	= md5(microtime()).'.'.$ext;
				Image::factory(DOCROOT.'assets/upload/jobs/'.$i->image_web_5)
					->resize(308, 163, Image::INVERSE)
					->crop(308, 163)
					->save(DOCROOT.'assets/upload/jobs/resize/img/'.$image_284_150);
					
				$image_668_376	= md5(microtime()).'.'.$ext;
				Image::factory(DOCROOT.'assets/upload/jobs/'.$i->image_web_5)
					->resize(NULL, 428)
					//->crop(818, 428)
					->save(DOCROOT.'assets/upload/jobs/resize/img/'.$image_668_376);
										
				$i->image_5_284_150 = $image_284_150;
				$i->image_5_668_376 = $image_668_376;
				$i->save();
				echo "save5/";
			}
			
			if($i->image_web_6 != '' && file_exists(DOCROOT.'assets/upload/jobs/'.$i->image_web_6)){
				$ext		= substr($i->image_web_6, strrpos($i->image_web_6, '.') + 1);
				
				$image_284_150	= md5(microtime()).'.'.$ext;
				Image::factory(DOCROOT.'assets/upload/jobs/'.$i->image_web_6)
					->resize(308, 163, Image::INVERSE)
					->crop(308, 163)
					->save(DOCROOT.'assets/upload/jobs/resize/img/'.$image_284_150);
					
				$image_668_376	= md5(microtime()).'.'.$ext;
				Image::factory(DOCROOT.'assets/upload/jobs/'.$i->image_web_6)
					->resize(NULL, 428)
					//->crop(818, 428)
					->save(DOCROOT.'assets/upload/jobs/resize/img/'.$image_668_376);
										
				$i->image_6_284_150 = $image_284_150;
				$i->image_6_668_376 = $image_668_376;
				$i->save();
				echo "save6";
			}
			
			//sleep(1);
			echo (">done[".$ii."]>".($i->id)."<br>");
		}
            }
		die(".");
        }
         
        
        if($year == 11001100) {
            error_reporting(E_ALL);
            ini_set('display_errors', 1);
            $inserts = ORM::factory('inserts')->find_all()->as_array();
            $sourcebase = $_SERVER['DOCUMENT_ROOT'].'/assets/upload/jobs/';
            $filebase = $_SERVER['DOCUMENT_ROOT'].'/assets/upload/export/';
            foreach($inserts as $i) {
                if($i->id == 730) { 
                $category = ORM::factory('categories',$i->main_category);
                $path = $filebase . $category->title . '/' . $i->code . '_' . $i->id . '/'; //
                echo $path;
                if (!file_exists($path)) {
                    mkdir($path, 0777, true);
                }
                
                    if($i->zip != '') {
                        copy($sourcebase . $i->zip, $path . $i->zip);
                    }
                    if($i->video != '') {
                    if(!copy($sourcebase . $i->video, $path . $i->video)) echo 'fail';
                    }
                    if($i->video_2 != '') {
                    copy($sourcebase . $i->video_2, $path . $i->video_2);
                    }
                    if($i->video_3 != '') {
                    copy($sourcebase . $i->video_3, $path . $i->video_3);
                    }
                    if($i->video_4 != '') {
                    copy($sourcebase . $i->video_4, $path . $i->video_4);
                    }
                    if($i->audio != '') {
                    copy($sourcebase . $i->audio, $path . $i->audio);
                    }
                    if($i->image_book_1 != '') {
                    copy($sourcebase . $i->image_book_1, $path . $i->image_book_1);
                    }
                    if($i->image_book_2 != '') {
                    copy($sourcebase . $i->image_book_2, $path . $i->image_book_2);
                    }
                    if($i->image_book_3 != '') {
                    copy($sourcebase . $i->image_book_3, $path . $i->image_book_3);
                    }
                    if($i->image_book_4 != '') {
                    copy($sourcebase . $i->image_book_4, $path . $i->image_book_4);
                    }
                    if($i->image_book_5 != '') {
                    copy($sourcebase . $i->image_book_5, $path . $i->image_book_5);
                    }
                    if($i->image_book_6 != '') {
                    copy($sourcebase . $i->image_book_6, $path . $i->image_book_6);
                    }
                    if($i->image_web_1 != '') {  
                    copy($sourcebase . $i->image_web_1, $path . $i->image_web_1);
                    }
                    if($i->image_web_2 != '') {
                    copy($sourcebase . $i->image_web_2, $path . $i->image_web_2);
                    }
                    if($i->image_web_3 != '') {
                    copy($sourcebase . $i->image_web_3, $path . $i->image_web_3);
                    }
                    if($i->image_web_4 != '') {
                    copy($sourcebase . $i->image_web_4, $path . $i->image_web_4);
                    }
                    if($i->image_web_5 != '') {
                    copy($sourcebase . $i->image_web_5, $path . $i->image_web_5);
                    }
                    if($i->image_web_6 != '') {
                    copy($sourcebase . $i->image_web_6, $path . $i->image_web_6);
                    }
                }
            }
        }
        
        if($year == 20482048) {
            error_reporting(E_ALL);
            ini_set('display_errors', 1);
            $inserts = ORM::factory('inserts')->find_all()->as_array();
            $sourcebase = $_SERVER['DOCUMENT_ROOT'].'/assets/upload/jobs/';
            $filebase = $_SERVER['DOCUMENT_ROOT'].'/assets/upload/export/all_videos/';
            foreach($inserts as $i) {
                if(true) { 
                $path = $filebase; //
                echo $path;
                if (!file_exists($path)) {
                    mkdir($path, 0777, true);
                }

                    if($i->video != '') {
                    if(!copy($sourcebase . $i->video, $path . $i->code . '_' . $i->id . '_' . $i->video)) echo 'fail';
                    }
                    if($i->video_2 != '') {
                    copy($sourcebase . $i->video_2, $path . $i->code . '_' . $i->id . '_' . $i->video_2);
                    }
                    if($i->video_3 != '') {
                    copy($sourcebase . $i->video_3, $path . $i->code . '_' . $i->id . '_' . $i->video_3);
                    }
                    if($i->video_4 != '') {
                    copy($sourcebase . $i->video_4, $path . $i->code . '_' . $i->id . '_' . $i->video_4);
                    }
                }
            }
        }
        */

        
        if(!in_array($year,$years)) {
            $year = 2016;
        }
        $spec = ORM::factory('winners')->where('year','=',$year)->where('award','IN',DB::expr('(SELECT id FROM awards WHERE special=1)'))->find_all();



        $categories = [];


        if($year >= 2015) {
            $cats = DB::select('category')->distinct(TRUE)->from('winners')->execute();
            foreach($cats as $cat) {
                $winners = ORM::factory('winners')->where('year','=',$year)->where('category','=',$cat['category'])->where('award','IN',DB::expr('(SELECT id FROM awards WHERE special=0)'))->order_by('award','DESC')->find_all();
                $categories[] = ['name' => $cat['category'], 'winners' => $winners];
            }
        } else {
            foreach($this->categories[$year] as $cat) {
                $winners = ORM::factory('winners')->where('year','=',$year)->where('category','=',$cat)->where('award','IN',DB::expr('(SELECT id FROM awards WHERE special=0)'))->order_by('award','DESC')->find_all();
                $categories[] = ['name' => $cat, 'winners' => $winners];
            }
        }
        



        $this->template->content = View::factory('winners/index')
                ->set('years',$years)
                ->set('specawards',$spec)
                ->set('categories',$categories)
                ->set('year',$year);
        $this->template->title = $this->section->title;
    }
}

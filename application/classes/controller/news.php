<?
class Controller_News extends Controller_Template {
	public $template = 'global/template';
	
	public function action_section(){
		
		if(Request::initial()->method() == Request::POST && Request::initial()->is_ajax()){
		
			$item = ORM::factory('news')
				->where('id', '=', $_POST['id'])
				->where('status', '=', ORM::STATUS_ACTIVE)
				->find()->as_array();
							
			$more = '<a href="'.URL::section($item['section_id']).'/'.$item['link'].'.html" title=""><img src="'.URL::base().'assets/css/img/btn-read-more.png" alt="" /></a>';
			$out['image'] = $item['image_527_235'];
			$out['title'] = $item['title'];
			$out['text'] = Text::limit_chars(strip_tags($item['description']),140).$more;
			$out['link'] = URL::section($item['section_id']).'/'.$item['link'].'.html';
			
			header('Content-Type: application/json');
			die(json_encode($out));
		
		}else{
		
			$ipp = 5;
			$page = Arr::get($_GET, 'p', 1);
			
			$all = ORM::factory('news')->where('status', '=', ORM::STATUS_ACTIVE)->where('section_id', '=', $this->section->id)->count_all();
			$pages = ceil($all/$ipp);
					
			$this->template->content = View::factory('news/list')
				->set('latest', ORM::factory('news')
					->where('status', '=', ORM::STATUS_ACTIVE)
					->where('section_id', '=', $this->section->id)
					->limit(1)->find())
					
				->set('news', ORM::factory('news')
					->where('status', '=', ORM::STATUS_ACTIVE)
					->where('section_id', '=', $this->section->id)
					->limit(100)->offset(1)
					->find_all()->as_array())
					
				->set('page',$page)
				->set('pages',$pages);
			
			$this->template->title = $this->section->title;
			
		}
		
	}
	
	
	public function action_item(){
	
		$item = ORM::factory('news')
			->where('link', '=', $this->request->param('id'))
			->where('status', '!=', ORM::STATUS_DRAFT)
			->find()->as_array();
				
		$view = View::factory('news/item')
            ->set('item', $item)
            ->set('section_id', $this->section->parent_id);
		
		$this->template->og_title = $item['title'];
		$this->template->og_image = $item['image_527_235'];
		$this->template->title = $item['title'];
                $this->template->og_url = substr(Kohana::config('site.domain'), 0, -1).URL::section($item['section_id']).'/'.$item['link'].'.html';
		$this->template->content = $view;
	}
}
?>
<?
class Controller_Firstpage extends Controller_Template {

	public $template = 'global/template';

	public function action_index(){

		$detect = new mobiledetect();
		if($detect->isMobile()){
			$this->template->firstpage = false;
		}else{
			$this->template->firstpage = true;
		}

		$view = View::factory('firstpage/index')
			->set('latest', ORM::factory('news')
				->where('status', '=', ORM::STATUS_ACTIVE)
				->where('section_id', '=', 76)
				->limit(1)->find())

			->set('news', ORM::factory('news')
				->where('status', '=', ORM::STATUS_ACTIVE)
				->where('section_id', '=', 76)
				->limit(100)->offset(1)
				->find_all()->as_array());
                $countdown = ORM::factory('countdown',1);
                if($countdown->loaded() && $countdown->enabled == 1)
                {
                    $this->template->countdown = $countdown->target;
                    $this->template->countdownmessage = $countdown->message;
                }

		$this->template->content = $view;

	}

}
?>


<?php 
class Controller_People extends Controller_Template {

	public $template = 'global/template';

	public function action_section()	{

		

		if (Request::initial()->method() == Request::POST && Request::initial()->is_ajax()) {
			$item = ORM::factory('people', $_POST['id']);
			$out['id'] = $item->id;
			$out['name'] = $item->title;
			$out['job'] = $item->job;
			$out['image'] = '<img style="width:648px;height:444px;" src="' . URL::base() . 'assets/media/images/' . $item->image_289_138 . '" alt="" />';
			$out['text'] = $item->description;
			$out['url'] = URL::site('lv/par-adwards/zurija') . '/' . $item->link;
			header('Content-Type: application/json');
			die(json_encode($out));
		}

		$path = end(explode('/', Request::initial()->param('path')));
		// $member = ORM::factory('people')->where('link', '=', $path)->find();


		$view = View::factory('textdoc/about')

				->set('people', ORM::factory('people')
				->where('status', '=', ORM::STATUS_ACTIVE)
				->where('section_id', '=', $this->section->id)
				->order_by('list_order', 'ASC')->find_all()->as_array())

				->set('sections', ORM::factory('section')
				->where('parent_id', '=', $this->section->parent_id)
				->where('status', '=', ORM::STATUS_ACTIVE)
				->order_by('list_order', 'ASC')
				->find_all())
				->set('sect', 'jury');


		// if ($path != 'zurija' && $member->loaded()) {
		// 	$view->set('jury', $member);
		// }

		$this->template->title = ORM::factory('section', $this->section->parent_id)->title;
		$this->template->content = $view;

		/*
		$this->template->content = View::factory('people/list')
		->set('people', ORM::factory('people')
		->where('status', '=', ORM::STATUS_ACTIVE)
		->where('section_id', '=', $this->section->id)
		->order_by('list_order', 'ASC')
		->find_all()->as_array());
		$this->template->title = $this->section->title;
		*/

	}
}

?>
<?
class Controller_Admin extends Controller_Template {
	public $template = 'admin/template';
	protected $user;
	
	public function before(){
		if(!$id = Session::instance()->get('admin_uid')) $this->request->redirect('admin/login');
		parent::before();
		$this->user = ORM::factory('user', $id);
		View::set_global('auth', $this->user);
	}
}
?>
<?
class Controller_Socials_Twitter extends Controller {
	public function action_login(){
		if(!empty($_GET['return'])) Session::instance()->set('auth_return', $_GET['return']);
		$config = Kohana::config('socials.site');
		Social_Twitter::instance()->request('POST', Social_Twitter::instance()->url('oauth/request_token', ''), array('oauth_callback' => $config['domain'].'socials/twitter/authorize'));
		if(Social_Twitter::instance()->response['code'] != 200) $this->request->redirect(Session::instance()->get_once('auth_return'));
		$session = Social_Twitter::instance()->extract_params(Social_Twitter::instance()->response['response']);
		Session::instance()->set('twitter_oauth', $session);
		$this->request->redirect(Social_Twitter::instance()->url('oauth/authenticate', '').'?'.http_build_query(array(
			'oauth_token'       => $session['oauth_token'],
			'oauth_access_type' => 'write'
		)));
	}
	
	public function action_authorize(){
		if(empty($_GET['oauth_verifier'])) $this->request->redirect(Session::instance()->get_once('auth_return'));
		$session = Session::instance()->get_once('twitter_oauth');
		Social_Twitter::instance()->config['user_token'] = $session['oauth_token'];
		Social_Twitter::instance()->config['user_secret'] = $session['oauth_token_secret'];
		Social_Twitter::instance()->request('POST', Social_Twitter::instance()->url('oauth/access_token', ''), array('oauth_verifier' => $_GET['oauth_verifier']));
		$token = Social_Twitter::instance()->extract_params(Social_Twitter::instance()->response['response']);
		Social_Twitter::instance()->config['user_token'] = $token['oauth_token'];
		Social_Twitter::instance()->config['user_secret'] = $token['oauth_token_secret'];
		Social_Twitter::instance()->request('GET', Social_Twitter::instance()->url('1/account/verify_credentials'));
		$data = json_decode(Social_Twitter::instance()->response['response']);
				
		$user = ORM::factory('social')->where('userid', '=', $data->id_str)->where('network', '=', 'twitter')->find();
		if(!$user->loaded()){
			$user = ORM::factory('social');
			$post['network'] = 'twitter';
			$post['userid'] = $data->id_str;
			$post['name'] = $data->screen_name;
			$post['image'] = 'https://api.twitter.com/1/users/profile_image?screen_name='.$data->screen_name.'&size=bigger';
			$user->values($post);
			$user->save();
			
			//Social_Twitter::instance()->request('POST', Social_Twitter::instance()->url('1/statuses/update','json'),array('status' => 'Es esmu par nopietnu pensiju. Ienāc arī tu www.nopietnapensija.lv'));
		}
		
		Session::instance()
			->set('show_popup', true)
			->set('uid', $data->id_str)
			->set('uname', $data->screen_name)
			->set('uimage', 'https://api.twitter.com/1/users/profile_image?screen_name='.$data->screen_name.'&size=bigger')
			->set('utype', 'twitter');
		$this->request->redirect('/');
	}
}
?>
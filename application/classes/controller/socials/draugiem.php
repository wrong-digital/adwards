<?
class Controller_Socials_Draugiem extends Controller {
	public function action_login(){
		Session::instance()->set('social_return', $_SERVER['HTTP_REFERER']);
		$this->request->redirect(Social_Draugiem::instance()->getLoginUrl());
	}
	
	public function action_authorize(){
		if(Arr::get($_GET, 'dr_auth_status') != 'ok' || empty($_GET['dr_auth_code'])) $this->request->redirect(Session::instance()->get_once('auth_return'));
		if(!($data = Social_Draugiem::instance()->api('authorize', array('code' => $_GET['dr_auth_code'])))) $this->request->redirect(Session::instance()->get_once('auth_return'));
		
		foreach($data->users as $u){
			Session::instance()
				->set('uid', $u->uid)
				->set('network', 'draugiem')
				->set('dr_uid', $u->uid);
		}

		$this->request->redirect(Session::instance()->get('social_return').'&voted=true');
	}
}
?>
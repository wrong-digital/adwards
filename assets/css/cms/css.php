<?php


   // this is prepended to all file / folder paths so files and archive folder should be specified relative to this


   $sDocRoot = FCPATH.'css/cms';


     


   // get file last modified dates


   $aLastModifieds = array();


	


    $cssFiles = array(


		'main.css',


		'styles.css',


		'login.css',


		'calendar.css',


		'jquery.treeview.css'


	);


	


   foreach ($cssFiles as $sFile) {


      $aLastModifieds[] = filemtime("$sDocRoot/$sFile");


   }


   // sort dates, newest first


   rsort($aLastModifieds);


      


   // output latest timestamp


   return $aLastModifieds[0];
$(function(){
  
	function equalize(){
		$('.page').each(function(){
			$(this).find('#cont > :last-child').equalizeBottoms();
		});
	};
				  
	$(document).bind( 'equalize', equalize ).trigger( 'equalize' );
				  
	var h = $("#left-white").height();
	var n = h + 100;
	$("#left-white").height(n);
				 
});
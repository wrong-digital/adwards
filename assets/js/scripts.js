var html5 = ["header", "nav", "section", "hgroup", "footer"];
for(var e in html5) document.createElement(html5[e]);

$(function(){
	
	$('input, textarea').placeholder();
	
	// Close popups with Esc
	$(document).keyup(function(e) {
	    if (e.keyCode == 27) { $('.overlay, .container, .jury-popup').fadeOut(250); } 
	});
	
	// Replace registration links with popup registration
	$('a[href*="/registreties"], a[href*="/registracija"]').click(function(e){
				
		if($('.login-box').is(':visible')) {
			$('.login-box').hide();
		}
		
		$.get(base+lang+'/actions/register', function(data){
			$('.popup-registration').css({top:$(window).scrollTop()+25+'px'});
			$('.popup-registration .content').html(data);
			$('.overlay, .popup-registration').fadeIn(250);
		})
		
		return false;
	})
	
	
	//
	$('a[href*="/profils"]').click(function(e){
				
		$.get(base+lang+'/actions/profile', function(data){
			$('.popup-registration').css({top:$(window).scrollTop()+25+'px'});
			$('.popup-registration .content').html(data);
			$('.overlay, .popup-registration').fadeIn(250);
		})
		
		return false;
	})
	
	
	// Disable page scroll when opened popup (and allow to scroll popup)
	$('.scrollable-content div').bind('mousewheel DOMMouseScroll', function(e) {
	    var scrollTo = null;
	
	    if (e.type == 'mousewheel') {
	        scrollTo = (e.originalEvent.wheelDelta * -1);
	    }
	    else if (e.type == 'DOMMouseScroll') {
	        scrollTo = 40 * e.originalEvent.detail;
	    }
	
	    if (scrollTo) {
	        e.preventDefault();
	        $(this).scrollTop(scrollTo + $(this).scrollTop());
	    }
	});
	
	$('body').bind('mousewheel', function(event, delta, deltaX, deltaY) {
		if($('.overlay').is(':visible') && $('.popup-registration').is(':hidden') && $('.jury-popup').is(':hidden') && $('.voting-popup').is(':hidden')) {
			return false;
		}
	});
			
	// To top button
	$('.to-top img').click(function(){
		$('html, body').animate({scrollTop:'0px'}, 450);
	})
	
	// Close popups
	$('.overlay, .container .close').click(function(){
		$('.overlay, .container, .jury-popup, .restore-pass, .login-box, .popup-registration').fadeOut(250);
		site.popup.hide();
	})
	
	$('.popup-registration .close').click(function(){
		$('.overlay, .popup-registration').fadeOut(250);
	})
	
	
	// Firstpage video ends
        /*
	var t = setTimeout(
		function(){
			if($(window).height()+'px' == $('.absolute-firstpage').css('marginTop')){
				$('.down-arrow').fadeOut(50);
				$('.absolute-firstpage').animate({marginTop:'0px'}, 550);
				$('.vidoverlay').fadeOut(550);
                                $('#countdown, #countdown-message').fadeOut(550);
				
			}
		}
	,30000);
        
        VIDEO DISABLED */

	$(".archive .paging a").click(function(){
		$(".archive .paging a").each(function(){ $(this).attr("class",""); });
		$(this).addClass("ac");
		
		var img = $(this).attr("data-image");
		var large = $(this).attr("data-large");
		var video = $(this).attr("data-video");
		
		if(img != ''){
			$(".archive .image").html("<div id='video'><a href='javascript:;' name=''><span class='img-popup-link'></span></a></div>");
			$(".archive .image").attr("style","background:url("+img+") no-repeat center;");
			$(".archive .image").attr("onclickimg",large);
		}else{
			large = '';
			$(".archive .image").attr("onclickimg",large);
			jwplayer("video").setup({
				flashplayer: base+"assets/js/mediaplayer-5.8/player.swf",
				file: video,
				image: "",
				autoplay: "true",
				width: 370,
				height: 274
			});
		}
	});
	
	$(".archive .image").click(function(){
		var img = $(".archive .image").attr("onclickimg");
		if (img.length > 0){
			$(".popup-image img").attr("src",img);
			$(".popup-image-background").show();
			$(".popup-image").show();
		}
	});
	
	$(".popup-image a").click(function(){
		$(".popup-image img").attr("src",'');
		$(".popup-image-background").hide();
		$(".popup-image").hide();
	});
	
	$(".close a").click(function(){
		site.popup.hide();
	});
	
	$("a#login, a#login_from_section").click(function(){
		$('.overlay, .login-box').fadeIn();
	});
	
	$('.login-box p.close a').click(function(){
		$('.overlay, .login-box').fadeOut();
	})
	
    $('.restore-pass p.close a').click(function(){
        $('.overlay, .restore-pass').fadeOut();
    })
	
	$("a#request_courier").click(function(){
		$(".courier-request-box").is(":hidden") ? $(".courier-request-box").show() : $(".courier-request-box").hide();
	});
	
	$("#antalis").change(function(){
		if(document.data.elements['antalis'].checked) {
			$("#antalis_type_div").show();
		}else{
			$("#antalis_type_div").hide();
		}
	});
	
	$("#antalis_type").focusin(function(){
		if ($("#antalis_type").val() == "Ieraksti, kādus Antalis papīrus vai materiālus izmantoji") {
			$("#antalis_type").val("");
		}
	});
	
	$("#antalis_type").focusout(function(){
		if ($("#antalis_type").val() == '') {
			$("#antalis_type").val("Ieraksti, kādus Antalis papīrus vai materiālus izmantoji");
		}
	});
	
	$("#antalis_technology").focusin(function(){
		if ($("#antalis_technology").val() == "Ieraksti, kādas drukas tehnoloģijas izmantoji") {
			$("#antalis_technology").val("");
		}
	});
	
	$("#antalis_technology").focusout(function(){
		if ($("#antalis_technology").val() == '') {
			$("#antalis_technology").val("Ieraksti, kādas drukas tehnoloģijas izmantoji");
		}
	});
	
	// Login
	var l = $(".login-box");
	if(l.length){
		var lf = $("form", l);
		lf.submit(function(){
			$.post(lf.attr("action"), lf.serialize(), function(data){
				$("input", lf).removeClass("error");
				if(data.status == 'ok'){
					window.location.reload();
				}else if(data.status == 'error'){
					site.popup.show(data.text);
				}else{
					site.popup.hide();
					$.each(data.errors, function(k, v){
						$(".login-box #"+k).removeClass("error");
						$(".login-box #"+k).addClass("error");
					});
				}
			}, "json");
			return false;
		});
	}
	
	$('.login-box a.login-restore').click(function(){
	    $(l).hide();
	    $('.restore-pass').show();
	});
	
	// Restore password
    var r = $(".restore-pass");
    if(r.length){
        var rf = $("form", r);
        rf.submit(function(){
        	site.popup.show('Uzgaidiet...');
            $.post(rf.attr("action"), rf.serialize(), function(data){
                $("input", rf).removeClass("error");
                if(data.status == 'ok'){
                    site.popup.show(data.text);
                    $(r).hide()
                    $(l).show();
                    $('.login-box #email').val($('.restore-pass #email').val());
                }else if(data.status == 'dont_exist'){
                    site.popup.show(data.text);
                }else{
                    site.popup.hide();
                    $.each(data.errors, function(k, v){
                        $(".restore-pass #"+k).removeClass("error");
                        $(".restore-pass #"+k).addClass("error");
                    });
                }
            }, "json");
            return false;
        });
    }
	
	$("form input, form textarea").click(function(){
		$(this).removeClass("error");
	});
        
        if(!Modernizr.svg)
        {
        // visus fileneimus turam vienādus gan svg, gan png !!
        $('.svg-img').each(function() {
            var png = $(this).css('background-image').replace('.svg','.png');
            $(this).css('background-image',png);
        });
        $('.inline-svg').each(function() {
            var png = $(this).data('fallback');
            $(this).append('<img src="'+png+'" alt="ADWARDS">');
        });
        }
        
        //$('.news-content p img')
    
});

var site = {
	vote: function(id){
		$.post("/profile/vote", { id:id }, function(data){
			$(".button").html(data.text);
			$(".votes span").html(data.votes);
		},"json");
	},
	load: {
		video: function(num,id,title){
			$.post("/profile/getvideo", { id:id }, function(data){
				$(".gallery .main .img").html(data);
				$("#desc").html(title);
				$(".count span").html(num);
			});
		},
		image: function(num,title,src){
			//$(".gallery .main .img").html("<a href='javascript:;' title=''><img src='/assets/css/img/ico-zoom.png' alt='' /></a>");
			$(".gallery .main .img").attr("style","background:url("+src+") no-repeat;");
			$(".count span").html(num);
			$("#desc").html(title);
		},
		thumb: function(id){
			var src = $("#thumb"+id).attr("data-img");
			var num = $("#thumb"+id).attr("data-nr");
			var title = $("#thumb"+id).attr("data-title");
			$(".gallery .main .img").attr("style","background:url(/"+src+") no-repeat;");
			$(".count span").html(num);
			$("#desc").html(title);
		},
		
		subsections: function(link){
			var id = $("#main_category").val();
			$.post(link, { id:id, action:'sub' }, function(data){
				$("#sub_category").html(data.html);
				site.load.getformats(link);
			},"json");
			
			if(id == 23){
				$("#all-files").hide();
				$("#digital-files").show();
			}else{
				$("#digital-files").hide();
				$("#all-files").show();
			}

			if(id == 23 || id == 19){
				$("#all-files").hide();
			}else{
				$("#all-files").show();
			}

			if(id == 17 || id == 23|| id == 25){
				$("#courier_button").hide();
			}else{
				$("#courier_button").show();
			}
			
			if (id == 19 || id == 17 || id == 23 || id == 25){
				$("#printed-2").hide();
			}else{
				$("#printed-2").show();
			}
			
			/*if(id == 25){
				$("#all-files").hide();
				$("#zip_upload2").show();
			}else{
				$("#zip_upload2").hide();
			}*/
		},
		getformats: function(link){
			var id = $("#sub_category").val();
			$.post(link, { id:id, action:'format' }, function(data){
				$("#info_book").show().html(data.book);
				$("#info_web").show().html(data.web);
			},"json");
			
		    //$("#printed-1,#printed-2,#printed-3").hide();
		    
		    // ATTENTION: THERE IS ANOTHER JAVASCRIPT FILE TO TAKE IN ACCOUNT FOR edit.php PAGE : edit.js
		    
		    var sub_id = $("#sub_category").val();
		    var main_id = $("#main_category").val();
		    
		
			/*if(sub_id == 7 || sub_id == 51 || sub_id >= 17 && sub_id <= 21){
		        $("#all-files").hide();
				$("#zip_upload").show();
			}else{
				$("#zip_upload").hide();
			}*/
		
		        
		    if (sub_id >= 51 && sub_id <=59 && sub_id != 53 && sub_id != 55){
		        $("#printed-1").show();
		    }else{
		    	$("#printed-1").hide();
		    }
		    
		    if (sub_id == 53){
		        $("#printed-2").show();
		    }
		
		    if (sub_id == 55 ){
		        $("#printed-3").show();
		    }else{
		    	$("#printed-3").hide();
		    }
		    
		    if (main_id == 17){
		        if (sub_id == 4 || sub_id == 43 || sub_id == 44){
		            $('#digi-video').hide();
		            $('#link-to-video').hide();
		            $('#for-book-img, #for-web-img').hide();
		            $('#digi-audio').show();
		        }else {
		            $('#digi-audio').hide();
		            $('#digi-video').show();
		            $('#link-to-video').show();
		            $('#for-book-img, #for-web-img').show();
		        }
		    }else {
		        $('#digi-video').show();
		        $('#digi-audio').show();
		        $('#for-book-img, #for-web-img').show();
		        $('#link-to-video').show();
		    }
		}
	},
	types: function(on,off){
		$("#type"+off).hide();
		$("#type"+on).fadeIn();
		if(on == 1){
			$(".register .area .types").removeClass("types2");
		}else{
			$(".register .area .types").addClass("types2");
		}
	},
	popup: {
		show: function(data){
			$(".popup .c").html(data);
			$(".popup, .overlay").show();
		},
		hide: function(){
			if($('.login-box').is(':visible') || $('.restore-pass').is(':visible')) {
				$(".upload, .popup").hide();
			}else{
				$(".upload, .popup, .overlay, .popup-registration").hide();
			}
		}
	},
	gallery: {
		prev: function(gallery){
			var start = $("#start").val();
			var link = $("#link").val();
			$.post(link, { data:'prev', start:start, id:gallery }, function(data){
				$(".thumbs ul").html(data.html);
				$("#start").val(data.start);
			},"json");
		},
		next: function(gallery){
			var start = $("#start").val();
			var link = $("#link").val();
			$.post(link, { data:'next', start:start, id:gallery }, function(data){
				$(".thumbs ul").html(data.html);
				$("#start").val(data.start);
			},"json");
		}
	},
	checkform: function(){
		return false;
	}
};


function limitText(limitField, limitCount, limitNum, id) {
	if (limitField.value.length > limitNum) {
		limitField.value = limitField.value.substring(0, limitNum);
	} else {
		//$("span#"+id).html(limitNum - limitField.value.length);
                $("span#"+id).html(limitField.value.length);
	}
}

function isEmailAddress(strEmailAddress) {			
	if (strEmailAddress.length == 0)
	return false;
	if (strEmailAddress.indexOf(" ") != -1)
	return false;
	var oRegExp2 = /\w+\@(\w[\w-]*\w)+\.\w+/;
	if (!oRegExp2.test(strEmailAddress))
	return false;
	var oRegExp = new RegExp("@","g");
	var oMatches = strEmailAddress.match(oRegExp);
	if (oMatches.length != 1)
	return false;
					
	return true;
}

/*2014*/
jQuery(document).ready(function(){
	
	//$clamp( $('.clamper'), {clamp: 1});
	
	$(function(){
		//var element = $('.scroll-pane').jScrollPane();
		/*/
		var api = element.data('jsp');
		element.bind(
		     'mousewheel',
		     function (event, delta, deltaX, deltaY)
		     {
			 api.scrollByX(delta * 30);
			 return false;
		     }
		);
		/**/
		/*/
		$('.scroll-pane-arrows').jScrollPane({
							showArrows: true,
							horizontalGutter: 10
						});
		/**/
	});
});
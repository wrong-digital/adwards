CKEDITOR.plugins.add("embedVideo", {
    init: function(editor){
        var pluginName = "embedVideo";
		CKEDITOR.dialog.add(pluginName, this.path+"dialogs/"+pluginName+".js");
		editor.addCommand(pluginName, new CKEDITOR.dialogCommand(pluginName));
		editor.ui.addButton(pluginName, {
			label: "Video from other resource",
			command: pluginName
		});
    }
});
/*
Copyright (c) 2003-2011, CKSource - Frederico Knabben. All rights reserved.
For licensing, see LICENSE.html or http://ckeditor.com/license
*/

CKEDITOR.editorConfig = function(config){
	config.removePlugins = "elementspath";
	config.toolbar = "Basic";
	config.extraPlugins = "embedVideo";
        config.disallowedContent = '*{font-family*}';
	config.toolbar_Basic =
	[
		['Source', 'Bold', 'Italic', '-', 'NumberedList', 'BulletedList', '-', 'Link', 'Unlink','-', 'Image', 'PasteText', '-', 'TextColor','BGColor']
	];
	config.toolbar_Full =
	[
		[ 'Source','-','Cut','Copy','Paste','PasteText','-','Undo','Redo' ],[ 'Styles','Format','Font','FontSize' ],[ 'TextColor','BGColor' ],
		'/',
		[ 'Bold','Italic','Underline','Strike','Subscript','Superscript'],
		[ 'NumberedList','BulletedList','-','Outdent','Indent','-','JustifyLeft','JustifyCenter','JustifyRight','JustifyBlock','-','BidiLtr','BidiRtl' ],
		[ 'Link','Unlink','Anchor' ], [ 'Image','Flash','Table','HorizontalRule','SpecialChar','Iframe',"embedVideo" ]
	];
};
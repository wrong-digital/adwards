(function(){
	CKEDITOR.dialog.add("embedVideo", function(editor){
		return {
			title : "video",
			minWidth : 400,
			minHeight : 100,
			buttons: [CKEDITOR.dialog.okButton, CKEDITOR.dialog.cancelButton],
			onOk: function(){
				var url = this.getContentElement("embed", "url").getValue();
				var str = "";
				yt_regexp = /^http\:\/\/(www\.)?youtube\.com\/watch\?v=([^&]+)(&.*)?/i;
				vm_regexp = /^http\:\/\/(www\.)?vimeo\.com\/(\d+)/i;
				if(url.match(yt_regexp)){
					str = url.replace(yt_regexp, '<iframe width="560" height="349" src="http://www.youtube.com/embed/$2?wmode=opaque" frameborder="0" allowfullscreen></iframe>');
				}else if(url.match(vm_regexp)){
					str = url.replace(vm_regexp, '<iframe src="http://player.vimeo.com/video/$2?title=0&amp;byline=0&amp;portrait=0" width="400" height="225" frameborder="0"></iframe>');
				}else{
					alert("Invalid video url!");
					return false;
				}
				editor.insertHtml(str);
				return true;
			},
			resizable: "none",
			contents: [{
				id: "embed",
				elements: [{
					type: "vbox",
					children: [{
						id: "url",
						type: "text",
						label: "Saite"
					}]
				}]
			}]
		};
	});
})();
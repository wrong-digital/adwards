/*
* Sortable Tree View
*
* Copyright (c) 2010 Tom Kay - oridan82@gmail.com
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.*
*
*/

(function ($) {
    var k = {
        topPad: 5,
        placeholder: 'ui-state-highlight',
        tree: 'ul',
        branch: 'li'
    };
    var l = null;
    $.fn.treeSort = function (i) {
        var j = $.extend({}, k, i);
        $(j.tree, this).not(j.tree + ' ' + j.tree).each(function () {
            var d = $(this);
            var f = $(j.branch, d).first().clone(false).empty().attr('id', 'ui-treesort-placeholder').text('treeSort Placeholder').addClass(j.placeholder).hide().appendTo(this);
            var g = null;
            var h = false;
            $(j.branch, this).draggable({
                appendTo: 'body',
                helper: 'clone',
                opacity: 0.5,
                refreshPositions: true,
                distance: 5,
                start: function (e, a) {
                    l = document.onselectstart;
                    document.onselectstart = function () {
                        return false
                    }
                    if (h) return false;
                    h = true;
                    f.html($(this).html()).show();
                    $(this).hide()
                },
                stop: function (e, a) {
                    document.onselectstart = l;
                    h = false;
                    if (!$(this).has(f)) return;
                    $(this).css({
                        top: null,
                        left: null
                    }).insertAfter(f);
                    f.detach();
                    $(this).show();
                    UpdateClasses();
                    if (j.change) j.change($(this))
                },
                drag: function (e, a) {
                    if (!g || !g.offset()) return;
                    cX = e.clientX + $("body").scrollLeft();
                    cY = e.clientY + $("body").scrollTop();
                    oX = cX - g.offset().left;
                    oY = cY - g.offset().top;
                    var b = 1;
                    if (oY < j.topPad) b = 0;
                    if (oX > 20) {
                        var c = g.children(j.tree + ':first');
                        if (c.length === 0) {
                            c = $(d).andSelf().filter(j.tree).first().clone(false).empty().appendTo(g)
                        }
                        c.append(f);
                        return
                    } else if (oX < 0) {
                        g = g.parents(j.branch).eq(0)
                    }
                    if (b) g.after(f);
                    else g.before(f)
                }
            });
            $(j.branch, this).droppable({
                accept: j.branch,
                tolerance: 'pointer',
                over: function (e, a) {
                    if ($(this).hasClass(j.placeholder)) return;
                    g = $(this)
                }
            });
            UpdateClasses();
            if (j.init) j.init(j);

            function UpdateClasses() {
                $(j.branch, d).addClass('ui-treesort-item').removeClass('ui-treesort-folder');
                var a = $(j.tree, d).not('.ui-draggable-dragging').not(f).not(':empty').parent(j.branch);
                a.addClass('ui-treesort-folder')
            }
        });
        return $(this)
    }
})(jQuery);
$(function(){
	$("div.table-data a").fancybox();
	logs.filters.init();
});

var logs = {
	filters: {
		init: function(){
			this.users.init();
		},
		date: {
			addPeriod: function(){
				var p = $("#periods");
				var n = $("div.period:last", p)
					.clone()
					.appendTo(p)
					.find("input")
					.val("")
					.removeClass("hasDatepicker")
					.removeAttr("id")
					.datepicker(admin.plugins.datepicker.config);
				return false;
			}
		},
		users: {
			list: {},
			selected: [],
			init: function(){
				var uft = $(document.createElement("a")).attr("href", "#usersFilter").hide().appendTo("body").fancybox();
				$("#users").click(function(){
					uft.click();
				});
				var timer = undefined;
				$("#usersFilter div.left input").keyup(function(){
					var str = $(this).val();
					timer = setTimeout(function(){
						logs.filters.users.autocomplete(str);
					}, 300);
				}).keydown(function(){
					clearTimeout(timer);
				});
				var l = $("#usersFilter div.left select");
				var r = $("#usersFilter div.right select");
				var s = this.selected;
				$.each(this.list, function(k, v){
					$(document.createElement("option")).val(k).text(v).appendTo($.inArray(parseInt(k), s) === -1 ? l : r);
				});
			},
			add: function(){
				$("#usersFilter div.left select option:selected").appendTo("#usersFilter div.right select").removeAttr("selected").each(function(){
					logs.filters.users.selected.push(parseInt($(this).val()));
				});
			},
			remove: function(){
				$("#usersFilter div.right select option:selected").appendTo("#usersFilter div.left select").removeAttr("selected").each(function(){
					logs.filters.users.selected.splice($.inArray(logs.filters.users.selected, parseInt($(this).val())), 1);
				});
			},
			submit: function(){
				var s = $("#seletcedUsers");
				$("input", s).remove();
				var u = $("#users").val("");
				var l = [];
				$("#usersFilter div.right select option").each(function(){
					$(document.createElement("input")).attr({
						type: "hidden",
						name: "user[]"
					}).val($(this).val()).appendTo(s);
					l.push($(this).text());
				});
				u.val(l.join(", "));
				$.fancybox.close();
			},
			autocomplete: function(str){
				str = this._escapeRegex(str);
				var l = this.selected;
				var s = $("#usersFilter div.left select");
				$("option", s).remove();
				$.each(this.list, function(k, v){
					if($.inArray(parseInt(k), l) === -1 && (new RegExp(str, "i")).test(v)) $(document.createElement("option")).val(k).text(v).appendTo(s);
				});
			},
			_escapeRegex: function(a){
				return a.replace(/[-[\]{}()*+?.,\\^$|#\s]/g, "\\$&");
			}
		}
	}
};
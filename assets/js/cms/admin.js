var admin = {
	url: {
		base: "",
		site: ""
	},
	tree: {
		active: 0,
		lang: "",
		show_by_lang: function(id){
			$("#tree").activity({
				segments: 12,
				steps: 3,
				width:2,
				space: 1,
				length: 3,
				color: '#030303',
				speed: 1.5
			});
			$("#browser").fadeOut(2000);
			$.cookie('cms_tree_lang', id);
			$("div[id^='lang']").removeClass("lang-ac").addClass("lang");
			$("#lang-"+id).addClass("lang-ac");
			$.get(admin.url.site+"admin/sections/tree/"+id, function(data){
				$("#tree")
					.html(data)
					.find("ul:first")
					.attr("id", "browser")
					.addClass("filetree treeview-red")
					.treeview()
					.show();
				$("#tree ul").sortable({
					cancel: ".last",
					update: function(){
						alert("Tu tikko izmainīji struktūru");
					}
				});
				admin.tree.set_context_menu();
				admin.tree.set_height();
				admin.tree.open_to(admin.tree.active);
			});
		},
		show: function(){
			$.cookie("cms_tree_status", "visible");
			$(".tree").show();
			$(".start-area, .content-area, .insert-area").removeAttr("style");
		},
		hide: function(){
			$.cookie("cms_tree_status", "hidden");
			$(".tree").hide();
			$(".start-area").css("margin", "0 470px 0 13px");
			$(".content-area").css("margin", "0 20px 0 13px");
			$(".insert-area").css("margin", "0 305px 0 13px");
		},
		toggle: function(){
			if($(".tree").is(":hidden")){
				this.show();
				admin.WYSIWYG.update("Basic");
			}else{
				this.hide();
				admin.WYSIWYG.update("Full");
			}
		},
		set_height: function(){
			$("#browser").css("min-height", $(document).height()-310).show();
			$(".tree-collapse a").css("min-height", $(document).height()-195);
		},
		set_context_menu: function(){
			$("#browser li a").contextMenu({
				menu: 'myMenu'
			}, function(action, el){
				var p = $(el).parent();
				var id = p.attr("id").substr(1);
				if(action == "delete"){
					if(!confirm("Tiešām vēlies dzēst šo sadaļu?")) return false;
					p.remove();
					$.get(admin.url.site+"admin/sections/delete/"+id);
				}else if(action == "edit"){
					document.location.href = admin.url.site+"admin/sections/view/"+id;
				}
			});
			$("#browser li a.add").disableContextMenu();
		},
		format: function(){
			$("#expanded").parents("li").removeClass("closed expandable").addClass("collapsable");
		},
		open_to: function(id){
			var m = $("#s"+id);
			m.children("a").addClass("selected");
			m.children("div.hitarea").click();
			m.parents("li").each(function(){
				$(this).children("div.hitarea").click();
				$(this).children("a").addClass("selected");
			});
		},
		reload: function(){
			this.show_by_lang(this.lang);
		}
	},
	tabs: {
		active: "",
		set: function(tab, content, id){
			this.active = id;
			$("div[id^='"+content+"']").hide();
			$("div[id^='"+tab+"']").removeClass("tab-ac").addClass("tab");
			$("#"+tab+id).addClass("tab-ac");
			$("#"+content+id).show();
		}
	},
	login: function(){
		$("div.input input").focus(function(){
			if($(this).val() == this.defaultValue) $(this).val("");
		}).blur(function(){
			if($(this).val() == "") $(this).val(this.defaultValue);
		});
		$("div.input:first input").focus();
		$("form").submit(function(){
			$(".error").fadeOut(300);
			var s = true;
			$("div.input input").each(function(){
				if($(this).val() == "" || $(this).val() == this.defaultValue){
					s = false;
					$(this).focus();
					return false;
				}
			});
			if(!s) return false;
			$.post(admin.url.site+"admin/login/go", $(this).serialize(), function(data){
				if(data.status){
					document.location.href = admin.url.site+"admin";
				}else{
					$(".error").fadeIn(300);
					$("div.input:first input").focus();
				}
			}, "json");
			return false;
		});
	},
	utils: {
		title: {
			lat_l: ["ā","č","ē","ģ","ī","ķ","ļ","ņ","š","ū","ž"],
			lat_u: ["Ā","Č","Ē","Ģ","Ī","Ķ","Ļ","Ņ","Š","Ū","Ž"],
			ascii: ["a","c","e","g","i","k","l","n","s","u","z"],
			rus: ['ё','ж','ц','ч','ш','щ','ю','я','Ё','Ж','Ц','Ч','Ш','Щ','Ю','Я','А','Б','В','Г','Д','Е','З','И','Й','К','Л','М','Н','О','П','Р','С','Т','У','Ф','Х','Ъ','Ы','Ь','Э','а','б','в','г','д','е','з','и','й','к','л','м','н','о','п','р','с','т','у','ф','х','ъ','ы','ь','э'],
			asc: ['yo','zh','tc','ch','sh','sh','yu','ya','YO','ZH','TC','CH','SH','SH','YU','YA','A','B','V','G','D','E','Z','I','J','K','L','M','N','O','P','R','S','T','U','F','H','_','I','_','E','a','b','v','g','d','e','z','i','j','k','l','m','n','o','p','r','s','t','u','f','h','','i','','e'],
			rewrite: function(e){
				var t = this;
				var str = $(e).val();
				$.each(t.lat_l, function(i){
					str = str.replace(new RegExp(this, "g"), t.ascii[i]);
				});
				$.each(t.lat_u, function(i){
					str = str.replace(new RegExp(this, "g"), t.ascii[i]);
				});
				$.each(t.rus, function(i){
					str = str.replace(new RegExp(this, "g"), t.asc[i]);
				});
				$(e).parents("div.insert-input").next().find("input").val(str.toLowerCase().replace(/\W/g, "-").replace(/\-+/g, "-").replace(/^\-/, "").replace(/\-$/, ""));
			}
		}
	},
	status: {
		icons: ["css/cms/img/table-ico-inactive.png", "css/cms/img/table-ico-active.png"],
		update: function(controller, id){
			var t = this;
			$.get(controller+"/"+id, function(data){
				$("#status_"+id).attr("src", t.icons[data]);
			});
		}
	},
	block: {
		toggle: function(parentid, contentid){
			var c = $("#"+contentid);
			if(c.is(":hidden")){
				$("#"+parentid).removeClass("h-col").addClass("h-exp");
				c.show();
			}else{
				$("#"+parentid).removeClass("h-exp").addClass("h-col");
				c.hide();
			}
		}
	},
	WYSIWYG: {
		selectors: [],
		params: {},
		ready: false,
		init: function(){
			if(this.ready) return true;
			this.ready = true;
			this.params = {
				filebrowserBrowseUrl: admin.url.base+"assets\/js\/cms\/ckfinder\/ckfinder.html",
				filebrowserImageBrowseUrl: admin.url.base+"assets\/js\/cms\/ckfinder\/ckfinder.html?type=Images",
				filebrowserFlashBrowseUrl: admin.url.base+"assets\/js\/cms\/ckfinder\/ckfinder.html?type=Flash",
				filebrowserUploadUrl: admin.url.base+"assets\/js\/cms\/ckfinder\/core\/connector\/php\/connector.php?command=QuickUpload&type=Files",
				filebrowserImageUploadUrl: admin.url.base+"assets\/js\/cms\/ckfinder\/core\/connector\/php\/connector.php?command=QuickUpload&type=Images",
				filebrowserFlashUploadUrl: admin.url.base+"assets\/js\/cms\/ckfinder\/core\/connector\/php\/connector.php?command=QuickUpload&type=Flash"
			};
			return true;
		},
		create: function(selector){
			this.init();
			if(typeof CKEDITOR == "undefined") return;
			this.selectors.push(selector);
			var params = this.params || {};
			//params.toolbar = $.cookie("cms_tree_status") == "hidden" ? "Full" : "Basic";
			params.toolbar = "Basic";
			if(params.toolbar == "Full") params.height = 320;
			$(selector).ckeditor(params);
		},
		update: function(toolbar){
			if(typeof CKEDITOR == "undefined") return;
			$.each(CKEDITOR.instances, function(){
				this.destroy();
			});
			var params = this.params || {};
			params.toolbar = toolbar;
			if(toolbar == "Full") params.height = 320;
			$.each(this.selectors, function(k, v){
				$(v).ckeditor(params);
			});
		},
		reset: function(){
			$.each(CKEDITOR.instances, function(){
				this.destroy();
			});
		}
	},
	plugins: {
		datepicker: {
			config: {
				dateFormat: "yy-mm-dd",
				dayNamesMin: ["Sv", "P", "O", "T", "C", "P", "S"],
				firstDay: 1,
				monthNames: ["Janvāris", "Februāris", "Marts", "Aprīlis", "Maijs", "Jūnijs", "Jūlijs", "Augusts", "Septembris", "Oktobris", "Novembris", "Decembris"],
				nextText: "Nākamais",
				prevText: "Iepriekšējais"
			}
		}
	}
};

$(function(){
	if(typeof $.cookie == "function" && $.cookie('cms_tree_status') == 'hidden') admin.tree.hide();
	$("div.tree-collapse a").click(function(){
		admin.tree.toggle();
		return false;
	});
	$("a[rel='action:delete'], a[data-action='delete']").click(function(){
		return confirm("Dzēst?");
	});
	if($("#tree").length){
		admin.tree.lang = $("div.tree div.small-tabs div.lang-ac").attr("id").split("-")[1];
		admin.tree.reload();
		admin.tree.format();
		$("#browser").treeview();
		admin.tree.set_height();
		admin.tree.set_context_menu();
		$('.table-item').mouseover(function(){
			$(this).addClass('hover');
		}).mouseout(function(){
			$(this).removeClass('hover');
		});
		$("div.tree-buttons a.cancel").click(function(){
			admin.tree.reload();
			return false;
		});
		$("div.tree-buttons a.save").click(function(){
			var data = {};
			$("#tree ul").each(function(){
				var parent = $(this).parent();
				var pid = parent.get(0).tagName == "LI" ? parent.attr("id").substr(1) : "0";
				data[pid] = $(this).sortable("toArray");
			});
			$.post(admin.url.site+"admin/sections/sort", {
				data: data
			}, function(){
				admin.tree.reload();
			});
			return false;
		});
	}
	if($.fn.datepicker) $("input.date").datepicker(admin.plugins.datepicker.config);
	if(typeof CKEDITOR == "object"){
		CKEDITOR.on("instanceReady", function(e){
			e.editor.on("key", function(){
			});
		});
	}
});
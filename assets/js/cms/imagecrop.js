$(function(){
    
    
    $('#smallpic').fileapi({
        url: 'http://www.adwards.lv/admin/news/cropper',
        accept: 'image/*',
        imageSize: { minWidth: 308, minHeight: 163 },
        elements: {
           active: { show: '.js-upload', hide: '.js-browse' },
           preview: {
              el: '.js-preview',
              width: 190,
              height: 100
           },
           progress: '.js-progress'
        },
        onFileComplete: function (evt, uiEvt){
            var json = uiEvt.result;
            $('input[name=image_sm]').val(json.image);
        },
        onSelect: function (evt, ui){
           var file = ui.files[0];
           if( !FileAPI.support.transform ) {
              alert('Your browser does not support Flash :(');
           }
           else if( file ){
              $('#smallpic-popup').modal({
                 closeOnEsc: true,
                 closeOnOverlayClick: false,
                 onOpen: function (overlay){
                    $(overlay).on('click', '.js-upload', function (){
                       $.modal().close();
                       $('#smallpic').fileapi('upload');
                    });
                    $('.js-img', overlay).cropper({
                       file: file,
                       bgColor: '#fff',
                       maxSize: [1600, 1600],
                       minSize: [308, 163],
                       aspectRatio: 1.9,
                       selection: '90%',
                       onSelect: function (coords){
                          $('#smallpic').fileapi('crop', file, coords);
                       }
                    });
                 }
              }).open();
           }
        }
     });
     
     $('#medpic').fileapi({
        url: 'http://www.adwards.lv/admin/news/cropper',
        accept: 'image/*',
        imageSize: { minWidth: 648, minHeight: 343 },
        elements: {
           active: { show: '.js-upload', hide: '.js-browse' },
           preview: {
              el: '.js-preview',
              width: 211,
              height: 111
           },
           progress: '.js-progress'
        },
        onFileComplete: function (evt, uiEvt){
            var json = uiEvt.result;
            $('input[name=image_md]').val(json.image);
        },
        onSelect: function (evt, ui){
           var file = ui.files[0];
           if( !FileAPI.support.transform ) {
              alert('Your browser does not support Flash :(');
           }
           else if( file ){
              $('#medpic-popup').modal({
                 closeOnEsc: true,
                 closeOnOverlayClick: false,
                 onOpen: function (overlay){
                    $(overlay).on('click', '.js-upload', function (){
                       $.modal().close();
                       $('#medpic').fileapi('upload');
                    });
                    $('.js-img', overlay).cropper({
                       file: file,
                       bgColor: '#fff',
                       maxSize: [1600, 1600],
                       minSize: [648, 343],
                       aspectRatio: 1.9,
                       selection: '90%',
                       onSelect: function (coords){
                          $('#medpic').fileapi('crop', file, coords);
                       }
                    });
                 }
              }).open();
           }
        }
     });
     
     $('#largepic').fileapi({
        url: 'http://www.adwards.lv/admin/news/cropper',
        accept: 'image/*',
        
        imageSize: { minWidth: 986, minHeight: 460 },
        elements: {
           active: { show: '.js-upload', hide: '.js-browse' },
           preview: {
              el: '.js-preview',
              width: 215,
              height: 100
           },
           progress: '.js-progress'
        },
        onFileComplete: function (evt, uiEvt){
            var json = uiEvt.result;
            $('input[name=image_lg]').val(json.image);
        },
        onSelect: function (evt, ui){
           var file = ui.files[0];
           if( !FileAPI.support.transform ) {
              alert('Your browser does not support Flash :(');
           }
           else if( file ){
              $('#largepic-popup').modal({
                 closeOnEsc: true,
                 closeOnOverlayClick: false,
                 onOpen: function (overlay){
                    $(overlay).on('click', '.js-upload', function (){
                       $.modal().close();
                       var upfile = $('#largepic').fileapi('upload');
                    });
                    $('.js-img', overlay).cropper({
                       file: file,
                       bgColor: '#fff',
                       maxSize: [1600, 1600],
                       minSize: [986, 460],
                       aspectRatio: 2.143,
                       selection: '90%',
                       onSelect: function (coords){
                          $('#largepic').fileapi('crop', file, coords);
                       }
                    });
                 }
              }).open();
           }
        }
     });
     
     

});